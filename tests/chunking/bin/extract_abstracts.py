import sys
import re
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = annotated corpus

# Load corpus, extract abstract texts and split sentences
corp_file = open(sys.argv[1], 'r')
for line in corp_file:
    if re.search('<AbstractText[^>]*>', line) != None:
        text = re.sub(r'<AbstractText[^>]*>', '', line)
        text = re.sub('</AbstractText>', '', text)
        print '<AbstractText>'
        for sent in text.strip().split('. '):
            print sent.rstrip('.')
        print '</AbstractText>\n'


