# Define vars
WORKDIR=$NANODEV/tests/chunking
corp_file=$WORKDIR/inp/pubmed_abstracts_train_annot.xml
plain_corp=$WORKDIR/inp/pubmed_abstracts_train_texts.txt
run_chunks=$WORKDIR/bin/run_chunkers.sh
exe_file=$WORKDIR/bin/chunk_annot_performance.py
genia_iob=$WORKDIR/out/genia_chunker.iob
genia_out=$WORKDIR/out/chunk_annot_genia.out
python_iob=$WORKDIR/out/python_chunker.iob
python_out=$WORKDIR/out/chunk_annot_python.out

# Choose pytagger and pychunker
pytagger='brill_rubt_tagger.pickle'
pychunker='maxent_chunker.pickle'

# Run GENIA and Python chunkers
echo 'Running GENIA and Python chunkers...'
$run_chunks $corp_file $plain_corp $genia_iob $python_iob $pytagger $pychunker

# Measure recall annotation of GENIA chunker and compare with full abstract
echo 'Measuring annotations counts in text and GENIA chunked abstract...'
python $exe_file $corp_file $genia_iob > $genia_out

# Measure recall annotation of Python chunker and compare with full abstract
echo 'Measuring annotations counts in text and Python chunked abstract...'
python $exe_file $corp_file $python_iob > $python_out
