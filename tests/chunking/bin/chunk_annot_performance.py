import sys
import re
import nltk
import pickle
import string
import nltk.chunk
from itertools import izip


####################################################################################
################################### FUNCTIONS ######################################
####################################################################################

# Correct IOB tags
#def correct_iobtags(sent):
    

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = annotated corpus
# sys.argv[2] = chunked corpus

# Import nano utils
sys.path.append('/media/Datos/Dropbox/PhD/Workdir/NLP/nano-tagger/utils')
import nanotools
from nanotools import Annotations

# Load annotated corpus, extract abstract texts and manual annotations
textlist = []
manual_annotlist = []
read_annot = False
corp_file = open(sys.argv[1], 'r')
for line in corp_file:
    if re.search('<AbstractText[^>]*>', line) != None:
        text = re.sub(r'<AbstractText[^>]*>', '', line)
        text = re.sub('</AbstractText>', '', text)
        textlist.append([text.strip()])
    elif re.search('<Annotations>', line) != None:
        manual_annot = set([])
        read_annot = True
    elif re.search('</Annotations>', line) != None:
        manual_annotlist.append(manual_annot)
        read_annot = False
    else:
        if read_annot:
            kind, annot = line.strip().split('|')[:2] 
            manual_annot.add((annot, kind))

# Load chunked corpus and extract chunks
chunklist = []
read_tags = False
chunk_file = open(sys.argv[2], 'r')
for line in chunk_file:
    if re.search('/AbstractText', line) != None:
        chunklist.append(npchunks)    
        read_tags = False
    elif re.search('AbstractText', line) != None:
        npchk = ''
        npchunks = []
        read_tags = True

    else:
        if read_tags: 
            iob_tag = line.strip().split('\t')
            if (len(iob_tag) == 3) and (iob_tag[2].find('NP') > 0):
                npchk += iob_tag[0] + ' '
            else:
                if len(npchk) > 0:
                    npchunks.append(npchk.strip())
                    npchk = ''

# Create and count annotations for texts and chunks
text_annotlist = []
chunk_annotlist = []
for text, chunks, annots in izip(textlist, chunklist, manual_annotlist):
    text_annots = Annotations()
    chunk_annots = Annotations()
    for annot, kind in annots:
        text_annots.add(annot, kind)
        chunk_annots.add(annot, kind)
    text_annots.count(text)
    chunk_annots.count(chunks)
    text_annotlist.append(text_annots)
    chunk_annotlist.append(chunk_annots)

# Extract kinds of all annotations
kindset = set([kd for annots in manual_annotlist for an, kd in annots])

# Measure annotations in text and chunks (each kind and overall)
text_count = {}
chunk_count = {}
for kind in kindset:
    text_count[kind] = 0
    chunk_count[kind] = 0
    for text_annots, chunk_annots in izip(text_annotlist, chunk_annotlist):
        text_count[kind]  += sum([annot[2] for annot in text_annots.get_content(kind)])
        chunk_count[kind] += sum([annot[2] for annot in chunk_annots.get_content(kind)])
text_count_overall = sum([count for kind, count in text_count.items()])
chunk_count_overall = sum([count for kind, count in chunk_count.items()])
        
# Print results for each kind
if sys.argv[2].find('genia') > 0:
    print '************************ GENIA Chunker performance ****************************'
else:
    print '************************ Python Chunker performance ***************************'
print
print 'Annot_Kind'.ljust(10), '| Full_Count'.ljust(10), '| Chunk_Count'.ljust(11), '| Accuracy'.ljust(8)
for kind in kindset:
    res = kind.ljust(10) + ' | '
    res += str(text_count[kind]).ljust(10) + ' | '
    res += str(chunk_count[kind]).ljust(11) + ' | '
    res += ('{0:.2f}'.format(float(chunk_count[kind]) / text_count[kind] * 100) + '%').ljust(8)
    print res

# Print overall results
res = 'Overall'.ljust(10) + ' | '
res += str(text_count_overall).ljust(10) + ' | '
res += str(chunk_count_overall).ljust(11) + ' | '
res += ('{0:.2f}'.format(float(chunk_count_overall) / text_count_overall * 100) + '%').ljust(8)
print res
print


####################################################################################################
########################## TESTING ROUTINE TO CHECK CORRECTNESS OF COUNTING ########################
####################################################################################################
# For each abstract, compare manual and counted annotations to check correctness
abstracts = izip(textlist, chunklist, manual_annotlist, text_annotlist, chunk_annotlist)
for text, chunks, manual_annots, text_annots, chunk_annots in abstracts:
    text_annot_content = text_annots.get_content()
    chunk_annot_content = chunk_annots.get_content()
   
    # Print text
    print '<Text>'
    for sent in text:
        print sent
    print '</Text>'
    print

    # Print all text annotations (all appearances for each type, singular and plural)
    print '<TextAnnots>'
    for ann, knd, cnt, pos in text_annot_content:
        if cnt > 0: #and (ann, knd) in manual_annots:
            res =  knd + ' | ' + str(cnt) + ' | ' + 'IN MANUAL ANNOT: '
            res += str((ann, knd) in manual_annots).ljust(5) + ' | ' + ann
            print res
    print '</TextAnnots>'
    print

    # Print chunks
    print '<Chunks>'
    res = ''
    for chk in chunks:
        res += chk + ' | '
    print res
    print '</Chunks>'
    print

    # Print all chunk annotations (all appearances for each type, singular and plural)
    print '<ChunkAnnots>'
    for ann, knd, cnt, pos in chunk_annot_content:
        if cnt > 0: #and (ann, knd) in manual_annots:
            res =  knd + ' | ' + str(cnt) + ' | ' + 'IN MANUAL ANNOT: '
            res += str((ann, knd) in manual_annots).ljust(5) + ' | ' + ann
            print res
    print '</ChunkAnnots>'
    print
    print

