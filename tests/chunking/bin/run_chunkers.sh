# Input parameters
# $1 = annotated corpus in xml format
# $2 = extracted texts from corpus as plain text
# $3 = genia chunker output in iob format
# $4 = python chunker output in iob format 
# $5 = name of python postagger in pickle format
# $6 = name of python chunker in pickle format

# Define vars
WORKDIR=$NANODEV/tests/chunking
genia_dir=$HOME/Software/genia-tagger
tagger_dir=$NANODEV/genia-files/postaggers
chunk_dir=$NANODEV/genia-files/chunkers
run_pychunker=$WORKDIR/bin/run_pychunker.py
run_extract=$WORKDIR/bin/extract_abstracts.py

# Extract each abstract text in corpus, split sentences and store in plain text
python $run_extract $1 > $2

# Perform Chunking with GENIA Tagger
echo 'Running GENIA chunker...'
cd $genia_dir
time ./geniatagger -nt $2 | awk '{print $1"\t"$3"\t"$4}' > $3

# Perform Chunking with Python Chunker
echo 'Running Python chunker (Brill-RUBT Tagger + Maxent Chunker)...'
cd $WORKDIR
time python $run_pychunker $2 $tagger_dir $5 $chunker_dir $6 > $4

