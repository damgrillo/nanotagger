import re
import sys
import nltk
import pickle
import string
import os.path
import nltk.tag
import nltk.chunk

#######################################################################################
#################################### MAIN PROGRAM #####################################
#######################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = corpus file
# sys.argv[2] = postagger dir
# sys.argv[3] = postagger file name
# sys.argv[4] = chunker dir
# sys.argv[5] = chunker file name

# Add chunkers classes file to path
sys.path.append(sys.argv[2])
from chunkers import MaxentChunker

# Import corpus
corp = open(sys.argv[1], 'r')
sents = [line for line in corp if (re.search('\w+', line) != None)]

# Load postagger
ft = open(sys.argv[2] + '/' + sys.argv[3], 'r')
postagger = pickle.load(ft)
    
# Load chunker
fc = open(sys.argv[4] + '/' + sys.argv[5], 'r')
chunker = pickle.load(fc)

# Chunking corpus
tok_sents = [s.split() for s in sents]
tag_sents = [postagger.tag(tks) for tks in tok_sents]
chk_sents = [chunker.parse(tgs) for tgs in tag_sents]
iob_sents = [nltk.chunk.tree2conlltags(cks) for cks in chk_sents]

# Print iob chunks
for sent in iob_sents:
    for (w,t,c) in sent:
        print  w + '\t' + t + '\t' + c
    print ''


