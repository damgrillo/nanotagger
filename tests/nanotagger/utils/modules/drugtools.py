import sys
import re
import string
from collections import deque
from chemtools import is_chebi_term, is_chem_comp, get_chem_form

####################################################################################
#################################### DRUG METHODS ##################################
####################################################################################

# Extract drug annotations in text
def get_annots(text, drugset, exceptset, chebiset, elemset):
    druglist = []
    find_drugs = _find_drugs(text, drugset, exceptset)
    drug_tags  = _set_tags(text, find_drugs, chebiset, elemset)
    group_tags = _split_groups(drug_tags)
    remov_tags = _remove_ending_tags(group_tags)
    valid_tags = _check_tags(remov_tags)
    for tgroup in valid_tags:
        drug = ''.join(zip(*tgroup)[0])
        druglist.append(drug)
    return druglist

# Extract drug annotations for common abbreviations
def get_abbvs(text, abbvset):
    abbvlist = []
    shf = 0
    patt = ''
    cptext = ' ' + text + ' '
    for abbv in abbvset:
        patt += '[^\w]' + abbv + '[^\w]' + '|'
    patt = patt[:-1]
    pmatch = re.search(patt, cptext[shf:])
    while (pmatch != None) and (pmatch.start() < pmatch.end()):
        stpos  = pmatch.start() + 1
        endpos = pmatch.end() - 1
        abbvlist.append(text[stpos+shf-1:endpos+shf-1])
        shf += endpos
        pmatch = re.search(patt, cptext[shf:])
    return abbvlist  

# Internal variables and data structures
_bracket_pairs = {'(':')', '[':']', '{':'}'}
_stopchars = set(['. ', ', ', '; ', ': '])

# Find drugs in text
def _find_drugs(text, drugset, exceptset):
    druglist = []
    toks = deque(re.findall('[a-zA-Z]+|\d+|[^\w]', text))
    while len(toks) > 0:
        match = False
        subtoks = list(toks)
        while (len(subtoks) > 0) and not match:
            term = ''.join(subtoks)
            if (term.lower() in drugset) and (term.lower() not in exceptset):
                druglist.append(term)
                match = True
            else:    
                subtoks.pop()
        num_erase = max(1, len(subtoks))
        for idx in range(num_erase):
            toks.popleft()
    return druglist

# Internal method: Tags text for DRUG annotations 
def _set_tags(text, druglist, chebiset, elemset):
    dtags = []
    dpatt = ''
    bcont = _bracket_cont(text)
    chemf = get_chem_form(text, elemset) 
    for sc in _stopchars:
        dpatt += re.escape(sc) + '|'
    for dr in druglist:
        dpatt += re.escape(dr) + '|'
    for bc in bcont:
        dpatt += re.escape(bc) + '|'
    for ch in chemf:
        dpatt += re.escape(ch) + '|'
    dpatt += '[a-zA-Z]+|\d|[^\w]'
    for tok in re.findall(dpatt, text):
        if tok in druglist:
            dtags.append((tok, 'DRUG'))
        elif tok in _stopchars:
            dtags.append((tok, 'STOP'))
        elif tok in bcont:
            dtags.append((tok, 'BRACK'))
        elif tok in chemf:
            dtags.append((tok, 'CHEML'))
        elif is_chebi_term(tok, chebiset):
            dtags.append((tok, 'CHEBI'))
        elif tok in string.punctuation:
            dtags.append((tok, 'PUNCT'))
        elif tok in string.whitespace:
            dtags.append((tok, 'WSPACE'))
        elif tok in string.digits:
            dtags.append((tok, 'DIGIT'))
        else:         
            dtags.append((tok, 'UNKNOWN'))
    return dtags

# Internal method: Bracket content
def _bracket_cont(text):
    pos = 0
    bcont = []
    while pos < len(text):
        count = 1
        while (pos < len(text)) and (text[pos] not in _bracket_pairs):
            pos += 1
        if pos < len(text):
            start = pos
            op = text[pos]
            cl = _bracket_pairs[op]
            pos += 1
            while (pos < len(text)) and (count > 0):
                if text[pos] == op:
                    count += 1
                elif text[pos] == cl:
                    count -= 1
                pos += 1
            if count == 0:
                bcont.append(text[start:pos])
            else:
                pos = start + 1       
    return bcont

# Internal method: Split tagged_text in groups (separator are STOP, BRACK, UNKNOWN)
def _split_groups(tagged_text):
    vtags = []
    vgroups = []
    separators = set(['STOP', 'BRACK', 'UNKNOWN'])
    for tok, tag in tagged_text:
        if tag not in separators:
            vtags.append((tok, tag))
        else:
            vgroups.append(vtags)
            vtags = []
    if len(vtags) > 0:
        vgroups.append(vtags)
    return vgroups

# Internal method: Remove unsuitable tags in ending positions
def _remove_ending_tags(tagged_groups):
    rgroups = []
    remove_beg = set(['WSPACE', 'PUNCT'])
    remove_end = set(['WSPACE', 'PUNCT', 'DIGIT'])
    for tgroup in tagged_groups:
        tdeque = deque(tgroup)
        while (len(tdeque) > 0) and (tdeque[0][1] in remove_beg):
            tdeque.popleft()
        while (len(tdeque) > 0) and (tdeque[-1][1] in remove_end):
            tdeque.pop()
        if len(tdeque) > 0:
            rgroups.append(list(tdeque))
    return rgroups

# Internal method: Check tags
def _check_tags(tagged_groups):
    vgroups = []
    for tgroup in tagged_groups:
        if len([tok for tok, tag in tgroup if tag == 'DRUG']) > 0:
            vgroups.append(tgroup)
    return vgroups
         

