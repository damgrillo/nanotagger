import re
import sys
import string
import polytools
import drugtools
import chemtools
import jointools
import nanotools
import celltools
import abbvtools
from collections import deque
from difflib import SequenceMatcher

####################################################################################
############################# TEXT ANNOTATIONS CLASS ###############################
####################################################################################

# TextAnnots Class
class TextAnnots:

    # Initialize object
    def __init__(self):
        self._text = ''
        self._abbv = {}
        self._annots = []

    # Set text
    def set_text(self, text):
        self._text = text

    # Get text
    def get_text(self, st=None, end=None):
        return self._text[st:end]

    # Add annotation
    def add_annot(self, kd, st, end):
        self._annots.append((kd, st, end))

    # Get positions for each annotation in text
    def get_annots(self, kindlist=None):
        if kindlist != None:
            annots = [(kd, st, end) for kd, st, end in self._annots if kd in kindlist]
        else:
            annots = self._annots
        return sorted(annots, key=lambda an: (an[1], an[2])) 

    # Add abbreviation
    def add_abbv(self, sform, lform):
        self._abbv[sform] = lform

    # Get abbreviations
    def get_abbvs(self):
        return self._abbv

    # Uniquify annotations
    def uniquify_annots(self):
        uniqlist = []
        kdsort = ['NANO', 'POLY', 'DRUG', 'CELL', 'CHEM']     

        # Drop repeated items and uniquify annotations with multiple kinds using kdsort order
        sortlist = sorted(set(self._annots), key=lambda an: (an[1], -an[2], kdsort.index(an[0])))
        for an in sortlist:
            if (len(uniqlist) == 0) or (uniqlist[-1][1:] != an[1:]):
                uniqlist.append(an)

        # Copy instances not contained in longer terms of the same kind
        posdic = {kind:[] for kind in kdsort}
        for kind in kdsort:
            poslist = [(st, end) for kd, st, end in uniqlist if kd == kind]
            for pos in poslist:
                if not _contained_not_equal(pos, poslist):
                    posdic[kind].append(pos)

        # Remove CHEM instances contained in longer terms of POLY, DRUG and CELL annotations
        posdic['CHEM'] = [pos for pos in posdic['CHEM'] if not _contained_not_equal(pos, posdic['POLY']) and
                                                           not _contained_not_equal(pos, posdic['DRUG']) and
                                                           not _contained_not_equal(pos, posdic['CELL'])]

        # Return uniquify annotations
        uniqlist = []
        for kind in kdsort:
            uniqlist.extend([(kind, st, end) for st, end in posdic[kind]])
        self._annots = uniqlist

    # Private method: Match each annot in text and return kinds and positions
    def _match_annots(self, annotlist, kind):
        poslist = []
        shf = 0
        patt = ''
        text = (' ' + self._text + ' ').lower()
        annotlist.sort(key=len, reverse=True)
        for an in annotlist:
            patt += '[^\w]' + re.escape(an.lower()) + '[^\w]' + '|'
        patt = patt[:-1]
        pmatch = re.search(patt, text)
        while (pmatch != None) and (pmatch.start() < pmatch.end()):
            st  = pmatch.start() + 1
            end = pmatch.end() - 1
            poslist.append((st+shf-1, end+shf-1))
            shf += end
            pmatch = re.search(patt, text[shf:])
        poslist.sort(key=lambda pos: (pos[0], pos[1]))
        return [(kind, st, end) for st, end in poslist]


####################################################################################
########################### NANOTAGGER ANNOTATIONS CLASS ###########################
####################################################################################

# NanoTaggerAnnots Class
class NanoTaggerAnnots(TextAnnots):

    # Search positions for POLY annotations in text
    def search_poly(self, chebiset, elemset, greekset, pconnset, otherset, abbvset):
        polyabbv = [sf for sf, lf in self._abbv.items() if lf[1] == 'POLY']
        polylist = polytools.get_annots(self._text, chebiset, elemset, greekset, pconnset, otherset)    
        polylist.extend([abbv for sf in polyabbv for abbv in (_get_sing(sf), _get_plur(sf))])
        polylist.extend([abbv for abbv in polytools.get_abbvs(self._text, abbvset) if abbv not in self._abbv])
        annotlist = self._match_annots(polylist, 'POLY')
        self._annots.extend(annotlist)

    # Search positions for CHEM annotations in text
    def search_chem(self, chebiset, elemset, greekset, otherset):
        chemabbv = [sf for sf, lf in self._abbv.items() if lf[1] == 'CHEM']
        chemlist = chemtools.get_annots(self._text, chebiset, elemset, greekset, otherset)
        chemlist.extend([abbv for sf in chemabbv for abbv in (_get_sing(sf), _get_plur(sf))])
        annotlist = self._match_annots(chemlist, 'CHEM')
        self._annots.extend(annotlist)

    # Search positions for DRUG annotations in text
    def search_drug(self, drugset, chebiset, elemset, exceptset, abbvset):
        drugabbv = [sf for sf, lf in self._abbv.items() if lf[1] == 'DRUG']
        druglist = drugtools.get_annots(self._text, drugset, exceptset, chebiset, elemset)
        druglist.extend([abbv for sf in drugabbv for abbv in (_get_sing(sf), _get_plur(sf))])
        druglist.extend([abbv for abbv in drugtools.get_abbvs(self._text, abbvset) if abbv not in self._abbv])
        annotlist = self._match_annots(druglist, 'DRUG')
        self._annots.extend(annotlist)

    # Search joint annotations
    def search_joint(self, chebiset, pconnset):
        annotlist = []
        kdlist = ['POLY', 'CHEM', 'DRUG']
        annotdic = {self._text[st:end]:kd for kd, st, end in self._annots if kd in kdlist}
        abbvdic  = {sf:lf[1] for sf, lf in self._abbv.items() if lf[1] in kdlist}
        joinlist = jointools.get_annots(self._text, annotdic, abbvdic, chebiset, pconnset)
        annotlist.extend(self._match_annots([an for an, kd in joinlist if kd == 'POLY'], 'POLY'))
        annotlist.extend(self._match_annots([an for an, kd in joinlist if kd == 'DRUG'], 'DRUG'))
        annotlist.extend(self._match_annots([an for an, kd in joinlist if kd == 'CHEM'], 'CHEM'))
        self._annots.extend(annotlist)

    # Search positions for CELL annotations in text
    def search_cell(self, cellset):
        cellabbv = [sf for sf, lf in self._abbv.items() if lf[1] == 'CELL']
        cellist = celltools.get_annots(self._text, cellset)
        cellist.extend([abbv for sf in cellabbv for abbv in (_get_sing(sf), _get_plur(sf))])
        annotlist = self._match_annots(cellist, 'CELL')
        self._annots.extend(annotlist)

    # Search positions for NANO annotations in text
    def search_nano(self, chebiset):
        kdlist = ['POLY', 'CHEM', 'DRUG']
        annotdic = {self._text[st:end]:kd for kd, st, end in self._annots if kd in kdlist}
        nanoabbv = [sf for sf, lf in self._abbv.items() if lf[1] == 'NANO']
        nanolist = nanotools.get_annots(self._text, annotdic, chebiset)
        nanolist.extend([abbv for sf in nanoabbv for abbv in (_get_sing(sf), _get_plur(sf))])
        annotlist = self._match_annots(nanolist, 'NANO')
        self._annots.extend(annotlist)

    # Update ABBV annotations and corresponding kind association
    def update_abbv(self, special_abbvs):
        annotlist = [(kd, self._text[st:end]) for kd, st, end in self._annots]
        for sf, lf in abbvtools.get_annots(self._text, special_abbvs):
            lfmatch = [(kd, _overlap_ratio_annot(lf, an)) for kd, an in annotlist]
            lfmatch.sort(key=lambda tup: tup[1], reverse=True)
            if sf not in self._abbv:
                self._abbv[sf] = [lf, 'NKND', 0.0]
            if len(lfmatch) > 0:
                old_ovp = self._abbv[sf][2]
                new_knd, new_ovp = lfmatch[0]                                 
                if (new_ovp >= 0.25) and (new_ovp >= old_ovp):                
                    self._abbv[sf] = [lf, new_knd, new_ovp]

    # Load abbreviations from external abbv dictionary
    def load_abbvs(self, ext_abbvs):
        self._abbv.update(ext_abbvs)

    # Shift annotation positions
    def shift_annot_pos(self, shift):
        self._annots = [(kd, st+shift, end+shift) for kd, st, end in self._annots]


####################################################################################
############################ MATCH ABBREVIATIONS CLASS #############################
####################################################################################

# MatchAbbvs Class
class MatchAbbvs:

    # Initialize object
    def __init__(self):
        self._tpos = []
        self._fpos = []
        self._fneg = []
        self._part = []
        self._pchk = []

    # Get matching parameters
    def get_params(self):
        return (self._tpos, self._fpos, self._fneg, self._part, self._pchk)

    # Calculate matching parameters
    def calc_params(self, manual_abbvs, auto_abbvs):

        # Calculate TPOS, FPOS, FNEG, PART and PCHK abbreviations for SFs in manual_abbvs
        for sf in manual_abbvs:
            mlf = manual_abbvs[sf]            
            if sf in auto_abbvs:
                alf = auto_abbvs[sf]           
                if mlf == alf:
                    self._tpos.append(('ABBV', sf, mlf))
                elif _overlap_ratio_annot(mlf, alf) > 0.5:
                    self._part.append(('ABBV', sf, mlf, sf, alf))
                elif _overlap_ratio_annot(mlf, alf) > 0.2:
                    self._pchk.append(('ABBV', sf, mlf, sf, alf))
                else:
                    self._fneg.append(('ABBV', sf, mlf))
                    self._fpos.append(('ABBV', sf, alf))
            else:
                self._fneg.append(('ABBV', sf, mlf))

        # Calculate rest of FPOS abbreviations for unvisited SFs in auto_abbvs
        for sf in auto_abbvs:
            alf = auto_abbvs[sf]
            if sf not in manual_abbvs:
                self._fpos.append(('ABBV', sf, alf))


####################################################################################
############################ MATCH ANNOTATIONS CLASS ###############################
####################################################################################

# MatchAnnots Class
class MatchAnnots:

    # Initialize object
    def __init__(self):
        self._text = ''
        self._tpos = []
        self._fpos = []
        self._fneg = []
        self._part = []
        self._pchk = []

    # Set text
    def set_text(self, text):
        self._text = text

    # Get text
    def get_text(self, st=None, end=None):
        return self._text[st:end]

    # Get matching parameters
    def get_params(self, kindlist=None):
        txt = self._text
        if kindlist != None:
            tpos = [an for an in self._tpos if an[0] in kindlist]
            fpos = [an for an in self._fpos if an[0] in kindlist]
            fneg = [an for an in self._fneg if an[0] in kindlist]
            part = [an for an in self._part if an[0] in kindlist]
            pchk = [an for an in self._pchk if an[0] in kindlist]
        else:
            tpos = self._tpos
            fpos = self._fpos
            fneg = self._fneg
            part = self._part
            pchk = self._pchk
        return (tpos, fpos, fneg, part, pchk)

    # Calculate matching parameters
    def calc_params(self, manual_annots, auto_annots):

        # Calculate TPOS, FPOS, FNEG annotations
        ma_sort = deque(sorted(manual_annots, key=lambda an: (an[0], an[1], -an[2])))
        aa_sort = deque(sorted(auto_annots,   key=lambda an: (an[0], an[1], -an[2])))
        while (len(ma_sort) > 0) and (len(aa_sort) > 0):
            mk = ma_sort[0][0]
            ak = aa_sort[0][0]
            mp = ma_sort[0][1:]
            ap = aa_sort[0][1:]
            if mk == ak:
                if mp == ap:
                    self._tpos.append((mk, mp[0], mp[1]))
                    ma_sort.popleft()
                    aa_sort.popleft()
                elif mp[0] <= ap[0]:
                    self._fneg.append((mk, mp[0], mp[1]))
                    ma_sort.popleft()
                else:
                    self._fpos.append((ak, ap[0], ap[1]))
                    aa_sort.popleft()
            elif mk < ak:
                self._fneg.append((mk, mp[0], mp[1]))
                ma_sort.popleft()
            else:
                self._fpos.append((ak, ap[0], ap[1]))
                aa_sort.popleft()

        # Remaining FNEG            
        while len(ma_sort) > 0:
            ma = ma_sort.popleft()
            self._fneg.append((ma[0], ma[1], ma[2]))

        # Remaining FPOS
        while len(aa_sort) > 0:
            aa = aa_sort.popleft()
            self._fpos.append((aa[0], aa[1], aa[2]))

        # Search PART annotations among FPOS and FNEG
        fp_sort = deque(sorted(self._fpos, key=lambda an: (an[0], an[1], -an[2])))
        fn_sort = deque(sorted(self._fneg, key=lambda an: (an[0], an[1], -an[2])))
        while (len(fp_sort) > 0) and (len(fn_sort) > 0):
            fpk = fp_sort[0][0]
            fnk = fn_sort[0][0]
            fpp = fp_sort[0][1:]
            fnp = fn_sort[0][1:]
            if fpk == fnk:
                if _overlap_ratio_pos(fpp, fnp) >= 0.5:
                    self._part.append((fnk, fnp[0], fnp[1], fpp[0], fpp[1]))
                    self._fpos.remove(fp_sort.popleft())
                    self._fneg.remove(fn_sort.popleft())
#                elif _overlap_ratio_pos(fpp, fnp) > 0.2:
#                    self._pchk.append((fnk, fnp[0], fnp[1], fpp[0], fpp[1]))
#                    self._fpos.remove(fp_sort.popleft())
#                    self._fneg.remove(fn_sort.popleft())
                elif fpp[1] <= fnp[1]:
                    fp_sort.popleft()
                else:
                    fn_sort.popleft()
            elif fpk < fnk:
                fp_sort.popleft()
            else:
                fn_sort.popleft()

        # Sort TPOS, FPOS, FNEG, PART, PCHK by positions
        self._tpos.sort(key=lambda an: (an[1], an[2]))     
        self._fpos.sort(key=lambda an: (an[1], an[2]))  
        self._fneg.sort(key=lambda an: (an[1], an[2]))   
        self._part.sort(key=lambda an: (an[1], an[2]))
        self._pchk.sort(key=lambda an: (an[1], an[2]))


####################################################################################
################################# AUXILIAR FUNCTIONS ###############################
####################################################################################

# Auxiliar function: Get singular form
def _get_sing(text):
    if text[-1] == 's':
        sform = text[:-1]
    else:
        sform = text
    return sform

# Auxiliar function: Get plural form
def _get_plur(text):
    if text[-1] == 's':
        pform = text
    else:
        pform = text + 's'
    return pform

# Auxiliar function: Test if position is contained but not equal to some instance of poslist
def _contained_not_equal(pos, poslist):
    cont_not_eq = False
    posdq = deque(sorted(poslist, key=lambda dp: (dp[0], -dp[1])))
    while (len(posdq) > 0) and (posdq[0][0] < pos[1]) and not cont_not_eq: 
        dpos = posdq.popleft()
        cont_not_eq = (dpos[0] <= pos[0]) and (pos[1] <= dpos[1]) and not (pos == dpos)
    return cont_not_eq

# Auxiliar function: Calculate overlap ratio between strings
def _overlap_ratio_annot(s1, s2):
    maxlen = max(len(s1), len(s2))
    sqmatch = SequenceMatcher(None, s1, s2)
    overlap = sqmatch.find_longest_match(0, len(s1), 0, len(s2)).size
    return float(overlap) / maxlen

# Auxiliar function: Calculate overlap ratio between string positions
def _overlap_ratio_pos(sp1, sp2):
    maxlen = max(sp1[1] - sp1[0], sp2[1] - sp2[0])
    if sp1[0] <= sp2[0]:
        prev = sp1
        last = sp2
    else:
        prev = sp2
        last = sp1
    if prev[1] <= last[0]:
        overlap = 0
    else:
        overlap = min(prev[1], last[1]) - last[0]
    return float(overlap) / maxlen

