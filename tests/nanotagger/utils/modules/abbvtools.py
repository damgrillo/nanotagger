import sys
import re
import math
import string
import nltk

#####################################################################################################
############################################ ABBV METHODS ###########################################
#####################################################################################################

# Extract abbrev annotations
def get_annots(text, special_abbvs):
    abbvlist = []
    startpos = 0
    bcklist = _bracket_cont(text)
    for bc in bcklist:

        # Match bracket content in text and set bracket and previous contents
        abbv = ()
        bmatch = re.search(re.escape(bc), text)
        bcont = text[bmatch.start():bmatch.end()][1:-1].strip()
        bprev = text[startpos:bmatch.start()].replace('; ', ', ').split(', ')[-1].strip()
        if text[bmatch.start()-1] in string.whitespace:

            # If bcont not in special_abbvs, do default search
            if bcont not in special_abbvs:
                abbv = _get_abbv_pair(bprev, bcont)

            # Else, search longest term in special_abbvs matching bprev
            else:
                lmatch = None
                lforms = sorted(special_abbvs[bcont], key=len)
                while (len(lforms) > 0) and (lmatch == None):
                    lmatch = re.search(re.escape(lforms.pop()), bprev.lower())
                if lmatch != None:
                    abbv = (bcont, bprev[lmatch.start():])        

        # If abbv pair found, append to abbvlist and update startpos, else search abbvs inside bracket
        if len(abbv) == 2:
            startpos = bmatch.end()
            abbvlist.append(abbv)
        else:
            abbvlist.extend(get_annots(bcont, special_abbvs))

    return abbvlist

# Internal variables
_MIN_CHLEN = 2
_MAX_SRLEN = 8 
_MAX_DIGIT = 0.5
_MIN_UPPER = 0.5
_FCHAR_VALUE = 3
_UPPER_VALUE = 2
_LOWER_VALUE = 1
_DIGIT_VALUE = 1
_MIN_SCORE = 2.3
_MIN_SDIFF = 0.1
_MAX_ITERS = 30
_bracket_pairs = {'(':')', '[':']', '{':'}'}

# Internal method:  

# Internal method: Brackets contents
def _bracket_cont(text):
    pos = 0
    bcont = []
    while pos < len(text):
        bpos = _next_bracket_pair(text, pos)
        if len(bpos) == 2:
            bcont.append(text[bpos[0]:bpos[1]])
            pos = bpos[1]
        else:
            pos = _next_bracket_pos(text, pos) + 1
    return bcont

# Internal method: Next bracket pair
def _next_bracket_pair(text, pos=0):
    bpos = ()
    count = 1
    pos = _next_bracket_pos(text, pos)
    if pos < len(text):
        start = pos
        op = text[pos]
        cl = _bracket_pairs[op]
        pos += 1
        while (pos < len(text)) and (count > 0):
            if text[pos] == op:
                count += 1
            elif text[pos] == cl:
                count -= 1
            pos += 1
    if count == 0:
        bpos = (start, pos)   
    return bpos

# Internal method: Next bracket position
def _next_bracket_pos(text, pos=0):
    while (pos < len(text)) and (text[pos] not in _bracket_pairs):
        pos += 1
    return pos

# Internal method: Get abbrev pairs
def _get_abbv_pair(bprev, bcont):
    abbv = ()
    sf, lf = _find_sflf(bprev, bcont)
    score = _test_sflf(sf, lf)
    if score > _MIN_SCORE:
        abbv = (sf, lf)
    return abbv               

# Internal method: Find SF, LF from bprev and bcont
def _find_sflf(bprev, bcont):
    pair = []
    if len(re.findall('; |, ', bcont)) == 1:
        pair = [term.strip() for term in bcont.replace('; ', ', ').split(', ')]
        pair.sort(key=len)
    if len(pair) != 2:
        sflf = [bcont, _find_complement_form(bprev, bcont)]
    elif _test_sflf(pair[0], pair[1]) <= _MIN_SCORE:
        sflf = [pair[0], _find_complement_form(bprev, pair[0])]
    else:
        sflf = pair
    sflf.sort(key=len)
    return sflf

# Internal method: Find complementary form in bprev for bcont
def _find_complement_form(bprev, bcont):
     
    # Search for complement form backwards
    iters = 0
    max_score = 0
    max_cform = ''
    next_start = len(bprev) - 1
    bform = bcont
    cform = ''
    while (next_start >= 0) and (iters < _MAX_ITERS):
        iters += 1

        # Search for the start position of the previous word in bprev
        start = next_start
        while (start >= 0) and ((bprev[start] in string.whitespace) or (bprev[start] in '})]/:;|.=<>#$%&')):
            start -= 1
        while (start >= 0) and not ((bprev[start] in string.whitespace) or (bprev[start] in '})]/:;|.=<>#$%&')):
            start -= 1
        next_start = start
        start += 1

        # Search for the first digit or letter to set the next complementary form
        first = start
        while (bprev[first] not in string.letters) and (bprev[first] not in string.digits):            
            first += 1
        
        # Set complementary, short and long forms
        cform = bprev[first:]
        if len(bform) <= len(cform):
            sform = bform
            lform = cform
        else:
            sform = cform
            lform = bform
        
        # Calculate score value and save cform with max score
        score = _test_sflf(sform, lform)
        sdiff = score - max_score
        if max_score > 0:
            sdiff /= max_score
        if sdiff > _MIN_SDIFF:
            max_score = score
            max_cform = cform

        
    # Return cform with max score value
    return max_cform  

# Internal method: Test sform, lform
def _test_sflf(sform, lform):        
    
    # REJECT CONDITIONS:
    # 1. Any of both forms have less two chars
    # 2. Any of both forms contain forbidden chars (<, >, =, ~, %, #, +, +/-) 
    # 3. Sform contains numbers like 5.325, 2:5, 2,34 or Lform contains floating point numbers
    # 4. Sform contains more digits than _MAX_DIGIT * total (total: number of chars in sform)

    # WEIGHT PARAMETERS FOR SCORE CALCULATION:
    # 1. Count ntotal +1 for each letter/digit in sform
    # 2. For each char (letter/digit) in sform that matches in lform, assign score (see _get_score):
    
    # FINAL SCORE:
    # Divide score by number of chars in sform, adding bonus proportional to the match ratio

    # Set initial values 
    fscore = 0
    llow = lform.lower()
    lchars = re.findall('\w', lform)
    schars = re.findall('\w', sform)
    nlseps = len(re.findall('[,.;] ', lform))
    nsforb = len(re.findall('[<>=~%#\+]|(\+/-)', sform))
    nlforb = len(re.findall('[<>=~%#\+]|(\+/-)', lform))
    nsnumb = len(re.findall('[^\w]*\d+[.,;:/]\d+[^\w]*', sform))
    nlnumb = len(re.findall('[^\w]*\d+[.]\d+[^\w]*', lform))
    digit  = len([ch for ch in schars if ch in string.digits])
    upper  = len([ch for ch in schars if ch in string.uppercase])
    wspace = len([ch for ch in sform if ch in string.whitespace])

    # Check reject conditions, if any of them is satisfied, skip calculation
    if not ((len(schars) < _MIN_CHLEN) or (len(lchars) < _MIN_CHLEN) or 
            (digit >= _MAX_DIGIT * len(schars)) or (nsforb > 0) or (nlforb > 0) or 
            (nsnumb > 0) or (nlnumb > 0) or (nlseps > 0) or (upper == 0) or (wspace > 0) or
            (sform.lower() == 'i') or (sform.lower() == 'ii') or (sform.lower() == 'iii')):

        # Get optimal sequence
        sqtable = [[None for lch in lform] for sch in schars]
        optseq = _get_optimal_sequence(schars, lform, sqtable)

        # Update final score
        fscore = float(_get_sqscore(optseq) + len(optseq) ** 2) / len(schars)

    # Return final score value
    return fscore

# Internal method: Get optimal sequence with max global score using dynamic programming approach 
def _get_optimal_sequence(schars, lform, sqtable, spos=0, lpos=0):
    optseq = []
    subseqs = []
    lmatchseq = []
      
    # Load sequence table for spos, lpos
    if sqtable[spos][lpos] != None:
        optseq = sqtable[spos][lpos]

    # Look for sequence in lform after lpos matching with char in spos
    else:
        maxscore = 0
        for lch in re.finditer(schars[spos].lower(), lform.lower()):
            score = _get_score(schars[spos], lch.start(), lform)
            if (lch.start() >= lpos) and (score > maxscore):
                lmatchseq.append((lch.start(), score))
                maxscore = score

        # Append matched sequence for lpos and search for matching subsequences for spos+1 
        for lmpos, score in lmatchseq:
            localseq = [(lmpos, score)]
            if (spos+1 < len(schars)) and (lmpos+1 < len(lform)):
                localseq.extend(_get_optimal_sequence(schars, lform, sqtable, spos+1, lmpos+1))
            subseqs.append(localseq)
            
        # Ignore matched sequence for lpos and search for matching subsequences for spos+1
        if spos+1 < len(schars):
            subseqs.append(_get_optimal_sequence(schars, lform, sqtable, spos+1, lpos))

        # Get lpos match sequence
        else:
            subseq = lmatchseq

        # Calculate optimal sequence
        optscore = 0
        for seq in subseqs:
            sqscore = sum([score for lmpos, score in seq])
            if sqscore > optscore:
                optscore = sqscore
                optseq = seq[:]

        # Store optimal sequence in sqtable
        sqtable[spos][lpos] = optseq[:]

    # Return optimal sequence
    return optseq

# Internal method: Get score for char sch matching in term in lpos position
def _get_score(sch, lpos, term):
    score = 0
    if (lpos == 0) or (term[lpos-1] in string.whitespace) or (term[lpos-1] in string.punctuation):
        score += _FCHAR_VALUE
    if sch in string.uppercase:
        score += _UPPER_VALUE
    elif sch in string.lowercase:
        score += _LOWER_VALUE
    else:
        score += _DIGIT_VALUE
    return score  

# Internal method_ Get sequence score
def _get_sqscore(seq):
    return reduce(lambda x,y: x+y, [score for lpos, score in seq], 0)


# Internal method: Convert lform sequence to string
def _seq2str(seq, lform):
    pos = 0
    seq_str = ''
    score_str = ''
    for lpos, score in seq:
        while pos < lpos:
            seq_str += ' '
            score_str += ' '
            pos += 1
        if score > 3:
            seq_str += lform[lpos].upper()
        else:
            seq_str += lform[lpos]
        score_str += str(score)
        pos += 1
    return (seq_str, score_str)

