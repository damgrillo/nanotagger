import sys
import re
import string
from collections import deque

####################################################################################
#################################### CELL METHODS ##################################
####################################################################################

# Extract cell annotations in chunk
def get_annots(chunk, cellset):
    cellist = []
    chksplit = re.split(', | and | or ', chunk)
    for chk in chksplit:
        matchpos = _get_match_pos(chk)
        if len(matchpos) > 0:
            lastpos = matchpos.pop()
            chkcell = _remove_bwords(chk[:lastpos[1]], cellset)
            if len(chkcell) > 0:
                cellist.append(chkcell)
    return cellist

# Internal global variables
_cell_patts = (r'\bcells?\b'      + '|' + r'\w+phages?\b'  + '|' + r'\w+cytes?\b'    + '|'
               r'\w*blasts?\b'    + '|' + r'\w+phils?\b'   + '|' + r'\bplatelets?\b' + '|' 
               r'\blineages?\b')

# Internal methods: Get positions of all suitable matching patterns
def _get_match_pos(chunk):
    return [(p.start(), p.end()) for p in re.finditer(_cell_patts, chunk)]

# Internal method: Remove non valid words from the beginning of the chunk
def _remove_bwords(chunk, cellset):
    chktoks = deque(re.findall('[a-zA-Z0-9-]+|\W', chunk))
    while (len(chktoks) > 0) and not _valid_token(chktoks[0], cellset):
        chktoks.popleft()    
    return ''.join(list(chktoks))

# Internal method: check if token is a valid token for cells
def _valid_token(tok, cellset):
    if tok[-1] == 's':
        tsing = tok.lower()[:-1]
        tplur = tok.lower()
    else:
        tsing = tok.lower()
        tplur = tok.lower() + 's'
    num_digit = len([ch for ch in tok if ch in string.digits])
    num_lettr = len([ch for ch in tok if ch in string.letters])
    num_upper = len([ch for ch in tok if ch in string.uppercase])
    cond1 = (re.search(_cell_patts, tok) != None) and (tsing != 'cell')
    cond2 = (num_upper > 1) and (num_digit > 0)    
    cond3 = (num_upper > 0) and (num_digit > 1)
    cond4 = (num_upper > 2) or ((num_digit > 1) and (num_lettr > 0))  
    return (tsing in cellset) or (tplur in cellset) or cond1 or cond2 or cond3 or cond4


