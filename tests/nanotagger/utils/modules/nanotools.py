import sys
import re
import string
from collections import deque
from chemtools import is_chebi_term

####################################################################################
#################################### NANO METHODS ##################################
####################################################################################

# Extract nano annotations in chunks
def get_annots(chunk, annotdic, chebiset):
    nanolist = []
    chunk_tags = _set_tags(chunk, annotdic, chebiset)
    group_tags = _split_groups(chunk_tags)  
    remov_tags = _remove_begin_tags(group_tags)
    valid_tags = _check_tags(remov_tags)
    for tgroup in valid_tags:
        nano = ''.join(zip(*tgroup)[0])
        nanolist.append(nano)
    return nanolist

# Internal global variables
_stopchars = set(['. ', ', ', '; ', ': ', '=', '<', '>', '#', '%', '$', '&', '+/-'])
_adjv_specs = r'\w*-loaded|\w*-encapsulated|\w*-coated'
_adjv_terms = r'\w+ed\b|\w+ing\b|\w+ic\b|\w+able\b'
_nano_specs = r'micelles?|vesicles?|dendrimers?|quantum dots?|nanotubes?|qdots?|\w+somes?' 
_nano_terms = (r'\w*particles?\b'    + '|' + r'\bnanostructures?\b' + '|' + r'\b\w*compsites?\b' + '|' +
               r'\bnanotubes?\b'     + '|' + r'\bnanospheres?\b'    + '|' + r'\bnanocrystals?\b'  + '|' +
               r'\bnanocapsules?\b'  + '|' + r'\bnanohorns?\b'      + '|' + r'\bnanoreactors?\b'  + '|' + 
               r'\bnanoropes?\b'     + '|' + r'\bnanocages?\b'      + '|' + r'\bnanoribbons?\b'   + '|' + 
               r'\bnanoshells?\b'    + '|' + r'\bnanorods?\b'       + '|' + r'\bnanosheets?\b'    + '|' +
               r'\bnanofibers?\b'    + '|' + r'\bnanowires?\b'      + '|' + r'\bnanocolumns?\b'   + '|' +
               r'\bmicelles?\b'      + '|' + r'\bvesicles?\b'       + '|' + r'\bdendrimers?\b'    + '|' +  
               r'\bquantum dots?\b'  + '|' + r'\bqdots?\b'          + '|' + r'\b\w+somes?\b'      + '|' + 
               r'\bmicrospheres?\b'  + '|' + r'\b\w*gels?\b'        + '|' + r'\b\w*films?\b'      + '|' +
               r'\bmicrocapsules?\b' + '|' + r'\bvectors?\b'        + '|' + r'\bNPs?\b')


# Internal method: Tags text for NANO annotations 
def _set_tags(text, annotdic, chebiset):
    ntags = []
    poly_annots = sorted([an for an, kd in annotdic.items() if kd == 'POLY'], key=len, reverse=True)
    drug_annots = sorted([an for an, kd in annotdic.items() if kd == 'DRUG'], key=len, reverse=True)
    chem_annots = sorted([an for an, kd in annotdic.items() if kd == 'CHEM'], key=len, reverse=True)
    npatt = ''
    for sc in _stopchars:
        npatt += re.escape(sc) + '|'
    for pa in poly_annots:
        npatt += re.escape(pa) + '|'
    for da in drug_annots:
        npatt += re.escape(da) + '|'
    for ca in chem_annots:
        npatt += re.escape(ca) + '|'
    npatt += _nano_terms + '|' + _adjv_specs  + '|' + _adjv_terms + '|' + '[a-zA-Z]+|\d|[^\w]'
    for tok in re.findall(npatt, text):
        if re.search(_nano_terms, tok) != None:
            ntags.append((tok, 'NANO'))
        elif tok in _stopchars:
            ntags.append((tok, 'STOP'))
        elif tok in annotdic:
            ntags.append((tok, annotdic[tok]))
        elif re.search(_adjv_specs, tok.lower()) != None:
            ntags.append((tok, 'ADJV-SP'))
        elif re.search(_adjv_terms, tok.lower()) != None:
            if is_chebi_term(tok, chebiset):
                ntags.append((tok, 'CHEBI'))
            else:
                ntags.append((tok, 'ADJV'))
        else:         
            ntags.append((tok, 'OTHER'))
    return ntags

# Internal method: Split tagged_text in groups
def _split_groups(tagged_text):
    vtags = []
    vgroups = []
    for tok, tag in tagged_text:
        if tag != 'STOP':
            vtags.append((tok, tag))
        if (tag == 'NANO') or (tag == 'STOP'):
            vgroups.append(vtags)
            vtags = []
    return vgroups

# Internal method: Remove unsuitable tags in beginning positions
def _remove_begin_tags(tagged_groups):
    rgroups = []
    remove_tags = set(['OTHER', 'ADJV'])
    for tgroup in tagged_groups:
        tdeque = deque(tgroup)
        while (len(tdeque) > 0) and (tdeque[0][1] in remove_tags):
            tdeque.popleft()
        rgroups.append(list(tdeque))
    return rgroups

# Internal method: Check tags
def _check_tags(tagged_groups):
    vgroups = []
    relev_tags = set(['POLY', 'CHEM', 'DRUG'])
    for tgroup in tagged_groups:
        chebi_terms = len([tag for tok, tag in tgroup if tag == 'CHEBI'])  
        relev_types = len([tag for tok, tag in tgroup if tag in relev_tags])
        nano_terms  = len([tok for tok, tag in tgroup if re.search(_nano_terms, tok) != None])
        nano_specs  = len([tok for tok, tag in tgroup if re.search(_nano_specs, tok) != None])
        adjv_specs  = len([tag for tok, tag in tgroup if tag == 'ADJV-SP'])      
        cond1 = (nano_specs > 0)
        cond2 = (nano_terms > 0) and (relev_types > 0)
        cond3 = (nano_terms > 0) and (adjv_specs + chebi_terms > 0) 
        if cond1 or cond2 or cond3:
            vgroups.append(tgroup)
    return vgroups



