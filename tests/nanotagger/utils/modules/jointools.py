import sys
import re
import string
from collections import deque
from chemtools import is_chebi_term

####################################################################################
#################################### JOIN METHODS ##################################
####################################################################################

# Extract joint annotations
def get_annots(text, annotdic, abbvdic, chebiset, pconnset):
    joinlist = []
    text_tags  = _set_tags(text, annotdic, abbvdic, chebiset, pconnset)
    group_tags = _split_groups(text_tags)
    remov_tags = _remove_ending_tags(group_tags)
    modif_tags = _modify_abbv_tags(remov_tags, abbvdic)

    # Add joint POLY annotations
    for tgroup in _joint_poly(modif_tags, abbvdic):
        if len(tgroup) > 0:
            poly = ''.join(zip(*tgroup)[0])
            joinlist.append((_balance_end_bracks(poly), 'POLY'))

    # Add joint DRUG annotations
    for tgroup in _joint_drug(modif_tags, abbvdic):
        if len(tgroup) > 0:
            drug = ''.join(zip(*tgroup)[0])
            joinlist.append((_balance_end_bracks(drug), 'DRUG'))

    # Add joint CHEM annotations
    for tgroup in _joint_chem(modif_tags, abbvdic):
        if len(tgroup) > 0:
            chem = ''.join(zip(*tgroup)[0])
            joinlist.append((_balance_end_bracks(chem), 'CHEM'))

    # Return joint annotations
    return joinlist

# Internal variables and data structures
_connectchars = set(['-', ':', '/', '@'])
_stopchars = set(['. ', ', ', '; ', ': ', '%'])

# Internal method: Tags text for joint annotations 
def _set_tags(text, annotdic, abbvdic, chebiset, pconnset):
    jtags = []
    unionset = set(annotdic.items()).union(set(abbvdic.items()))
    poly_annots = sorted([an for an, kd in unionset if kd == 'POLY'], key=len, reverse=True)
    drug_annots = sorted([an for an, kd in unionset if kd == 'DRUG'], key=len, reverse=True)
    chem_annots = sorted([an for an, kd in unionset if kd == 'CHEM'], key=len, reverse=True)
    excep_adjv = set(['loaded', 'coated', 'encapsulated'])
    adjv_patt = r'\w+ed\b|\w+ing\b|\w+ic\b|\w+able\b'
    jpatt = ''
    for abbv in abbvdic:
        jpatt += '[\(\[\{]' + re.escape(abbv) + '[\)\]\};,]' + '|'
    for pa in poly_annots:
        jpatt += re.escape(pa) + '|'
    for da in drug_annots:
        jpatt += re.escape(da) + '|'
    for ca in chem_annots:
        jpatt += re.escape(ca) + '|'
    for sc in _stopchars:
        jpatt += re.escape(sc) + '|'
    jpatt += r'\w+ated\b|\w+ic\b|[a-zA-Z]+|\d|[^\w]'
    for tok in re.findall(jpatt, text):
        if (len(tok) > 2) and (tok[1:-1] in abbvdic):
            jtags.append((tok, 'ABBV-DEF'))
        elif tok in abbvdic:
            jtags.append((tok, 'ABBV-INST'))
        elif tok in annotdic:
            jtags.append((tok, annotdic[tok]))
        elif is_chebi_term(tok, chebiset):
            jtags.append((tok, 'CHEBI'))
        elif (re.search(adjv_patt, tok) != None) and (tok not in excep_adjv):
            jtags.append((tok, 'ADJV'))
        elif tok in _stopchars:
            jtags.append((tok, 'STOP'))
        elif tok in _connectchars:
            jtags.append((tok, 'CCONN'))
        elif tok.lower() in pconnset:
            jtags.append((tok, 'PCONN'))
        elif tok in string.digits:
            jtags.append((tok, 'DIGIT'))
        elif tok in string.whitespace:
            jtags.append((tok, 'WSPACE'))
        elif tok in string.punctuation:
            jtags.append((tok, 'PUNCT'))
        else:         
            jtags.append((tok, 'UNKNOWN'))
    return jtags

# Internal method: Split tagged_text in groups (separators are STOP, UNKNOWN)
def _split_groups(tagged_text):
    vtags = []
    vgroups = []
    for tok, tag in tagged_text:
        if (tag != 'STOP') and (tag != 'UNKNOWN'):
            vtags.append((tok, tag))
        else:
            vgroups.append(vtags)
            vtags = []
    if len(vtags) > 0:
        vgroups.append(vtags)
    return vgroups

# Internal method: Remove unsuitable tags in ending positions
def _remove_ending_tags(tagged_groups):
    rgroups = []
    remove_tags = set(['WSPACE', 'PUNCT', 'CCONN', 'PCONN', 'ADJV'])
    except_beg  = set(['(', '[', '{'])
    except_end  = set([')', ']', '}', 's'])
    for tgroup in tagged_groups:
        pre_deque = deque()
        tag_deque = deque(tgroup)
        while len(tag_deque) != len(pre_deque):
            pre_deque = deque(tag_deque)        
            while (len(tag_deque) > 0) and (tag_deque[0][1] in remove_tags) and (tag_deque[0][0] not in except_beg):
                tag_deque.popleft()
            while (len(tag_deque) > 0) and (tag_deque[-1][1] in remove_tags) and (tag_deque[-1][0] not in except_end):
                tag_deque.pop()
            if (len([tok for tok, tag in tag_deque if tag == 'ABBV-DEF']) == 1) and (tag_deque[-1][1] == 'ABBV-DEF'):
                tag_deque.pop()
        rgroups.append(list(tag_deque))
    return rgroups

# Internal method: Modify ABBV tags
def _modify_abbv_tags(tagged_groups, abbvdic):
    mgroups = []
    for tgroup in tagged_groups:
        mtags = []
        for tok, tag in tgroup:
            if tag == 'ABBV-DEF':
                newtag = abbvdic[tok[1:-1]]
            elif tag == 'ABBV-INST':
                newtag = abbvdic[tok]
            else:
                newtag = tag
            mtags.append((tok, newtag))
        mgroups.append(mtags)
    return mgroups

# Internal method: get joint POLY
def _joint_poly(tagged_groups, abbvdic):
    pgroups = []
    for tgroup in tagged_groups:
        npoly = len([tag for tok, tag in tgroup if (tag == 'POLY')])
        ndrug = len([tag for tok, tag in tgroup if (tag == 'DRUG')])
        nchem = len([tok for tok, tag in tgroup if (tag == 'CHEM')])
        nchem_abbv = len([tok for tok, tag in tgroup if (tag == 'CHEM') and (tok in abbvdic)])
        if (npoly > 0) and (ndrug == 0) and (nchem_abbv == 0) and (npoly + nchem > 1):
            pgroups.append(tgroup)
    return pgroups

# Internal method: get joint DRUG
def _joint_drug(tagged_groups, abbvdic):
    dgroups = []
    for tgroup in tagged_groups:
        npoly = len([tok for tok, tag in tgroup if (tag == 'POLY')])
        ndrug = len([tag for tok, tag in tgroup if (tag == 'DRUG')])
        nchem = len([tok for tok, tag in tgroup if (tag == 'CHEM')])
        if (ndrug == 1) and (npoly == 0) and (nchem > 0):
            dgroups.append(tgroup)
    return dgroups

# Internal method: get joint CHEM
def _joint_chem(tagged_groups, abbvdic):
    cgroups = []
    for tgroup in tagged_groups:
        npoly = len([tok for tok, tag in tgroup if (tag == 'POLY')])
        ndrug = len([tag for tok, tag in tgroup if (tag == 'DRUG')])
        nchem = len([tok for tok, tag in tgroup if (tag == 'CHEM')])
        if (nchem > 1) and (npoly == 0) and (ndrug == 0):
            cgroups.append(tgroup)
    return cgroups

# Internal method: Balance ending brackets sequentially (round, square and curly brackets)
def _balance_end_bracks(tok):
    tok = _balance_end_bracks_one_type(tok, '(', ')')
    tok = _balance_end_bracks_one_type(tok, '[', ']')
    tok = _balance_end_bracks_one_type(tok, '{', '}')
    return tok

# Internal method: Balance ending brackets for one bracket type
def _balance_end_bracks_one_type(tok, op_brack, cl_brack):
    prev_tok = ''
    while prev_tok != tok:
        prev_tok = tok
        if (tok[0] == op_brack) and (tok.count(op_brack) > tok.count(cl_brack)):
            tok = tok[1:]
        elif (tok[-1] == cl_brack) and (tok.count(op_brack) < tok.count(cl_brack)):
            tok = tok[:-1]
    return tok
    
