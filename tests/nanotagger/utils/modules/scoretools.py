
####################################################################################
################################ SCORING FUNCTIONS  ################################
####################################################################################

# Weight for partial matches
pw = 0.0

# Recall
def recall(tpos, fpos, fneg, part):
    if tpos + fneg + part > 0:
        rc = (tpos + pw * part) / (tpos + fneg + part)
    else:
        rc = 0.0
    return rc

# Precision
def precision(tpos, fpos, fneg, part):
    if tpos + fpos + part > 0:
        pr = (tpos + pw * part) / (tpos + fpos + part)
    else:
        pr = 0.0
    return pr

# F1-score
def f1score(recall, precision):
    if recall + precision > 0:
        f1 = 2 * recall * precision / (recall + precision)
    else:
        f1 = 0.0
    return f1

