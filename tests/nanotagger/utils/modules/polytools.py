import sys
import re
import string
from collections import deque
from chemtools import is_chebi_term, is_chem_comp, get_chem_form

####################################################################################
#################################### POLY METHODS ##################################
####################################################################################

# Extract polymer annotations in text
def get_annots(text, chebiset, elemset, greekset, pconnset, otherset):
    polylist = []
    if re.search('(P|p)oly', text) != None:
        text_tags  = _set_tags(text, chebiset, elemset, greekset, pconnset, otherset)
        corr_tags  = _correct_tags(text_tags, chebiset)
        group_tags = _split_groups(corr_tags)         
        poly_tags  = _split_poly(group_tags)     
        remov_tags = _remove_ending_tags(poly_tags)
        final_tags = _correct_groups(remov_tags)
        valid_tags = _check_tags(final_tags)

        # Add polymer annotations from valid groups
        for tgroup in valid_tags:
            poly = ''.join(zip(*tgroup)[0])
            polylist.append(_balance_end_bracks(poly))

        # Extract polymer annotations from brackets
        for tok, tag in corr_tags:
            if re.search('BRACK', tag) != None:
                polylist.extend(get_annots(tok[1:-1], chebiset, elemset,
                                           greekset, pconnset, otherset))

    # Return polymer annotations
    return polylist

# Extract poly annotations for common abbreviations
def get_abbvs(text, abbvset):
    abbvlist = []
    shf = 0
    patt = ''
    cptext = ' ' + text + ' '
    for abbv in abbvset:
        patt += '[^\w]' + abbv + '[^\w]' + '|'
    patt = patt[:-1]
    pmatch = re.search(patt, cptext[shf:])
    while (pmatch != None) and (pmatch.start() < pmatch.end()):
        stpos  = pmatch.start() + 1
        endpos = pmatch.end() - 1
        abbvlist.append(text[stpos+shf-1:endpos+shf-1])
        shf += endpos
        pmatch = re.search(patt, cptext[shf:])
    return abbvlist        

# Internal variables and data structures
_bracket_pairs = {'(':')', '[':']', '{':'}'}
_stopchars = set(['. ', ', ', '; ', ': ', '%'])

# Internal method: Tags text for POLY annotations 
def _set_tags(text, chebiset, elemset, greekset, pconnset, otherset):
    ppatt = ''
    ptags = []
    miscset = greekset.union(pconnset, otherset)
    bcont = _bracket_cont(text)
    chemf = get_chem_form(text, elemset)
    for sc in _stopchars:
        ppatt += re.escape(sc) + '|'
    for bc in bcont:
        ppatt += re.escape(bc) + '|'
    for ch in chemf:
        ppatt += re.escape(ch) + '|'
    ppatt += 'poly|[a-zA-Z]+|\d|[^\w]'
    for tok in re.findall(ppatt, text):
        if tok in bcont:
            if _valid_bracket(tok, chebiset, elemset, miscset):
                ptags.append((tok, 'V-BRACK'))
            else:
                ptags.append((tok, 'I-BRACK'))
        elif tok in chemf:
            ptags.append((tok, 'CHEMF'))
        elif tok in _stopchars:
            ptags.append((tok, 'STOP'))
        elif is_chebi_term(tok, chebiset):
            ptags.append((tok, 'CHEBI'))
        elif tok.lower() == 'poly':
            ptags.append((tok, 'POLY'))
        elif tok.lower() in greekset:
            ptags.append((tok, 'GREEK'))
        elif tok.lower() in pconnset:
            ptags.append((tok, 'PCONN'))
        elif tok.lower() in otherset:
            ptags.append((tok, 'OTHER'))
        elif tok in string.punctuation:
            ptags.append((tok, 'PUNCT'))
        elif tok in string.whitespace:
            ptags.append((tok, 'WSPACE'))
        elif tok in string.digits:
            ptags.append((tok, 'DIGIT'))
        else:         
            ptags.append((tok, 'UNKNOWN'))
    return ptags

# Internal method: Correct UNKNOWN or CHEBI tags containing poly
def _correct_tags(tagged_text, chebiset):
    ctags = []
    for tok, tag in tagged_text:
        if (tag == 'UNKNOWN') or (tag == 'CHEBI'):
            pmatch = re.search('poly', tok)
            while pmatch != None:
                st  = pmatch.start()
                end = pmatch.end()
                if st > 0:
                    if is_chebi_term(tok[:st], chebiset):
                        ctags.append((tok[:st], 'CHEBI'))
                    else:
                        ctags.append((tok[:st], 'UNKNOWN'))
                ctags.append((tok[st:end], 'POLY'))
                tok = tok[end:]
                pmatch = re.search('poly', tok)
            if len(tok) > 0:
                if is_chebi_term(tok, chebiset):
                    ctags.append((tok, 'CHEBI'))
                else:
                    ctags.append((tok, 'UNKNOWN'))
        else:
            ctags.append((tok, tag))
    return ctags

# Internal method: Bracket content
def _bracket_cont(text):
    pos = 0
    bcont = []
    while pos < len(text):
        count = 1
        while (pos < len(text)) and (text[pos] not in _bracket_pairs):
            pos += 1
        if pos < len(text):
            start = pos
            op = text[pos]
            cl = _bracket_pairs[op]
            pos += 1
            while (pos < len(text)) and (count > 0):
                if text[pos] == op:
                    count += 1
                elif text[pos] == cl:
                    count -= 1
                pos += 1
            if count == 0:
                bcont.append(text[start:pos])
            else:
                pos = start + 1       
    return bcont   

# Internal method: Check if bracket content is valid
def _valid_bracket(bcont, chebiset, elemset, miscset):
    brack_toks = re.findall('poly|[a-zA-Z]+|\d|[^\w]', bcont)
    valid_toks = [tok for tok in brack_toks if _valid_token(tok, chebiset, elemset, miscset)]
    return (len(brack_toks) == len(valid_toks))

# Internal method: Check if token is valid
def _valid_token(tok, chebiset, elemset, miscset):
    tlow = tok.lower()
    return ((tlow in string.punctuation) or (tlow in string.digits) or
            (tlow in string.whitespace) or (tlow in miscset) or
            is_chebi_term(tlow, chebiset) or (tlow == 'poly') or
            is_chem_comp(tok, elemset))

# Internal method: Split tagged_text in groups (separators are STOP, I-BRACK, UNKNOWN)
def _split_groups(tagged_text):
    vtags = []
    vgroups = []
    separators = set(['STOP', 'I-BRACK', 'UNKNOWN'])
    for tok, tag in tagged_text:
        if tag not in separators:
            vtags.append((tok, tag))
        else:
            vgroups.append(vtags)
            vtags = []
    if len(vtags) > 0:
        vgroups.append(vtags)
    return vgroups

# Internal method: Split tagged_groups in smaller groups containing one only POLY tag 
def _split_poly(tagged_groups):
    sgroups = []    
    for tgroup in tagged_groups:
        sg = []
        cnt_poly = 0
        for tk, tg in tgroup:
            if tg != 'POLY':
                sg.append((tk, tg))
            else:                
                if cnt_poly == 0:
                    sg.append((tk, tg))
                    cnt_poly += 1
                else:
                    sgroups.append(sg)
                    sg = [(tk, tg)]
        if (len(sg) > 0) and (cnt_poly == 1):
            sgroups.append(sg)                
    return sgroups

# Internal method: Remove unsuitable tags in ending positions
def _remove_ending_tags(tagged_groups):
    rgroups = []
    remove_beg = set(['WSPACE', 'PUNCT', 'PCONN', 'OTHER'])
    remove_end = set(['WSPACE', 'PUNCT', 'PCONN', 'OTHER', 'GREEK'])
    except_beg = set(['(', '[', '{'])
    except_end = set([')', ']', '}', 's'])
    for tgroup in tagged_groups:
        tdeque = deque(tgroup)
        while (len(tdeque) > 0) and (tdeque[0][1] in remove_beg) and (tdeque[0][0] not in except_beg):
            tdeque.popleft()
        while (len(tdeque) > 0) and (tdeque[-1][1] in remove_end) and (tdeque[-1][0] not in except_end):
            tdeque.pop()
        if (len(tdeque) > 1) and (tdeque[-1][1] == 'V-BRACK') and (tdeque[-2][1] == 'WSPACE'):
            tdeque.pop()
            tdeque.pop()
        if len(tdeque) > 0:
            rgroups.append(list(tdeque))
    return rgroups

# Internal method: Correct groups containing more than one poly token or subtoken
def _correct_groups(tagged_groups):
    cgroups = []
    for tgroup in tagged_groups:
        cnt_poly = 0
        cnt_dash = 0
        for tok, tag in tgroup:
            if len(re.findall('poly', tok)) > 0:
                cnt_poly += 1
            if tok == '-':
                cnt_dash += 1
        if (cnt_poly > 1) and (cnt_dash > 0):
            cg = []
            for tok, tag in tgroup:
                if tok != '-':
                    cg.append((tok, tag))
                else:
                    cgroups.append(cg)
                    cg = []
            if len(cg) > 0:
                cgroups.append(cg)
        else:
            cgroups.append(tgroup)
    return cgroups

# Internal method: Check tags
def _check_tags(tagged_groups):  
    return [tgroup for tgroup in tagged_groups if (len(tgroup) >= 2) and (tgroup[-1][1] != 'POLY')]

# Internal method: Balance ending brackets sequentially (round, square and curly brackets)
def _balance_end_bracks(tok):
    tok = _balance_end_bracks_one_type(tok, '(', ')')
    tok = _balance_end_bracks_one_type(tok, '[', ']')
    tok = _balance_end_bracks_one_type(tok, '{', '}')
    return tok

# Internal method: Balance ending brackets for one bracket type
def _balance_end_bracks_one_type(tok, op_brack, cl_brack):
    prev_tok = ''
    while prev_tok != tok:
        prev_tok = tok
        if (tok[0] == op_brack) and (tok.count(op_brack) > tok.count(cl_brack)):
            tok = tok[1:]
        elif (tok[-1] == cl_brack) and (tok.count(op_brack) < tok.count(cl_brack)):
            tok = tok[:-1]
    return tok

