import sys
import re
import string
from collections import deque

####################################################################################
################################### AUX CHEM METHODS ###############################
####################################################################################

# Check chebi term
def is_chebi_term(term, chebiset):
    return  ((len(term) >= _CHEBI_MINLEN) and 
             (len(_chebi_decomp(term.lower(), chebiset, last_word=True)) > 0))

# Check chemical compound
def is_chem_comp(term, elemset):
    return len(_chem_elem_decomp(term, elemset)) > 0

# Get chemical formulas
def get_chem_form(text, elemset):
    ctoks = []
    cgroups = []
    chemlist = []
    for tok in re.findall('[a-zA-Z]+|\d|[^\w]', text):
        if is_chem_comp(tok, elemset):
            chemlist.append(tok)  
    return chemlist      

# Internal variables
_CHEBI_MINLEN = 2

# Internal method: Decompose term in chebi subterms
def _chebi_decomp(term, chebiset, last_word=False):
    subterms = []
    splitted = False

    # Try splitting term in sub and rest if len >= 2*_CEHBI_MINLEN     
    if len(term) >= 2*_CHEBI_MINLEN:
        sublen = len(term) - 1
        while (sublen >= _CHEBI_MINLEN) and not splitted:
            if term[:sublen] in chebiset:
                sublist = _chebi_decomp(term[:sublen], chebiset)
                restlist = _chebi_decomp(term[sublen:], chebiset, last_word) 
                if len(restlist) > 0:
                    subterms.extend(sublist)
                    subterms.extend(restlist)
                    splitted = True
            sublen -= 1

    # If not splitted, search exact match or plural form
    if not splitted:
        plural = (term == 's') # or (term == 'es')
        if (term in chebiset) or (last_word and plural):
            subterms.append(term)

    # Return subterms list
    return subterms

# Internal method: Decompose term in chemical elements subterms
def _chem_elem_decomp(term, elemset):
    subterms = []

    # Match complete term   
    if (term in elemset) and ((len(term) == 2) or (term in 'CHNOFS')):
        subterms.append(term)

    # Try splitting term in sub and rest 
    else:
        splitted = False
        sublen = min(2, len(term))
        while (sublen > 0) and not splitted:
            if (((sublen == 2) and (term[:sublen] in elemset)) or
                ((sublen == 1) and (term[:sublen] in 'CHNOFS'))):
                sublist = _chem_elem_decomp(term[:sublen], elemset)
                restlist = _chem_elem_decomp(term[sublen:], elemset) 
                if len(restlist) > 0:
                    subterms.extend(sublist)
                    subterms.extend(restlist)
                    splitted = True
            sublen -= 1

    # Return subterms list
    return subterms


############################################################################################
######################################### CHEM METHODS #####################################
############################################################################################

# Extract chemical entities annotations in text
def get_annots(text, chebiset, elemset, greekset, otherset):
    chemlist = []
    text_tags  = _set_tags(text, chebiset, elemset, greekset, otherset)
    corr_tags  = _correct_tags(text_tags)
    group_tags = _split_groups(corr_tags)            
    remov_tags = _remove_ending_tags(group_tags)
    valid_tags = _check_tags(remov_tags, chebiset, elemset, greekset, otherset)

    # Add chemical annotations from valid groups
    for tgroup in valid_tags:
        chem = ''.join(zip(*tgroup)[0])
        chemlist.append(_balance_end_bracks(chem))

    # Extract chemical annotations from brackets
    for tok, tag in corr_tags:
        if re.search('BRACK', tag) != None:
            chemlist.extend(get_annots(tok[1:-1], chebiset, elemset, greekset, otherset))

    # Return annotations
    return chemlist

# Internal variables and data structures
_bracket_pairs = {'(':')', '[':']', '{':'}'}
_spec_toks = set(['acid', 'acids', 'salt', 'salts'])
_roman_num = set(['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii'])
_stopchars = set(['. ', ', ', '; ', ': ', '/', '=', '<', '>', '#', '%', '$', '&', '+/-'])
_except_terms = set(['mono', 'bi', 'di', 'tri', 'pent', 'penta', 'pentyl', 'hex', 
                     'hexa', 'hexyl', 'hept', 'hepta',  'cis', 'trans'])

# Internal method: Tags text for CHEM annotations 
def _set_tags(text, chebiset, elemset, greekset, otherset):
    ctags = []
    miscset = greekset.union(otherset)
    bcont = _bracket_cont(text)
    chemf = get_chem_form(text, elemset)
    cpatt = ''
    for sc in _stopchars:
        cpatt += re.escape(sc) + '|'
    for bc in bcont:
        cpatt += re.escape(bc) + '|'
    for ch in chemf:
        cpatt += re.escape(ch) + '|'
    cpatt += '[a-zA-Z]+|\d|[^\w]'
    for tok in re.findall(cpatt, text):
        if tok in bcont:
            if _roman_bracket(tok.lower()):
                ctags.append((tok, 'R-BRACK'))
            elif _valid_bracket(tok, chebiset, elemset, miscset):
                ctags.append((tok, 'V-BRACK'))
            else:
                ctags.append((tok, 'I-BRACK'))
        elif tok in chemf:
            ctags.append((tok, 'CHEMF'))
        elif tok in _spec_toks:
            ctags.append((tok, 'SPEC'))
        elif tok in _stopchars:
            ctags.append((tok, 'STOP'))
        elif is_chebi_term(tok, chebiset):
            ctags.append((tok, 'CHEBI'))
        elif tok.lower() in greekset:
            ctags.append((tok, 'GREEK'))
        elif tok.lower() in otherset:
            ctags.append((tok, 'OTHER'))
        elif tok in string.punctuation:
            ctags.append((tok, 'PUNCT'))
        elif tok in string.whitespace:
            ctags.append((tok, 'WSPACE'))
        elif tok in string.digits:
            ctags.append((tok, 'DIGIT'))
        else:         
            ctags.append((tok, 'UNKNOWN'))
    return ctags

# Internal method: Correct tags
def _correct_tags(tagged_text):
    ctags  = []
    prev_tag = ''
    adjv_patt = r'\w+ed\b|\w+ing\b|\w+ic\b|\w+able\b'
    for tok, tag in tagged_text:
        if (re.search(adjv_patt, tok) != None) and (tag == 'CHEBI'):
            ctags.append((tok, 'ADJV'))
        else:
            ctags.append((tok, tag))
        prev_tag = tag
    return ctags

# Internal method: Bracket content
def _bracket_cont(text):
    pos = 0
    bcont = []
    while pos < len(text):
        count = 1
        while (pos < len(text)) and (text[pos] not in _bracket_pairs):
            pos += 1
        if pos < len(text):
            start = pos
            op = text[pos]
            cl = _bracket_pairs[op]
            pos += 1
            while (pos < len(text)) and (count > 0):
                if text[pos] == op:
                    count += 1
                elif text[pos] == cl:
                    count -= 1
                pos += 1
            if count == 0:
                bcont.append(text[start:pos])
            else:
                pos = start + 1       
    return bcont

# Chemical formula
def chem_form(text, elemset):
    ctoks = []
    cgroups = []
    chemlist = []
    for tok in re.findall('[a-zA-Z]+|\d|[^\w]', text):
        if is_chem_comp(tok, elemset):
            chemlist.append(tok)  
    return chemlist          

# Internal method: Check if bracket content are roman numbers
def _roman_bracket(bcont):
    return (bcont[1:-1] in _roman_num)

# Internal method: Check if bracket content is valid
def _valid_bracket(bcont, chebiset, elemset, miscset):
    brack_toks = re.findall('[a-zA-Z]+|\d|[^\w]', bcont)
    ntoks = len([tok for tok in brack_toks])
    numbs = len([tok for tok in brack_toks if _numb_token(tok)])
    chems = len([tok for tok in brack_toks if _chem_token(tok, chebiset, elemset)])
    other = len([tok for tok in brack_toks if _other_token(tok, miscset)])
    return (ntoks == numbs + chems + other)

# Internal method: Check if token is a number
def _numb_token(tok):
    return (tok in string.digits)

# Internal method: Check if token is a chemical entity
def _chem_token(tok, chebiset, elemset):
    return is_chebi_term(tok.lower(), chebiset) or is_chem_comp(tok, elemset)

# Internal method: Check if token is other valid token
def _other_token(tok, miscset):
    tlow = tok.lower()
    return (tlow in string.whitespace) or (tlow in string.punctuation) or (tlow in miscset) 

# Internal method: Split tagged_text in groups (separators are STOP, I-BRACK, UNKNOWN)
def _split_groups(tagged_text):
    vtags = []
    vgroups = []
    separators = set(['STOP', 'I-BRACK', 'UNKNOWN'])
    for tok, tag in tagged_text:
        if tag not in separators:
            vtags.append((tok, tag))
        else:
            vgroups.append(vtags)
            vtags = []
    if len(vtags) > 0:
        vgroups.append(vtags)
    return vgroups

# Internal method: Remove unsuitable tags in ending positions
def _remove_ending_tags(tagged_groups):
    rgroups = []
    remove_beg = set(['WSPACE', 'PUNCT'])
    remove_end = set(['WSPACE', 'PUNCT', 'ADJV', 'OTHER', 'GREEK'])
    except_beg = set(['(', '[', '{'])
    except_end = set([')', ']', '}', 's', '+'])
    remove_tok = set(['In', 'As'])
    for tgroup in tagged_groups:
        tdeque = deque(tgroup)
        while (len(tdeque) > 0) and (tdeque[0][1] in remove_beg) and (tdeque[0][0] not in except_beg):
            tdeque.popleft()
        while (len(tdeque) > 0) and (tdeque[-1][1] in remove_end) and (tdeque[-1][0] not in except_end):
            tdeque.pop()
        if (len(tdeque) == 1) and (tdeque[0][0] in remove_tok): 
            tdeque.pop()
        if (len(tdeque) > 1) and (tdeque[-1][1].find('BRACK') > 0) and (tdeque[-2][1] == 'WSPACE'):
            tdeque.pop()
            tdeque.pop()
        if len(tdeque) > 0:
            rgroups.append(list(tdeque))
    return rgroups

# Internal method: Check tags
def _check_tags(tagged_groups, chebiset, elemset, greekset, otherset):
    vgroups = []
    for tgroup in tagged_groups:
        strgrp = ''.join([tok for tok, tag in tgroup])
        lgroup = sum([len(tok) for tok, tag in tgroup])    
        nchemf = len([tok for tok, tag in tgroup if tag == 'CHEMF'])
        nchebi = len([tok for tok, tag in tgroup if tag == 'CHEBI'])
        nadjv  = len([tok for tok, tag in tgroup if tag == 'ADJV'])
        nspec  = len([tok for tok, tag in tgroup if tag == 'SPEC'])
        ndigit = len([ch for tok, tag in tgroup for ch in tok if ch in string.digits])
        nletts = len([ch for tok, tag in tgroup for ch in tok if ch in string.letters])
        nupper = len([ch for tok, tag in tgroup for ch in tok if ch in string.uppercase])
        dbrack = [_set_tags(tok[1:-1], chebiset, elemset, greekset, otherset) for tok, tag in tgroup if tag == 'V-BRACK']
        cond1 = (lgroup > 1) and (lgroup > nupper) and (ndigit <= lgroup) and (strgrp not in _except_terms)
        cond2 = (nchemf + nchebi > 0) or (nadjv + nspec > 1)
        if cond1 and cond2:
            vgroups.append(tgroup)
        elif len(dbrack) > 0:
            for bgroup in _check_tags(dbrack, chebiset, elemset, greekset, otherset):
                vgroups.append(bgroup)
    return vgroups

# Internal method: Balance ending brackets sequentially (round, square and curly brackets)
def _balance_end_bracks(tok):
    tok = _balance_end_bracks_one_type(tok, '(', ')')
    tok = _balance_end_bracks_one_type(tok, '[', ']')
    tok = _balance_end_bracks_one_type(tok, '{', '}')
    return tok

# Internal method: Balance ending brackets for one bracket type
def _balance_end_bracks_one_type(tok, op_brack, cl_brack):
    prev_tok = ''
    while prev_tok != tok:
        prev_tok = tok
        if (tok[0] == op_brack) and (tok.count(op_brack) > tok.count(cl_brack)):
            tok = tok[1:]
        elif (tok[-1] == cl_brack) and (tok.count(op_brack) < tok.count(cl_brack)):
            tok = tok[:-1]
    return tok


