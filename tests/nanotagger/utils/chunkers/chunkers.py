import nltk
import string
import nltk.chunk
from nltk.tag import brill
from nltk.tag import DefaultTagger
from nltk.tag import UnigramTagger
from nltk.tag import BigramTagger
from nltk.tag import TrigramTagger
from nltk.tag import ClassifierBasedTagger
from nltk.classify import MaxentClassifier

#######################################################################################
#################################### FUNCTIONS ########################################
#######################################################################################

# Trainer function for Brill Tagger
def brill_trainer(init_tagger, train_sents):
    sym_bounds = [(1,1), (2,2), (1,2), (1,3)]
    asym_bounds = [(-1,-1), (1,1), (-1,1), (-2,2)]
    t1 = brill.SymmetricProximateTokensTemplate(brill.ProximateTagsRule, *sym_bounds)
    t2 = brill.SymmetricProximateTokensTemplate(brill.ProximateWordsRule, *sym_bounds)
    t3 = brill.ProximateTokensTemplate(brill.ProximateTagsRule, *asym_bounds)
    t4 = brill.ProximateTokensTemplate(brill.ProximateWordsRule, *asym_bounds)
    temp = [t1, t2, t3, t4]
    brill_trainer = brill.FastBrillTaggerTrainer(init_tagger, temp, deterministic=True, trace=1)
    return brill_trainer.train(train_sents)

# Feature detector function
# tokens = list of (word, tag); history = list of IOB tags
def prev_next_pos_iob(tokens, index, history):
    word, pos = tokens[index]

    # Set previous features
    if index == 0:
        prevword, prevpos, previob = ('<START>', '<START>', '<START>')
    else:
        prevword, prevpos = tokens[index-1]
        previob = history[index-1]

    # Set next features
    if index == len(tokens) - 1:
        nextword, nextpos = ('<END>', '<END>')
    else:
        nextword, nextpos = tokens[index+1]

    # Set and return features 
    feats = {'word': word, 'pos': pos, 'nextword': nextword, 'nextpos': nextpos,
             'prevword': prevword, 'prevpos': prevpos, 'previob': previob}
    return feats

#######################################################################################
#################################### CHUNKER CLASS ####################################
#######################################################################################

# Ubt Chunker
class UbtChunker(nltk.chunk.ChunkParserI):

    # Train Chunker based on ubt tagger using chunk_sents in tree format
    def __init__(self, chunk_sents):
        wtc_sents = [nltk.chunk.tree2conlltags(tree) for tree in chunk_sents]  
        tc_sents = [[(t,c) for (w,t,c) in sent] for sent in wtc_sents]
        dtagger  = DefaultTagger('O')
        utagger  = UnigramTagger(tc_sents, backoff=dtagger)
        ubtagger = BigramTagger(tc_sents, backoff=utagger)
        self.tagger = TrigramTagger(tc_sents, backoff=ubtagger)

    # Parse tagged_sent in (w,t) format to obtain the chunk_sent in tree format
    def parse(self, tagged_sent):
        ws,ts = zip(*tagged_sent) 
        cs = [c for (t,c) in self.tagger.tag(ts)]
        wtc = zip(ws, ts, cs)
        return nltk.chunk.conlltags2tree(wtc)

# Brill Chunker
class BrillChunker(nltk.chunk.ChunkParserI):

    # Train Chunker based on ubt tagger using chunk_sents in tree format
    def __init__(self, chunk_sents):
        wtc_sents = [nltk.chunk.tree2conlltags(tree) for tree in chunk_sents]  
        tc_sents = [[(t,c) for (w,t,c) in sent] for sent in wtc_sents]
        dtagger  = DefaultTagger('O')
        utagger  = UnigramTagger(tc_sents, backoff=dtagger)
        ubtagger = BigramTagger(tc_sents, backoff=utagger)
        ubttagger = TrigramTagger(tc_sents, backoff=ubtagger)
        self.tagger = brill_trainer(ubttagger, tc_sents)

    # Parse tagged_sent in (w,t) format to obtain the chunk_sent in tree format
    def parse(self, tagged_sent):
        ws,ts = zip(*tagged_sent) 
        cs = [c for (t,c) in self.tagger.tag(ts)]
        wtc = zip(ws, ts, cs)
        return nltk.chunk.conlltags2tree(wtc)    

# Max Entropy Chunker
class MaxentChunker(nltk.chunk.ChunkParserI):

    # Train NPChunker based on Maxent classifier using MEGAM algorithm
    def __init__(self, chunk_sents):
        wtc_sents = [nltk.chunk.tree2conlltags(tree) for tree in chunk_sents]  
        tr_sents = [[((w,t),c) for (w,t,c) in sent] for sent in wtc_sents]
        maxent_builder = lambda toks: MaxentClassifier.train(toks, trace=3, max_iter=40,
                                                             algorithm='megam', min_lldelta=0.001)
        self.tagger = ClassifierBasedTagger(train=tr_sents,
                                            feature_detector=prev_next_pos_iob,
                                            classifier_builder=maxent_builder)

    # Parse tagged_sents in (w,t) format to obtain the chunk_sents in tree format 
    def parse(self, tagged_sents):
        wtc = [(w,t,c) for ((w,t),c) in self.tagger.tag(tagged_sents)]
        return nltk.chunk.conlltags2tree(wtc)    

