import re
import sys
import string
from itertools import izip
from scoretools import recall, precision, f1score
from collections import deque

####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################

# Global vars
kdlist = ['ABBV', 'POLY', 'CHEM', 'DRUG', 'NANO', 'CELL', 'CHMT']       

# Check if line is associated to abstract content
def abst_cont(line):
    return len(re.findall('</?AbstractText[^>]*>', line)) == 2

# Check if line is associated to annotation content
def annot_cont(line):
    mpatt = 'ABBV|POLY|CHEM|DRUG|NANO|CELL|CHMT|TPOS|FPOS|FNEG|PART'
    return re.match(mpatt, line.strip()) != None

# Split text results by abstracts
def split_abstracts(textfile):
    abst = []
    abstlist = []
    for line in [line.strip() for line in textfile if abst_cont(line) or annot_cont(line)]:
        if line.startswith('<AbstractText'):
            if len(abst) > 0:
                abstlist.append(abst)
                abst = []
        abst.append(line)
    if len(abst) > 0:
        abstlist.append(abst)
    return abstlist

# Read match annotations from abstract
def read_match_annots(abst):
    text = ''
    tpos = {kd:0 for kd in kdlist}
    fpos = {kd:0 for kd in kdlist}
    fneg = {kd:0 for kd in kdlist}
    part = {kd:0 for kd in kdlist}
    kpatt = 'ABBV|POLY|CHEM|DRUG|NANO|CELL|CHMT'
    read_tpos, read_fpos, read_fneg, read_part = (False, False, False, False)

    # Read lines
    abstdq = deque(abst)
    while len(abstdq) > 0: 
        
        # Enable TPOS, FPOS, FNEG or PART reading
        line = abstdq.popleft()
        if line.startswith('TPOS:'):
            read_tpos, read_fpos, read_fneg, read_part = (True, False, False, False)
        elif line.startswith('FPOS:'):
            read_tpos, read_fpos, read_fneg, read_part = (False, True, False, False)
        elif line.startswith('FNEG:'):
            read_tpos, read_fpos, read_fneg, read_part = (False, False, True, False)
        elif line.startswith('PART:'):
            read_tpos, read_fpos, read_fneg, read_part = (False, False, False, True)

        # Read TPOS, FPOS, FNEG or PART matching
        else:
            if not line.startswith('<AbstractText'):
                terms = line.split('|')
                if read_tpos:
                    tpos[terms[0]] += 1
                elif read_fpos:
                    fpos[terms[0]] += 1
                elif read_fneg:
                    fneg[terms[0]] += 1 
                elif read_part:
                    part[terms[0]] += 1

    # Return TPOS, FPOS, FNEG, PART
    return (tpos, fpos, fneg, part)


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = match annotations file
# sys.argv[2] = annotation method

# Load match annotations
matchfile = open(sys.argv[1], 'r')
paramlist = [read_match_annots(abst) for abst in split_abstracts(matchfile)]
tpos = {kd:sum([params[0][kd] for params in paramlist]) for kd in kdlist}
fpos = {kd:sum([params[1][kd] for params in paramlist]) for kd in kdlist}
fneg = {kd:sum([params[2][kd] for params in paramlist]) for kd in kdlist}
part = {kd:sum([params[3][kd] for params in paramlist]) for kd in kdlist}

# Evaluate performance over POLY + CHEM + DRUG only for NanoTagger
if sys.argv[2] == 'NANO':

    # Calculate TPOS, FPOS, FNEG and PART for POLY + CHEM + DRUG
    tpc = sum([tpos[kd] for kd in ['POLY', 'CHEM', 'DRUG']])
    fpc = sum([fpos[kd] for kd in ['POLY', 'CHEM', 'DRUG']])
    fnc = sum([fneg[kd] for kd in ['POLY', 'CHEM', 'DRUG']])
    ptc = sum([part[kd] for kd in ['POLY', 'CHEM', 'DRUG']])

    # Calculate recall, precision and F1-score
    rc = recall(tpc, fpc, fnc, ptc)
    pr = precision(tpc, fpc, fnc, ptc)
    f1 = f1score(rc, pr)

    # Show scores
    print '\n****************************************************************************'
    print '*********************** POLY + CHEM + DRUG ANNOTATIONS *********************'
    print '****************************************************************************\n'
    print '    TPOS =', tpc
    print '    FPOS =', fpc
    print '    FNEG =', fnc
    print '    PART =', ptc
    print '    RECALL = {0:.3f}'.format(rc)
    print '    PRECISION = {0:.3f}'.format(pr)
    print '    F1-SCORE = {0:.3f}'.format(f1)

# Redefine kdlist to show only relevant categories for each method
if sys.argv[2] == 'AB3P':
    kdlist = ['ABBV']
elif sys.argv[2] == 'GENIA':
    kdlist = ['CELL']
elif sys.argv[2] == 'OSCAR':
    kdlist = ['CHMT']
elif sys.argv[2] == 'CHEMT':
    kdlist = ['CHMT']
elif sys.argv[2] == 'CSPOT':
    kdlist = ['ABBV', 'CHMT']
elif sys.argv[2] == 'TMCHEM':
    kdlist = ['CHMT']
if sys.argv[2] == 'ABNK':
    kdlist = ['ABBV']
elif sys.argv[2] == 'NANO':
    kdlist = ['ABBV', 'POLY', 'CHEM', 'DRUG', 'NANO', 'CELL']

# Evaluate performance by CATEGORY
print '\n****************************************************************************'
print '************************** ANNOTATIONS BY CATEGORY *************************'
print '****************************************************************************\n'
for kd in kdlist:

    # Calculate recall, precision and F1-score
    rc = recall(tpos[kd], fpos[kd], fneg[kd], part[kd])
    pr = precision(tpos[kd], fpos[kd], fneg[kd], part[kd])
    f1 = f1score(rc, pr)

    # Show scores
    print 'RESULTS FOR {0}:'.format(kd)
    print '    TPOS =', tpos[kd]
    print '    FPOS =', fpos[kd]
    print '    FNEG =', fneg[kd]
    print '    PART =', part[kd]
    print '    RECALL = {0:.3f}'.format(rc)
    print '    PRECISION = {0:.3f}'.format(pr)
    print '    F1-SCORE = {0:.3f}'.format(f1)
    print

