# Loop for training set and gold standard
for N in 0 1
do
    # Define vars
    CHK='_with_chunk'
    NOCHK='_wout_chunk'
    DOCLIST=('train' 'gold_standard')
    DOCTYPE=${DOCLIST[$N]}
    AB3PDIR=$HOME/Software/Ab3P-v1.5
    GENIADIR=$HOME/Software/genia-tagger
    OSCARDIR=$HOME/Software/jruby_oscar
    CHEMTDIR=$HOME/Software/jruby_chemtagger
    TMCHMDIR=$HOME/Software/tmChem
    SPOTDIR=$HOME/Software/chemspot-2.0
    WORKDIR=$NANODEV/tests/nanotagger

    xml_corp=$WORKDIR/inp/pubmed_abstracts_$DOCTYPE.xml

    ab3p_abs=$WORKDIR/inp/ab3p_abstracts.tmp
    genia_abs=$WORKDIR/inp/genia_abstracts.tmp
    oscar_abs=$WORKDIR/inp/oscar_abstracts.tmp
    chemt_abs=$WORKDIR/inp/chemt_abstracts.tmp
    spot_abs=$WORKDIR/inp/spot_abstracts.tmp
    nano_abs=$WORKDIR/inp/nano_abstracts.tmp

    tmp_inp=$WORKDIR/inp/tmp
    ab3p_inp=$WORKDIR/inp/ab3p_input.tmp
    genia_inp=$WORKDIR/inp/genia_input.tmp

    genia_tmp=$WORKDIR/out/genia_annots_out.tmp
    spot_tmp=$WORKDIR/out/spot_annots_out.tmp

    tmp_out=$WORKDIR/out/tmp
    manual_out=$WORKDIR/out/manual_$DOCTYPE.out
    ab3p_out=$WORKDIR/out/ab3p_$DOCTYPE.out
    genia_out=$WORKDIR/out/genia_$DOCTYPE.out
    oscar_out=$WORKDIR/out/oscar_$DOCTYPE.out
    chemt_out=$WORKDIR/out/chemt_$DOCTYPE.out
    tmchm_out=$WORKDIR/out/tmchm_$DOCTYPE.out
    spot_out=$WORKDIR/out/spot_$DOCTYPE.out
    nano_out_chk=$WORKDIR/out/nano_$DOCTYPE$CHK.out
    nano_out_nochk=$WORKDIR/out/nano_$DOCTYPE$NOCHK.out

    proc_manual=$WORKDIR/bin/process_manual_out.py
    proc_genia=$WORKDIR/bin/process_genia_out.py
    proc_spot=$WORKDIR/bin/process_spot_out.py
    proc_tmchm_inp=$WORKDIR/bin/process_tmchm_inp.py
    proc_tmchm_out=$WORKDIR/bin/process_tmchm_out.py

    run_split=$WORKDIR/bin/sent_splitter.py
    run_nano=$WORKDIR/bin/nanotagger.py

    # Define path to extra python and perl modules
    export PERLLIB=$TMCHMDIR
    export PYTHONPATH=$WORKDIR/utils/modules:$WORKDIR/utils/chunkers  

    # Start annotation process
    echo
    echo "########## Annotations for $DOCTYPE corpus ##########"

    # Process manual abbreviations from annotated corpus
    echo "Processing manual annotations from $DOCTYPE file..."
    python $proc_manual $xml_corp > $manual_out

#    # Extract abbreviations from corpus file using Ab3P method
#    cd $AB3PDIR
#    echo -n '' > $ab3p_out 
#    echo 'Extracting abbreviations using Ab3P method...'
#    grep '<AbstractText' $xml_corp > $ab3p_abs
#    while read line
#    do
#        echo $line > $ab3p_inp
#        $AB3PDIR/identify_abbr $ab3p_inp >> $ab3p_out
#        echo '' >> $ab3p_out
#    done < $ab3p_abs
#    rm $ab3p_abs $ab3p_inp
#    echo

#    # Extract annotations from corpus file using GENIA Tagger
#    cd $GENIADIR
#    echo -n '' > $genia_out 
#    echo 'Extracting annotations using GENIA Tagger...'
#    grep '<AbstractText' $xml_corp > $genia_abs
#    python $run_split $genia_abs > $genia_inp
#    time $GENIADIR/geniatagger < $genia_inp > $genia_tmp
#    python $proc_genia $genia_abs $genia_tmp > $genia_out
#    rm $genia_abs $genia_inp $genia_tmp
#    echo

#    # Extract annotations from corpus file using OSCAR4
#    echo 'Extracting annotations using OSCAR4...'
#    grep '<AbstractText' $xml_corp > $oscar_abs 
#    time $OSCARDIR/oscar.sh $oscar_abs > $oscar_out
#    rm $oscar_abs
#    echo

#    # Extract annotations from corpus file using ChemicalTagger
#    echo 'Extracting annotations using ChemicalTagger...'
#    grep '<AbstractText' $xml_corp > $chemt_abs 
#    time $CHEMTDIR/chemtagger.sh $chemt_abs > $chemt_out
#    rm $chemt_abs
#    echo

#    # Extract annotations from corpus file using ChemSpot
#    echo 'Extracting annotations using ChemSpot...'
#    grep '<AbstractText' $xml_corp > $spot_abs 
#    time java -Xmx16G -jar $SPOTDIR/chemspot.jar -t $spot_abs -o $spot_tmp -M $SPOTDIR/multiclass.bin -d $SPOTDIR/dict.zip -i ""
#    python $proc_spot $spot_abs $spot_tmp > $spot_out
#    rm $spot_abs $spot_tmp
#    echo

#    # Extract annotations from corpus file using tmChem
#    echo 'Extracting annotations using tmChem...'
#    cd $TMCHMDIR
#    mkdir $tmp_inp $tmp_out
#    python $proc_tmchm_inp $xml_corp $tmp_inp
#    time $TMCHMDIR/tmChem.pl -i $tmp_inp -o $tmp_out
#    python $proc_tmchm_out $tmp_out > $tmchm_out
#    rm -r $tmp_inp $tmp_out
#    echo

#    # Extract abbreviations from corpus file using NanoTagger without chunking
#    cd $WORKDIR
#    echo 'Extracting abbreviations using NanoTagger without chunking...'
#    grep '<AbstractText' $xml_corp > $nano_abs 
#    time python $run_nano $nano_abs 'NOCHK' > $nano_out_nochk
#    rm $nano_abs

#    # Extract abbreviations from corpus file using NanoTagger with chunking
#    cd $WORKDIR
#    echo 'Extracting abbreviations using NanoTagger with chunking...'
#    grep '<AbstractText' $xml_corp > $nano_abs 
#    time python $run_nano $nano_abs > $nano_out_chk
#    rm $nano_abs

done
echo

