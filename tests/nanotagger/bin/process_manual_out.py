import re
import sys
import string
from itertools import izip
from annotclass import TextAnnots

####################################################################################
############################# MANUAL ANNOTATIONS CLASS #############################
####################################################################################

# ManualAnnots Class
class ManualAnnots(TextAnnots):    

    # Initialize object
    def __init__(self):
        self._text = ''
        self._kind = {}
        self._abbv = {}
        self._annots = []  

    # Add abbreviation
    def add_abbv(self, kind, sform, lform):
        self._abbv[sform] = [lform, kind]
        self._add_singular_form(kind, sform)
        self._add_plural_form(kind, sform)        

    # Add annotation
    def add_annot(self, kind, annot):
        self._add_singular_form(kind, annot)
        self._add_plural_form(kind, annot)

    # Search positions for each annotation in text
    def search_annots(self):
        for kind in ['POLY', 'CHEM', 'DRUG', 'NANO', 'CELL']:
            annotlist = [an for an, kd in self._kind.items() if kd == kind]
            self._annots.extend(self._match_annots(annotlist, kind))

    # Private method: add singular form
    def _add_singular_form(self, kind, annot):
        sing = _get_sing(annot)
        self._kind[sing] = kind

    # Private method: add plural form
    def _add_plural_form(self, kind, annot):
        plur = _get_plur(annot)
        self._kind[plur] = kind


####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################        

# Auxiliar function: Get singular form
def _get_sing(text):
    if text[-1] == 's':
        sform = text[:-1]
    else:
        sform = text
    return sform

# Auxiliar function: Get plural form
def _get_plur(text):
    if text[-1] == 's':
        pform = text
    else:
        pform = text + 's'
    return pform

# Check if line is associated to abstract content
def abst_cont(line):
    return len(re.findall('</?AbstractText[^>]*>', line)) == 2

# Check if line is associated to annotation content
def annot_cont(line):
    mpatt = 'POLY|CHEM|DRUG|NANO|CELL|ABBV|</?Annotations>'
    return (re.match(mpatt, line) != None) or (len(line.split('|')) >= 3)

# Split corpus in abstracts
def split_abstracts(corp):
    abst = []
    abstlist = []
    for line in [line.strip() for line in corp if abst_cont(line) or annot_cont(line)]:
        if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
            if len(abst) > 0:
                abstlist.append(abst)
                abst = []
        abst.append(line)
    if len(abst) > 0:
        abstlist.append(abst)
    return abstlist

# Import manual annotations from abstract
def import_manual_annots(abst):
    ma = ManualAnnots()
    read_annots = False
    for line in abst:
        if re.search('<AbstractText[^>]*>', line) != None:
            ma.set_text(line)
        elif re.search('<Annotations>', line) != None:
            read_annots = True
        elif re.search('</Annotations>', line) != None:
            read_annots = False
        else:
            if read_annots:
                terms = line.strip().split('|')
                if len(terms) > 1:
                    if terms[0] == 'ABBV':
                        if len(terms) == 4:
                            ma.add_abbv(terms[3], terms[1], terms[2])
                        else:
                            ma.add_abbv('OTHER', terms[1], terms[2])
                    else:
                        ma.add_annot(terms[0], terms[1])
    ma.search_annots()
    ma.uniquify_annots()
    return ma
            

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = manual annotated corpus

# Load abstracts and corresponding manual annotations
corpfile = open(sys.argv[1], 'r')
manual_annots = [import_manual_annots(abst) for abst in split_abstracts(corpfile)]

# Print abstracts and corresponding manual annotations
for ma in manual_annots:
    print ma.get_text()
    if len(ma.get_abbvs()) > 0:
        for sf, lf in sorted(ma.get_abbvs().items(), key=lambda tup: tup[0]):
            print 'ABBV|' + sf + '|' + lf[0]
    if len(ma.get_annots()) > 0:
        for kd, st, end in ma.get_annots():
            print kd + '|' + str(st) + '|' + str(end) + '|' + ma.get_text(st, end)
    print
        
