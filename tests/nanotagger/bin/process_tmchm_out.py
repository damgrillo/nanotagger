import os
import re
import sys
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = tmChem annotated abstracts folder

# Load annotated corpus in PubTator format
abstlist = []
for n in range(110):
    abst = []
    filename = sys.argv[1] + '/abstract' + str(n).zfill(4) + '.tmChem'
    if os.path.isfile(filename):
        abstfile = open(filename, 'r')
        for line in abstfile:
            abst.append(line.strip())
        abstlist.append(abst)
        
# Print annotated corpus in convenient format to be processed later
for abst in abstlist:
    for line in abst:
        pipesplit = line.split('|')
        tabsplit  = line.split('\t')
        if (len(pipesplit) > 1) and (pipesplit[1] == 'a'):
            abstext = line.split('|')[2]
            print abstext
        elif len(tabsplit) > 3:
            stpos  = int(tabsplit[1]) - 13
            endpos = int(tabsplit[2]) - 13
            print 'TMCHEM|' + str(stpos) + '|' + str(endpos) + '|' + abstext[stpos:endpos]
    print

