# Loop for training set and gold standard
for N in 0 1
do
    # Define vars
    CHK='_with_chunk'
    NOCHK='_wout_chunk'
    DOCLIST=('train' 'gold_standard') 
    DOCTYPE=${DOCLIST[$N]}
    WORKDIR=$NANODEV/tests/nanotagger

    manual_out=$WORKDIR/out/manual_$DOCTYPE.out
    ab3p_out=$WORKDIR/out/ab3p_$DOCTYPE.out
    genia_out=$WORKDIR/out/genia_$DOCTYPE.out
    oscar_out=$WORKDIR/out/oscar_$DOCTYPE.out
    chemt_out=$WORKDIR/out/chemt_$DOCTYPE.out
    spot_out=$WORKDIR/out/spot_$DOCTYPE.out 
    tmchm_out=$WORKDIR/out/tmchm_$DOCTYPE.out   
    nano_out_chk=$WORKDIR/out/nano_$DOCTYPE$CHK.out  
    nano_out_nochk=$WORKDIR/out/nano_$DOCTYPE$NOCHK.out

    ab3p_mch=$WORKDIR/mch/ab3p_$DOCTYPE.mch
    genia_mch=$WORKDIR/mch/genia_$DOCTYPE.mch
    oscar_mch=$WORKDIR/mch/oscar_$DOCTYPE.mch
    chemt_mch=$WORKDIR/mch/chemt_$DOCTYPE.mch
    spot_mch=$WORKDIR/mch/spot_$DOCTYPE.mch
    tmchm_mch=$WORKDIR/mch/tmchm_$DOCTYPE.mch
    nano_chk_mch=$WORKDIR/mch/nano_$DOCTYPE$CHK.mch
    nano_nochk_mch=$WORKDIR/mch/nano_$DOCTYPE$NOCHK.mch
    nano_chk_abnk_mch=$WORKDIR/mch/nano_abnk_$DOCTYPE$CHK.mch
    nano_nochk_abnk_mch=$WORKDIR/mch/nano_abnk_$DOCTYPE$NOCHK.mch

    run_mch=$WORKDIR/bin/matching.py

    # Define path to extra python modules
    export PYTHONPATH=$WORKDIR/utils/modules:$WORKDIR/utils/chunkers

    # Match manual and automatic annotations extracted by each method
    echo
    echo "########## Matching annotations for $DOCTYPE corpus ##########"
    echo 'Matching annotations for AB3P method...'
    python $run_mch $manual_out $ab3p_out 'AB3P' > $ab3p_mch
    echo 'Matching annotations for GENIA...'
    python $run_mch $manual_out $genia_out 'GENIA' > $genia_mch
    echo 'Matching annotations for OSCAR...'
    python $run_mch $manual_out $oscar_out 'OSCAR' > $oscar_mch
    echo 'Matching annotations for ChemicalTagger...'
    python $run_mch $manual_out $chemt_out 'CHEMT' > $chemt_mch
    echo 'Matching annotations for CHEMSPOT...'
    python $run_mch $manual_out $spot_out 'CSPOT' > $spot_mch
    echo 'Matching annotations for tmChem...'
    python $run_mch $manual_out $tmchm_out 'TMCHEM' > $tmchm_mch
    echo 'Matching annotations for NanoTagger without chunking...'
    python $run_mch $manual_out $nano_out_nochk 'NANO' > $nano_nochk_mch
    echo 'Matching annotations for NanoTagger without chunking including no kind abbvs...'
    python $run_mch $manual_out $nano_out_nochk 'ABNK' > $nano_nochk_abnk_mch
    echo 'Matching annotations for NanoTagger with chunking...'
    python $run_mch $manual_out $nano_out_chk 'NANO' > $nano_chk_mch
    echo 'Matching annotations for NanoTagger with chunking including no kind abbvs...'
    python $run_mch $manual_out $nano_out_chk 'ABNK' > $nano_chk_abnk_mch
done
echo

