import re
import sys
import nltk
import string
import pickle
from chunkers import MaxentChunker
from annotclass import NanoTaggerAnnots

####################################################################################
################### MODULES, DATA STRUCTURES AND VARIABLES #########################
####################################################################################

# POS-Tagger
ft = open('utils/postaggers/brill_rubt_tagger.pickle', 'r')
postagger = pickle.load(ft)
    
# Chunker
fc = open('utils/chunkers/maxent_chunker.pickle', 'r')
chunker = pickle.load(fc)

# Elements
elemfile = open('dict/elems.txt', 'r')
elemset = set([line.split()[1] for line in elemfile])

# CHEBI terms for polymers
pchebifile = open('dict/poly_chebi.txt', 'r')
pchebiset = set([line.strip() for line in pchebifile if len(line.strip()) > 0])

# CHEBI terms for chemical entities
cchebifile = open('dict/chem_chebi.txt', 'r')
cchebiset = set([line.strip() for line in cchebifile if len(line.strip()) > 0])

# Drugs
drugfile = open('dict/drugs.txt', 'r')
drugset = set([line.strip() for line in drugfile if len(line.strip()) > 0])

# Cells
cellfile = open('dict/cells.txt', 'r')
cellset = set([line.strip() for line in cellfile if len(line.strip()) > 0])

# Greek letters
greekfile = open('dict/greek_letters.txt', 'r')
greekset = set([line.strip() for line in greekfile if len(line.strip()) > 0])

# Polymer connectors
pconnfile = open('dict/poly_connect.txt', 'r')
pconnset = set([line.strip() for line in pconnfile if len(line.strip()) > 0])

# Other common terms for chemical entities
otherfile = open('dict/other_terms.txt', 'r')
otherset = set([line.strip() for line in otherfile if len(line.strip()) > 0])

# Common abbreviations for polymers
abbv_file = open('dict/poly_abbvs.txt', 'r')
poly_abbvs = set([line.strip() for line in abbv_file if len(line.strip()) > 0])

# Common abbreviations for drugs
abbv_file = open('dict/drug_abbvs.txt', 'r')
drug_abbvs = set([line.strip() for line in abbv_file if len(line.strip()) > 0])

# Drugs exceptions
excep_file = open('dict/drug_except.txt', 'r')
drug_excep = set([line.strip() for line in excep_file if len(line.strip()) > 0])

# Special abbreviations for ABBV method
spec_abbvs = {}
spec_file = open('dict/special_abbvs.txt', 'r')
for line in spec_file:
    terms = line.split('\t')
    if len(terms) == 2:
        sform = terms[0].strip()
        lforms = [lf.strip() for lf in terms[1].split('|')]
        spec_abbvs[sform] = lforms


####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################           

# Perform text chunking and get noun phrase chunks
def get_chunks(text, tagger, chunker):
    sents = []
    chunks = []
    for s in text.strip().split('. '):
        sents.append(s.rstrip('.'))
    tok_sents = [s.split() for s in sents]
    tag_sents = [tagger.tag(tks) for tks in tok_sents]
    chk_sents = [chunker.parse(tgs) for tgs in tag_sents]
    iob_sents = [nltk.chunk.tree2conlltags(cks) for cks in chk_sents]
    for sent in iob_sents:
        npchk = ''
        for tok, tag, chk in sent:
            if re.search('NP', chk) != None:
                npchk += tok + ' '
            else:
                if len(npchk) > 0:
                    chunks.append(npchk.strip())
                    npchk = ''
        if len(npchk) > 0:
            chunks.append(npchk.strip())
            npchk = ''
    return chunks

# Map chunks to text positions
def chunks2textpos(chunks, text):
    shft = 0
    chunkmap = []
    for chk in chunks:
        match = re.search(re.escape(chk), text)
        if match != None:
            chunkmap.append((chk, match.start()+shft))
        text = text[match.end():]
        shft += match.end()
    if len(chunkmap) == len(chunks):
        return chunkmap
    else:
        raise ValueError('Error mapping chunks to text positions')
            

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input corpus
# sys.argv[2] = if specified, extract annotations with no chunking

# Load texts from corpus
corpfile = open(sys.argv[1], 'r')
textlist = [line.strip() for line in corpfile]

# Create file for chunks
fchunks = open('chk/nano_chunks.chk', 'w')

# Extract annotations for each text
for text in textlist:

    # Get chunks and map chunks to text positions
    if (len(sys.argv) > 2) and (sys.argv[2] == 'NOCHK'):
        chunks = text.strip().split('. ')
    else:
        chunks = get_chunks(text, postagger, chunker)
    chunkmap = chunks2textpos(chunks, text)

    # Save text and chunks for further analysis
    fchunks.write(text + '\n')
    for chk in chunks:
        fchunks.write(chk + '\n')
    fchunks.write('\n')

    # First scan: Look for ABBV annotations. Do not use common abbvs for POLY and DRUG annots 
    global_abbvs = {} 
    for chktext, chkpos in chunkmap:
        nta = NanoTaggerAnnots()
        nta.set_text(chktext)

        # Search POLY, CHEM, DRUG, CELL annotations and corresponding ABBV annotations   
        nta.search_poly(pchebiset, elemset, greekset, pconnset, otherset, set([]))
        nta.search_chem(cchebiset, elemset, greekset, otherset)
        nta.search_drug(drugset, pchebiset, elemset, drug_excep, set([]))
        nta.search_cell(cellset)
        nta.uniquify_annots()
        nta.update_abbv(spec_abbvs)

        # Search joint POLY, CHEM and DRUG annotations and corresponding ABBV annotations
        nta.search_joint(pchebiset, pconnset)
        nta.uniquify_annots()
        nta.update_abbv(spec_abbvs)

        # Search NANO annotations and corresponding ABBV annotations
        nta.search_nano(cchebiset)
        nta.uniquify_annots()
        nta.update_abbv(spec_abbvs)

        # Update global ABBVs
        global_abbvs.update(nta.get_abbvs())

    # Second scan: Look for rest of annotations
    global_annots = []
    for chktext, chkpos in chunkmap:
        nta = NanoTaggerAnnots()
        nta.set_text(chktext)

        # Load global ABBVs
        nta.load_abbvs(global_abbvs)

        # Search POLY, CHEM, DRUG, CELL annotations       
        nta.search_poly(pchebiset, elemset, greekset, pconnset, otherset, poly_abbvs)
        nta.search_chem(cchebiset, elemset, greekset, otherset)
        nta.search_drug(drugset, pchebiset, elemset, drug_excep, drug_abbvs)
        nta.search_cell(cellset)
        nta.uniquify_annots()

        # Search joint POLY, CHEM and DRUG annotations
        nta.search_joint(cchebiset, pconnset)
        nta.uniquify_annots()

        # Search NANO annotations
        nta.search_nano(cchebiset)
        nta.uniquify_annots()

        # Shift positions to fit annotations in the original text
        nta.shift_annot_pos(chkpos)
        global_annots.append(nta)

    # Print annotations
    print text     
    if len(global_abbvs) > 0:
        for sf, lf in global_abbvs.items():
            if lf[1] != 'NKND':
                print 'ABBV|' + sf + '|' + lf[0]
            else:
                print 'ABNK|' + sf + '|' + lf[0]  
    for an in global_annots:
        annots = an.get_annots()
        if len(annots) > 0:
            for kd, st, end in annots:
                print kd + '|' + str(st) + '|' + str(end) + '|' + text[st:end]
    print 

