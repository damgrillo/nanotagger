import sys
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input file to be split

# Load text and split sentences
textfile = open(sys.argv[1], 'r')
sentlist = [sent for line in textfile for sent in line.strip().split('. ')]
for sent in sentlist:
    print sent


