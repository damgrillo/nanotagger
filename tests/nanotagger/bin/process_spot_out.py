import re
import sys
import string
from itertools import izip

########################################################################################
##################################### MAIN PROGRAM #####################################
########################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = abstract corpus
# sys.argv[2] = chemspot annotations

# Load abstracts
corpfile = open(sys.argv[1], 'r')
abstlist = [line.strip() for line in corpfile]

# Load annotations
spotfile  = open(sys.argv[2], 'r')
annotlist = [line.strip().split('\t') for line in spotfile if len(line.strip().split('\t')) > 3]

# Group annotations by abstract and correct indexes for annotations
abstpos = 0
shiftpos = 0
grouplist = []
for abst in abstlist:
    group = []
    abstpos += len(abst) + 1
    while (len(annotlist) > 0) and (int(annotlist[0][1]) < abstpos):
        elems  = annotlist.pop(0)
        stpos  = int(elems[0])
        endpos = int(elems[1])
        annot  = elems[2:]
        annot[0], annot[1] = annot[1], annot[0]
        if len(group) == 0:
            shiftpos = re.search(re.escape(annot[1]), abst).start() - stpos
        stpos  += shiftpos
        endpos += shiftpos + 1
        corr_annot = [stpos, endpos]
        corr_annot.extend(annot)
        group.append(corr_annot)
    grouplist.append(group)

# Print results
for abst, group in izip(abstlist, grouplist):
    print abst
    for annot in group:
        res = ''
        for a in annot:
            res += str(a) + '|'
        print res[:-1]
    print
        
    
    


