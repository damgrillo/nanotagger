# Define vars
WORKDIR=$NANODEV/tests/nanotagger
xml_corp_dam=$WORKDIR/inp/pubmed_abstracts_test_annot_damian_v2.xml
xml_corp_dav=$WORKDIR/inp/pubmed_abstracts_test_annot_david_v2.xml
tmp_corp_dam=$WORKDIR/out/manual_test_damian.tmp
tmp_corp_dav=$WORKDIR/out/manual_test_david.tmp
proc_manual=$WORKDIR/bin/process_manual_out.py
run_agree=$WORKDIR/bin/agreement.py
iaa_eval=$WORKDIR/inp/iaa.eval

# Define path to extra python modules
export PYTHONPATH=$WORKDIR/utils/modules:$WORKDIR/utils/chunkers

# Process annotations from each annotated corpus
python $proc_manual $xml_corp_dam > $tmp_corp_dam
python $proc_manual $xml_corp_dav > $tmp_corp_dav

# Evaluate inter annotation agreement
python $run_agree $tmp_corp_dam $tmp_corp_dav > $iaa_eval
rm $tmp_corp_dam $tmp_corp_dav

