import re
import sys
import string
from itertools import izip
from difflib import SequenceMatcher


# Helper function: Get text block matching term with highest similarity 
def get_hsim_block(term, text):
    match_block = re.search(re.escape(term), text)
    if match_block != None:
        hsim_block = (match_block.start(), match_block.end())
    else:
        stpos = 0
        hsim_stpos = 0   
        hsim_ratio = 0     
        hsim_length = 0

        # Calculate start position of highest similarity matching block   
        while (len(text[stpos:]) > len(term)) and ((hsim_ratio < 0.8) or (ratio > 0.8)):
            ratio = SequenceMatcher(None, term, text[stpos:stpos+len(term)]).ratio()
            if ratio > hsim_ratio:
                hsim_ratio = ratio
                hsim_stpos = stpos
            stpos += 1       

        # Calculate length of highest similarity matching block
        length = len(term)
        ratio = hsim_ratio
        while ratio > 0.8:
            ratio = SequenceMatcher(None, term, text[hsim_stpos:hsim_stpos+length]).ratio()
            if ratio > hsim_ratio:
                hsim_ratio = ratio
                hsim_length = length
            length += 1

        # Set highest similarity block positions 
        hsim_block = (hsim_stpos, hsim_stpos + hsim_length)

    return hsim_block

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = abstract corpus
# sys.argv[2] = GENIA annotations

# Load abstracts
corpfile = open(sys.argv[1], 'r')
abstlist = [line.strip() for line in corpfile]

# Load annotations
an = ''
prev_tok = ''
annots = []
annotlist = []
geniafile  = open(sys.argv[2], 'r')
for line in geniafile:
    if re.match('AbstractText', line) != None:
        annots = []
    elif re.match('/AbstractText', line) != None:
        annotlist.append(annots)
    else:
        if len(line.strip().split('\t')) == 5:
            tok, root, ptag, chk, netag = line.strip().split('\t')
            if re.search('cell_type|cell_line', netag) != None:
                if (prev_tok not in string.punctuation) and (tok not in string.punctuation):
                    an += ' '
                an += tok
            else:
                if len(an) > 0:
                    annots.append(an.strip())
                    an = ''
            prev_tok = tok

# Match annotations in abstracts and calculate positions for each annotation
annotposlist = []
for abst, annots in izip(abstlist, annotlist):
    shift = 0
    annotpos = []
    for annot in annots:
        hsblock = get_hsim_block(annot, abst[shift:])
        annotpos.append((hsblock[0] + shift, hsblock[1] + shift))
        shift += hsblock[1]
    annotposlist.append(annotpos)

# Print annotations
for abst, annotpos in izip(abstlist, annotposlist):
    print abst
    for pos in annotpos:
        print str(pos[0]) + '|' + str(pos[1]) + '|CELL|' + abst[pos[0]:pos[1]]
    print

