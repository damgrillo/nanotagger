import re
import sys
import string
from itertools import izip
from annotclass import TextAnnots, MatchAnnots


####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################           

# Check if line is associated to abstract content
def abst_cont(line):
    return len(re.findall('</?AbstractText[^>]*>', line)) == 2

# Check if lineis associated to annotation content
def annot_cont(line):
    return re.match('POLY|CHEM|DRUG|NANO|CELL|ABBV|</?Annotations>', line) != None

# Check annotation positions
def check_annot_pos(text, stpos, endpos, annot):
    return text[stpos:endpos] == annot

# Correct annotations positions
def correct_annot_pos(text, stpos, endpos, annot):
    iforw  = 0
    stpos = max(0, stpos - 5)
    while (iforw <= 10) and (text[stpos:stpos+len(annot)] != annot):
        stpos += 1
        iforw += 1
    return (stpos, stpos + len(annot))

# Split corpus in abstracts
def split_abstracts(corp):
    abst = []
    abstlist = []
    for line in [line.strip() for line in corp if abst_cont(line) or annot_cont(line)]:
        if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
            if len(abst) > 0:
                abstlist.append(abst)
                abst = []
        abst.append(line)
    if len(abst) > 0:
        abstlist.append(abst)
    return abstlist

# Import annotations from abstract
def import_annots(abst):
    ta = TextAnnots()
    read_annots = False
    for line in abst:
        if re.search('<AbstractText[^>]*>', line) != None:
            ta.set_text(line)
        else:
            terms = line.strip().split('|')
            if len(terms) > 2:
                if terms[0] == 'ABBV':
                    ta.add_abbv(terms[1], terms[2])
                else:
                    stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), terms[0], terms[3] 
                    if check_annot_pos(ta.get_text(), stpos, endpos, annot):
                        ta.add_annot(kind, stpos, endpos)
                    else:
                        stpos, endpos = correct_annot_pos(ta.get_text(), stpos, endpos, annot)
                        ta.add_annot(kind, stpos, endpos)
    return ta


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = annotated corpus 1
# sys.argv[2] = annotated corpus 2

# Initalize variables
kindlist = ['ABBV', 'POLY', 'CHEM', 'DRUG', 'NANO', 'CELL']

# Load annotations
corpfile1 = open(sys.argv[1], 'r')
corpfile2 = open(sys.argv[2], 'r')
annots1 = [import_annots(abst) for abst in split_abstracts(corpfile1)]
annots2 = [import_annots(abst) for abst in split_abstracts(corpfile2)]

# Get abstracts annotations
abstracts = []
for a1, a2 in izip(annots1, annots2):

    # Get annotations
    ann1  = a1.get_annots()
    abbv1 = a1.get_abbvs()
    ann2  = a2.get_annots()
    abbv2 = a2.get_abbvs()

    # Match annotations
    match_annots = MatchAnnots()
    match_annots.set_text(a1.get_text())
    match_annots.calc_params(ann1, abbv1, ann2, abbv2)

    # Add annotations and scores to abstracts
    abstracts.append((a1, a2, match_annots))


####################################################################################
############################# PERFORMANCE ANALYSIS #################################
####################################################################################

# Match annotations OVERALL
matchlist = [abst[2] for abst in abstracts]
tpos = len([an for mch in matchlist for an in mch.get_params()[0]])
fpos = len([an for mch in matchlist for an in mch.get_params()[1]])
fneg = len([an for mch in matchlist for an in mch.get_params()[2]])
part = len([an for mch in matchlist for an in mch.get_params()[3]]) 
if tpos > 0:
    recall = float(tpos) / (tpos + fneg + part)
    precision = float(tpos) / (tpos + fpos + part)
    f1score = 2 * recall * precision / (recall + precision)
else:
    recall = 0.0
    precision = 0.0
    f1score = 0.0

# Match annotations by KIND
tposk = {kind:len([an for mch in matchlist for an in mch.get_params(kind)[0]]) for kind in kindlist}
fposk = {kind:len([an for mch in matchlist for an in mch.get_params(kind)[1]]) for kind in kindlist}
fnegk = {kind:len([an for mch in matchlist for an in mch.get_params(kind)[2]]) for kind in kindlist}
partk = {kind:len([an for mch in matchlist for an in mch.get_params(kind)[3]]) for kind in kindlist}
recallk = {}
precisionk = {}
f1scorek = {}
for kind in kindlist:
    if tposk[kind] > 0:
        recallk[kind] = float(tposk[kind]) / (tposk[kind] + fnegk[kind] + partk[kind])
        precisionk[kind] = float(tposk[kind]) / (tposk[kind] + fposk[kind] + partk[kind])
        f1scorek[kind] = 2 * recallk[kind] * precisionk[kind] / (recallk[kind] + precisionk[kind])
    else:
        recallk[kind] = 0.0
        precisionk[kind] = 0.0
        f1scorek[kind] = 0.0

# Show final scores for annotations OVERALL
print '\n\n****************************************************************************'
print '*********************************** OVERALL ********************************'
print '****************************************************************************\n'
print '    BOTH ANNOTS =', tpos
print '    ONLY ANNOT1 =', fpos
print '    ONLY ANNOT2 =', fneg
print '    PARTIAL =', part
print '    F1-SCORE = {0:.3f}'.format(f1score)
print

# Show final scores for annotations by KIND
print '\n\n****************************************************************************'
print '********************************** BY KIND *********************************'
print '****************************************************************************\n'
for kind in kindlist:
    print 'RESULTS FOR {0}:'.format(kind)
    print '    BOTH ANNOTS =', tposk[kind]
    print '    ONLY ANNOT1 =', fposk[kind]
    print '    ONLY ANNOT2 =', fnegk[kind]
    print '    PARTIAL =', partk[kind]
    print '    F1-SCORE = {0:.3f}'.format(f1scorek[kind])
    print

# Show results for each abstract comparing annotations
absnum = 0
for a1, a2, match_annots in abstracts:

    # Show abstract text
    absnum += 1
    print '\n\n*********************** Abstract #{0} *******************\n'.format(absnum)
    print a1.get_text()
    print

    # Get matching parameters tpos, fpos, fneg and partial
    match_params = match_annots.get_params(kindlist)

    # Show tpos
    print 'BOTH ANNOTS:'
    if len(match_params[0]) > 0:
        for an in match_params[0]:
            if an[0] == 'ABBV':
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
            else: 
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
        print

    # Show fpos
    print 'ONLY ANNOT1:'
    if len(match_params[1]) > 0:
        for an in match_params[1]:
            if an[0] == 'ABBV':
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
            else: 
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
    print

    # Show fneg
    print 'ONLY ANNOT2:'
    if len(match_params[2]) > 0:
        for an in match_params[2]:
            if an[0] == 'ABBV':
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
            else: 
                print '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
    print

    # Show partial
    print 'PARTIAL:'
    if len(match_params[3]) > 0:
        for an in match_params[3]:
            if an[0] == 'ABBV':
                print  '    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + '|' + str(an[3]) + '|' + str(an[4])
            else:
                print  ('    ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2]) +
                                         '|' + str(an[3]) + '|' + str(an[4]) + '|' + match_annots.get_text(an[3], an[4]))
    print


