import re
import sys
import string
from itertools import izip
from annotclass import TextAnnots, MatchAnnots, MatchAbbvs

####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################        

# Check if line is associated to abstract content
def abst_cont(line):
    return len(re.findall('</?AbstractText[^>]*>', line)) == 2

# Check if line is associated to annotation content
def annot_cont(line):
    mpatt = 'POLY|CHEM|DRUG|NANO|CELL|ABBV|</?Annotations>'
    return (re.match(mpatt, line) != None) or (len(line.split('|')) >= 3)

# Check annotation positions
def check_annot_pos(text, stpos, endpos, annot):
    return text[stpos:endpos] == annot

# Correct annotations positions
def correct_annot_pos(text, stpos, endpos, annot):
    iforw  = 0
    stpos = max(0, stpos - 10)
    while (iforw <= 20) and (text[stpos:stpos+len(annot)] != annot):
        stpos += 1
        iforw += 1
    return (stpos, stpos + len(annot))

# Split corpus in abstracts
def split_abstracts(corp):
    abst = []
    abstlist = []
    for line in [line.strip() for line in corp if abst_cont(line) or annot_cont(line)]:
        if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
            if len(abst) > 0:
                abstlist.append(abst)
                abst = []
        abst.append(line)
    if len(abst) > 0:
        abstlist.append(abst)
    return abstlist

# Import manual annotations from abstract
def import_manual_annots(abst, method):
    ma = TextAnnots()
    for line in abst:
        if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
            ma.set_text(line)
        else:
            terms = line.strip().split('|')
            if len(terms) > 2:
                if terms[0] == 'ABBV':
                    ma.add_abbv(terms[1], terms[2])
                else:
                    stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), terms[0], terms[3]
                    if (kind in ['POLY', 'CHEM', 'DRUG']) and (method != 'NANO'):
                        kind = 'CHMT'
                    if check_annot_pos(ma.get_text(), stpos, endpos, annot):
                        ma.add_annot(kind, stpos, endpos)
                    else:
                        stpos, endpos = correct_annot_pos(ma.get_text(), stpos, endpos, annot)
                        ma.add_annot(kind, stpos, endpos)
    return ma

# Import auto annotations from abstract
def import_auto_annots(abst, method):
    aa = TextAnnots()
    for line in abst:
        if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
            aa.set_text(line)
        else:
            terms = line.strip().split('|')

            # Ab3P annotations
            if (method == 'AB3P') and (len(terms) > 1) and (len(terms[0]) > 1):
                aa.add_abbv(terms[0], terms[1])

            # GENIA annotations
            elif (method == 'GENIA') and (len(terms) == 4):
                stpos, endpos, kind, annot = int(terms[0]), int(terms[1]), terms[2], terms[3] 
                if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                    aa.add_annot(kind, stpos, endpos)
                else:
                    stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                    aa.add_annot(kind, stpos, endpos)

            # OSCAR annotations
            elif (method == 'OSCAR') and (len(terms) == 4) and (terms[0] == 'CM'):
                stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), 'CHMT', terms[3] 
                if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                    aa.add_annot(kind, stpos, endpos)
                else:
                    stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                    aa.add_annot(kind, stpos, endpos)

            # ChemicalTagger annotations
            elif (method == 'CHEMT') and (len(terms) == 4) and (terms[0] == 'MOLEC'):
                stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), 'CHMT', terms[3] 
                if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                    aa.add_annot(kind, stpos, endpos)
                else:
                    stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                    aa.add_annot(kind, stpos, endpos)

            # ChemSpot annotations
            elif (method == 'CSPOT') and (len(terms) >= 4):
                stpos, endpos, kind, annot = int(terms[0]), int(terms[1]), terms[2], terms[3]
                if kind == 'ABBREVIATION':
                    if len(terms) == 5:
                        aa.add_abbv(terms[3], terms[4])
                    else:
                        aa.add_abbv(terms[3], '')
                if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                    aa.add_annot('CHMT', stpos, endpos)
                else:
                    stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                    aa.add_annot('CHMT', stpos, endpos)

            # tmChem annotations
            elif (method == 'TMCHEM') and (len(terms) == 4) and (terms[0] == 'TMCHEM'):
                stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), 'CHMT', terms[3] 
                if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                    aa.add_annot(kind, stpos, endpos)
                else:
                    stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                    aa.add_annot(kind, stpos, endpos)


            # NanoTagger annotations
            elif ((method == 'NANO') or (method == 'ABNK')) and (len(terms) > 2):
                if terms[0] == 'ABBV': 
                    aa.add_abbv(terms[1], terms[2])
                elif terms[0] == 'ABNK':
                    if method == 'ABNK':
                        aa.add_abbv(terms[1], terms[2])
                else:
                    stpos, endpos, kind, annot = int(terms[1]), int(terms[2]), terms[0], terms[3] 
                    if check_annot_pos(aa.get_text(), stpos, endpos, annot):
                        aa.add_annot(kind, stpos, endpos)
                    else:
                        stpos, endpos = correct_annot_pos(aa.get_text(), stpos, endpos, annot)
                        aa.add_annot(kind, stpos, endpos)
    return aa
           

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = manual annotated corpus
# sys.argv[2] = auto annotated corpus
# sys.argv[3] = annotation method

# Load manual annotations
corpfile = open(sys.argv[1], 'r')
manual_annots = [import_manual_annots(abst, sys.argv[3]) for abst in split_abstracts(corpfile)]

# Load auto annotations
autofile = open(sys.argv[2], 'r')
auto_annots = [import_auto_annots(abst, sys.argv[3]) for abst in split_abstracts(autofile)]

# Load annotations including no kind abbreviations
abnkfile = open(sys.argv[2], 'r')
abnk_annots = [import_auto_annots(abst, sys.argv[3]) for abst in split_abstracts(abnkfile)]

# Match annotation for each abstract
matchlist = []
for ma, aa, nk in izip(manual_annots, auto_annots, abnk_annots):

    # Get manual and auto annotations
    manual_ann  = ma.get_annots()
    manual_abbv = ma.get_abbvs()
    auto_ann  = aa.get_annots()
    auto_abbv = aa.get_abbvs()
    auto_abnk = nk.get_abbvs()

    # Match abbreviations
    match_abbvs = MatchAbbvs()
    match_abbvs.calc_params(manual_abbv, auto_abbv)

    # Match abbreviations including no kind
    match_abnks = MatchAbbvs()
    match_abnks.calc_params(manual_abbv, auto_abnk)

    # Match annotations
    match_annots = MatchAnnots()
    match_annots.set_text(ma.get_text())
    match_annots.calc_params(manual_ann, auto_ann)

    # Save match abbreviations and annotations
    matchlist.append((match_abbvs, match_abnks, match_annots))

# Show matching results
absnum = 0
for match_abbvs, match_abnks, match_annots in matchlist:

    # Show abstract text
    absnum += 1
    print '\n\n*********************** Abstract #{0} *******************\n'.format(absnum)
    print match_annots.get_text()
    print

    # Get TPOS, FPOS, FNEG and PART abbreviations and annotations
    if sys.argv[3] == 'AB3P':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = match_abbvs.get_params()
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = ([], [], [], [], [])
    elif sys.argv[3] == 'GENIA':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = ([], [], [], [], [])
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params(['CELL'])
    elif sys.argv[3] == 'OSCAR':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = ([], [], [], [], [])
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params(['CHMT'])
    elif sys.argv[3] == 'CHEMT':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = ([], [], [], [], [])
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params(['CHMT'])
    elif sys.argv[3] == 'CSPOT':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = match_abbvs.get_params()
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params(['CHMT'])
    elif sys.argv[3] == 'TMCHEM':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = ([], [], [], [], [])
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params(['CHMT'])
    elif sys.argv[3] == 'ABNK':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = match_abnks.get_params()
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = ([], [], [], [], [])
    elif sys.argv[3] == 'NANO':
        tp_abbvs,  fp_abbvs,  fn_abbvs,  pt_abbvs,  pchk_abbvs  = match_abbvs.get_params()
        tp_annots, fp_annots, fn_annots, pt_annots, pchk_annots = match_annots.get_params()

    # Show TPOS
    print 'TPOS:'
    if len(tp_abbvs) > 0:
        for an in tp_abbvs:
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
    if len(tp_annots) > 0:
        for an in tp_annots: 
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
    print

    # Show FPOS
    print 'FPOS:'
    if len(fp_abbvs) > 0:
        for an in fp_abbvs:
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
    if len(fp_annots) > 0:
        for an in fp_annots: 
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
    print

    # Show FNEG
    print 'FNEG:'
    if len(fn_abbvs) > 0:
        for an in fn_abbvs:
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2])
    if len(fn_annots) > 0:
        for an in fn_annots: 
            print '     ' + an[0] + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
    print

    # Show PART
    print 'PART:'
    if len(pt_abbvs) > 0:
        for an in pt_abbvs:
                print '     ' + an[0]  + '|' + str(an[1]) + '|' + str(an[2])
                print '     ' + '    ' + '|' + str(an[3]) + '|' + str(an[4])
    if len(pt_annots) > 0:
        for an in pt_annots:
                print '     ' + an[0]  + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
                print '     ' + '    ' + '|' + str(an[3]) + '|' + str(an[4]) + '|' + match_annots.get_text(an[3], an[4])
    print

    # Show PARTIAL TO BE CHECKED
    print 'TO BE CHECKED:'
    if len(pchk_abbvs) > 0:
        for an in pchk_abbvs:
                print '     ' + an[0]  + '|' + str(an[1]) + '|' + str(an[2])
                print '     ' + '    ' + '|' + str(an[3]) + '|' + str(an[4])
    if len(pchk_annots) > 0:
        for an in pchk_annots:
                print '     ' + an[0]  + '|' + str(an[1]) + '|' + str(an[2]) + '|' + match_annots.get_text(an[1], an[2])
                print '     ' + '    ' + '|' + str(an[3]) + '|' + str(an[4]) + '|' + match_annots.get_text(an[3], an[4])
    print

