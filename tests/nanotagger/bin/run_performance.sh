# Loop for training set and gold standard
for N in 0 1
do
    # Define vars
    CHK='_with_chunk'
    NOCHK='_wout_chunk'
    DOCLIST=('train' 'gold_standard') 
    DOCTYPE=${DOCLIST[$N]}
    WORKDIR=$NANODEV/tests/nanotagger

    ab3p_mch=$WORKDIR/mch/ab3p_$DOCTYPE.mch
    genia_mch=$WORKDIR/mch/genia_$DOCTYPE.mch
    oscar_mch=$WORKDIR/mch/oscar_$DOCTYPE.mch
    chemt_mch=$WORKDIR/mch/chemt_$DOCTYPE.mch
    spot_mch=$WORKDIR/mch/spot_$DOCTYPE.mch
    tmchm_mch=$WORKDIR/mch/tmchm_$DOCTYPE.mch
    nano_chk_mch=$WORKDIR/mch/nano_$DOCTYPE$CHK.mch
    nano_nochk_mch=$WORKDIR/mch/nano_$DOCTYPE$NOCHK.mch
    nano_chk_abnk_mch=$WORKDIR/mch/nano_abnk_$DOCTYPE$CHK.mch
    nano_nochk_abnk_mch=$WORKDIR/mch/nano_abnk_$DOCTYPE$NOCHK.mch

    ab3p_perf=$WORKDIR/perf/ab3p_$DOCTYPE.perf
    genia_perf=$WORKDIR/perf/genia_$DOCTYPE.perf
    oscar_perf=$WORKDIR/perf/oscar_$DOCTYPE.perf
    chemt_perf=$WORKDIR/perf/chemt_$DOCTYPE.perf
    spot_perf=$WORKDIR/perf/spot_$DOCTYPE.perf
    tmchm_perf=$WORKDIR/perf/tmchm_$DOCTYPE.perf
    nano_chk_perf=$WORKDIR/perf/nano_$DOCTYPE$CHK.perf
    nano_nochk_perf=$WORKDIR/perf/nano_$DOCTYPE$NOCHK.perf
    nano_chk_abnk_perf=$WORKDIR/perf/nano_abnk_$DOCTYPE$CHK.perf
    nano_nochk_abnk_perf=$WORKDIR/perf/nano_abnk_$DOCTYPE$NOCHK.perf

    run_perf=$WORKDIR/bin/performance.py

    # Define path to extra python modules
    export PYTHONPATH=$WORKDIR/utils/modules:$WORKDIR/utils/chunkers

    # Comparison between manual and extracted annotations by each method
    echo
    echo "########## Performance for $DOCTYPE corpus ##########"
    echo 'Performance for AB3P method...'
    python $run_perf $ab3p_mch 'AB3P' > $ab3p_perf
    echo 'Performance for GENIA...'
    python $run_perf $genia_mch 'GENIA' > $genia_perf
    echo 'Performance for OSCAR...'
    python $run_perf $oscar_mch 'OSCAR' > $oscar_perf
    echo 'Performance for ChemicalTagger...'
    python $run_perf $chemt_mch 'CHEMT' > $chemt_perf
    echo 'Performance for CHEMSPOT...'
    python $run_perf $spot_mch  'CSPOT' > $spot_perf
    echo 'Performance for tmChem...'
    python $run_perf $tmchm_mch  'TMCHEM' > $tmchm_perf
    echo 'Performance for NanoTagger without chunking...'
    python $run_perf $nano_nochk_mch 'NANO' > $nano_nochk_perf
    echo 'Performance for NanoTagger without chunking for no kind abbvs...'
    python $run_perf $nano_nochk_abnk_mch 'ABNK' > $nano_nochk_abnk_perf
    echo 'Performance for NanoTagger with chunking...'
    python $run_perf $nano_chk_mch 'NANO' > $nano_chk_perf
    echo 'Performance for NanoTagger with chunking for no kind abbvs...'
    python $run_perf $nano_chk_abnk_mch 'ABNK' > $nano_chk_abnk_perf
done
echo

