# Define vars
WORKDIR=$NANODEV
DICFOLD=$WORKDIR/tests/nanotagger/dict
POLYDIR=$WORKDIR/chem-files/poly-terms
CHEMDIR=$WORKDIR/chem-files/chem-terms
ABBVDIR=$WORKDIR/abbv-files
DRUGDIR=$WORKDIR/drug-files
CELLDIR=$WORKDIR/cell-files

# Update dics
echo 'Updating POLY dict'
$POLYDIR/add_remove_terms.sh
echo 'Updating CHEM dict'
$CHEMDIR/add_remove_terms.sh
echo 'Updating DRUG dict'
$DRUGDIR/add_remove_terms.sh
echo 'Updating CELL dict'
$CELLDIR/add_remove_terms.sh

# Update dict folder
echo 'Copying POLY dict'
cp $POLYDIR/terms/chebi_terms_version_final.txt $DICFOLD/poly_chebi.txt
echo 'Copying CHEM dict'
cp $CHEMDIR/terms/chebi_terms_version_final.txt $DICFOLD/chem_chebi.txt
echo 'Copying DRUG dict'
cp $DRUGDIR/terms/drug_terms.txt $DICFOLD/drugs.txt
echo 'Copying CELL dict'
cp $CELLDIR/terms/cell_terms.txt $DICFOLD/cells.txt
echo 'Copying POLY_ABBV dict'
cp $ABBVDIR/terms/poly_abbvs.txt $DICFOLD/poly_abbvs.txt
echo 'Copying DRUG_ABBV dict'
cp $ABBVDIR/terms/drug_abbvs.txt $DICFOLD/drug_abbvs.txt
echo 'Copying DRUG_EXCEP dict'
cp $DRUGDIR/terms/except_terms.txt $DICFOLD/drug_except.txt
echo 'Copying SPECIAL_ABBV dict'
cp $ABBVDIR/terms/special_abbvs.txt $DICFOLD/special_abbvs.txt
