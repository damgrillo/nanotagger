import re
import sys
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = abstract corpus
# sys.argv[2] = output folder

# Load abstracts
corpfile = open(sys.argv[1], 'r')
abstlist = [line.strip() for line in corpfile]

# Write abstracts in PubTator format
n = 0
for line in abstlist:
    abstext = ''
    if re.match('<AbstractText[^>]*>', line.lstrip()) != None:
        n += 1
        outfile = open(sys.argv[2] + '/abstract' + str(n).zfill(4), 'w')
        abstext = line.lstrip()
        outfile.write( str(n).zfill(4) + '|t|' + 'Abstract' + str(n).zfill(4) + '\n' )
        outfile.write( str(n).zfill(4) + '|a|' + abstext + '\n' )
        outfile.write( '\n' )

