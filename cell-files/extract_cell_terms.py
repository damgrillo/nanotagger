import sys
import re
import string

################################################################################
################################## FUNCTIONS ###################################
################################################################################

# Replace punctuation symbols by whitespaces in text
def punct_to_wspace(text):
    new_text = text  
    for char in string.punctuation:
        if char != '-':
            new_text = new_text.replace(char, ' ')
    return new_text

# Load terms file. Extract labels and superclasses.
def load_class(fileterm, superclass, labelclass):

    # Load data in rows form csv file
    # row[0]  = Class_ID; row[1] = Label; row[7] = Parents_List

    # Join broken lines
    linelist = []
    prev_line = ''
    for line in fileterm:     
        if line.startswith('http://'):
            linelist.append(line[:-1])
        else:
            if len(linelist) > 0:
                linelist[-1] += line[:-1]

    # Load each field in each line and split the 7th element to generate the parents list
    rowlist = []
    patt = '"[^"]*",|[^,]*,'    
    for line in linelist:        
        row = [field[:-1] for field in re.findall(patt, line)]
        row[7] = row[7].split('|')
        rowlist.append(row)

    # Set superclass and labelclass dics from rowlist
    for row in rowlist:
        labelclass[row[0]] = row[1]
        superclass[row[0]] = row[7]


# Walk through superclass from node to root and build subclass branch
def build_branch(node_id, visited, superclass, subclass):

    # Subclass parameters: Key = Parent_Class_ID, Value = Child_Class_ID_List
    if node_id in superclass and not visited[node_id]:       
        visited[node_id] = True

        # Create parent node
        for parent_id in superclass[node_id]:
            if not parent_id in subclass:
                subclass[parent_id] = []
            if not node_id in subclass[parent_id]:
                subclass[parent_id].append(node_id)

        # Build the next upper level in the hierarchy
        for parent_id in superclass[node_id]:            
            build_branch(parent_id, visited, superclass, subclass)


# Find the node_id corresponding to node_label
def find_node_id(node_label, labelclass):
    node_id = ''
    for item_id, item_label in labelclass.items():
        if node_label == item_label:
            node_id = item_id
    return node_id


# Extract branch terms and copy in termset
def extract_branch_terms(node_id, visited, subclass, labelclass, termset):
    termset.add(labelclass[node_id])
    if node_id in subclass and not visited[node_id]:
        visited[node_id] = True
        for child_id in subclass[node_id]:
            extract_branch_terms(child_id, visited, subclass, labelclass, termset)     


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = Cell Ontology class terms file
# sys.argv[1] = Cell Line Ontology class terms file

# Load terms file and extract labels and superclasses from Cell Ontology
labelclass = {}
superclass = {}
fileterm = open(sys.argv[1], 'r')
load_class(fileterm, superclass, labelclass)

# Build subclass dic
visited = {}
subclass = {}
visited = {node_id:False for node_id in superclass}
for node_id in superclass:
    build_branch(node_id, visited, superclass, subclass)

# Extract labels from corresponding type category in Cell Ontology
clset = set([])
visited = {node_id:False for node_id in subclass}
node_id = find_node_id('native cell', labelclass)
extract_branch_terms(node_id, visited, subclass, labelclass, clset)
node_id = find_node_id('neural cell', labelclass)
extract_branch_terms(node_id, visited, subclass, labelclass, clset)
node_id = find_node_id('retinal cell', labelclass)
extract_branch_terms(node_id, visited, subclass, labelclass, clset)
node_id = find_node_id('muscle precursor cell', labelclass)
extract_branch_terms(node_id, visited, subclass, labelclass, clset)

# Load terms file and extract labels and superclasses from Cell Line Ontology
labelclass = {}
superclass = {}
fileterm = open(sys.argv[2], 'r')
load_class(fileterm, superclass, labelclass)

# Build subclass dic
visited = {}
subclass = {}
visited = {node_id:False for node_id in superclass}
for node_id in superclass:
    build_branch(node_id, visited, superclass, subclass)

# Extract labels from corresponding type category in Cell Line Ontology
closet = set([])
visited = {node_id:False for node_id in subclass}
node_id = find_node_id('cell type', labelclass)
extract_branch_terms(node_id, visited, subclass, labelclass, closet)

# Copy terms to termlist, replacing punct by wspaces and splitting
clspset  = set([word.lower() for term in clset for word in punct_to_wspace(term).split()])
clospset = set([word.lower() for term in closet for word in punct_to_wspace(term).split()])
termlist = sorted(clspset.union(clospset))

# Print terms
for term in termlist:
    num_digits  = len([ch for ch in term if ch in string.digits])
    num_letters = len([ch for ch in term if ch in string.letters])
    if (len(term) > 1) and (num_letters > 0) and (num_digits < 4):
        print term

