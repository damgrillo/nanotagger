# Define vars
WORKDIR=$NANODEV/cell-files
cl_file=$WORKDIR/CL.csv
clo_file=$WORKDIR/CLO.csv
onto_terms=$WORKDIR/terms/onto_terms.txt
exe_file=$WORKDIR/extract_cell_terms.py

# Run script to extract terms
time python $exe_file $cl_file $clo_file > $onto_terms

