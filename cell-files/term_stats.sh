# Define vars
WORKDIR=$NANODEV/cell-files
inp_file=$NANODEV/abstract-corpus/texts/pubmed_abstracts_all.txt
ont_file=$WORKDIR/terms/onto_terms.txt
out_file=$WORKDIR/terms/term_stats.txt
exe_file=$WORKDIR/term_stats.py

# Run script to analyze term statistics
time python $exe_file $ont_file $inp_file > $out_file


