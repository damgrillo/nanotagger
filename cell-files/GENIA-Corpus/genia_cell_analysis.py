import sys
import re
import string

# Replace punctuation by whitespaces
def punct2wspace(text):
    for ch in string.punctuation:
        text = text.replace(ch, ' ')
    return text
            

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = chunked corpus
# sys.argv[2] = stat file
# sys.argv[3] = filter file

# Load chunked corpus and extract cell_type chunks
cell = ''
cellist = []
chunk_file = open(sys.argv[1], 'r')
for line in chunk_file: 
    ne_tag = line.strip().split('\t')
    if (len(ne_tag) == 2) and (ne_tag[1].find('cell_type') > 0):
        cell += ne_tag[0] + ' '
    else:
        if len(cell) > 0:
            cellist.append(cell.strip())
            cell = ''

# Analysis of last terms
countdic = {}
for cell in cellist:
    cwords = punct2wspace(cell).split()
    lword = cwords[-1]
    if lword not in countdic:
        countdic[lword] = 1
    else:
        countdic[lword] += 1
        
# Write count freq
stat_file = open(sys.argv[2], 'w')
end_patts = [r'\bcells?\b', r'\w+phages?\b', r'\w+cytes?\b', 
             r'\w*blasts?\b', r'\w+phils?\b', r'\bplatelets?\b', 
             r'\bcell types?\b', r'\blineages?\b']
countlist = [(word, cnt) for word, cnt in countdic.items()]
countlist.sort(key=lambda pair: pair[1], reverse=True)
total_sum = sum([cnt for word, cnt in countlist])
acum_sum  = 0 
for word, cnt in countlist:
    if any(re.search(patt, word) for patt in end_patts):
        acum_sum += cnt
        acum_rat = '{0:.2f}%'.format(float(acum_sum) / total_sum * 100)
        stat_file.write(str(cnt).ljust(6) + ' ' + word.ljust(20) + acum_rat.rjust(6) + '\n')
    else:
        stat_file.write(str(cnt).ljust(6) + ' ' + word.ljust(20) + '\n')

# Write filtered terms
filter_file = open(sys.argv[3], 'w')
end_search = r'\bcells?\b|\w+phages?\b|\w+cytes?\b|\w*blasts?\b|\w+phils?\b'
for cell in cellist:
    cwords = punct2wspace(cell).split()
    if re.search(end_search, cwords[-1]) != None:
        filter_file.write(cwords[-1] + '\n')
    else:
        filter_file.write(cwords[-1].ljust(15) + ' ' + cell + '\n')

