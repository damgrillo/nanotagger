# Define vars
WORKDIR=$NANOTAGGER/cell-files/genia-corpus
genia_file=$WORKDIR/GeniaERtags.iob
genia_stat=$WORKDIR/genia_cell_stats.txt
genia_filt=$WORKDIR/genia_cell_filter.txt
exe_file=$WORKDIR/genia_cell_analysis.py

# Run script to extract cell terms from GENIA tags
time python $exe_file $genia_file $genia_stat $genia_filt
