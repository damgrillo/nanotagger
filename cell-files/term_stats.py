import sys
import re
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = cell terms file
# sys.argv[2] = input text file

# Load cell terms
cellfile = open(sys.argv[1], 'r')
cellset  = set([line.strip() for line in cellfile])

# Load text
textfile = open(sys.argv[2], 'r')
textlist = [line.strip()for line in textfile]

# Match and split cell terms
patt = '[^ ]+[ -]cell'
matchlist = [term for line in textlist for term in re.findall(patt, line)]
firstlist = [term.split()[0] for term in matchlist]

# Term statistics
countdic = {term:0 for term in matchlist}
for term in matchlist:
    countdic[term] += 1

# First term statistics
firstdic = {term:0 for term in firstlist}
for term in firstlist:
    firstdic[term] += 1

# Sort by frequency
sortmlist = sorted(countdic.items(), key=lambda tup: tup[1], reverse=True)
sortflist = sorted(firstdic.items(), key=lambda tup: tup[1], reverse=True)

# Print terms stats
print 'Term stats'
for term, freq in sortmlist:
    print str(freq).ljust(7), term
print

# Print first terms statistics not present in cell terms
print 'First term stats not present in cell list'
for term, freq in sortflist:
    if term.lower() not in cellset:
        print str(freq).ljust(7), term
print

# Print first terms statistics present in cell terms
print 'First term stats present in cell list'
for term, freq in sortflist:
    if term.lower() in cellset:
        print str(freq).ljust(7), term
print

