# Define vars
WORKDIR=$NANODEV/cell-files
add_file=$WORKDIR/terms/add_terms.txt
rmv_file=$WORKDIR/terms/remove_terms.txt
term_file=$WORKDIR/terms/onto_terms.txt
cell_file=$WORKDIR/terms/cell_terms.txt
exe_file=$WORKDIR/add_remove_terms.py

# Run script to extract add and remove terms
python $exe_file $add_file $rmv_file $term_file > $cell_file


