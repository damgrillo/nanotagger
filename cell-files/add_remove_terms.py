import sys
import re
import string

####################################################################################
################################### MAIN PROGRAM ###################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = add terms file
# sys.argv[2] = remove terms file
# sys.argv[3] = cell terms file

# Load add terms
addfile = open(sys.argv[1], 'r')
addset = set([line.strip() for line in addfile])

# Load remove terms
rmvfile = open(sys.argv[2], 'r')
rmvset = set([line.strip() for line in rmvfile])

# Load cell terms
cellfile = open(sys.argv[3], 'r')
cellset = set([line.strip() for line in cellfile])

# Final cell list       
cellset.update(addset)
fcellist = sorted([term for term in cellset if term not in rmvset])

# Print terms
for term in fcellist:
    print term 
            
