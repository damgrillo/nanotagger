Signaling	NN	B-NP
events	NNS	I-NP
induced	VBN	B-VP
by	IN	B-PP
lipopolysaccharide-activated	JJ	B-NP
toll-like	JJ	I-NP
receptor	NN	I-NP
2	CD	I-NP
.	.	B-NP

Human	JJ	B-NP
Toll-like	JJ	I-NP
receptor	NN	I-NP
2	CD	I-NP
(	(	I-NP
TLR2	NN	I-NP
)	)	I-NP
is	VBZ	B-VP
a	DT	B-NP
signaling	NN	I-NP
receptor	NN	I-NP
that	WDT	O
responds	VBZ	B-VP
to	TO	B-PP
LPS	NN	B-NP
and	CC	B-VP
activates	VBZ	B-VP
NF-kappaB	NN	B-NP
.	.	O

Here	RB	O
,	,	O
we	PRP	B-NP
investigate	VBP	B-VP
further	RBR	O
the	DT	B-NP
events	NNS	I-NP
triggered	VBN	B-VP
by	IN	B-PP
TLR2	NN	B-NP
in	IN	B-PP
response	NN	B-NP
to	TO	B-PP
LPS	NN	B-NP
.	.	O

We	PRP	B-NP
show	VBP	B-VP
that	IN	O
TLR2	NN	B-NP
associates	NNS	B-VP
with	IN	B-PP
the	DT	B-NP
high-affinity	JJ	I-NP
LPS	NN	I-NP
binding	NN	I-NP
protein	NN	I-NP
membrane	NN	I-NP
CD14	NN	I-NP
to	TO	B-VP
serve	VB	B-VP
as	IN	B-PP
an	DT	B-NP
LPS	NN	I-NP
receptor	NN	I-NP
complex	NN	I-NP
,	,	O
and	CC	O
that	IN	O
LPS	NN	B-NP
treatment	NN	I-NP
enhances	VBZ	B-VP
the	DT	B-NP
oligomerization	NN	I-NP
of	IN	B-PP
TLR2	NN	B-NP
.	.	O

Concomitant	JJ	B-PP
with	IN	B-PP
receptor	NN	B-NP
oligomerization	NN	I-NP
,	,	O
the	DT	B-NP
IL-1R-associated	JJ	I-NP
kinase	NN	I-NP
(	(	I-NP
IRAK	NN	I-NP
)	)	I-NP
is	VBZ	B-VP
recruited	VBN	B-VP
to	TO	B-PP
the	DT	B-NP
TLR2	NN	I-NP
complex	NN	I-NP
.	.	O

Intracellular	JJ	B-NP
deletion	NN	I-NP
variants	NNS	I-NP
of	IN	B-PP
TLR2	NN	B-NP
lacking	VBG	B-VP
C-terminal	JJ	B-NP
13	CD	I-NP
or	CC	B-NP
141	CD	B-NP
aa	NN	I-NP
fail	VBP	B-VP
to	TO	B-VP
recruit	VB	B-VP
IRAK	NN	B-NP
,	,	B-NP
which	WDT	O
is	VBZ	B-VP
consistent	JJ	O
with	IN	B-PP
the	DT	B-NP
inability	NN	I-NP
of	IN	B-PP
these	DT	B-NP
mutants	NNS	I-NP
to	TO	B-VP
transmit	VB	B-VP
LPS	NN	B-NP
cellular	JJ	I-NP
signaling	NN	I-NP
.	.	O

Moreover	RB	O
,	,	O
both	DT	B-NP
deletion	NN	I-NP
mutants	NNS	I-NP
could	MD	B-VP
still	RB	O
form	VB	B-VP
complexes	NNS	B-NP
with	IN	B-PP
wild-type	JJ	B-NP
TLR2	NN	I-NP
and	CC	B-VP
act	VB	B-VP
in	IN	B-PP
a	DT	B-NP
dominant-negative	JJ	I-NP
(	(	I-NP
DN	JJ	I-NP
)	)	I-NP
fashion	NN	I-NP
to	TO	B-VP
block	VB	B-VP
TLR2-mediated	JJ	B-NP
signal	NN	I-NP
transduction	NN	I-NP
.	.	O

DN	JJ	B-NP
constructs	NNS	I-NP
of	IN	B-PP
myeloid	JJ	B-NP
differentiation	NN	I-NP
protein	NN	I-NP
,	,	B-NP
IRAK	NN	B-NP
,	,	B-NP
TNF	NN	B-NP
receptor-associated	JJ	I-NP
factor	NN	I-NP
6	CD	I-NP
,	,	B-NP
and	CC	I-NP
NF-kappaB-inducing	JJ	B-NP
kinase	NN	I-NP
,	,	B-NP
when	WRB	O
coexpressed	VBN	B-VP
with	IN	B-PP
TLR2	NN	B-NP
,	,	O
abrogate	VBP	B-VP
TLR2-mediated	JJ	B-NP
NF-kappaB	NN	I-NP
activation	NN	I-NP
.	.	O

These	DT	B-NP
results	NNS	I-NP
reveal	VBP	B-VP
a	DT	B-NP
conserved	VBN	I-NP
signaling	NN	I-NP
pathway	NN	I-NP
for	IN	B-PP
TLR2	NN	B-NP
and	CC	B-NP
IL-1Rs	NNS	B-NP
and	CC	B-VP
suggest	VBP	B-VP
a	DT	B-NP
molecular	JJ	I-NP
mechanism	NN	I-NP
for	IN	B-PP
the	DT	B-NP
inhibition	NN	I-NP
of	IN	B-PP
TLR2	NN	B-NP
by	IN	B-PP
DN	JJ	B-NP
variants	NNS	I-NP
.	.	O

