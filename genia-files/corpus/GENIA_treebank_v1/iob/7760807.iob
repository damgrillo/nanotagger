MIP1	NN	B-NP
alpha	NN	I-NP
nuclear	JJ	I-NP
protein	NN	I-NP
(	(	O
MNP	NN	B-NP
)	)	O
,	,	B-NP
a	DT	B-NP
novel	JJ	I-NP
transcription	NN	B-NP
factor	NN	I-NP
expressed	VBN	B-VP
in	IN	B-PP
hematopoietic	JJ	B-NP
cells	NNS	I-NP
that	WDT	O
is	VBZ	B-VP
crucial	JJ	O
for	IN	B-PP
transcription	NN	B-NP
of	IN	B-PP
the	DT	B-NP
human	JJ	B-NP
MIP-1	NN	I-NP
alpha	NN	I-NP
gene	NN	I-NP
.	.	B-NP

Murine	JJ	B-NP
macrophage	NN	I-NP
inflammatory	JJ	I-NP
protein	NN	I-NP
1	CD	I-NP
alpha	NN	I-NP
(	(	O
MIP-1	NN	B-NP
alpha	NN	I-NP
)	)	O
and	CC	B-NP
its	PRPP	B-NP
human	JJ	B-NP
equivalent	NN	I-NP
(	(	O
GOS19	NN	B-NP
,	,	B-NP
LD78	NN	B-NP
,	,	B-NP
or	CC	I-NP
AT464	NN	B-NP
)	)	O
are	VBP	B-VP
members	NNS	B-NP
of	IN	B-PP
the	DT	B-NP
-C-C	NN	B-NP
family	NN	I-NP
of	IN	B-PP
low-molecular-weight	JJ	B-NP
chemokines	NNS	I-NP
.	.	O

Secreted	VBN	B-VP
from	IN	B-PP
activated	VBN	B-NP
T	NN	I-NP
cells	NNS	I-NP
and	CC	B-NP
macrophages	NNS	B-NP
,	,	O
bone	NN	B-NP
marrow-derived	JJ	B-NP
MIP-1	NN	I-NP
alpha/GOS19	NN	I-NP
inhibits	VBZ	B-VP
primitive	JJ	B-NP
hematopoietic	JJ	I-NP
stem	NN	I-NP
cells	NNS	I-NP
and	CC	B-VP
appears	VBZ	B-VP
to	TO	B-VP
be	VB	B-VP
involved	VBN	B-VP
in	IN	B-PP
the	DT	B-NP
homeostatic	JJ	I-NP
control	NN	I-NP
of	IN	B-PP
stem	NN	B-NP
cell	NN	I-NP
proliferation	NN	B-NP
.	.	O

It	PRP	B-NP
also	RB	O
induces	VBZ	B-VP
chemotaxis	NN	B-NP
and	CC	B-NP
inflammatory	JJ	B-NP
responses	NNS	I-NP
in	IN	B-PP
mature	JJ	B-NP
cell	NN	I-NP
types	NNS	I-NP
.	.	O

Therefore	RB	O
,	,	O
it	PRP	B-NP
is	VBZ	B-VP
important	JJ	O
to	TO	B-VP
understand	VB	B-VP
the	DT	B-NP
mechanisms	NNS	I-NP
which	WDT	O
control	VBP	B-VP
the	DT	B-NP
expression	NN	I-NP
of	IN	B-PP
MIP-1	NN	B-NP
alpha/GOS19	NN	I-NP
.	.	O

Previous	JJ	B-NP
work	NN	I-NP
has	VBZ	B-VP
shown	VBN	B-VP
that	IN	O
in	IN	B-PP
Jurkat	NN	B-NP
T	NN	I-NP
cells	NNS	I-NP
,	,	O
a	DT	B-NP
set	NN	I-NP
of	IN	B-PP
widely	RB	O
expressed	VBN	O
transcription	NN	B-NP
factors	NNS	I-NP
(	(	O
the	DT	B-NP
ICK-1	NN	B-NP
family	NN	I-NP
)	)	O
affect	VBP	B-VP
the	DT	B-NP
GOS19	NN	B-NP
promoter	NN	I-NP
.	.	O

One	CD	B-NP
member	NN	I-NP
,	,	B-NP
ICK-1A	NN	B-NP
,	,	B-NP
behaves	VBZ	B-VP
as	IN	B-PP
a	DT	B-NP
strong	JJ	B-NP
negative	JJ	I-NP
regulator	NN	I-NP
.	.	O

In	IN	B-PP
this	DT	B-NP
communication	NN	I-NP
,	,	O
we	PRP	B-NP
provide	VBP	B-VP
evidence	NN	B-NP
that	IN	O
the	DT	B-NP
pathway	NN	I-NP
of	IN	B-PP
induction	NN	B-NP
in	IN	B-PP
the	DT	B-NP
macrophage	NN	B-NP
cell	NN	I-NP
line	NN	I-NP
U937	NN	B-NP
is	VBZ	B-VP
different	JJ	O
from	IN	B-PP
that	DT	B-NP
in	IN	B-PP
Jurkat	NN	B-NP
cells	NNS	I-NP
.	.	O

Furthermore	RB	O
,	,	O
we	PRP	B-NP
show	VBP	B-VP
that	IN	O
the	DT	B-NP
ICK-1	NN	B-NP
binding	NN	I-NP
site	NN	I-NP
does	VBZ	B-VP
not	RB	I-VP
confer	VB	B-VP
negative	JJ	B-NP
regulation	NN	I-NP
in	IN	B-PP
U937	NN	B-NP
cells	NNS	I-NP
.	.	O

We	PRP	B-NP
provide	VBP	B-VP
evidence	NN	B-NP
for	IN	B-PP
an	DT	B-NP
additional	JJ	I-NP
binding	VBG	B-NP
site	NN	I-NP
,	,	B-NP
the	DT	B-NP
MIP-1	NN	B-NP
alpha	NN	I-NP
nuclear	JJ	I-NP
protein	NN	I-NP
(	(	I-NP
MNP	NN	I-NP
)	)	I-NP
site	NN	I-NP
,	,	B-NP
which	WDT	O
overlaps	VBZ	B-VP
the	DT	B-NP
ICK-1	NN	B-NP
site	NN	I-NP
.	.	O

Interaction	NN	B-NP
of	IN	B-PP
nuclear	JJ	B-NP
extracts	NNS	I-NP
from	IN	B-PP
various	JJ	B-NP
cell	NN	I-NP
lines	NNS	I-NP
and	CC	B-NP
tissue	NN	B-NP
with	IN	B-PP
the	DT	B-NP
MNP	NN	B-NP
site	NN	I-NP
leads	VBZ	B-VP
to	TO	B-PP
the	DT	B-NP
formation	NN	I-NP
of	IN	B-PP
fast-migrating	JJ	B-NP
protein-DNA	JJ	I-NP
complexes	NNS	I-NP
with	IN	B-PP
similar	JJ	O
but	CC	O
distinct	JJ	O
electrophoretic	JJ	B-NP
mobilities	NNS	I-NP
.	.	O

A	DT	B-NP
mutation	NN	I-NP
of	IN	B-PP
the	DT	B-NP
MNP	NN	B-NP
site	NN	I-NP
which	WDT	O
does	VBZ	B-VP
not	RB	I-VP
abrogate	VB	B-VP
ICK-1	NN	B-NP
binding	NN	I-NP
inactivates	VBZ	B-VP
the	DT	B-NP
GOS19.1	NN	B-NP
promoter	NN	I-NP
in	IN	B-PP
U937	NN	B-NP
cells	NNS	I-NP
and	CC	B-VP
reduces	VBZ	B-VP
its	PRPP	B-NP
activity	NN	I-NP
by	IN	B-PP
fourfold	RB	B-NP
in	IN	B-PP
Jurkat	NN	B-NP
cells	NNS	I-NP
.	.	O

We	PRP	B-NP
propose	VBP	B-VP
that	IN	O
the	DT	B-NP
MNP	NN	B-NP
protein	NN	I-NP
(	(	B-NP
s	NNS	I-NP
)	)	I-NP
binding	VBG	B-VP
at	IN	B-PP
the	DT	B-NP
MNP	NN	B-NP
site	NN	I-NP
constitutes	VBZ	B-VP
a	DT	B-NP
novel	JJ	I-NP
transcription	NN	B-NP
factor	NN	I-NP
(	(	B-NP
s	NNS	I-NP
)	)	I-NP
expressed	VBN	B-VP
in	IN	B-PP
hematopoietic	JJ	B-NP
cells	NNS	I-NP
.	.	O

