Suppressive	JJ	B-NP
effects	NNS	I-NP
of	IN	B-PP
anti-inflammatory	JJ	B-NP
agents	NNS	I-NP
on	IN	B-PP
human	JJ	B-NP
endothelial	JJ	I-NP
cell	NN	I-NP
activation	NN	I-NP
and	CC	B-NP
induction	NN	B-NP
of	IN	B-PP
heat	NN	B-NP
shock	NN	I-NP
proteins	NNS	I-NP
.	.	B-NP

BACKGROUND	NN	B-NP
:	:	B-NP
Studies	NNS	B-NP
from	IN	B-PP
our	PRPP	B-NP
laboratory	NN	I-NP
have	VBP	B-VP
shown	VBN	B-VP
that	IN	O
the	DT	B-NP
earliest	JJS	I-NP
stages	NNS	I-NP
of	IN	B-PP
atherosclerosis	NN	B-NP
may	MD	B-VP
be	VB	B-VP
mediated	VBN	B-VP
by	IN	B-PP
an	DT	B-NP
autoimmune	JJ	I-NP
reaction	NN	I-NP
against	IN	B-PP
heat	NN	B-NP
shock	NN	I-NP
protein	NN	I-NP
60	CD	I-NP
(	(	O
Hsp60	NN	B-NP
)	)	O
.	.	B-NP

The	DT	B-NP
interactions	NNS	I-NP
of	IN	B-PP
Hsp60-specific	JJ	B-NP
T	NN	I-NP
cells	NNS	I-NP
with	IN	B-PP
arterial	JJ	B-NP
endothelial	JJ	B-NP
cells	NNS	I-NP
(	(	O
EC	NN	B-NP
)	)	O
require	VBP	B-VP
expression	NN	B-NP
of	IN	B-PP
both	CC	B-NP
Hsp60	NN	B-NP
and	CC	B-NP
certain	JJ	B-NP
adhesion	NN	I-NP
molecules	NNS	I-NP
shown	VBN	B-VP
to	TO	B-VP
be	VB	B-VP
induced	VBN	B-VP
simultaneously	RB	O
in	IN	B-PP
EC	NN	B-NP
by	IN	B-PP
mechanical	JJ	O
and	CC	O
other	JJ	O
types	NNS	B-NP
of	IN	B-PP
stress	NN	B-NP
.	.	O

Recently	RB	O
,	,	O
it	PRP	B-NP
was	VBD	B-VP
shown	VBN	B-VP
that	IN	O
suppression	NN	B-NP
of	IN	B-PP
T	NN	O
cell-mediated	JJ	O
immune	JJ	B-NP
responses	NNS	I-NP
by	IN	B-PP
cyclosporin	NN	B-NP
A	NN	I-NP
(	(	O
CyA	NN	B-NP
)	)	O
enhanced	VBD	B-VP
atherosclerotic	JJ	B-NP
lesion	NN	I-NP
formation	NN	I-NP
in	IN	B-PP
mice	NNS	B-NP
.	.	O

In	IN	B-PP
contrast	NN	B-NP
,	,	O
aspirin	NN	B-NP
was	VBD	B-VP
found	VBN	B-VP
to	TO	B-VP
lower	VB	B-VP
the	DT	B-NP
risk	NN	I-NP
of	IN	B-PP
myocardial	JJ	B-NP
infarction	NN	I-NP
in	IN	B-PP
men	NNS	B-NP
.	.	O

These	DT	B-NP
conflicting	VBG	I-NP
observations	NNS	I-NP
may	MD	B-VP
be	VB	B-VP
due	JJ	B-PP
to	TO	I-PP
different	JJ	B-NP
effects	NNS	I-NP
of	IN	B-PP
anti-inflammatory	JJ	B-NP
agents	NNS	I-NP
on	IN	B-PP
adhesion	NN	B-NP
molecule	NN	I-NP
and	CC	I-NP
Hsp	NN	I-NP
expression	NN	I-NP
in	IN	B-PP
EC	NN	B-NP
,	,	B-NP
respectively	RB	O
.	.	O

MATERIAL	NN	B-NP
AND	CC	B-NP
METHODS	NNS	B-NP
:	:	B-NP
In	IN	B-PP
the	DT	B-NP
present	JJ	I-NP
study	NN	I-NP
,	,	O
we	PRP	B-NP
analyzed	VBD	B-VP
the	DT	B-NP
effects	NNS	I-NP
of	IN	B-PP
CyA	NN	B-NP
,	,	B-NP
aspirin	NN	B-NP
,	,	B-NP
and	CC	I-NP
indomethacin	NN	B-NP
on	IN	B-PP
T	NN	B-NP
cell	NN	I-NP
proliferation	NN	I-NP
using	VBG	B-VP
a	DT	B-NP
proliferation	NN	I-NP
assay	NN	I-NP
.	.	B-NP

To	TO	B-VP
explore	VB	B-VP
the	DT	B-NP
expression	NN	I-NP
of	IN	B-PP
adhesion	NN	B-NP
molecules	NNS	I-NP
,	,	B-NP
monocyte	NN	B-NP
chemoattractant	NN	I-NP
protein-1	NN	I-NP
(	(	O
MCP-1	NN	B-NP
)	)	O
,	,	B-NP
and	CC	I-NP
Hsp60	NN	B-NP
in	IN	B-PP
human	JJ	B-NP
umbilical	JJ	I-NP
vein	NN	I-NP
endothelial	JJ	I-NP
cells	NNS	I-NP
(	(	O
HUVECs	NNS	B-NP
)	)	O
,	,	O
Northern	NN	B-NP
blot	NN	I-NP
analyses	NNS	I-NP
were	VBD	B-VP
used	VBN	B-VP
.	.	O

To	TO	B-VP
examine	VB	B-VP
the	DT	B-NP
activation	NN	I-NP
status	NN	I-NP
of	IN	B-PP
the	DT	B-NP
transcription	NN	I-NP
factors	NNS	I-NP
nuclear	JJ	B-NP
factor	NN	I-NP
kappaB	NN	I-NP
(	(	O
NF-kappaB	NN	B-NP
)	)	O
and	CC	B-NP
heat	NN	B-NP
shock	NN	I-NP
factor-1	NN	I-NP
(	(	O
HSF-1	NN	O
)	)	O
,	,	O
electrophoretic	JJ	B-NP
mobility	NN	I-NP
shift	NN	I-NP
assays	NNS	I-NP
were	VBD	B-VP
performed	VBN	B-VP
.	.	O

RESULTS	NNS	B-NP
:	:	B-NP
With	IN	B-PP
the	DT	B-NP
exception	NN	I-NP
of	IN	B-PP
indomethacin	NN	B-NP
,	,	O
the	DT	B-NP
used	VBN	I-NP
immunosuppressive	JJ	O
and	CC	O
anti-inflammatory	JJ	O
agents	NNS	B-NP
significantly	RB	O
inhibited	VBD	B-VP
T	NN	B-NP
cell	NN	I-NP
proliferation	NN	I-NP
in	IN	B-PP
response	NN	I-PP
to	TO	I-PP
influenza	NN	B-NP
virus	NN	I-NP
antigen	NN	I-NP
in	IN	B-PP
a	DT	B-NP
dose-dependent	JJ	I-NP
manner	NN	I-NP
.	.	B-NP

Interestingly	RB	O
,	,	O
CyA	NN	B-NP
and	CC	B-NP
indomethacin	NN	B-NP
did	VBD	B-VP
not	RB	I-VP
suppress	VB	B-VP
tumor	NN	O
necrosis	NN	O
factor-alpha	NN	O
(	(	O
TNF-alpha	NN	O
)	)	O
-induced	JJ	O
adhesion	NN	B-NP
molecule	NN	I-NP
expression	NN	I-NP
on	IN	B-PP
HUVECs	NNS	B-NP
,	,	O
whereas	IN	O
aspirin	NN	B-NP
had	VBD	B-VP
an	DT	B-NP
inhibitory	JJ	I-NP
effect	NN	I-NP
.	.	O

These	DT	B-NP
observations	NNS	I-NP
correlated	VBD	B-VP
with	IN	B-PP
the	DT	B-NP
modulation	NN	I-NP
of	IN	B-PP
NF-kappaB	NN	B-NP
activity	NN	I-NP
in	IN	B-PP
EC	NN	B-NP
.	.	O

All	DT	B-NP
agents	NNS	I-NP
tested	VBN	B-VP
induced	VBD	B-VP
expression	NN	B-NP
of	IN	B-PP
Hsp60	NN	B-NP
6	CD	B-NP
hr	NN	I-NP
after	IN	B-PP
application	NN	B-NP
.	.	O

In	IN	B-PP
addition	NN	B-NP
,	,	O
aspirin	NN	B-NP
and	CC	B-NP
indomethacin	NN	B-NP
,	,	B-NP
but	CC	O
not	RB	O
CyA	NN	B-NP
,	,	B-NP
induced	VBD	B-VP
Hsp70	NN	B-NP
expression	NN	I-NP
in	IN	B-PP
HUVECs	NNS	B-NP
that	WDT	O
correlated	VBD	B-VP
with	IN	B-PP
induction	NN	B-NP
of	IN	B-PP
HSF-1	NN	B-NP
activity	NN	I-NP
.	.	O

CONCLUSION	NN	B-NP
:	:	B-NP
Our	PRPP	B-NP
results	NNS	I-NP
show	VBP	B-VP
that	IN	O
the	DT	B-NP
tested	VBN	I-NP
agents	NNS	I-NP
(	(	B-NP
except	IN	B-PP
indomethacin	NN	B-NP
)	)	B-NP
are	VBP	B-VP
inhibitors	NNS	B-NP
of	IN	B-PP
the	DT	B-NP
T	NN	O
cell-mediated	JJ	O
immune	JJ	B-NP
response	NN	I-NP
,	,	B-VP
as	IN	O
expected	VBN	B-VP
,	,	O
that	IN	O
aspirin	NN	B-NP
is	VBZ	B-VP
an	DT	B-NP
effective	JJ	I-NP
suppressor	NN	I-NP
of	IN	B-PP
adhesion	NN	B-NP
molecule	NN	I-NP
expression	NN	I-NP
,	,	O
and	CC	O
that	IN	O
all	DT	B-NP
three	CD	I-NP
agents	NNS	I-NP
can	MD	B-VP
induce	VB	B-VP
Hsp60	NN	B-NP
in	IN	B-PP
HUVECs	NNS	B-NP
.	.	B-NP

These	DT	B-NP
data	NNS	I-NP
provide	VBP	B-VP
the	DT	B-NP
molecular	JJ	I-NP
basis	NN	I-NP
for	IN	B-PP
the	DT	B-NP
notion	NN	I-NP
that	IN	O
(	(	O
1	LS	O
)	)	O
part	NN	B-NP
of	IN	B-PP
the	DT	B-NP
anti-atherogenic	JJ	I-NP
effect	NN	I-NP
of	IN	B-PP
aspirin	NN	B-NP
may	MD	B-VP
be	VB	B-VP
due	JJ	B-PP
to	TO	I-PP
the	DT	B-NP
prevention	NN	I-NP
of	IN	B-PP
the	DT	B-NP
adhesion	NN	I-NP
of	IN	B-PP
sensitized	VBN	B-NP
T	NN	I-NP
cells	NNS	I-NP
to	TO	B-PP
stressed	JJ	B-NP
EC	NN	I-NP
;	:	O
(	(	O
2	LS	O
)	)	O
that	IN	O
part	NN	B-NP
of	IN	B-PP
the	DT	B-NP
atherosclerosis-promoting	JJ	I-NP
effect	NN	I-NP
of	IN	B-PP
CyA	NN	B-NP
may	MD	B-VP
be	VB	B-VP
due	JJ	B-PP
to	TO	I-PP
its	PRPP	B-NP
potential	NN	I-NP
as	IN	B-PP
an	DT	B-NP
inducer	NN	I-NP
of	IN	B-PP
Hsp60	NN	B-NP
expression	NN	I-NP
and	CC	B-NP
its	PRPP	B-NP
inability	NN	I-NP
to	TO	B-VP
down-regulate	VB	B-VP
adhesion	NN	B-NP
molecule	NN	I-NP
expression	NN	I-NP
on	IN	B-PP
EC	NN	B-NP
;	:	O
and	CC	O
(	(	O
3	LS	O
)	)	O
that	IN	O
down-regulation	NN	B-NP
of	IN	B-PP
MCP-1	NN	B-NP
expression	NN	I-NP
by	IN	B-PP
aspirin	NN	B-NP
may	MD	B-VP
result	VB	B-VP
in	IN	B-PP
decreased	VBN	B-NP
recruitment	NN	I-NP
of	IN	B-PP
monocytes	NNS	B-NP
into	IN	B-PP
the	DT	B-NP
arterial	JJ	I-NP
intima	NN	I-NP
beneath	IN	B-PP
stressed	JJ	B-NP
EC	NN	I-NP
.	.	O

