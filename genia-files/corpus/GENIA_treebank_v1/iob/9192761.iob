Thrombin	NN	B-NP
generation	NN	B-NP
by	IN	B-PP
apoptotic	JJ	B-NP
vascular	JJ	I-NP
smooth	NN	I-NP
muscle	NN	I-NP
cells	NNS	I-NP
.	.	B-NP

Thrombin	NN	B-NP
activation	NN	B-NP
requires	VBZ	B-VP
assembly	NN	B-NP
of	IN	B-PP
a	DT	B-NP
prothrombinase	NN	B-NP
complex	NN	I-NP
of	IN	B-PP
activated	VBN	B-NP
coagulation	NN	B-NP
factors	NNS	I-NP
on	IN	B-PP
an	DT	B-NP
anionic	JJ	I-NP
phospholipid	NN	I-NP
surface	NN	I-NP
,	,	B-NP
classically	RB	O
provided	VBN	B-VP
by	IN	B-PP
activated	VBN	B-NP
platelets	NNS	I-NP
.	.	O

We	PRP	B-NP
have	VBP	B-VP
previously	RB	O
shown	VBN	B-VP
that	IN	O
anionic	JJ	B-NP
phosphatidylserine	NN	I-NP
is	VBZ	B-VP
exposed	VBN	B-VP
by	IN	B-PP
rat	NN	B-NP
vascular	JJ	I-NP
smooth	NN	I-NP
muscle	NN	I-NP
cells	NNS	I-NP
(	(	O
VSMCs	NNS	B-NP
)	)	O
undergoing	VBG	B-VP
apoptosis	NN	B-NP
after	IN	B-PP
serum	NN	B-NP
withdrawal	NN	I-NP
.	.	O

In	IN	B-PP
this	DT	B-NP
study	NN	I-NP
,	,	O
using	VBG	B-VP
a	DT	B-NP
chromogenic	JJ	I-NP
assay	NN	I-NP
,	,	O
we	PRP	B-NP
have	VBP	B-VP
shown	VBN	B-VP
thrombin	NN	B-NP
generation	NN	B-NP
by	IN	B-PP
apoptotic	JJ	B-NP
VSMCs	NNS	I-NP
expressing	VBG	B-VP
c-myc	NN	B-NP
(	(	O
VSMC-myc	NN	B-NP
)	)	O
with	IN	B-PP
an	DT	B-NP
area	NN	I-NP
under	IN	B-PP
the	DT	B-NP
thrombin-generation	NN	I-NP
curve	NN	I-NP
(	(	O
AUC	NN	B-NP
)	)	O
of	IN	B-PP
305	CD	O
+/-	CC	O
17	CD	O
nmol	NN	B-NP
x	CC	I-NP
min/L	NN	I-NP
and	CC	B-NP
a	DT	B-NP
peak	NN	I-NP
thrombin	NN	B-NP
(	(	O
PT	NN	B-NP
)	)	O
of	IN	B-PP
154	CD	O
+/-	CC	O
9	CD	O
nmol/L	NN	B-NP
.	.	O

The	DT	B-NP
thrombin-generating	JJ	I-NP
potential	NN	I-NP
of	IN	B-PP
the	DT	B-NP
apoptotic	JJ	B-NP
VSMC-myc	NN	I-NP
cells	NNS	I-NP
was	VBD	B-VP
greater	JJR	O
than	IN	B-PP
that	DT	B-NP
of	IN	B-PP
unactivated	JJ	B-NP
platelets	NNS	I-NP
(	(	O
P	NN	B-NP
=	JJ	O
.003	CD	B-NP
for	IN	B-PP
AUC	NN	B-NP
;	:	B-NP
P	NN	B-NP
=	JJ	O
.0002	NN	B-NP
for	IN	B-PP
PT	NN	B-NP
)	)	O
and	CC	O
similar	JJ	O
to	TO	B-PP
calcium-ionophore	JJ	B-NP
activated	VBN	B-NP
platelets	NNS	I-NP
(	(	O
AUC	NN	B-NP
of	IN	B-PP
332	CD	O
+/-	CC	O
15	CD	O
nmol	NN	B-NP
x	NN	I-NP
min/L	NN	I-NP
,	,	B-NP
P	NN	B-NP
=	JJ	O
.3	NN	B-NP
;	:	B-NP
PT	NN	B-NP
of	IN	B-PP
172	CD	O
+/-	CC	O
8	CD	O
nmol/L	NN	B-NP
,	,	B-NP
P	NN	B-NP
=	JJ	O
.2	NN	B-NP
)	)	O
.	.	O

Thrombin	NN	B-NP
activation	NN	B-NP
was	VBD	B-VP
also	RB	O
seen	VBN	B-VP
with	IN	B-PP
apoptotic	JJ	B-NP
human	JJ	I-NP
VSMCs	NNS	I-NP
(	(	O
AUC	NN	B-NP
of	IN	B-PP
211	CD	O
+/-	CC	O
8	CD	O
nmol	NN	B-NP
x	NN	I-NP
min/L	NN	I-NP
;	:	B-NP
PT	NN	B-NP
of	IN	B-PP
103	CD	O
+/-	CC	O
4	CD	O
nmol/L	NN	B-NP
)	)	O
and	CC	B-VP
was	VBD	B-VP
inhibited	VBN	B-VP
by	IN	B-PP
annexin	NN	B-NP
V	NN	I-NP
(	(	O
P	NN	B-NP
<	JJR	O
.0001	CD	B-NP
for	IN	B-PP
AUC	NN	B-NP
and	CC	B-NP
PT	NN	B-NP
)	)	O
.	.	O

VSMC-myc	JJ	B-NP
cells	NNS	I-NP
maintained	VBN	B-VP
in	IN	B-PP
serum	NN	B-NP
generated	VBD	B-VP
less	RBR	B-NP
thrombin	NN	B-NP
than	IN	B-PP
after	IN	B-PP
serum	NN	B-NP
withdrawal	NN	I-NP
(	(	O
P	NN	B-NP
=	JJ	O
.0002	NN	B-NP
for	IN	B-PP
AUC	NN	B-NP
and	CC	B-NP
PT	NN	B-NP
)	)	O
.	.	O

VSMCs	NNS	B-NP
derived	VBN	B-VP
from	IN	B-PP
human	JJ	O
coronary	JJ	O
atherosclerotic	JJ	O
plaques	NNS	B-NP
that	WDT	O
apoptose	VBP	B-VP
even	RB	O
in	IN	B-PP
serum	NN	B-NP
also	RB	O
generated	VBN	B-VP
thrombin	NN	B-NP
(	(	O
AUC	NN	B-NP
of	IN	B-PP
260	CD	O
+/-	CC	O
2	CD	O
nmol	NN	B-NP
x	NN	I-NP
min/L	NN	I-NP
;	:	B-NP
PT	NN	B-NP
of	IN	B-PP
128	CD	O
+/-	CC	O
4	CD	O
nmol/L	NN	B-NP
)	)	O
.	.	O

We	PRP	B-NP
conclude	VBP	B-VP
that	IN	O
apoptotic	JJ	B-NP
VSMCs	NNS	I-NP
possess	VBP	B-VP
a	DT	B-NP
significant	JJ	O
thrombin-generating	NN	O
capacity	NN	B-NP
secondary	JJ	O
to	TO	B-PP
phosphatidylserine	NN	B-NP
exposure	NN	I-NP
.	.	O

Apoptotic	JJ	B-NP
cells	NNS	I-NP
within	IN	B-PP
atherosclerotic	JJ	B-NP
plaques	NNS	I-NP
may	MD	B-VP
allow	VB	B-VP
local	JJ	B-NP
thrombin	NN	B-NP
activation	NN	B-NP
,	,	B-VP
thereby	RB	O
contributing	VBG	B-VP
to	TO	B-PP
disease	NN	B-NP
progression	NN	I-NP
.	.	O

