Signaling	VBG	B-NP
through	IN	B-PP
the	DT	B-NP
lymphotoxin-beta	NN	I-NP
receptor	NN	I-NP
stimulates	VBZ	B-VP
HIV-1	NN	B-NP
replication	NN	I-NP
alone	RB	O
and	CC	O
in	IN	B-PP
cooperation	NN	B-NP
with	IN	B-PP
soluble	JJ	B-NP
or	CC	B-NP
membrane-bound	JJ	B-NP
TNF-alpha	NN	I-NP
.	.	O

The	DT	B-NP
level	NN	I-NP
of	IN	B-PP
ongoing	JJ	B-NP
HIV-1	NN	I-NP
replication	NN	I-NP
within	IN	B-PP
an	DT	B-NP
individual	NN	I-NP
is	VBZ	B-VP
critical	JJ	O
to	TO	B-PP
HIV-1	NN	B-NP
pathogenesis	NN	I-NP
.	.	O

Among	IN	B-PP
host	NN	B-NP
immune	JJ	I-NP
factors	NNS	I-NP
,	,	O
the	DT	B-NP
cytokine	NN	I-NP
TNF-alpha	NN	I-NP
has	VBZ	B-VP
previously	RB	O
been	VBN	B-VP
shown	VBN	B-VP
to	TO	B-VP
increase	VB	B-VP
HIV-1	NN	B-NP
replication	NN	I-NP
in	IN	B-PP
various	JJ	B-NP
monocyte	NN	B-NP
and	CC	B-NP
T	NN	B-NP
cell	NN	I-NP
model	NN	I-NP
systems	NNS	I-NP
.	.	O

Here	RB	O
,	,	O
we	PRP	B-NP
demonstrate	VBP	B-VP
that	IN	O
signaling	VBG	B-NP
through	IN	B-PP
the	DT	B-NP
TNF	NN	I-NP
receptor	NN	I-NP
family	NN	I-NP
member	NN	I-NP
,	,	B-NP
the	DT	B-NP
lymphotoxin-beta	NN	I-NP
(	(	I-NP
LT-beta	NN	I-NP
)	)	I-NP
receptor	NN	I-NP
(	(	O
LT-betaR	NN	B-NP
)	)	O
,	,	B-PP
also	RB	O
regulates	VBZ	B-VP
HIV-1	NN	B-NP
replication	NN	I-NP
.	.	O

Furthermore	RB	O
,	,	O
HIV-1	NN	B-NP
replication	NN	I-NP
is	VBZ	B-VP
cooperatively	RB	O
stimulated	VBN	B-VP
when	WRB	O
the	DT	B-NP
distinct	JJ	I-NP
LT-betaR	NN	B-NP
and	CC	B-NP
TNF	NN	B-NP
receptor	NN	I-NP
systems	NNS	I-NP
are	VBP	B-VP
simultaneously	RB	O
engaged	VBN	B-VP
by	IN	B-PP
their	PRPP	B-NP
specific	JJ	I-NP
ligands	NNS	I-NP
.	.	O

Moreover	RB	O
,	,	O
in	IN	B-PP
a	DT	B-NP
physiological	JJ	I-NP
coculture	NN	I-NP
cellular	JJ	I-NP
assay	NN	I-NP
system	NN	I-NP
,	,	O
we	PRP	B-NP
show	VBP	B-VP
that	IN	O
membrane-bound	JJ	B-NP
TNF-alpha	NN	I-NP
and	CC	B-NP
LT-alpha1beta2	NN	B-NP
act	VBP	B-VP
virtually	RB	O
identically	RB	O
to	TO	B-PP
their	PRPP	B-NP
soluble	JJ	I-NP
forms	NNS	I-NP
in	IN	B-PP
the	DT	B-NP
regulation	NN	I-NP
of	IN	B-PP
HIV-1	NN	B-NP
replication	NN	I-NP
.	.	O

Thus	RB	O
,	,	O
cosignaling	NN	B-NP
via	IN	B-PP
the	DT	B-NP
LT-beta	NN	B-NP
and	CC	B-NP
TNF-alpha	NN	B-NP
receptors	NNS	B-NP
is	VBZ	B-VP
probably	RB	O
involved	VBN	B-VP
in	IN	B-PP
the	DT	B-NP
modulation	NN	I-NP
of	IN	B-PP
HIV-1	NN	B-NP
replication	NN	I-NP
and	CC	B-NP
the	DT	B-NP
subsequent	JJ	I-NP
determination	NN	I-NP
of	IN	B-PP
HIV-1	NN	B-NP
viral	JJ	I-NP
burden	NN	I-NP
in	IN	B-PP
monocytes	NNS	B-NP
.	.	O

Intriguingly	RB	O
,	,	O
surface	NN	B-NP
expression	NN	I-NP
of	IN	B-PP
LT-alpha1beta2	NN	B-NP
is	VBZ	B-VP
up-	RB	B-VP
regulated	VBN	I-VP
on	IN	B-PP
a	DT	B-NP
T	NN	I-NP
cell	NN	I-NP
line	NN	I-NP
acutely	RB	O
infected	VBN	B-VP
with	IN	B-PP
HIV-1	NN	B-NP
,	,	B-VP
suggesting	VBG	B-PP
a	DT	B-NP
positive	JJ	I-NP
feedback	NN	I-NP
loop	NN	I-NP
between	IN	B-PP
HIV-1	NN	B-NP
infection	NN	I-NP
,	,	B-NP
LT-alpha1beta2	NN	B-NP
expression	NN	I-NP
,	,	B-NP
and	CC	I-NP
HIV-1	NN	B-NP
replication	NN	I-NP
.	.	O

Given	IN	B-PP
the	DT	B-NP
critical	JJ	I-NP
role	NN	I-NP
that	WDT	O
LT-alpha1beta2	NN	B-NP
plays	VBZ	B-VP
in	IN	B-PP
lymphoid	JJ	B-NP
architecture	NN	I-NP
,	,	O
we	PRP	B-NP
speculate	VBP	B-VP
that	IN	O
LT-alpha1beta2	NN	B-NP
may	MD	B-VP
be	VB	B-VP
involved	VBN	B-VP
in	IN	B-PP
HIV-associated	JJ	B-NP
abnormalities	NNS	I-NP
of	IN	B-PP
the	DT	B-NP
lymphoid	JJ	I-NP
organs	NNS	I-NP
.	.	O

