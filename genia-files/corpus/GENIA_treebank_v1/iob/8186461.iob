Calcineurin	NN	B-NP
potentiates	VBZ	B-VP
activation	NN	B-NP
of	IN	B-PP
the	DT	B-NP
granulocyte-macrophage	JJ	I-NP
colony-stimulating	JJ	I-NP
factor	NN	I-NP
gene	NN	I-NP
in	IN	B-PP
T	NN	B-NP
cells	NNS	I-NP
:	:	O
involvement	NN	B-NP
of	IN	B-PP
the	DT	B-NP
conserved	VBN	I-NP
lymphokine	NN	I-NP
element	NN	I-NP
0	CD	I-NP
.	.	B-NP

Granulocyte-macrophage	JJ	B-NP
colony-stimulating	JJ	I-NP
factor	NN	I-NP
(	(	O
GM-CSF	NN	B-NP
)	)	O
and	CC	B-NP
interleukin-2	NN	B-NP
(	(	O
IL-2	NN	B-NP
)	)	O
are	VBP	B-VP
produced	VBN	B-VP
by	IN	B-PP
stimulation	NN	B-NP
with	IN	B-PP
phorbol-12-myristate	NN	B-NP
acetate	NN	I-NP
(	(	O
PMA	NN	B-NP
)	)	O
and	CC	B-NP
calcium	NN	B-NP
ionophore	NN	I-NP
(	(	O
A23187	NN	B-NP
)	)	O
in	IN	B-PP
human	JJ	B-NP
T	NN	I-NP
cell	NN	I-NP
leukemia	NN	I-NP
Jurkat	NN	I-NP
cells	NNS	I-NP
.	.	O

The	DT	B-NP
expression	NN	I-NP
of	IN	B-PP
GM-CSF	NN	B-NP
and	CC	B-NP
IL-2	NN	B-NP
is	VBZ	B-VP
inhibited	VBN	B-VP
by	IN	B-PP
immunosuppressive	JJ	B-NP
drugs	NNS	I-NP
such	JJ	B-PP
as	IN	I-PP
cyclosporin	NN	B-NP
A	NN	I-NP
(	(	O
CsA	NN	B-NP
)	)	O
and	CC	B-NP
FK506	NN	B-NP
.	.	O

Earlier	JJR	B-NP
studies	NNS	I-NP
on	IN	B-PP
the	DT	B-NP
IL-2	NN	I-NP
gene	NN	I-NP
expression	NN	I-NP
showed	VBD	B-VP
that	IN	O
overexpression	NN	B-NP
of	IN	B-PP
calcineurin	NN	B-NP
(	(	O
CN	NN	B-NP
)	)	O
,	,	B-NP
a	DT	B-NP
Ca2+/calmodulin-dependent	JJ	I-NP
protein	NN	I-NP
phosphatase	NN	I-NP
,	,	B-NP
can	MD	B-VP
stimulate	VB	B-VP
transcription	NN	B-NP
from	IN	B-PP
the	DT	B-NP
IL-2	NN	I-NP
promoter	NN	I-NP
through	IN	B-PP
the	DT	B-NP
NF-AT-binding	JJ	I-NP
site	NN	I-NP
.	.	O

In	IN	B-PP
this	DT	B-NP
study	NN	I-NP
,	,	O
we	PRP	B-NP
obtained	VBD	B-VP
evidence	NN	B-NP
that	IN	O
transfection	NN	B-NP
of	IN	B-PP
the	DT	B-NP
cDNAs	NNS	I-NP
for	IN	B-PP
CN	NN	B-NP
A	NN	I-NP
(	(	O
catalytic	JJ	O
)	)	O
and	CC	B-NP
CN	NN	B-NP
B	NN	I-NP
(	(	O
regulatory	JJ	O
)	)	O
subunits	NNS	B-NP
also	RB	O
augments	VBZ	B-VP
transcription	NN	B-NP
from	IN	B-PP
the	DT	B-NP
GM-CSF	NN	I-NP
promoter	NN	I-NP
and	CC	B-VP
recovers	VBZ	B-VP
the	DT	B-NP
transcription	NN	I-NP
inhibited	VBN	B-VP
by	IN	B-PP
CsA	NN	B-NP
.	.	O

The	DT	B-NP
constitutively	RB	O
active	JJ	O
type	NN	B-NP
of	IN	B-PP
the	DT	B-NP
CN	NN	I-NP
A	NN	I-NP
subunit	NN	I-NP
,	,	B-NP
which	WDT	O
lacks	VBZ	B-VP
the	DT	B-NP
auto-inhibitory	JJ	O
and	CC	O
calmodulin-binding	JJ	O
domains	NNS	B-NP
,	,	B-NP
acts	VBZ	B-VP
in	IN	B-PP
synergy	NN	B-NP
with	IN	B-PP
PMA	NN	B-NP
to	TO	B-VP
activate	VB	B-VP
transcription	NN	B-NP
from	IN	B-PP
the	DT	B-NP
GM-CSF	NN	I-NP
promoter	NN	I-NP
.	.	O

We	PRP	B-NP
also	RB	O
found	VBD	B-VP
that	IN	O
the	DT	B-NP
active	JJ	I-NP
CN	NN	I-NP
partially	RB	O
replaces	VBZ	B-VP
calcium	NN	B-NP
ionophore	NN	I-NP
in	IN	B-PP
synergy	NN	B-NP
with	IN	B-PP
PMA	NN	B-NP
to	TO	B-VP
induce	VB	B-VP
expression	NN	B-NP
of	IN	B-PP
endogenous	JJ	B-NP
GM-CSF	NN	B-NP
and	CC	B-NP
IL-2	NN	B-NP
.	.	O

By	IN	B-PP
multimerizing	VBG	B-VP
the	DT	B-NP
regulatory	JJ	I-NP
elements	NNS	I-NP
of	IN	B-PP
the	DT	B-NP
GM-CSF	NN	I-NP
promoter	NN	I-NP
,	,	O
we	PRP	B-NP
found	VBD	B-VP
that	IN	O
one	CD	B-NP
of	IN	B-PP
the	DT	B-NP
target	NN	I-NP
sites	NNS	I-NP
for	IN	B-PP
the	DT	B-NP
CN	NN	I-NP
action	NN	I-NP
is	VBZ	B-VP
the	DT	B-NP
conserved	VBN	I-NP
lymphokine	NN	I-NP
element	NN	I-NP
0	CD	I-NP
(	(	O
CLE0	NN	B-NP
)	)	O
,	,	B-NP
located	JJ	O
at	IN	B-PP
positions	NNS	B-NP
between	IN	B-PP
-54	CD	B-NP
and	CC	B-NP
-40	CD	B-NP
.	.	O

Mobility	NN	B-NP
shift	NN	I-NP
assays	NNS	I-NP
showed	VBD	B-VP
that	IN	O
the	DT	B-NP
CLE0	NN	I-NP
sequence	NN	I-NP
has	VBZ	B-VP
an	DT	B-NP
AP1-binding	JJ	I-NP
site	NN	I-NP
and	CC	B-VP
is	VBZ	B-VP
associated	VBN	B-VP
with	IN	B-PP
an	DT	B-NP
NF-AT-like	JJ	I-NP
factor	NN	I-NP
,	,	B-NP
termed	VBN	B-VP
NF-CLE0	NN	B-NP
gamma	NN	I-NP
.	.	O

NF-CLE0	NN	B-NP
gamma	NN	I-NP
binding	NN	I-NP
is	VBZ	B-VP
induced	VBN	B-VP
by	IN	B-PP
PMA/A23187	NN	B-NP
and	CC	B-VP
is	VBZ	B-VP
inhibited	VBN	B-VP
by	IN	B-PP
treatment	NN	B-NP
with	IN	B-PP
CsA	NN	B-NP
.	.	O

These	DT	B-NP
results	NNS	I-NP
suggest	VBP	B-VP
that	IN	O
CN	NN	B-NP
is	VBZ	B-VP
involved	VBN	B-VP
in	IN	B-PP
the	DT	B-NP
coordinated	VBN	I-NP
induction	NN	I-NP
of	IN	B-PP
the	DT	B-NP
GM-CSF	NN	B-NP
and	CC	B-NP
IL-2	NN	B-NP
genes	NNS	B-NP
and	CC	O
that	IN	O
the	DT	B-NP
CLE0	NN	I-NP
sequence	NN	I-NP
of	IN	B-PP
the	DT	B-NP
GM-CSF	NN	I-NP
gene	NN	I-NP
is	VBZ	B-VP
a	DT	B-NP
functional	JJ	I-NP
analogue	NN	I-NP
of	IN	B-PP
the	DT	B-NP
NF-AT-binding	JJ	I-NP
site	NN	I-NP
in	IN	B-PP
the	DT	B-NP
IL-2	NN	I-NP
promoter	NN	I-NP
,	,	B-NP
which	WDT	O
mediates	VBZ	B-VP
signals	NNS	B-NP
downstream	RB	O
of	IN	B-PP
T	NN	B-NP
cell	NN	I-NP
activation	NN	I-NP
.	.	O

