# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/xml2postag.py
xml_file=$WORKDIR/corpus/GENIAcorpus3.02p/GENIAcorpus3.02.pos.xml
wtag_file=$WORKDIR/corpus/GENIAcorpus3.02p/GENIAcorpus3.02.pos.txt

# Convert xml file in word/tag file
echo "Converting xml file to word/tag format..." 
python $exe_file $xml_file $wtag_file

