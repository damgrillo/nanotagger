import sys
import string
import xml.dom.minidom

####################################################################################
#################################### FUNCTIONS #####################################
####################################################################################

# Punctuation and bracket sets
punctset = set([',', '.', ';', ':'])
brackset = set(['-LRB-', '-LSB-', '-LCB-', '-RRB-', '-RSB-', '-RCB-' ])

# Replace brackets by their corresponding treebank syntax
def brackets2syntax(text):
    newtext = text
    newtext = newtext.replace('(', '-LRB-')
    newtext = newtext.replace('[', '-LSB-')
    newtext = newtext.replace('{', '-LCB-')
    newtext = newtext.replace(')', '-RRB-')
    newtext = newtext.replace(']', '-RSB-')
    newtext = newtext.replace('}', '-RCB-')
    return newtext

# Convert get standard POS Tags for a given tag
def get_standard_tag(tag):
    if tag == 'LRB':
        std_tag = '('
    elif tag == 'RRB':
        std_tag = ')'
    elif tag == 'LQT':
        std_tag = '``'
    elif tag == 'RQT':
        std_tag = "''"
    elif tag == 'COMMA':
        std_tag = ','
    elif tag == 'COLON':
        std_tag = ':'
    elif tag == 'PERIOD':
        std_tag = '.'
    else:
        std_tag = tag
    return std_tag

# Recursive function to get the treebank branch corresponding to node
def get_treebank_branch(sent, node, shift):

    # Get id for sent node and explore its child nodes  
    if (node.nodeName == 'sentence') and not node.hasAttribute('null'):
        sent[0] += shift + ' (' + node.getAttribute('id') + '\n'
        for child in node.childNodes:
            get_treebank_branch(sent, child, shift + '    ')
        sent[0] += shift + ')' + '\n'

    # Get category for cons node and explore its child nodes  
    elif (node.nodeName == 'cons') and not node.hasAttribute('null'):
        sent[0] += shift + '(' + node.getAttribute('cat') + '\n'
        for child in node.childNodes:
            get_treebank_branch(sent, child, shift + '    ')
        sent[0] += shift +  ')' + '\n'

    # Get category and value for tok node
    elif (node.nodeName == 'tok') and not node.hasAttribute('null'):
        node_cat = brackets2syntax(get_standard_tag(node.getAttribute('cat'))) 
        node_value = brackets2syntax(node.firstChild.nodeValue.encode('utf-8'))
        sent[0] += shift + '(' + str(node_cat) + ' ' + str(node_value) + ')' + '\n'
        

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = xml corpus file
# sys.argv[2] = treebank corpus file

# Load xml file and write treebank file
tbank_file = open(sys.argv[2], 'w')
xmlcorp = xml.dom.minidom.parse(sys.argv[1])
xmlsents = xmlcorp.getElementsByTagName('sentence')
for xsnode in xmlsents:
    tsent = ['']
    get_treebank_branch(tsent, xsnode, '')
    tbank_file.write(tsent[0][1:] + '\n')


