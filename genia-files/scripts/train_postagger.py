import sys
import nltk
import time
import pickle
import random
import string
import os.path
from itertools import izip
from nltk.tag import untag
from nltk.tag import brill
from nltk.tag import AffixTagger
from nltk.tag import RegexpTagger
from nltk.tag import DefaultTagger
from nltk.tag import UnigramTagger
from nltk.tag import BigramTagger
from nltk.tag import TrigramTagger
from nltk.corpus.reader import TaggedCorpusReader


####################################################################################
#################################### FUNCTIONS #####################################
####################################################################################

# Trainer function for Brill Tagger
def brill_trainer(init_tagger, train_sents):
    sym_bounds = [(1,1), (2,2), (1,2), (1,3)]
    asym_bounds = [(-1,-1), (1,1), (-1,1), (-2,2)]
    t1 = brill.SymmetricProximateTokensTemplate(brill.ProximateTagsRule, *sym_bounds)
    t2 = brill.SymmetricProximateTokensTemplate(brill.ProximateWordsRule, *sym_bounds)
    t3 = brill.ProximateTokensTemplate(brill.ProximateTagsRule, *asym_bounds)
    t4 = brill.ProximateTokensTemplate(brill.ProximateWordsRule, *asym_bounds)
    temp = [t1, t2, t3, t4]
    brill_trainer = brill.FastBrillTaggerTrainer(init_tagger, temp, deterministic=True, trace=1)
    return brill_trainer.train(train_sents)

# Measure elapsed time
def elapsed_time(start, end):
    elapsed = end - start
    mm = int(elapsed) / 60
    ss = int(elapsed) % 60
    cc = int((elapsed - int(elapsed)) * 100) 
    return str(mm).zfill(2) + ':' + str(ss).zfill(2) + '.' + str(cc).zfill(2) 

# Evaulate tagger
def evaluate_tagger(tagger, gold_sents):
    match = 0
    total = 0
    for gsent in gold_sents:
        tsent = tagger.tag(untag(gsent))
        mtags = [tt for (gt,tt) in izip(gsent, tsent) if gt == tt]
        match += len(mtags)
        total += len(gsent)
    print 'Accuracy...', float(match) / total


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = corpus dir
# sys.argv[2] = tagger dir

# Import tagged corpus
print 'Loading tagged corpus...'
tcorp = TaggedCorpusReader(sys.argv[1], r'.*\.txt')
tsents = [sent for sent in tcorp.tagged_sents()]

# Set training and testing sets
random.shuffle(tsents)
ntrain = len(tsents) * 3 / 4
train_sents = tsents[:ntrain]
test_sents = tsents[ntrain:]
print 'Total sents:', len(tsents)
print 'Training sents:', len(train_sents)
print 'Testing sents: ', len(test_sents), '\n'

# Check if rubt tagger file exists
ftagger = sys.argv[2] + '/rubt_tagger.pickle'
if os.path.isfile(ftagger):
    
    # Tagger file exists, then load rubt tagger
    print 'Loading rubt tagger...\n'
    ft = open(ftagger, 'r')
    rubt_tagger = pickle.load(ft)
else:

    # Tagger file does not exist, then train and save rubt tagger
    print 'Training rubt tagger...'
    start = time.time()
    regexps   = [(r'.*-.*ed$', 'JJ'), (r'.*-.*ing$', 'JJ'), (r'.*able$', 'JJ'),  
                 (r'.*ly$', 'RB'), (r'.*ed$', 'VBD'), (r'.*ing$', 'VBG')]
    dtagger   = DefaultTagger('NN')
    rtagger   = RegexpTagger(regexps, backoff=dtagger)
    rutagger  = UnigramTagger(train_sents, backoff=rtagger)
    rubtagger = BigramTagger(train_sents, backoff=rutagger)
    rubt_tagger = TrigramTagger(train_sents, backoff=rubtagger)
    end = time.time()
    print 'Elapsed time...', elapsed_time(start, end)
    print 'Saving rubt tagger...\n'
    ft = open(ftagger, 'w')
    pickle.dump(rubt_tagger, ft)

# Check if brill tagger file exists
ftagger = sys.argv[2] + '/brill_rubt_tagger.pickle'
if os.path.isfile(ftagger):
    
    # Tagger file exists, then load brill tagger
    print 'Loading brill_rubt tagger...\n'
    ft = open(ftagger, 'r')
    brill_tagger = pickle.load(ft)
else:

    # Tagger file does not exist, then train and save brill tagger
    print 'Training brill_rubt tagger...'
    start = time.time() 
    brill_tagger  = brill_trainer(rubt_tagger, train_sents)
    end = time.time()
    print 'Elapsed time...', elapsed_time(start, end)
    print 'Saving brill_rubt tagger...\n'
    ft = open(ftagger, 'w')
    pickle.dump(brill_tagger, ft)

# Evaluating rubt tagger
print 'Evaluating rubt tagger...'
start = time.time()
evaluate_tagger(rubt_tagger, test_sents)
end = time.time()
print 'Elapsed time...', elapsed_time(start, end), '\n'

# Evaluating brill tagger
print 'Evaluating brill_rubt tagger...'
start = time.time()
evaluate_tagger(brill_tagger, test_sents)
end = time.time()
print 'Elapsed time...', elapsed_time(start, end), '\n'


