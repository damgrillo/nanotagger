import sys
import nltk
import string
import nltk.chunk
from nltk.tree import Tree
from nltk.corpus.reader import BracketParseCorpusReader


##############################################################################################
######################################## FUNCTIONS ###########################################
##############################################################################################

# Valid Chunks 
valid_chunks = set(['NP', 'ADJP', 'QP', 'PRN', 'FRAG', 'UCP'])

# Recover the bracket corresponding to the treebank syntax
def syntax2brackets(text):
    newtext = text
    newtext = newtext.replace('-LRB-', '(')
    newtext = newtext.replace('-LSB-', '(')
    newtext = newtext.replace('-LCB-', '{')
    newtext = newtext.replace('-RRB-', ')')
    newtext = newtext.replace('-RSB-', ')')
    newtext = newtext.replace('-RCB-', '}')
    return newtext

# Check if all children in tree are valid (only contain valid chunks and POS tags)
def valid_children(tree):
    idx = 0
    valid = True
    children = [ch for ch in tree]
    while (idx < len(children)) and valid:
        ch = children[idx]
        valid = (ch.height() == 2) or ((ch.node in valid_chunks) and valid_children(ch))
        idx += 1
    return valid

# Flatten children
def flatten_children(tree):
    childnodes = []
    if (tree.height() == 2):
        childnodes.extend(tree.pos())
    else:
        if (tree.node == 'NP') and valid_children(tree):           
            childnodes.append(Tree(tree.node, tree.pos()))
        else:
            for child in tree:
                childnodes.extend(flatten_children(child))
    return childnodes

# Flatten trees
def flatten_tree(tree):
    children = []
    for child in tree:
        children.extend(flatten_children(child)) 
    return Tree(tree.node, children)


##############################################################################################
######################################### MAIN PROGRAM #######################################
##############################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = treebank corpus dir
# sys.argv[2] = chunk corpus file

# Import treebank corpus
print 'Loading treebank corpus...'
tbkcorp = BracketParseCorpusReader(sys.argv[1], r'.*\.tbk')
tbklist = tbkcorp.fileids()

# Convert each treebank file to chunk format
for tbk in tbklist:
    print 'Converting', tbk, 'to chunk format...'
    chk_name = sys.argv[2] + '/' + tbk[:-4] + '.chunk'
    chk_file = open(chk_name, 'w')
    tbk_trees = [tree for tree in tbkcorp.parsed_sents(tbk)]    
    flat_trees = [flatten_tree(tree) for tree in tbk_trees]

    # Write chunk file
    for tree in flat_trees:
        text = ''
        if tree.height() > 3:
            msg_error = 'Error in ' + tbk + ' file. Tree too deep, cannot be chunked'
            sys.exit(msg_error)
        for ch in tree:
            if type(ch) == type(nltk.tree.Tree(None)):
                text += '[ '
                for tag in ch:
                    text +=  tag[0] + '/' + tag[1] + ' ' 
                text += '] '
            elif type(ch) == type((None, None)):
                text += ch[0] + '/' + ch[1] + ' '
        text += '\n'
        chk_file.write(text)


