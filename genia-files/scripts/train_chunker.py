import sys
import re
import nltk
import time
import pickle
import string
import random
import os.path
import nltk.chunk
from nltk.corpus.reader import ChunkedCorpusReader

#######################################################################################
#################################### FUNCTIONS ########################################
#######################################################################################

# Measure elapsed time
def elapsed_time(start, end):
    elapsed = end - start
    mm = int(elapsed) / 60
    ss = int(elapsed) % 60
    cc = int((elapsed - int(elapsed)) * 100) 
    return str(mm).zfill(2) + ':' + str(ss).zfill(2) + '.' + str(cc).zfill(2) 


#######################################################################################
#################################### MAIN PROGRAM #####################################
#######################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = corpus dir
# sys.argv[2] = tagger dir
# sys.argv[3] = MEGAM dir
# sys.argv[4] = training or evaluation mode

# Set path to MEGAM binary 
nltk.classify.megam.config_megam(sys.argv[3] + '/megam_i686.opt')

# Add chunkers classes file to path
sys.path.append(sys.argv[2])
from chunkers import UbtChunker, BrillChunker, MaxentChunker

# Import treebank corpus
print 'Loading treebank corpus in chunk format...'
chkcorp = ChunkedCorpusReader(sys.argv[1], r'.*\.chunk')
chksents = [sent for sent in chkcorp.chunked_sents()]

# Set training and testing sets
random.shuffle(chksents)
ntrain = len(chksents) * 3 / 4
train_sents = chksents[:ntrain]
test_sents = chksents[ntrain:]
print 'Total sents:', len(chksents)
print 'Training sents:', len(train_sents)
print 'Testing sents: ', len(test_sents), '\n'

## Check if ubt chunker file exists
#fchunker = sys.argv[2] + '/ubt_chunker.pickle'
#if os.path.isfile(fchunker):
#    
#    # Chunker file exists, then load ubt chunker
#    print 'Loading ubt chunker...\n'
#    fc = open(fchunker, 'r')
#    ubt_chunker = pickle.load(fc)
#else:

#    # Chunker file does not exist, then train and save ubt chunker
#    print 'Training ubt chunker...'
#    start = time.time()
#    ubt_chunker = UbtChunker(train_sents)
#    end = time.time()
#    print 'Elapsed time...', elapsed_time(start, end)
#    print 'Saving ubt chunker...\n'
#    fc = open(fchunker, 'w')
#    pickle.dump(ubt_chunker, fc)

## Check if brill chunker file exists
#fchunker = sys.argv[2] + '/brill_chunker.pickle'
#if os.path.isfile(fchunker):
#    
#    # Chunker file exists, then load brill chunker
#    print 'Loading brill chunker...\n'
#    fc = open(fchunker, 'r')
#    brill_chunker = pickle.load(fc)
#else:

#    # Chunker file does not exist, then train and save brill chunker
#    print 'Training brill chunker...'
#    start = time.time()
#    brill_chunker = BrillChunker(train_sents)
#    end = time.time()
#    print 'Elapsed time...', elapsed_time(start, end)
#    print 'Saving ubt chunker...\n'
#    fc = open(fchunker, 'w')
#    pickle.dump(brill_chunker, fc)

# Check if maxent chunker file exists
fchunker = sys.argv[2] + '/' + 'maxent_chunker.pickle'
if os.path.isfile(fchunker):
    
    # Chunker file exists, then load maxent chunker
    print 'Loading maxent chunker...\n'
    fc = open(fchunker, 'r')
    maxent_chunker = pickle.load(fc)
else:

    # Chunker file does not exist, then train and save maxent chunker
    print 'Training maxent chunker...'
    start = time.time()
    maxent_chunker = MaxentChunker(train_sents)
    end = time.time()
    print 'Elapsed time...', elapsed_time(start, end)
    print 'Saving maxent chunker...\n'
    fc = open(fchunker, 'w')
    pickle.dump(maxent_chunker, fc)

## Evaluating ubt chunker
#print 'Evaluating ubt chunker...'
#start = time.time()
#score = ubt_chunker.evaluate(test_sents)
#end = time.time()
#print 'Elapsed time...', elapsed_time(start, end)
#print 'Accuracy...', score.accuracy()
#print 'Precision...', score.precision()
#print 'Recall...', score.recall(), '\n'

## Evaluating brill chunker
#print 'Evaluating brill chunker...'
#start = time.time()
#score = brill_chunker.evaluate(test_sents)
#end = time.time()
#print 'Elapsed time...', elapsed_time(start, end)
#print 'Accuracy...', score.accuracy()
#print 'Precision...', score.precision()
#print 'Recall...', score.recall(), '\n'

# Evaluating maxent chunker
print 'Evaluating maxent chunker...'
start = time.time()
score = maxent_chunker.evaluate(test_sents)
end = time.time()
print 'Elapsed time...', elapsed_time(start, end)
print 'Accuracy...', score.accuracy()
print 'Precision...', score.precision()
print 'Recall...', score.recall(), '\n'

