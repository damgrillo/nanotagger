# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/treebank2iob.py
tbk_dir=$WORKDIR/corpus/GENIA_treebank_v1/treebank
iob_dir=$WORKDIR/corpus/GENIA_treebank_v1/iob

# Create or clean iob dir
if [ ! -d "$iob_dir" ]; then
    echo "Creating iob dir..."
    mkdir $iob_dir
else
    echo "Cleaning iob dir..."
    rm $iob_dir/*
fi

# Load files in treebank dir and convert in iob format
echo "Converting trebank files to iob format..." 
python $exe_file $tbk_dir $iob_dir


