# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/treebank2chunk.py
tbk_dir=$WORKDIR/corpus/GENIA_treebank_v1/treebank
chk_dir=$WORKDIR/corpus/GENIA_treebank_v1/chunk

# Create or clean chunk dir
if [ ! -d "$chk_dir" ]; then
    echo "Creating chunk dir..."
    mkdir $chk_dir
else
    echo "Cleaning chunk dir..."
    rm $chk_dir/*
fi

# Load files in treebank dir and convert in chunk format
echo "Converting trebank files to chunk format..." 
python $exe_file $tbk_dir $chk_dir


