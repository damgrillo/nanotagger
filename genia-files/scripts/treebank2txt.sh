# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/treebank2txt.py
tbk_dir=$WORKDIR/corpus/GENIA_treebank_v1/treebank
txt_dir=$WORKDIR/corpus/GENIA_treebank_v1/txt

# Create or clean txt dir
if [ ! -d "$txt_dir" ]; then
    echo "Creating txt dir..."
    mkdir $txt_dir
else
    echo "Cleaning txt dir..."
    rm $txt_dir/*
fi

# Load files in treebank dir and convert in txt format
echo "Converting trebank files to txt format..." 
python $exe_file $tbk_dir $txt_dir


