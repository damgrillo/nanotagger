# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/extract_cell_annots.py
xml_file=$WORKDIR/corpus/GENIA_term_3.02/GENIAcorpus3.02.xml
ann_file=$WORKDIR/corpus/GENIA_term_3.02/GENIAcorpus3.02.cells.txt

# Extract text and annotations from xml file
echo "Extracting text and cell annotations from xml file..." 
python $exe_file $xml_file > $ann_file

