import sys
import string
import xml.dom.minidom

####################################################################################################
############################################ MAIN PROGRAM ##########################################
####################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = xml corpus file
# sys.argv[2] = word/tag corpus file

# Note: Error in many * tags were detected (* tags here correspond to SYM tags in standard notation)
#       Since most of these tags actually correspond to NN tags, further corrections are required. 

# Load xml file and write word/tag file
# Change incorrect * tags by NN tags. Change the correct ones by SYM tags.  
wtagfile = open(sys.argv[2], 'w')
corp  = xml.dom.minidom.parse(sys.argv[1])
sents = corp.getElementsByTagName('sentence')
for sent in sents:
    text = ''
    for wnode in sent.getElementsByTagName('w'):
        word = wnode.firstChild.nodeValue.encode('utf-8')
        tag  = wnode.getAttribute('c')
        if tag == '*':
            if word in string.punctuation:
                tag = 'SYM'
            else:
                tag = 'NN'
        text += word + '/' + tag + ' '
    wtagfile.write(text + '\n')


