import sys
import nltk
import string
from nltk.corpus.reader import BracketParseCorpusReader

######################################################################################################
############################################ FUNCTIONS ###############################################
######################################################################################################

# Recover the bracket corresponding to the treebank syntax
def syntax2brackets(text):
    newtext = text
    newtext = newtext.replace('-LRB-', '(')
    newtext = newtext.replace('-LSB-', '[')
    newtext = newtext.replace('-LCB-', '{')
    newtext = newtext.replace('-RRB-', ')')
    newtext = newtext.replace('-RSB-', ']')
    newtext = newtext.replace('-RCB-', '}')
    return newtext

# Convert tree to txt sentence in list format
def tree2txt(tree):
    words = []
    for child in tree:
        if child.height() == 2:
            words.extend([syntax2brackets(w) for (w,t) in child.pos()])
        elif child.height() > 2:
            words.extend(tree2txt(child))
    return words


######################################################################################################
########################################### MAIN PROGRAM #############################################
######################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = treebank corpus dir
# sys.argv[2] = iob corpus file

# Import treebank corpus
print 'Loading treebank corpus...'
tbkcorp = BracketParseCorpusReader(sys.argv[1], r'.*\.tbk')
tbklist = tbkcorp.fileids()

# Convert each treebank file to txt format
for tbk in tbklist:
    print 'Converting', tbk, 'to txt format...'
    txt_name = sys.argv[2] + '/' + tbk[:-4] + '.txt'
    txt_file = open(txt_name, 'w')
    tbk_trees = [tree for tree in tbkcorp.parsed_sents(tbk)]    
    txt_sents = [tree2txt(tree) for tree in tbk_trees]
    
    # Write txt file
    for sent in txt_sents:
        text = ''
        prev_word = ''
        for word in sent:
            if (((prev_word not in string.punctuation) and (word not in string.punctuation)) or
                ((prev_word not in string.punctuation) and (word in '([{"')) or
                ((prev_word in ')]}",.:;%?') and (word not in string.punctuation))):
                text += ' '
            text += word
            prev_word = word
        text += '\n'
        txt_file.write(text)

