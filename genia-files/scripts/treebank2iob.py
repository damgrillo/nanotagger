import sys
import nltk
import string
import nltk.chunk
from nltk.tree import Tree
from nltk.tree import ParentedTree
from nltk.corpus.reader import BracketParseCorpusReader

######################################################################################################
############################################ FUNCTIONS ###############################################
######################################################################################################

# Chunk Tagset 
chunk_tags = set(['NP', 'PP', 'VP']) #, 'ADVP', 'ADJP', 'SBAR'])

# Recover the bracket corresponding to the treebank syntax
def syntax2brackets(text):
    newtext = text
    newtext = newtext.replace('-LRB-', '(')
    newtext = newtext.replace('-LSB-', '[')
    newtext = newtext.replace('-LCB-', '{')
    newtext = newtext.replace('-RRB-', ')')
    newtext = newtext.replace('-RSB-', ']')
    newtext = newtext.replace('-RCB-', '}')
    return newtext

# Flatten deep children
def flatten_children(tree):
    childnodes = []
    childlist = []
    for child in tree:

        # Add child nodes with heigth = 2.
        if child.height() == 2:
            childlist.extend([(syntax2brackets(w), syntax2brackets(t)) for (w,t) in child.pos()])

            # If next node not present or with height != 2, add childlist to childnodes and clean it.
            if (child.right_sibling == None) or (child.right_sibling.height() != 2):

                # If tree is chunk tag, add childlist embedded in a tree, else just add childlist.               
                if tree.node in chunk_tags:
                    childnodes.append(Tree(tree.node, childlist))
                else:
                    childnodes.extend(childlist)
                childlist = []

        # Flatten child nodes with height > 2
        elif child.height() > 2:
            childnodes.extend(flatten_children(child))
    return childnodes

# Flatten deep trees
def flatten_deeptree(tree):
    children = []
    for child in tree:
        children.extend(flatten_children(child)) 
    return Tree(tree.node, children)

######################################################################################################
########################################### MAIN PROGRAM #############################################
######################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = treebank corpus dir
# sys.argv[2] = iob corpus file

# Import treebank corpus
print 'Loading treebank corpus...'
tbkcorp = BracketParseCorpusReader(sys.argv[1], r'.*\.tbk')
tbklist = tbkcorp.fileids()

# Convert each treebank file to iob format
for tbk in tbklist:
    print 'Converting', tbk, 'to iob format...'
    iob_name = sys.argv[2] + '/' + tbk[:-4] + '.iob'
    iob_file = open(iob_name, 'w')
    tbk_trees = [tree for tree in tbkcorp.parsed_sents(tbk)]    
    flat_trees = [flatten_deeptree(ParentedTree.convert(tree)) for tree in tbk_trees]
    iob_sents = [nltk.chunk.tree2conlltags(tree) for tree in flat_trees]

    # Write iob file
    for sent in iob_sents:
        text = ''
        for tag in sent:
            text += str(tag[0]) + '\t' + str(tag[1]) + '\t' + str(tag[2]) + '\n'
        text += '\n'
        iob_file.write(text)

