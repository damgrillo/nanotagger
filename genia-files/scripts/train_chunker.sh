# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/train_chunker.py
corp_dir=$WORKDIR/corpus/GENIA_treebank_v1/chunk
chnk_dir=$WORKDIR/chunkers
megam_dir=$HOME/Software/megam

# Train chunk tagger for chunked corpus
time python $exe_file $corp_dir $chnk_dir $megam_dir
