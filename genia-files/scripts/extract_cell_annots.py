import re
import sys
import string
import xml.dom.minidom
from itertools import izip

# Extract text from node
def extract_text(node):
    text = ''
    for child in node.childNodes:
        if child.nodeType == 3:
            text += child.nodeValue
        else:
            text += extract_text(child)
    return text  

# Extract cells from node
def extract_cells(node):
    cells = []
    for cons in node.getElementsByTagName('cons'):
        if cons.hasAttribute('sem') and cons.getAttribute('sem').startswith('G#cell'):
            cells.append(extract_text(cons))
    return cells

# Match annots in text
def match_annots(text, annots):
    poslist = []
    shf = 0
    patt = ''
    ctext = (' ' + text + ' ').lower()
    annots.sort(key=len, reverse=True)
    for an in annots:
        patt += '[^\w]' + re.escape(an.lower()) + '[^\w]' + '|'
    patt = patt[:-1]
    pmatch = re.search(patt, ctext)
    while (pmatch != None) and (pmatch.start() < pmatch.end()):
        st  = pmatch.start() + 1
        end = pmatch.end() - 1
        poslist.append((st+shf-1, end+shf-1))
        shf += end
        pmatch = re.search(patt, ctext[shf:])
    poslist.sort(key=lambda pos: (pos[0], pos[1]))
    return poslist
             

####################################################################################################
############################################ MAIN PROGRAM ##########################################
####################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = xml corpus file
# sys.argv[2] = annotated corpus file

# Read articles
corp = xml.dom.minidom.parse(sys.argv[1])
arts = corp.getElementsByTagName('article')
for art in arts:
    sents = []
    annots = []

    # Extract sentences and cell annotations
    for sent in art.getElementsByTagName('sentence'):
        sents.append(extract_text(sent))
        annots.extend(extract_cells(sent))

    # Print sentences and cell annotations
    text = '<AbstractText>' + ' '.join(sents) + '</AbstractText>'
    print text
    print '<Annotations>'
    for st, end in match_annots(text, annots):
        print 'CELL|' + str(st) + '|' + str(end) + '|' + text[st:end]
    print '</Annotations>'
    print
                


