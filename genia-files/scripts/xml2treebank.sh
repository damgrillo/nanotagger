# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/xml2treebank.py
xml_dir=$WORKDIR/corpus/GENIA_treebank_v1/xml
tbk_dir=$WORKDIR/corpus/GENIA_treebank_v1/treebank

# Create treebank dir
if [ ! -d "$tbk_dir" ]; then
    echo "Creating treebank dir..."
    mkdir $tbk_dir
else
    echo "Cleaning treebank dir..."
    rm $tbk_dir/*
fi

# Read list of xml files
ls -l $xml_dir | grep '.xml' | awk '{print $9}' > list.txt

# For each xml in list, convert xml in treebank format
while read xml
do
    echo "Converting $xml file to treebank format..." 
    python $exe_file $xml_dir/$xml $tbk_dir/${xml%.xml}.tbk
done < list.txt

#echo '' > $tbk_dir/all_trees.txt
#ls -l $tbk_dir | grep '.tbk' | awk '{print $9}' > list.txt
#while read tbk
#do
#    cat $tbk_dir/$tbk >> $tbk_dir/all_trees.txt
#    echo '' >> $tbk_dir/all_trees.txt
#done < list.txt

rm list.txt
