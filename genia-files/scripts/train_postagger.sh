# Define vars
WORKDIR=$NANODEV/genia-files
exe_file=$WORKDIR/scripts/train_postagger.py
corp_dir=$WORKDIR/corpus/GENIAcorpus3.02p
tagg_dir=$WORKDIR/postaggers

# Train pos tagger for tagged corpus
time python $exe_file $corp_dir $tagg_dir
