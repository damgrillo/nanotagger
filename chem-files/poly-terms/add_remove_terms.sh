# Define vars
WORKDIR=$NANODEV/chem-files/poly-terms
add_file=$WORKDIR/terms/add_terms.txt
remove_file=$WORKDIR/terms/remove_terms.txt
chebi_file_0=$WORKDIR/terms/chebi_terms_version_0.txt
chebi_file_1=$WORKDIR/terms/chebi_terms_version_1.txt
chebi_file_f=$WORKDIR/terms/chebi_terms_version_final.txt
exe_file=$WORKDIR/add_remove_terms.py

# Run scripts to create the updated versions
python $exe_file 'REMOVE' $remove_file $chebi_file_0 > $chebi_file_1
python $exe_file 'ADD'    $add_file $chebi_file_1 > $chebi_file_f 
#time python $exe_file 'DECOMP' $chebi_file_2 > $chebi_file_f

