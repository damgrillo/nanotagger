import sys
import string


####################################################################################
#################################### FUNCTIONS #####################################
####################################################################################

# Global vars
MINLEN = 2


# Insert term in termlist by alphabetical order
def insert_by_alphabet(term, termlist, start = None, end = None):

    # Default values
    if start == None:
        start = 0
    if end == None:
        end = len(termlist) - 1

    # Empty termlist
    if end - start == -1:
        termlist.insert(start, term)
    
    # One element in termlist
    elif end - start == 0:
        if term < termlist[start]:
            termlist.insert(start, term)
        else:
            termlist.insert(start+1, term)

    # Two elements in termlist
    elif end - start == 1:
        if term < termlist[start]:
            termlist.insert(start, term)
        elif term < termlist[start+1]:
            termlist.insert(start+1, term)
        else:
            termlist.insert(start+2, term)        

    # More elements in termlist: binary search insertion
    else:
        mid = (start + end) / 2
        if term < termlist[mid]:
            end = mid
        else:
            start = mid
        insert_by_alphabet(term, termlist, start, end)


# Search word in chebiset matching exactly within term
def find_match_exact(term, chebiset):
    res = ''
    if term in chebiset:
        res += term
    return res


# Search all subterms in chebiset matching at beginning of term
# Perform the search from the longest to the shortest
def find_match_begin(term, chebiset):
    matchlist = []
    sublen = len(term)-1

    # Search for all subterms in decreasing length
    while sublen >= MINLEN:
        subterm = term[:sublen]
        match = find_match_exact(subterm, chebiset)
        if len(match) > 0:
            matchlist.append(match)
        sublen -= 1
    return matchlist


# Extract chebi subterms from term 
# termlist sorted by alphabetical order
def extract_chebi_subterms(term, chebiset, last_word=False):

    # Initial values
    match = ''
    subterms = []
    splitted = False

    # Search subterms for terms with length >= 2*MINLEN     
    if len(term) >= 2*MINLEN:

        # Find all subterms matching at the beginning of term
        matchlist = find_match_begin(term, chebiset)

        # Try splitting term in sub and rest for each match
        idx = 0
        while (idx < len(matchlist)) and not splitted:
            sub = matchlist[idx]
            rest = term.replace(sub, '', 1)
            sublist = extract_chebi_subterms(sub, chebiset)
            restlist = extract_chebi_subterms(rest, chebiset, last_word) 
            if len(restlist) > 0:
                subterms.extend(sublist)
                subterms.extend(restlist)
                splitted = True
            else:
                idx += 1

    # If not splitted, search exact match or plural form
    if not splitted:
        match = find_match_exact(term, chebiset)
        plural = ((term == 's') or (term == 'es'))
        if (len(match) > 0) or (last_word and plural):
            subterms.append(term)

    # Return subterms list
    return subterms


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Load select method
select_method = str(sys.argv[1])

# Select decompose terms
if select_method == 'DECOMP':

    # Load file
    chebi_prev_file = open(sys.argv[2], 'r')

    # Load CHEBI terms in list
    chebilist = []
    for term in chebi_prev_file:
        chebilist.append(term.strip())
    chebiset = set(chebilist)

    # Add poly temp to split terms with poly
    chebiset.add('poly')

    # Copy new CHEBI terms in list
    newlist = []
    newset = set([])
    for term in chebilist:   
        subterms = extract_chebi_subterms(term, chebiset, True)
        for sub in subterms:
            if (sub != 's') and (sub != 'es'):
                newset.add(sub)

    # Remove poly from set and copy elems to list
    if 'poly' in newset:
        newset.remove('poly')
    for term in newset:
        insert_by_alphabet(term, newlist)

    # Print new CHEBI terms
    for term in newlist:
        print term


# Select add terms
elif select_method == 'ADD':

    # Load files
    add_file = open(sys.argv[2], 'r')
    chebi_prev_file = open(sys.argv[3], 'r')

    # Load chebi terms in list
    chebi_list = []
    for term in chebi_prev_file:
        chebi_list.append(term.strip())

    # Load add terms to the list 
    for term in add_file:
        if not term in chebi_list:
            chebi_list.append(term.strip())

    # Sort list
    chebi_list.sort()

    # Print new CHEBI terms
    for term in chebi_list:
        print term


# Select remove terms
elif select_method == 'REMOVE':

    # Load files
    remove_file = open(sys.argv[2], 'r')
    chebi_prev_file = open(sys.argv[3], 'r')

    # Load chebi terms in list
    chebi_list = []
    for term in chebi_prev_file:
        chebi_list.append(term.strip())

    # Load remove terms in set 
    remove_set = set([])
    for term in remove_file:
        remove_set.add(term.strip())

    # Print new CHEBI terms
    for term in chebi_list:
        if term not in remove_set:
            print term

# Throw error
else:
    print 'Unknown select_method (Options: DECOMP, ADD, REMOVE)'





