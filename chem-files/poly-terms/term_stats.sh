# Define vars
WORKDIR=$NANODEV/chem-files/poly-terms
inp_file=$NANODEV/abstract-corpus/texts/pubmed_abstracts_all.txt
trm_file=$WORKDIR/terms/chebi_terms_version_0.txt
chk_file=$WORKDIR/terms/pubmed_abstracts_poly.chk
rmv_file=$WORKDIR/terms/remove_terms.txt
out_file=$WORKDIR/terms/term_stats.txt
exe_poly=$WORKDIR/extract_poly_chunks.py
exe_file=$WORKDIR/term_stats.py

# Define path to extra python modules
export PYTHONPATH=$WORKDIR/chunkers

# Run script to extract poly chunks from input text
#cd $WORKDIR
#time python $exe_poly $inp_file > $chk_file

# Run script to analyze term statistics
cd $WORKDIR
time python $exe_file $trm_file $rmv_file $chk_file > $out_file
