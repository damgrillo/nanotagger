import re
import sys
import nltk
import string
import pickle
from chunkers import MaxentChunker

# POS-Tagger
ft = open('postaggers/brill_rubt_tagger.pickle', 'r')
postagger = pickle.load(ft)
    
# Chunker
fc = open('chunkers/maxent_chunker.pickle', 'r')
chunker = pickle.load(fc)

####################################################################################
################################ AUXILIAR FUNCTIONS ################################
####################################################################################           

# Perform text chunking and get noun phrase chunks
def get_chunks(text, tagger, chunker):
    sents = []
    chunks = []
    for s in text.strip().split('. '):
        sents.append(s.rstrip('.'))
    tok_sents = [s.split() for s in sents]
    tag_sents = [tagger.tag(tks) for tks in tok_sents]
    chk_sents = [chunker.parse(tgs) for tgs in tag_sents]
    iob_sents = [nltk.chunk.tree2conlltags(cks) for cks in chk_sents]
    for sent in iob_sents:
        npchk = ''
        for tok, tag, chk in sent:
            if re.search('NP', chk) != None:
                npchk += tok + ' '
            else:
                if len(npchk) > 0:
                    chunks.append(npchk.strip())
                    npchk = ''
        if len(npchk) > 0:
            chunks.append(npchk.strip())
            npchk = ''
    return chunks
            

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input corpus

# Load texts from corpus
corpfile = open(sys.argv[1], 'r')
textlist = [line.strip() for line in corpfile]

# Extract chunks for each text
for text in textlist:
    chunks = get_chunks(text, postagger, chunker)
    for chk in chunks:
        if chk.lower().find('poly') >= 0:
            print chk


