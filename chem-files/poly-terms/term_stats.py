import sys
import re
import string

################################################################################
################################## FUNCTIONS ###################################
################################################################################

# Replace punctuation symbols by whitespaces in text
def punct2wspace(text):
    new_text = text  
    for char in string.punctuation:
        new_text = new_text.replace(char, ' ')
    return new_text


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = chebi term file
# sys.argv[2] = remove term file
# sys.argv[3] = input text file

# Import terms
termfile = open(sys.argv[1], 'r')
termset = set([line.strip() for line in termfile])

# Remove terms
rmvfile = open(sys.argv[2], 'r')
rmvset = set([line.strip() for line in rmvfile])

# Load text
textfile = open(sys.argv[3], 'r')
textlist = [punct2wspace(line).lower() for line in textfile]

# Term statistics
countdic = {term:0 for term in termset}
for line in textlist:
    for word in line.split():
        if word in termset:
            countdic[word] += 1

# Sort terms by frequency
sortlist  = sorted(countdic.items(), key=lambda tup: tup[1], reverse=True)

# Print terms and freqs
for term, freq in sortlist: 
    if term in rmvset:
        print str(freq).ljust(7), 'RMV'.ljust(6), term
    else:
        print str(freq).ljust(7), '   '.ljust(6), term


