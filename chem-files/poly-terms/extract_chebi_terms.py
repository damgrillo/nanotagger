import sys
import re
import string

################################################################################
################################## FUNCTIONS ###################################
################################################################################

# Replace punctuation symbols by whitespaces in text
def punct_to_wspace(text):
    new_text = text  
    for char in string.punctuation:
        new_text = new_text.replace(char, ' ')
    return new_text


# Insert term in termlist by alphabetical order
def insert_by_alphabet(term, termlist, start = None, end = None):

    # Default values
    if start == None:
        start = 0
    if end == None:
        end = len(termlist) - 1

    # Empty termlist
    if end - start == -1:
        termlist.insert(start, term)
    
    # One element in termlist
    elif end - start == 0:
        if term < termlist[start]:
            termlist.insert(start, term)
        else:
            termlist.insert(start+1, term)

    # Two elements in termlist
    elif end - start == 1:
        if term < termlist[start]:
            termlist.insert(start, term)
        elif term < termlist[start+1]:
            termlist.insert(start+1, term)
        else:
            termlist.insert(start+2, term)        

    # More elements in termlist: binary search insertion
    else:
        mid = (start + end) / 2
        if term < termlist[mid]:
            end = mid
        else:
            start = mid
        insert_by_alphabet(term, termlist, start, end)


# Load terms file. Extract terms and superclasses. Build superclass dic.
def load_class(fileterm):
    superclass = {}
    for line in fileterm:
        matchline = re.findall('"[^"]*"', line)
        if len(matchline) == 2:

            # Extract term and superclass
            term = matchline[0][1:-1]
            sup  = matchline[1][1:-1]

            # Create or update item
            if not term in superclass:
                superclass[term] = [sup]
            else:
                superclass[term].append(sup)
    return superclass


# Walk through superclass dic from node to root and build subclass branch
def build_branch(node, visited, superclass, subclass):
    if node in superclass and not visited[node]:       
        visited[node] = True

        # Create supnode and subclasses in subclass dic
        for supnode in superclass[node]:
            if not supnode in subclass:
                subclass[supnode] = []
            if not node in subclass[supnode]:
                subclass[supnode].append(node)       

        # Build the next upper level in the hierarchy
        for supnode in superclass[node]:            
            build_branch(supnode, visited, superclass, subclass)


# Extract branch terms and copy in termset
def extract_branch_terms(node, subclass, termset):
    termset.add(node)
    if node in subclass:
        for value in subclass[node]:
            extract_branch_terms(value, subclass, termset)


# Split terms. Remove terms with numbers or abbreviations. Convert lowercase.
def split_terms(termlist):
    spset = set([])
    for term in termlist:
        for elem in term.split():
            match_num = re.findall('[0-9]+', elem)
            match_abbrev = re.findall('[A-Z]{2,}', elem)
            if len(match_num) == 0 and len(match_abbrev) == 0:
                if len(elem) > 1:
                    spset.add(elem.lower())
    splist = []
    for term in spset:
        insert_by_alphabet(term, splist)
    return splist
         


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = chebi class terms file

# Load terms file and extract terms and superclasses
fileterm = open(sys.argv[1], 'r')
superclass = load_class(fileterm)

# Build subclass dic
visited = {}
subclass = {}
for term in superclass:
    visited[term] = False
for term in superclass:
    build_branch(term, visited, superclass, subclass)  

# Extract terms from group and molecular entity category
termset = set([])
extract_branch_terms('group', subclass, termset)
extract_branch_terms('polymer', subclass, termset)
extract_branch_terms('molecular entity', subclass, termset)

# Copy terms to termlist, replacing punct by wspaces
termlist = []
for term in termset:
    insert_by_alphabet(punct_to_wspace(term), termlist)

# Split and print terms
for term in split_terms(termlist): 
    print term

