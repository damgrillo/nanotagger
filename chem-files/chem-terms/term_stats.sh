# Define vars
WORKDIR=$NANODEV/chem-files/chem-terms
inp_file=$NANODEV/abstract-corpus/texts/pubmed_abstracts_all.txt
trm_file=$WORKDIR/terms/chebi_terms_version_1.txt
rmv_file=$WORKDIR/terms/remove_terms.txt
slt_file=$WORKDIR/terms/salt_stats.txt
acd_file=$WORKDIR/terms/acid_stats.txt
out_file=$WORKDIR/terms/term_stats.txt
exe_file=$WORKDIR/term_stats.py

# Run script to analyze term statistics
time python $exe_file $trm_file $rmv_file $inp_file $acd_file $slt_file > $out_file


