import sys
import re
import string

################################################################################
################################## FUNCTIONS ###################################
################################################################################

# Replace punctuation symbols by whitespaces in text
def punct2wspace(text):
    new_text = text  
    for char in string.punctuation:
        new_text = new_text.replace(char, ' ')
    return new_text


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = chebi term file
# sys.argv[2] = remove term file
# sys.argv[3] = input text file
# sys.argv[4] = acid stat file
# sys.argv[5] = salt stat file

# Import terms
termfile = open(sys.argv[1], 'r')
termset = set([line.strip() for line in termfile])

# Remove terms
rmvfile = open(sys.argv[2], 'r')
rmvset = set([line.strip() for line in rmvfile])

# Load text
textfile = open(sys.argv[3], 'r')
textlist = [punct2wspace(line).lower() for line in textfile]

# Acid statistics
acids = {}
for line in textlist:
    for tup in [term.split() for term in re.findall('\w+ acid', line)]:
        if tup[0] not in termset:
            if tup[0] not in acids:
                acids[tup[0]] = 1
            else:
                acids[tup[0]] += 1

# Salt statistics
salts = {}
for line in textlist:
    for tup in [term.split() for term in re.findall('\w+ salt', line)]:
        if tup[0] not in termset:
            if tup[0] not in salts:
                salts[tup[0]] = 1
            else:
                salts[tup[0]] += 1

# Term statistics
terms = {term:0 for term in termset}
for line in textlist:
    for word in line.split():
        if word in termset:
            terms[word] += 1

# Sort terms by frequency
acidstat = sorted(acids.items(), key=lambda tup: tup[1], reverse=True)
saltstat = sorted(salts.items(), key=lambda tup: tup[1], reverse=True)
termstat = sorted(terms.items(), key=lambda tup: tup[1], reverse=True)

# Write acid statistics
acidfile = open(sys.argv[4], 'w')
for term, freq in acidstat:
    acidfile.write(str(freq).ljust(7) + ' ' + term + '\n')

# Write salt statistics
saltfile = open(sys.argv[5], 'w')
for term, freq in saltstat:
    saltfile.write(str(freq).ljust(7) + ' ' + term + '\n')

# Print terms and freqs
for term, freq in termstat: 
    if term in rmvset:
        print str(freq).ljust(7), 'RMV'.ljust(6), term
    else:
        print str(freq).ljust(7), '   '.ljust(6), term


