# Define vars
WORKDIR=$NANODEV/chem-files/chem-terms
class_file=$WORKDIR/chebi_term_all.out
term_file=$WORKDIR/terms/chebi_terms_version_0.txt
exe_file=$WORKDIR/extract_chebi_terms.py

# Run script to extract chebi terms
time python $exe_file $class_file > $term_file


