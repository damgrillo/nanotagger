import sys
import re
import string
from collections import deque

####################################################################################
##################################### FUNCTIONS ####################################
####################################################################################

# Load terms from file. Load only those terms which are associated with some disease
def load_terms_with_disease(fileterm):
    termset = set([])
    for line in fileterm:
        fields = line.split('\t')
        if len(fields) > 2:
            drug = fields[1].strip()
            disease = fields[2].strip()
            if (len(drug) > 0) and (len(disease) > 0):
                for dsplit in re.split('[;&/\+]', drug):
                    npatt = len(re.findall('[0-9]{2,}', dsplit))
                    upatt = len(re.findall('[A-Z]{2,}', dsplit))
                    nlett = len([ch for ch in dsplit if ch in string.letters])
                    if (npatt == 0) and (upatt == 0) and (nlett > 3):
                        termset.add(dsplit.strip().lower())
    return termset

# Load terms from file. Load only those terms which are not associated with disease
def load_terms_wout_disease(fileterm):
    termset = set([])
    for line in fileterm:
        fields = line.split('\t')
        if len(fields) > 2:
            drug = fields[1].strip()
            disease = fields[2].strip()
            if (len(drug) > 0) and (len(disease) == 0):
                for dsplit in re.split('[;&/\+]', drug):
                    nlett = len([ch for ch in dsplit if ch in string.letters])
                    if nlett > 3:
                        termset.add(dsplit.strip().lower())
    return termset

# Find drugs in text
def find_drugs(sentlist, drugset):
    druglist = []
    for sent in sentlist:
        toks = deque(re.findall('[a-zA-Z]+|\d+|[^\w]', sent))
        while len(toks) > 0:
            match = False
            subtoks = list(toks)
            while (len(subtoks) > 0) and not match:
                term = ''.join(subtoks)
                if term.lower() in drugset:
                    druglist.append(term)
                    match = True
                else:    
                    subtoks.pop()
            num_erase = max(1, len(subtoks))
            for idx in range(num_erase):
                toks.popleft()
    return druglist

# Count elems
def count_elems(elemlist):
    count = {}
    for elem in elemlist:
        if elem in count:
            count[elem] += 1
        else:
            count[elem] = 1
    return count


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = drug term file
# sys.argv[2] = input text file

# Load terms
fileterm = open(sys.argv[1], 'r')
terms_with_disease = load_terms_with_disease(fileterm)
fileterm = open(sys.argv[1], 'r')
terms_wout_disease = load_terms_wout_disease(fileterm)

# Load text
textfile = open(sys.argv[2], 'r')
sentlist = [sent for line in textfile for sent in re.split('\. |, |; ', line.strip().lower())][:1000]

# Term statistics
countwith = count_elems(find_drugs(sentlist, terms_with_disease))
countwout = count_elems(find_drugs(sentlist, terms_wout_disease))
repswith = count_elems([term.split()[0] for term in terms_with_disease if len(term.split()[0]) > 2])
repswout = count_elems([term.split()[0] for term in terms_wout_disease if len(term.split()[0]) > 2])

# Sort terms by frequency
sortwith  = sorted(countwith.items(), key=lambda tup: tup[1], reverse=True)
sortwout  = sorted(countwout.items(), key=lambda tup: tup[1], reverse=True)

# Print terms and freqs with disease
print 'With disease'
for term, freq in sortwith: 
    print str(freq).ljust(7), term
print

# Print terms and freqs without disease
print 'Wout disease'
for term, freq in sortwout:
    if term not in countwith:
        print str(freq).ljust(7), term
print

# Print terms with disease with repeated initial term
print 'With disease - With repeated initial term'
for term in sorted(terms_with_disease):
    init = term.split()[0]
    if (init in repswith) and (init not in terms_with_disease) and (repswith[init] > 1):
        print term
print

# Print terms without disease with repeated initial term
print 'Wout disease - With repeated initial term'
for term in sorted(terms_wout_disease):
    init = term.split()[0]
    if (init in repswout) and (init not in terms_with_disease) and (repswout[init] > 1):
        print term
print


