import sys
import re
import string

####################################################################################
################################### MAIN PROGRAM ###################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = add terms file
# sys.argv[2] = remove terms file
# sys.argv[3] = drug terms file

# Load add terms
addfile = open(sys.argv[1], 'r')
addset = set([line.strip() for line in addfile])

# Load remove terms
rmvfile = open(sys.argv[2], 'r')
rmvset = set([line.strip() for line in rmvfile])

# Load drug terms
drugfile = open(sys.argv[3], 'r')
drugset = set([line.strip() for line in drugfile])

# Final drug list       
drugset.update(addset)
fdruglist = sorted([term for term in drugset if term not in rmvset])

# Print terms
for term in fdruglist:
    print term
            
