import sys
import re
import string

####################################################################################
##################################### FUNCTIONS ####################################
####################################################################################

# Load terms from file. Load only those terms which are associated with some disease
def load_terms(fileterm):
    termset = set([])
    for line in fileterm:
        fields = line.split('\t')
        if len(fields) > 2:
            drug = fields[1].strip()
            disease = fields[2].strip()
            if (len(drug) > 0) and (len(disease) > 0):
                for dsplit in re.split('[;&/\+]', drug):
                    npatt = len(re.findall('[0-9]{2,}', dsplit))
                    upatt = len(re.findall('[A-Z]{2,}', dsplit))
                    nlett = len([ch for ch in dsplit if ch in string.letters])
                    if (npatt == 0) and (upatt == 0) and (nlett > 0):
                        termset.add(dsplit.strip().lower())
    return termset

####################################################################################
################################### MAIN PROGRAM ###################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = drug terms file

# Load terms
fileterm = open(sys.argv[1], 'r')
termlist = sorted(load_terms(fileterm))
       
# Print terms
for term in termlist:
    if (len(term) > 2) and (term[0] not in string.punctuation) and (term.find('therapy') < 0):
        print term
            

