# Define vars
WORKDIR=$NANODEV/drug-files
inp_file=$NANODEV/abstract-corpus/texts/pubmed_abstracts_all.xml
drug_file=$WORKDIR/TTD_drug_disease.txt
out_file=$WORKDIR/terms/term_stats.txt
exe_file=$WORKDIR/term_stats.py

# Run script to analyze term statistics
time python $exe_file $drug_file $inp_file > $out_file


