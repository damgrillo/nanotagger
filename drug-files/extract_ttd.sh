# Define vars
WORKDIR=$NANODEV/drug-files
drug_file=$WORKDIR/TTD_drug_disease.txt
term_file=$WORKDIR/terms/ttd_terms.txt
exe_file=$WORKDIR/extract_ttd.py

# Run script to extract drug terms
time python $exe_file $drug_file > $term_file


