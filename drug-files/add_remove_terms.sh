# Define vars
WORKDIR=$NANODEV/drug-files
add_file=$WORKDIR/terms/add_terms.txt
rmv_file=$WORKDIR/terms/remove_terms.txt
term_file=$WORKDIR/terms/ttd_terms.txt
drug_file=$WORKDIR/terms/drug_terms.txt
exe_file=$WORKDIR/add_remove_terms.py

# Run script to extract add and remove terms
python $exe_file $add_file $rmv_file $term_file > $drug_file


