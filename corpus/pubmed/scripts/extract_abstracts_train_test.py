import re
import sys
import random
import string

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input abstract file (all)
# sys.argv[2] = train abstract file (selection)
# sys.argv[3] = test abstract file (selection)

# Load abstracts and shuffle
textlist = []
filetext = open(sys.argv[1], 'r')
for text in filetext:
    textlist.append(text)
random.shuffle(textlist)

# Select 25 texts for each pattern for training (total = 100)
# Select 125 texts for each pattern for testing (total = 500)
trainlist = []
testlist  = []
maxtrain = 25
maxtest  = 125
doctype = ['train', 'test'] 
patterns = ['poly\[', 'poly\(', 'poly-', 'poly\w+']
for patt in patterns:
    ntext = 0
    for dt in doctype:              
        selectlist = [] 
        if dt == 'train':
            maxdocs = maxtrain
        else:
            maxdocs = maxtest 
        while (ntext < len(textlist)) and (len(selectlist) < maxdocs):
            text = textlist[ntext]
            matchlist = re.findall(patt, text)
            if (len(matchlist) > 0):
                selectlist.append(text)
                textlist.pop(ntext)
            else:
                ntext += 1

        # Add selected documents to corresponding list
        if dt == 'train':
            trainlist.extend(selectlist)
        else:
            testlist.extend(selectlist)

# Shuffle lists and write selected abstracts
random.shuffle(trainlist)
random.shuffle(testlist)
traintext = open(sys.argv[2], 'w')
testtext  = open(sys.argv[3], 'w')
for text in trainlist: 
    traintext.write(text)
for text in testlist: 
    testtext.write(text)

