# Define vars
WORKDIR=$NANODEV/abstract-corpus
inp_file=$WORKDIR/texts/pubmed_abstracts_all.xml
train_file=$WORKDIR/texts/pubmed_abstracts_train.xml
test_file=$WORKDIR/texts/pubmed_abstracts_test.xml
exe_file=$WORKDIR/scripts/extract_abstracts_train_test.py

# Run script to extract random selected abstracts
#time python $exe_file $inp_file $train_file $test_file

