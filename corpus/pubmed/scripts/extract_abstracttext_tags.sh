# Define vars
WORKDIR=$NANODEV/abstract-corpus

# Extract AbstractText content for PubMed All
xml_file=$WORKDIR/texts/pubmed_abstracts_all.xml
txt_file=$WORKDIR/texts/pubmed_abstracts_all.txt
cat $xml_file > $txt_file
sed -i 's/<[/]\?AbstractText[^>]*>//g' $txt_file

# Extract AbstractText content for PubMed Train
xml_file=$WORKDIR/texts/pubmed_abstracts_train.xml
txt_file=$WORKDIR/texts/pubmed_abstracts_train.txt
cat $xml_file > $txt_file
sed -i 's/<[/]\?AbstractText[^>]*>//g' $txt_file

 
# Extract AbstractText content for PubMed Test
xml_file=$WORKDIR/texts/pubmed_abstracts_test.xml
txt_file=$WORKDIR/texts/pubmed_abstracts_test.txt
cat $xml_file > $txt_file
sed -i 's/<[/]\?AbstractText[^>]*>//g' $txt_file


