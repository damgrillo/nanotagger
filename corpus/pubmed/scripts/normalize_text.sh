# Define vars
WORKDIR=$NANODEV/corpus/pubmed

# Normalizing Greek symbols
echo 'Normalizing greek symbols'
sed -i 's/α/alpha/g' $1
sed -i 's/β/beta/g' $1
sed -i 's/γɣΓ/gamma/g' $1
sed -i 's/δΔ/delta/g' $1
sed -i 's/[єεɛƐϵ]/epsilon/g' $1
sed -i 's/ζ/zeta/g' $1
sed -i 's/η/eta/g' $1
sed -i 's/θΘ/theta/g' $1
sed -i 's/κк/kappa/g' $1
sed -i 's/λΛ/lambda/g' $1
sed -i 's/μµ/mu/g' $1
sed -i 's/ν/nu/g' $1
sed -i 's/ξ/xi/g' $1
sed -i 's/πΠ/pi/g' $1
sed -i 's/ρ/rho/g' $1
sed -i 's/σΣ/sigma/g' $1
sed -i 's/τ/tau/g' $1
sed -i 's/φΦϕ/phi/g' $1
sed -i 's/χ/chi/g' $1
sed -i 's/ψΨ/psi/g' $1
sed -i 's/ωΩΩ/omega/g' $1

# Normalize non-ascii letters
echo 'Normalizing non-ascii letters'
sed -i 's/[áàåäã]/a/g' $1
sed -i 's/[ÅÂÄ]/A/g' $1
sed -i 's/Β/B/g' $1
sed -i 's/[ćčç]/c/g' $1
sed -i 's/Č/C/g' $1
sed -i 's/Đ/D/g' $1
sed -i 's/[éèĕê]/e/g' $1
sed -i 's/[íï]/i/g' $1
sed -i 's/К/K/g' $1
sed -i 's/[ńňñ]/n/g' $1
sed -i 's/[óôö]/o/g' $1
sed -i 's/ß/b/g' $1
sed -i 's/ţ/t/g' $1
sed -i 's/Т/T/g' $1
sed -i 's/ü/u/g' $1
sed -i 's/Ζ/Z/g' $1

# Normalize non-ascii numbers
echo 'Normalizing non-ascii numbers'
sed -i 's/⁰/0/g' $1
sed -i 's/¹/1/g' $1
sed -i 's/²/2/g' $1
sed -i 's/³/3/g' $1
sed -i 's/⁴/4/g' $1
sed -i 's/⁵/5/g' $1
sed -i 's/⁶/6/g' $1
sed -i 's/⁷/7/g' $1
sed -i 's/⁸/8/g' $1
sed -i 's/⁹/9/g' $1
sed -i 's/½/1\/2/g' $1

# Normalize other non-ascii symbols
echo 'Normalizing other non-ascii symbols'
sed -i "s/×/x/g" $1
sed -i "s/≠/=!/g" $1
sed -i "s/≥/>=/g" $1
sed -i "s/≤/<=/g" $1
sed -i "s/°/ /g" $1
sed -i "s/±/+\/-/g" $1
sed -i "s/[⁻¯]/-/g" $1
sed -i "s/⁺/+/g" $1
sed -i "s/´/'/g" $1
sed -i "s/″/''/g" $1
sed -i "s/.̊/./g" $1
sed -i "s/.̇/./g" $1
sed -i "s/©/C/g" $1
sed -i "s/®/R/g" $1
sed -i "s/™/TM/g" $1
sed -i "s/→/->/g" $1
sed -i "s/∼/~/g" $1
sed -i "s/≳/>/g" $1
sed -i "s/񥌈/ /g" $1
sed -i "s// /g" $1
sed -i "s/∝/~/g" $1

