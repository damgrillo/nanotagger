# Define vars
WORKDIR=$NANODEV/abstract-corpus
down_file=$WORKDIR/pubmed_result.tar.gz
abs_file=$WORKDIR/texts/pubmed_abstracts_all.xml

# Extract pubmed xml from tar.gz file 
echo 'Extracting pubmed xml file'
tar -O -xzf $down_file > aux.xml

# Extract AbstractText from ex
echo 'Extracting <AbstractText> line ...'
grep '<AbstractText>' aux.xml > $abs_file
echo 'Extracting <AbstractText NlmCategory="UNLABELLED"> line ...'
grep '<AbstractText NlmCategory="UNLABELLED">' aux.xml >> $abs_file 

# Remove all whitespaces at the beginning 
sed -i 's/[ ]*<Abstract/<Abstract/g' $abs_file

# Replace non recognized xml characters
echo 'Replacing non recognized xml characters'
sed -i 's/&lt;/</g' $abs_file
sed -i 's/&gt;/>/g' $abs_file
sed -i 's/&quot;/"/g' $abs_file
sed -i "s/&apos;/'/g" $abs_file

# Normalize patterns for poly[ and poly(
echo 'Normalizing patterns poly[ and poly('
sed -i 's/Poly/poly/g' $abs_file
sed -i 's/poly[[:space:]]*\[/poly\[/g' $abs_file
sed -i 's/poly[[:space:]]*(/poly(/g' $abs_file

# Remove aux file
rm aux.xml


