# Define vars
WORKDIR=$NANODEV/chebi-files
SPARQL=$WORKDIR/run_sparql.sh
CHDBASE=$WORKDIR/queries/chebi.ttl
all_rq=$WORKDIR/queries/chebi_term_all.rq
all_out=$WORKDIR/output/chebi_term_all.out
synon_rq=$WORKDIR/queries/chebi_term_synon.rq
synon_out=$WORKDIR/output/chebi_term_synon.out
atoms_rq=$WORKDIR/queries/chebi_term_atoms.rq
atoms_out=$WORKDIR/output/chebi_term_atoms.out
drugs_rq=$WORKDIR/queries/chebi_term_drugs.rq
drugs_out=$WORKDIR/output/chebi_term_drugs.out

## Run SPARQL queries to extract all terms
#echo 'Extracting all terms...'
#time $SPARQL $CHDBASE $all_rq $all_out

## Run SPARQL queries to extract synonyms
#echo 'Extracting synonyms...'
#time $SPARQL $CHDBASE $synon_rq $synon_out

## Run SPARQL queries to extract atoms
#echo 'Extracting atoms...'
#time $SPARQL $CHDBASE $atoms_rq $atoms_out

## Run SPARQL queries to extract drugs
#echo 'Extracting drugs...'
#time $SPARQL $CHDBASE $drugs_rq $drugs_out

