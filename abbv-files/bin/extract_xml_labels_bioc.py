import sys
import re
import string
import xml.dom.minidom

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = tagged corpus file
# sys.argv[2] = untagged corpus file

# Load each document from corpus file and save its content in untagged corpus file 
untagfile = open(sys.argv[2], 'w')
corp = xml.dom.minidom.parse(sys.argv[1])
docs = corp.getElementsByTagName('document')
for doc in docs:
    for txt in doc.getElementsByTagName('text'):
        if txt.parentNode.tagName == 'passage':
            untagfile.write(txt.firstChild.nodeValue.encode('utf-8') + '\n')

# Extract abbreviations and save short and long forms
sforms = []
lforms = []
for doc in docs:
    for abbv in doc.getElementsByTagName('annotation'):
        if abbv.hasAttribute('id') and (abbv.getAttribute('id').find('SF') >= 0):
            for text in abbv.getElementsByTagName('text'):
                sforms.append(text.firstChild.nodeValue.encode('utf-8'))
        if abbv.hasAttribute('id') and (abbv.getAttribute('id').find('LF') >= 0):
            for text in abbv.getElementsByTagName('text'):
                lforms.append(text.firstChild.nodeValue.encode('utf-8'))

# Print abbreviations
for idx in range(len(sforms)):
    res = sforms[idx] + '\t' + lforms[idx]
    print res   

