# Define vars
WORKDIR=$NANODEV/abbv-tagger
exe_file=$WORKDIR/bin/optimize_parameters.py
temp_file=$WORKDIR/results/performance.txt

# Run optimization program for Biotext and calculate Recall, Precision, F1
abs_file=$WORKDIR/corpus/biotext/texts/biotext_corpus.xml
gold_file=$WORKDIR/corpus/biotext/labels/biotext_labels_gold_standard.txt
python $exe_file $abs_file $gold_file $temp_file

# Run optimization program for Medstract and calculate Recall, Precision, F1
abs_file=$WORKDIR/corpus/medstract/texts/medstract_corpus.txt
gold_file=$WORKDIR/corpus/medstract/labels/medstract_labels_gold_standard.txt
python $exe_file $abs_file $gold_file $temp_file

rm $temp_file
