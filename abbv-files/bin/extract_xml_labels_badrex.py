import sys
import re
import string
import xml.dom.minidom

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = labeled corpus file
# sys.argv[2] = label for short forms
# sys.argv[3] = label for long forms

# Load tags from text file
doc = xml.dom.minidom.parse(sys.argv[1])
sfnodes = doc.getElementsByTagName('Abbrev')
lfnodes  = doc.getElementsByTagName('Term')

# Copy node values in lists and print short and long forms
sforms = []
lforms = []
for node in sfnodes:
    val = str(node.firstChild.nodeValue).replace('\n', ' ')
    sforms.append(val)
for node in lfnodes:
    val = str(node.firstChild.nodeValue).replace('\n', ' ')
    lforms.append(val)
for idx in range(len(sforms)):
    res = sforms[idx] + '\t' + lforms[idx]
    idx += 1
    print res   

