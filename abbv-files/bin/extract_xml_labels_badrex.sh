# Define vars
WORKDIR=$NANODEV/abbv-tagger
exe_file=$WORKDIR/bin/extract_xml_labels_badrex.py

# Extract short and long form abbrev from BioC-BioADI
badrex_file=$WORKDIR/corpus/bioc-bioadi/texts/bioadi_corpus_badrex.xml
badrex_tags=$WORKDIR/corpus/bioc-bioadi/labels/bioadi_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags

# Extract short and long form abbrev from BioC-Ab3P
badrex_file=$WORKDIR/corpus/bioc-ab3p/texts/ab3p_corpus_badrex.xml
badrex_tags=$WORKDIR/corpus/bioc-ab3p/labels/ab3p_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags

# Extract short and long form abbrev from BioC-SH
badrex_file=$WORKDIR/corpus/bioc-sh/texts/sh_corpus_badrex.xml
badrex_tags=$WORKDIR/corpus/bioc-sh/labels/sh_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags

# Extract short and long form abbrev from Medstract
badrex_file=$WORKDIR/corpus/medstract/texts/medstract_corpus_badrex.xml
badrex_tags=$WORKDIR/corpus/medstract/labels/medstract_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags

# Extract short and long form abbrev from PubMed train abstracts
badrex_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_train_labeled_badrex.xml
badrex_tags=$WORKDIR/corpus/pubmed/labels/pubmed_train_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags

# Extract short and long form abbrev from PubMed test abstracts
badrex_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_test_labeled_badrex.xml
badrex_tags=$WORKDIR/corpus/pubmed/labels/pubmed_test_labels_badrex.txt
time python $exe_file $badrex_file > $badrex_tags  
 


