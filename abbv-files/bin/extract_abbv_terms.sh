# Define vars
WORKDIR=$NANODEV/abbv-files

########### Extract abbreviations using Ab3P-v1.5 ################# 
echo '# Extracting abbreviations with Ab3P-v1.5 method'
exe_dir=~/Software/Ab3P-v1.5
cd $exe_dir

# Extract abbreviations from BioC-BIOADI corpus
echo 'BIOADI Corpus..'
abs_file=$WORKDIR/corpus/bioc-bioadi/texts/bioadi_corpus.txt
term_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file

# Extract abbreviations from BioC-Ab3P corpus
echo 'AB3P Corpus...'
abs_file=$WORKDIR/corpus/bioc-ab3p/texts/ab3p_corpus.txt
term_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file

# Extract abbreviations from BioC-SH corpus
echo 'Schwartz-Hearst Corpus...'
abs_file=$WORKDIR/corpus/bioc-sh/texts/sh_corpus.txt
term_file=$WORKDIR/results/bioc-sh/sh_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file

# Extract abbreviations from Medstract corpus
echo 'Medstract Corpus...'
abs_file=$WORKDIR/corpus/medstract/texts/medstract_corpus.txt
term_file=$WORKDIR/results/medstract/medstract_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file

# Extract abbreviations from Pubmed Train corpus
echo 'Pubmed Train Corpus...'
abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_train.txt
term_file=$WORKDIR/results/pubmed/pubmed_train_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file

# Extract abbreviations from Pubmed Test corpus
echo 'Pubmed Test Corpus...'
abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_test.txt
term_file=$WORKDIR/results/pubmed/pubmed_test_labels_ab3p.txt
time $exe_dir/identify_abbr $abs_file | grep '|' | sed 's/  //g' | sed 's/|/\t/g' > $term_file


########### Extract abbreviations using our method ################# 
echo '# Extracting abbreviations with our method'
exe_file=$WORKDIR/bin/extract_abbv_terms.py
tagger_dir=$NLP/nano-tagger/dev/genia-files/postaggers
chunker_dir=$NLP/nano-tagger/dev/genia-files/chunkers
pytagger='brill_rubt_tagger.pickle'
pychunker='maxent_chunker.pickle'
cd $WORKDIR

# Extract abbreviations from BioC-BIOADI corpus
echo 'BIOADI Corpus..'
abs_file=$WORKDIR/corpus/bioc-bioadi/texts/bioadi_corpus.txt
term_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

# Extract abbreviations from BioC-Ab3P corpus
echo 'AB3P Corpus...'
abs_file=$WORKDIR/corpus/bioc-ab3p/texts/ab3p_corpus.txt
term_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

# Extract abbreviations from BioC-SH corpus
echo 'Schwartz-Hearst Corpus...'
abs_file=$WORKDIR/corpus/bioc-sh/texts/sh_corpus.txt
term_file=$WORKDIR/results/bioc-sh/sh_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

# Extract abbreviations from Medstract corpus
echo 'Medstract Corpus...'
abs_file=$WORKDIR/corpus/medstract/texts/medstract_corpus.txt
term_file=$WORKDIR/results/medstract/medstract_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

# Extract abbreviations from Pubmed Train corpus
echo 'Pubmed Train Corpus...'
abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_train.txt
term_file=$WORKDIR/results/pubmed/pubmed_train_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

# Extract abbreviations from Pubmed Test corpus
echo 'Pubmed Test Corpus...'
abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_test.txt
term_file=$WORKDIR/results/pubmed/pubmed_test_labels_myabbv.txt
time python $exe_file $abs_file $tagger_dir $pytagger $chunker_dir $pychunker > $term_file

## Extract abbreviations from Pubmed All corpus
#abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_all.txt
#term_file=$WORKDIR/results/pubmed/pubmed_all_myabbv.txt
#time python $exe_file $abs_file > $term_file

