# Define vars
WORKDIR=$NANODEV/abbv-files
exe_file=$WORKDIR/bin/calculate_recall_precision.py

# Calculate recall and precision for BioC-BioADI corpus
gold_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_gold_standard.txt
inp1_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_badrex.txt
out1_file=$WORKDIR/results/bioc-bioadi/bioadi_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_ab3p.txt
out2_file=$WORKDIR/results/bioc-bioadi/bioadi_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/bioc-bioadi/bioadi_labels_myabbv.txt
out3_file=$WORKDIR/results/bioc-bioadi/bioadi_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file

# Calculate recall and precision for BioC-Ab3P corpus
gold_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_gold_standard.txt
inp1_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_badrex.txt
out1_file=$WORKDIR/results/bioc-ab3p/ab3p_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_ab3p.txt
out2_file=$WORKDIR/results/bioc-ab3p/ab3p_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/bioc-ab3p/ab3p_labels_myabbv.txt
out3_file=$WORKDIR/results/bioc-ab3p/ab3p_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file

# Calculate recall and precision for BioC-SH corpus
gold_file=$WORKDIR/results/bioc-sh/sh_labels_gold_standard.txt
inp1_file=$WORKDIR/results/bioc-sh/sh_labels_badrex.txt
out1_file=$WORKDIR/results/bioc-sh/sh_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/bioc-sh/sh_labels_ab3p.txt
out2_file=$WORKDIR/results/bioc-sh/sh_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/bioc-sh/sh_labels_myabbv.txt
out3_file=$WORKDIR/results/bioc-sh/sh_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file

# Calculate recall and precision for Medstract corpus
gold_file=$WORKDIR/results/medstract/medstract_labels_gold_standard.txt
inp1_file=$WORKDIR/results/medstract/medstract_labels_badrex.txt
out1_file=$WORKDIR/results/medstract/medstract_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/medstract/medstract_labels_ab3p.txt
out2_file=$WORKDIR/results/medstract/medstract_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/medstract/medstract_labels_myabbv.txt
out3_file=$WORKDIR/results/medstract/medstract_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file

# Calculate recall and precision for Pubmed Train corpus
#gold_file=$WORKDIR/results/biotext/pubmed_train_labels_gold_standard.txt
gold_file=$WORKDIR/results/pubmed/pubmed_train_labels_myabbv.txt
inp1_file=$WORKDIR/results/pubmed/pubmed_train_labels_badrex.txt
out1_file=$WORKDIR/results/pubmed/pubmed_train_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/pubmed/pubmed_train_labels_ab3p.txt
out2_file=$WORKDIR/results/pubmed/pubmed_train_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/pubmed/pubmed_train_labels_myabbv.txt
out3_file=$WORKDIR/results/pubmed/pubmed_train_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file

# Calculate recall and precision for Pubmed Test corpus
#gold_file=$WORKDIR/results/biotext/pubmed_test_labels_gold_standard.txt
gold_file=$WORKDIR/results/pubmed/pubmed_test_labels_myabbv.txt
inp1_file=$WORKDIR/results/pubmed/pubmed_test_labels_badrex.txt
out1_file=$WORKDIR/results/pubmed/pubmed_test_recall_precision_badrex.txt
inp2_file=$WORKDIR/results/pubmed/pubmed_test_labels_ab3p.txt
out2_file=$WORKDIR/results/pubmed/pubmed_test_recall_precision_ab3p.txt
inp3_file=$WORKDIR/results/pubmed/pubmed_test_labels_myabbv.txt
out3_file=$WORKDIR/results/pubmed/pubmed_test_recall_precision_myabbv.txt
time python $exe_file $gold_file $inp1_file > $out1_file
time python $exe_file $gold_file $inp2_file > $out2_file
time python $exe_file $gold_file $inp3_file > $out3_file


