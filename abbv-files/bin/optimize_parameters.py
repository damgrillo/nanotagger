import sys
import math
import string
import difflib
from extract_abbv_terms import set_parameter
from extract_abbv_terms import extract_abbv_pairs
from calculate_recall_precision import get_performance

##############################################################################################
######################################### MAIN PROGRAM #######################################
##############################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = abstract text file
# sys.argv[2] = gold standard file
# sys.argv[3] = temporary file

# Load text in textlist
textlist = []
filetext = open(sys.argv[1], 'r')
for line in filetext:
    textlist.append(line[:-1])

# Extract abbrev for different sets of parameters and calculate recall, precision and F1
for thold_target in [2.5, 2.6, 2.7]:
    for upper_penalty in [0.5]:
        for lower_penalty in [0.5]:
            for min_range in [1]:
                for max_range in [3]: 
                    set_parameter('MIN_RANGE', min_range)
                    set_parameter('MAX_RANGE', max_range)   
                    set_parameter('UPPER_PENALTY', upper_penalty)
                    set_parameter('LOWER_PENALTY', lower_penalty)
                    set_parameter('THOLD_TARGET', thold_target)

                    # Extract abbreviations from text and copy in temp file            
                    abbvfile = open(sys.argv[3], 'w')
                    for text in textlist:
                        abbvlist = extract_abbv_pairs(text)
                        for pair in abbvlist:
                            spos = pair[0]
                            lpos = pair[1]
                            abbvfile.write(text[spos[0]:spos[1]] + '\t')
                            abbvfile.write(text[lpos[0]:lpos[1]].lower() + '\n')
                    abbvfile.close()

                    # Run calculation of recall, precision, F1
                    print 'MIN_RANGE: ', min_range
                    print 'MAX_RANGE: ', max_range   
                    print 'UPPER_PENALTY: ', upper_penalty
                    print 'LOWER_PENALTY: ', lower_penalty
                    print 'THOLD_TARGET: ', thold_target
                    get_performance(sys.argv[2], sys.argv[3])


