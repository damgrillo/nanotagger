import sys
import math
import string
import difflib

##############################################################################################
######################################### FUNCTIONS ##########################################
##############################################################################################

# Check similarity between strings
def check_similarity(s1, s2):
    similarity_ratio = difflib.SequenceMatcher(None, s1, s2).ratio() 
    return ((s1.find(s2) >= 0) or (s2.find(s1) >= 0)) and (similarity_ratio > 0.65)

# Load annotations
def load_annot(textname, pairs):
    text = open(textname, 'r')
    for line in text:
        abbv_pair = line[:-1].split('\t')
        sf = abbv_pair[0]
        lf = abbv_pair[1].lower()
        if sf in pairs:
            pairs[sf].append(lf)
        else:
            pairs[sf] = [lf]
    for sf in pairs:
        pairs[sf].sort()

# Classify terms
def classify_terms(gold_pairs, extr_pairs, tpos, fpos, fneg, part):

    # Calculate true_pos, false_pos, false_neg, partial
    for gsf in gold_pairs:
        if gsf in extr_pairs:
            idx1 = 0
            idx2 = 0
            glf_list = gold_pairs[gsf]
            elf_list = extr_pairs[gsf]
            while (idx1 < len(glf_list)) and (idx2 < len(elf_list)):
                glf = glf_list[idx1]
                elf = elf_list[idx2]
                if glf == elf:
                    tpos.append([gsf, glf, elf])
                    idx1 += 1
                    idx2 += 1            
                elif check_similarity(glf, elf):
                    part.append([gsf, glf, elf])
                    idx1 += 1
                    idx2 += 1
                else:
                    if glf < elf:
                        fneg.append([gsf, glf])
                        idx1 += 1
                    else:
                        fpos.append([gsf, elf])
                        idx2 += 1
            if idx1 < len(glf_list):
                while idx1 < len(glf_list):
                    glf = glf_list[idx1]
                    fneg.append([gsf, glf])
                    idx1 += 1
            else:
                while idx2 < len(elf_list):
                    elf = elf_list[idx2]
                    fpos.append([gsf, elf])
                    idx2 += 1
        else:
            idx1 = 0
            glf_list = gold_pairs[gsf]
            while idx1 < len(glf_list):
                glf = glf_list[idx1]
                fneg.append([gsf, glf])
                idx1 += 1
            
    # Calculate the remaining false positive terms
    for esf in extr_pairs:
        if esf not in gold_pairs:
            idx2 = 0
            elf_list = extr_pairs[esf]
            while idx2 < len(elf_list):
                elf = elf_list[idx2]
                fpos.append([esf, elf])
                idx2 += 1 

    # Compare false positives and false negatives and determine the presence of other partial matches
    idx1 = 0
    idx2 = 0
    while (idx1 < len(fneg)) and (idx2 < len(fpos)):
        if (fneg[idx1][0] == fpos[idx2][0]) and check_similarity(fneg[idx1][1], fpos[idx2][1]):
            part.append([fneg[idx1][0], fneg[idx1][1], fpos[idx2][1]])
            fneg.pop(idx1)
            fpos.pop(idx2)
            idx2 = 0
        elif idx2 < len(fpos)-1 :
            idx2 += 1
        else: 
            idx1 += 1
            idx2 = 0     


# Get performance
def get_performance(tpos, fpos, fneg, part):

    # Calculate recall and precision
    recall_only_tpos = float(len(tpos)) / (len(tpos) + len(fneg) + len(part))
    precis_only_tpos = float(len(tpos)) / (len(tpos) + len(fpos) + len(part))
    f1_only_tpos = 0.5 * recall_only_tpos + 0.5 * precis_only_tpos
    recall_tpos_part = float(len(tpos) + len(part)) / (len(tpos) + len(fneg) + len(part))
    precis_tpos_part = float(len(tpos) + len(part)) / (len(tpos) + len(fpos) + len(part))
    f1_tpos_part = 0.5 * recall_tpos_part + 0.5 * precis_tpos_part

    print '################################### RESULTS ######################################'
    print 'TPos:       ', len(tpos)
    print 'FPos:       ', len(fpos)    
    print 'FNeg:       ', len(fneg)
    print 'Partial     ', len(part) 
    print ''
    print 'Total_GS:   ', len(tpos) + len(fneg) + len(part)
    print 'Total_Extr: ', len(tpos) + len(fpos) + len(part)
    print
    print '               TPos     TPos+Partial' 
    print 'Recall:     ', '{0:.4f}'.format(recall_only_tpos), ' ', '{0:.4f}'.format(recall_tpos_part)
    print 'Precision:  ', '{0:.4f}'.format(precis_only_tpos), ' ', '{0:.4f}'.format(precis_tpos_part)
    print 'F1-Measure: ', '{0:.4f}'.format(f1_only_tpos), ' ', '{0:.4f}'.format(f1_tpos_part), '\n'
    print '##################################################################################\n\n\n'


##############################################################################################
######################################### MAIN PROGRAM #######################################
##############################################################################################

# Run program
# sys.argv[0] = program itself
# sys.argv[1] = gold standard annotations file
# sys.argv[2] = extraction method annotations file

# Load gold standard annotations
gold_pairs = {}
load_annot(sys.argv[1], gold_pairs)

# Load sform and lform for extracted annotations
extr_pairs = {}
load_annot(sys.argv[2], extr_pairs)

# Classify terms and get performance
tpos = []
fpos = []
fneg = []
part = []
classify_terms(gold_pairs, extr_pairs, tpos, fpos, fneg, part)
get_performance(tpos, fpos, fneg, part)


####################################################################################
############################ TESTING FUNCTION ######################################
####################################################################################


# Show comparison
def show_comparison(gold_pairs, extr_pairs):
    tpos = []
    fpos = []
    fneg = []
    part = []

    # Calculate true_pos, false_pos, false_neg, partial
    for gsf in gold_pairs:
        if gsf in extr_pairs:
            idx1 = 0
            idx2 = 0
            glf_list = gold_pairs[gsf]
            elf_list = extr_pairs[gsf]
            print '\n####################################################################################################'
            print gsf.ljust(25), glf_list  
            print gsf.ljust(25), elf_list 
            while (idx1 < len(glf_list)) and (idx2 < len(elf_list)):
                glf = glf_list[idx1]
                elf = elf_list[idx2]
                if glf == elf:
                    print '\nTPOS (MATCH):'
                    print gsf.ljust(25), glf
                    print gsf.ljust(25), elf
                    tpos.append([gsf, glf, elf])
                    idx1 += 1
                    idx2 += 1            
                elif check_similarity(glf, elf):
                    print '\nPARTIAL (POSSIBLE MATCH):'
                    print gsf.ljust(25), glf
                    print gsf.ljust(25), elf
                    part.append([gsf, glf, elf])
                    idx1 += 1
                    idx2 += 1
                else:
                    if glf < elf:
                        print '\nFNEG (MISSING):'
                        print gsf.ljust(25), glf
                        fneg.append([gsf, glf])
                        idx1 += 1
                    else:
                        print '\nFPOS (WRONG):'
                        print gsf.ljust(25), elf
                        fpos.append([gsf, elf])
                        idx2 += 1
            if idx1 < len(glf_list):
                while idx1 < len(glf_list):
                    glf = glf_list[idx1]
                    print '\nFNEG (MISSING):'
                    print gsf.ljust(25), glf
                    fneg.append([gsf, glf])
                    idx1 += 1
            else:
                while idx2 < len(elf_list):
                    elf = elf_list[idx2]
                    print '\nFPOS (WRONG):'
                    print gsf.ljust(25), elf
                    fpos.append([gsf, elf])
                    idx2 += 1
        else:
            idx1 = 0
            glf_list = gold_pairs[gsf]
            print '\n####################################################################################################'
            print gsf.ljust(20), glf_list              
            glf_list = gold_pairs[gsf]
            while idx1 < len(glf_list):
                glf = glf_list[idx1]
                print '\nFNEG (MISSING):'
                print gsf.ljust(25), glf
                fneg.append([gsf, glf])
                idx1 += 1
            
    # Calculate the remaining false positive terms
    for esf in extr_pairs:
        if esf not in gold_pairs:
            idx2 = 0
            elf_list = extr_pairs[esf]
            print '\n####################################################################################################'
            print esf.ljust(20), elf_list 
            while idx2 < len(elf_list):
                elf = elf_list[idx2]
                print '\nFPOS (WRONG):'
                print esf.ljust(25), elf
                fpos.append([esf, elf])
                idx2 += 1

    # Compare false positives and false negatives and determine the presence of other partial matches
    idx1 = 0
    idx2 = 0
    print '\n####################################################################################################'
    while (idx1 < len(fneg)) and (idx2 < len(fpos)):
        if (fneg[idx1][0] == fpos[idx2][0]) and check_similarity(fneg[idx1][1], fpos[idx2][1]):
            print '\nPARTIAL (POSSIBLE MATCH):'
            print fneg[idx1]
            print fpos[idx2]
            print fneg[idx1][0].ljust(25), fneg[idx1][1]
            print fpos[idx2][0].ljust(25), fpos[idx2][1]
            part.append([fneg[idx1][0], fneg[idx1][1], fpos[idx2][1]])
            fneg.pop(idx1)
            fpos.pop(idx2)
            idx2 = 0
        elif idx2 < len(fpos)-1 :
            idx2 += 1
        else: 
            idx1 += 1
            idx2 = 0   

# Show complete matches
def show_matches(tpos):
    print '\n\n\n################################ COMPLETE MATCHES ##################################################'
    for triplet in tpos:
        print triplet[0].ljust(25), triplet[1]
        print triplet[0].ljust(25), triplet[2]
        print

# Show partial matches
def show_partial(part):
    print '\n\n\n################################ PARTIAL MATCHES ##################################################'
    for triplet in part:
        print triplet[0].ljust(25), triplet[1]
        print triplet[0].ljust(25), triplet[2]
        print

# Show false negatives
def show_fneg(fneg):
    print '\n\n\n################################ FNEG (MISSING TERMS) ##################################################'
    for pair in fneg:
        print pair[0].ljust(25), pair[1]

# Show false positives
def show_fpos(fpos):
    print '\n\n\n################################ FPOS (SPURIOUS TERMS) #################################################'
    for pair in fpos:
        print pair[0].ljust(25), pair[1]

## Show comparison
#show_comparison(gold_pairs, extr_pairs)

## Show complete matches
#show_matches(tpos)

## Show partial matches
#show_partial(part)

## Show false negatives
#show_fneg(fneg)

## Show false negatives
#show_fpos(fpos)

 
