# Define vars
WORKDIR=$NANODEV/abbv-tagger
exe_file=$WORKDIR/bin/extract_xml_labels_bioc.py

# Extract text without tags and Gold Standard abbreviations (BioC-Bioadi)
text_file=$WORKDIR/corpus/bioc-bioadi/texts/bioadi_corpus_gold.xml
untag_file=$WORKDIR/corpus/bioc-bioadi/texts/bioadi_corpus.txt
text_tags=$WORKDIR/corpus/bioc-bioadi/labels/bioadi_labels_gold_standard.txt
time python $exe_file $text_file $untag_file > $text_tags

# Extract text without tags and Gold Standard abbreviations (BioC-Ab3P)
text_file=$WORKDIR/corpus/bioc-ab3p/texts/ab3p_corpus_gold.xml
untag_file=$WORKDIR/corpus/bioc-ab3p/texts/ab3p_corpus.txt
text_tags=$WORKDIR/corpus/bioc-ab3p/labels/ab3p_labels_gold_standard.txt
time python $exe_file $text_file $untag_file > $text_tags

# Extract text without tags and Gold Standard abbreviations (BioC-SH)
text_file=$WORKDIR/corpus/bioc-sh/texts/sh_corpus_gold.xml
untag_file=$WORKDIR/corpus/bioc-sh/texts/sh_corpus.txt
text_tags=$WORKDIR/corpus/bioc-sh/labels/sh_labels_gold_standard.txt
time python $exe_file $text_file $untag_file > $text_tags
