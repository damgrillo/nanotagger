import sys
import re
import string

################################################################################
################################## FUNCTIONS ###################################
################################################################################

bpairs = {'(':')', '[':']'}

# Get bracket contents. Char previous to the bracket must be whitespace
def get_bracket_cont(text):
    pos = 0
    bcont = []
    while pos < len(text):
        bpos = next_bracket_pair(text, pos)
        if (len(bpos) == 2) and (text[bpos[0]-1] in string.whitespace):
            bcont.append(text[bpos[0]:bpos[1]])
            pos = bpos[1]
        else:
            pos = next_bracket_pos(text, pos) + 1
    return bcont

# Next bracket pair position
def next_bracket_pair(text, pos=0):
    bpos = ()
    count = 1
    pos = next_bracket_pos(text, pos)
    if pos < len(text):
        start = pos
        op = text[pos]
        cl = bpairs[op]
        pos += 1
        while (pos < len(text)) and (count > 0):
            if text[pos] == op:
                count += 1
            elif text[pos] == cl:
                count -= 1
            pos += 1
    if count == 0:
        bpos = (start, pos)   
    return bpos

# Next bracket position
def next_bracket_pos(text, pos=0):
    while (pos < len(text)) and (text[pos] not in bpairs):
        pos += 1
    return pos

# Number of uppercase letters in text
def upper_num(text):
    return len([ch for ch in text if ch in string.uppercase])

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = abstracts file
# sys.argv[2] = freq stats file

# Load text and match bracket patterns
textfile = open(sys.argv[1], 'r')
textlist = [line.strip() for line in textfile]
bcontlist = [brack[1:-1] for line in textlist for brack in get_bracket_cont(line) if upper_num(brack) > 0]

# Bracket statistics
countdic = {}
for bcont in bcontlist:
    if bcont not in countdic:
        countdic[bcont] = 1
    else:
        countdic[bcont] += 1       

# Sort bracket content by frequency
sortlist = sorted(countdic.items(), key=lambda tup: tup[1], reverse=True)

# Write bracket content by frequency
statfile = open(sys.argv[2], 'w')
for bcont, freq in sortlist:
    # if re.search('\(\d+\)', bcont) != None:
        statfile.write(str(freq).ljust(7) + ' ' + bcont + '\n')

# Match first 100 most common abbvs in text and print matching lines
for bcont, freq in sortlist[:1500]:
    if bcont.startswith('P'):
        print bcont + ':'
        for line in textlist:
            match = re.search(re.escape('(' + bcont + ')'), line)
            if match != None:
                stpos = max(0, match.start() - 30)
                endpos = match.end()
                print line[stpos:endpos]
        print



