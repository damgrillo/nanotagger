WORKDIR=$NANODEV/abbv-files
exe_file=$WORKDIR/stats/abbv_stats.py
abs_file=$WORKDIR/corpus/pubmed/texts/pubmed_abstracts_all.txt
stat_file=$WORKDIR/stats/abbv_stats.txt
match_file=$WORKDIR/stats/match_abbvs.txt

# Run abbv statistics on All Pubmed Abstracts
time python $exe_file $abs_file $stat_file > $match_file

