EFPO	Educating Future Physicians for Ontario
COFM	Council of Ontario Faculties of Medicine
AMS	Associated Medical Services
sites	five medical schools and two regional health centers
URMs	underrepresented minorities
CME	continuing medical education
CME	continuing medical education
PREP	Physician Review Program
RVUs	relative value units
EBM	Evidence-based medicine
AAMC's	Association of American Medical Colleges'
AMCs	academic medical centers
PBL	problem-based learning
OSCE	objective structure clinical examination
CPA	clinical problem analysis
GME	graduate medical education
StAR	steroidogenic acute regulatory protein
ACTH	acute stimulation by corticotrophin
LH	luteinizing hormone
IDI	iodothyronine deiodinase
LESP	lizard epididymal secretory protein
17beta-HSD2	estrone: microsomal 17beta-hydroxysteroid dehydrogenase type 2
hCG	human chorionic gonadotrophin
CI	confidence interval
PgR	progesterone receptor
ECM	extracellular matrix
CRF-BP	corticotrophin-releasing factor binding protein
NO	nitric oxide
eNOS	endothelial type of NO synthase
RA	Retinoic acid
NaB	Retinoic acid (RA) and sodium butyrate
SV	seminal vesicle
tTGase	tissue transglutaminase
RAR	RA receptor
PRL	prolactin
PLF-RP	proliferin-related protein
PLP-F	prolactin-like protein-F
ASA	anti-sperm
APA	anti-phospholipid
AZA	and antizonal
IVF	in vitro fertilization
ICSI	intracytoplasmic sperm injection
TAT	tray agglutination test
MAR	mixed anti-globulin reaction test
ET	embryo transfer
IGF	insulin-like growth factor
PDGF	platelet-derived growth factor
EGF	epidermal growth factor
FF	follicular fluid
ICSI	intra-cytoplasmic sperm injection
LH	luteinizing hormone
FSH	follicle-stimulating hormone
hMG	human menopausal gonadotrophin
CA	cancer antigen
IVF ET	in vitro fertilization/embryo transfer
LHRH	luteinizing hormone-releasing hormone
hMG	human menopausal gonadotrophin
hCG	human chorionic gonadotrophin
OPU	oocyte pick-up
VEGF	vascular endothelial growth factor
IVF	in vitro fertilization
APCR	activated protein C resistance
APC SR	APC sensitivity ratio
IVF ET	in vitro fertilization embryo transfer
ACA	anti-cardiolipin antibodies
ANA	anti-nuclear antibodies
ds	anti-double-stranded
RF	rheumatoid factor
LAC	lupus anti-coagulant
IL	interleukin
LPS	lipopolysaccharide
OTA	oligoteratoasthenospermic
FCS	fetal calf serum
IFN	interferon
VEGF	vascular endothelial growth factor
ES	endometrial stromal
GnRHa	gonadotropin-releasing hormone agonist
IL	interleukin
AECC	autologous endometrial coculture
IVF	in vitro fertilization
CM	conditioned media
frag	fragmentation
AOS	allene oxide synthase
ABA	abscisic acid
LS	Linsmaier-Skoog
IAA	3-indoleacetic acid
PAL	phenylalanine ammonia-lyase
AIP	2-aminoindan-2-phosphonic acid
S/G	syringyl to guaiacyl
FT-IR	Fourier-transform infrared
SLNB	sentinel lymph node biopsy
not	contemporaneous nonpathway group
QOL	quality-of-life
CT/RT	chemotherapy and radiotherapy
HCR	Histologic complete response
CCR	clinical complete response
SCC	squamous cell carcinoma
H&N	head and neck
END	elective neck dissection
CT	computed tomography
OPM	oropharyngeal motility
SPSS	Swallowing Performance Status Scale
3-D	3-dimensional
SOH	supraomohyoid
SOHND	Supraomohyoid neck dissection
END	endoscopic neck dissection
SLN	sentinel lymph node
Sf9	Spodoptera frugiperda
22K hGH	22 kDa growth hormone
ESI-MS	electrospray ionization mass spectrometry
MAD	multiple wavelength anomalous diffraction
HMDM	human monocyte-derived macrophages
EGFP	enhanced green fluorescent protein
SH	subtractive hybridization
DCH	differential colony hybridization
EXACCT	exonuclease-amplification coupled capture technique
PNA	peptide nucleic acid
CFLP	Cleavase Fragment Length Polymorphism
DGC	dystrophin-glycoprotein complex
alpha-DB	alpha-dystrobrevin
NMJ	neuromuscular junction
MARCM	mosaic analysis with a repressible cell marker
MB	mushroom body
Nb	neuroblast
bHLH	basic helix-loop-helix
CNS	central nervous system
Shh	Sonic hedgehog
bHLH	Basic helix-loop-helix
PDGFRalpha	platelet-derived growth factor receptor alpha
NGF	nerve growth factor
DRG	dorsal root ganglion
POMA	paraneoplastic opsoclonus-myoclonus ataxia
NMDARs	N-methyl-D-aspartate receptors
HMs	hypoglossal motoneurons
OEG	olfactory ensheathing glia
SCN	suprachiasmatic nuclei
SL-M	Stratum lacunosum-moleculare
IPSCs	inhibitory postsynaptic currents
NMDAR	NMDA receptor
HVC	high vocal center
MRI	magnetic resonance imaging
DTI	diffusion tensor imaging
M-CSFR	M-CSF receptor
IGF-1	insulin-like growth factor-1
LTGF-beta 1	latent transforming growth factor beta 1
M6P	mannose-6-phosphate
OP-1	Osteogenic protein-1
BMP-7	bone morphogenetic protein-7
IGF-1R	Insulin-like growth factor-1 receptor
FGF	fibroblast growth factors
FGFR	factors (FGF) are mediated by specific cell membrane receptors
b FGF	basic fibroblast growth factor
PUD	peptic ulcer disease
CI	confidence interval
LR	likelihood ratio
GPs'	general practitioners'
EDI-2	Eating Disorders Inventory
TAS-20	Toronto Alexithymia Scale
ANOVA	analysis of variance
PEFR	peak expiratory flow rate
A&E	and emergency
rate	including reattendances
GP	general practitioner
ACE	angiotensin-converting enzyme
FEV1	forced expiratory volume
ER	emergency room
oc	Osteosclerosis
BAC	bacterial artificial chromosome
L/B/K	Liver/bone/kidney
ALP	alkaline phosphatase
PTH	parathyroid hormone
PTHrP	parathyroid hormone (PTH) and PTH-related peptide
PTH	Parathyroid hormone
TGF-beta	transforming growth factor-beta
PTH	parathyroid hormone
NTx	N-telopeptides of type I collagen
IL-6	interleukin-6
TGF-beta	Transforming growth factor-beta
GH	Growth hormone
GH rats	GH3-bearing rats
ALP	alkaline phosphatase levels
BMD	bone mineral density
BMT	bone marrow transplantation
BMD	Bone mineral density
Ca	calcium
TRAP	Tartrate-resistant acid phosphatase
IGF-I	Insulin-like growth factor I
COLIA1	collagen I alpha 1 gene
BMD	bone mineral density
CI	confidence interval
2-D	two-dimensional
3-D	three-dimensional
SMI	structure model index
DGA	dental general anaesthesia
LA	local anaesthetic
GDPs	general dental practitioners
PE	parietal endoderm
PrE	primitive endoderm
EMT	epithelio-mesenchyme transition
EMI	epithelium-mesenchyme interactions
RPTPs	Receptor Protein-Tyrosine Phosphatases
RARs/RXRs	Retinoids and their multiple receptors
PGCs	primordial germ cells
YSL	yolk syncitial layer
AD	atopic dermatitis
SSLF	skin surface lipid film
SC	stratum corneum
LCs	Langerhans cells
DETCs	dendritic epidermal T cells
IFN	interferon
TNF	tumour necrosis factor
IL	interleukin
CGRP	calcitonin gene-related peptide
TGF	Transforming growth factor
ELISA	enzyme-linked immunosorbent assay
NO	Nitric oxide
NOS	NO synthase
EPF	eosinophilic pustular folliculitis
CTCL	cutaneous T-cell lymphoma
EGF	epidermal growth factor
TGF-alpha	and transforming growth factor-alpha
EGFr	EGF receptor
ECTL	epidermotropic cutaneous T-cell lymphoma
AA	Alopecia areata
MoAb	monoclonal antibody
CIU	chronic idiopathic urticaria
ASST	autologous serum skin test
AD	atopic dermatitis
EPPK	Epidermolytic palmoplantar keratoderma
K9	keratin 9
DSH	dyschromatosis symmetrica hereditaria
NK	natural killer
EBV	Epstein-Barr virus
TCR	T-cell receptor
HPV	human papillomavirus
PCR	polymerase chain reaction
SCC	squamous cell carcinoma
VIN	vulval intraepithelial neoplasia
EQ	Erythroplasia of Queyrat
ALA	5-aminolaevulinic acid
PDT	photodynamic therapy
SSSS	staphylococcal scalded skin syndrome
MRSA	methicillin-resistant Staphylococcus aureus
RPC	Reactive perforating collagenosis
ARPC	the acquired form of RPC
CN	2-Chloracetophenone
K2e	Keratin 2e
EGA	estimated gestational age
hHb1	Human hair keratin basic 1
BM	basement membrane
PKC	protein kinase C
IFN-gamma	Interferon-gamma
IL	interleukin
AD	atopic dermatitis
PMA	phorbol myristate acetate
bp	base pair
NTB	nitrotetrazolium blue chloride
UV	ultraviolet
CP	cicatricial pemphigoid
AD	atopic dermatitis
HDMs	house dust mites
PUVA	psoralen-ultraviolet A
MPD	minimal phototoxic dose
DLQI	Dermatology Life Quality Index
DPU	delayed pressure urticaria
GHQ-28	General Health Questionnaire
SF-36	Short Form 36
GLA	gamma-linolenic acid
IBS	ichthyosis bullosa of Siemens
AeCP	antiepiligrin (laminin 5) cicatricial pemphigoid
BCC	Basal cell carcinoma
SS	Sezary syndrome
CLL	chronic lymphatic leukaemia
CBCL	cutaneous B-cell lymphoma
HV	hydroa vacciniforme
EBV	Epstein-Barr virus
EBER	EBV-encoded small nuclear RNA
BHLF	l-fragment
APS	antiphospholipid syndrome
SA	salicylic acid
SLS	sodium lauryl sulphate
TEWL	Transepidermal water loss
SCI	spinal cord injury
TBI	traumatic brain injury
PTA	post-traumatic amnesia
ATL	adult T-cell leukemia
PCD	plasma cell dyscrasias
MM	multiple myeloma
WM	Waldenstrom's macroglobulinemia
Dex	dexamethasone
IFN-gamma	and interferon-gamma
EORTC	European Organization for Research and Treatment of Cancer
BLPD	B-lymphoproliferative post-transplant disorder
MoAbs	monoclonal antibodies
Ab	Rituximab-MABTHERA Roche
MCL	mantle-cell lymphoma
CR	complete
PR	partial response
TTP	time to disease progression
RR	response rate
FL	follicular lymphoma
MCL	mantle-cell lymphoma
MCL	Mantle-cell lymphoma
FISH	fluorescence in situ hybridization
YACs	yeast artificial chromosomes
PAC	P1-derived artificial chromosome
IgH	immunoglobulin heavy chain gene
beta 2M	beta 2-microglobulin
FFS	failure-free survival
FL	follicular lymphoma
pb	peripheral blood
AT	Ataxia teleangiectasia
NBS	Nijmegen breakage syndrome
NHL	non-Hodgkin's lymphoma
NHL-BFM	non-Hodgkin's lymphoma (NHL)
HD	Hodgkin's disease
NHL	non-Hodgkin's lymphoma
I.L.S.G.	International Lymphoma Study Group
R.E.A.L.	Revised European-American Classification of Lymphoid Neoplasms'
EAHP	European Association of Pathologists
SH	Society for Hematopathology
WHO	World Health Organization
CAC	Clinical Advisory Committee
IPI	International Prognostic Index
BLL	Burkitt-like lymphoma
BL	Burkitt lymphoma
LBCL	large B-cell lymphoma
NHL	non-Hodgkin's lymphomas
PCL	primary cerebral lymphoma
PTLD	Post-transplant lymphoproliferative disorders
SCT	stem-cell transplantation
CLL	chronic lymphocytic leukemia
BMT	bone marrow transplantation
LGL	low-grade lymphoma
CLL	chronic lymphocytic leukemia
CR	complete remission
NHL	non-Hodgkin's lymphoma
NHL	non-Hodgkin's lymphoma
OR	Odds ratios
NHL	non-Hodgkin's lymphomas
HD	Hodgkin's disease
GHSG	German Hodgkin's Lymphoma Study Group
CSS	cause specific survival
C	chemotherapy
U	unpurged
P	purged
AIDS	Acquired immune deficiency syndrome
HIV	human immunodeficiency virus
DPPIV	dipeptidyl peptidase IV
ADA	adenosine deaminase
SDF-1	stromal cell-derived factor-1
HCV	hepatitis C virus
EB	Epstein Barr
HHV-6	human herpesvirus 6
HCV	hepatitis C virus
EBV	Epstein Barr virus
NK	natural killer
CMV	cytomegalovirus
MHC	major histocompatibility complex
HHV-6	Human herpesvirus 6
HHV-7	human herpesvirus 7
ROP	retinopathy of prematurity
group B	grid treatment of the posterior pole (19 bilaterally)
VAS	visual analogue scale
ROP	retinopathy of prematurity
FTMH	full thickness macular holes
tHcy	total homocysteine
GAC	Glasgow acuity card
CIN	congenital idiopathic nystagmus
SDN	sensory defect nystagmus
VEPs	visual evoked potentials
OKN	optokinetic nystagmus
IOP	intraocular pressure
PAC	Primary angle closure
PACG	PAC with glaucoma
VZV	Varicella zoster virus
ARN	acute retinal necrosis
PCR	polymerase chain reaction
tRCEC	transformed rabbit corneal epithelial cells
EMIU	Experimental melanin induced uveitis
EIU	Endotoxin induced uveitis
TUNEL	transferase mediated deoxyuridine triphosphate biotin nick end labelling
iNOS	inducible nitric oxide synthase
TdT	terminal deoxynucleotide transferase
TUNEL	terminal deoxynucleotide transferase (TdT) mediated nick end labelling
TRAP	telomeric repeat amplification protocol
hTR	the telomerase RNA component
SO	sympathetic ophthalmia
NFL	nerve fibre layer
S/N	Superior to nasal
I/N	inferior to nasal
S/I	superior to inferior
5-FU	5-fluorouracil
CIN	conjunctival intraepithelial neoplasia
IOP	intraocular pressure
ECCE	extracapsular cataract extraction
HRP	horseradish peroxidase
GFAP	glial fibrillary acidic protein
EWCL	extended wear contact lens
DCR	dacryocystorhinostomies
scFv	single-chain variable fragment
ORFs	open reading frames
Epo	erythropoietin
SIVs	simian immunodeficiency viruses
MuLV	murine leukemia virus
VSV-G	vesicular stomatitis virus
rAAV	recombinant adeno-associated virus
GFP	green fluorescent protein
ELISA	enzyme-linked immunoabsorbent assay
Ag	antigens
HR	hyperacute rejection
RT1.Aa	recipients 1 day prior to heterotopic ACI
HIS	hyperimmune serum
EBV	Epstein-Barr virus
EBNA-1	Epstein-Barr virus (EBV) nuclear antigen 1
HVJ	hemagglutinating virus of Japan
PSEs	polyion-sensitive electrodes
E0'	equilibrium reduction potential
NO	nitric oxide
SHR	spontaneously hypertensive rats
eNOS	Endothelial NO synthase
ITIES	interface between two immiscible electrolyte solutions
GluR	glutamate receptor
LOD	limits of detection
CE	Capillary electrophoresis
EC	electrophoresis (CE)/electrochemical detection
PED	Pulsed electrochemical detection
IPAD	Integrated pulsed amperometric detection
TFA	trifluoroacetic acids
SSC	S-sulfocysteine
EC	electrochemical detection
ELISA	enzyme-linked immunoassay
GlOx	L-glutamate oxidase
HRP	horseradish peroxidase
FI	flow injection
PEGDGE	Poly(ethylene glycol) diglycidyl ether
HPLC	high-performance liquid chromatographic
LC/ESI/MS/MS	HPLC/electrospray ionization tandem mass spectrometric
MTIC	5-(3-N-methyltriazen-1-yl)-imidazole-4-carboxamide
SRM	selected reaction monitoring
LLOQ	lower limit of quantitation
QC	quality control
CE	capillary electrophoresis
CD	cyclodextrins
CYSP	cysteine peptide
ESI/MS	electrospray ionization mass spectrometry
Lid	Lidocaine
IS	internal standard
MH	Meclizine Hydrochloride
SRCL	specific residual cleaning Level
VLOD	visual limit of detection
RSD	relative standard deviations
TQ	thymoquinone
DTQ	dithymoquinone
THQ	thymohydroquinone
THY	thymol
CE	capillary electrophoresis
SEC	size exclusion chromatography
ET	endothelin
DEX-MPS	dextran-methylprednisolone succinate
MPS	methylprednisolone hemisuccinate
MP	methylprednisolone
HPLC	high performance liquid chromatographic
UV	ultraviolet
CPPD	calcium pyrophosphate dihydrate
SAS	Sleep apnea syndrome
NMR	nuclear magnetic resonance imaging
HFM	hemifacial microsomia
DO	distraction osteogenesis
PA	posteroanterior
