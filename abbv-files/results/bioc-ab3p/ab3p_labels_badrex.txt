TAI	two timed artificial insemination
GA	general anesthesia
MS	multiple sclerosis
5HT	5-hydroxytryptamine
ICG	impedance cardiogram
OCP	oestrogen containing contraceptive pill
EG	ethylene glycol
DEG	diethylene glycol
TNF-sR	TNF soluble receptors
GC-MS	gas chromatography-mass spectrometry
FSE	fast spin-echo
IR	inversion recovery
FSE	fast spin-echo
SE	spin-echo
FS	fat-saturated
aa	amino acid
Ad	adenovirus
MEF cells	mouse embryo fibroblast cells
KOR	Kappa opioid receptors
MCL	mantle cell lymphoma
ALS	amyotrophic lateral sclerosis
PB	peripheral blood
LCM	Lymphocytic choriomeningitis
CZE	capillary zone electrophoresis
IBV	infectious bronchitis virus
ELISA	enzyme-linked immunosorbent assay
PI	postinfection
P less than 0.05	paired samples showed no significant difference
LPS	lipopolysaccharide
SEB	Staphylococcus aureus enterotoxin-B
DSP	deoxyspergualin
D-Gal	D-Galactosamine
NK1R-ir	neurokinin-1 receptor immunoreactivity
RTN/Ppy	retrotrapezoid nucleus/parapyramidal region
DL	difference limen
HNSCC	head and neck squamous cell carcinoma
NREM	non-rapid-eye-movement
TP	tensor palatini
GG	genioglossus
EMG	electromyograph
MEFV	maximum expiratory flow-volume
VC	vital capacity
hEGF	human epidermal growth factor
HCC	hepatocellular carcinoma
gamma IFN	gamma Interferon
PMP22	Peripheral myelin protein 22
DRG	dorsal root ganglion
PG	prostaglandins
Hh	Hedgehog
Smo	serpentine transmembrane protein Smoothened
Ci	Cubitus interruptus
PKA	Protein kinase A
NSCLC	non-small cell lung cancer
NMDA	N-methyl-D-aspartate
LH	luteinizing hormone
NMDA	N-methyl-D-aspartate
MPO	medial preoptic nucleus
AHY	anterior hypothalamic nucleus
VMH	ventromedial hypothalamic nucleus
ARC	arcuate nucleus
NMR	nuclear magnetic resonance
ODC	ornithine decarboxylase
MTX	methotrexate
NCC	Neurocysticercosis
Ca2+	cell populations following sudden
Ca2+	constant conditions the extracellular
Ca2+	cell density at the time of
SNURF	small nuclear RING finger protein
AR	androgen receptor
ZFR	zinc finger region
TNF	tumor necrosis factor
CI	confidence interval
HSV	herpes simplex virus
DSGF	diabetic serum growth factor
MZ	monozygotic
DZ	dizygotic
h2	heritability estimate
CGN	cerebellar granule neurons
Ca2+	caspase-3 induction and diminishes death but does not alter
Ca2+	caspase-3 are induced by a fall in
IIEF	International Index of Erectile Function
BALT	bronchus-associated lymphoid tissue
mMGL	mouse macrophage galactose-/N-acetylgalactosamine-specific-lectin
LC	Langerhans cells
HUVEC	human umbilical vein endothelial cells
ePTFE	expanded polytetrafluoroethylene
NRN	National Research Network
NCEP	National Cholesterol Education Program
ATP	Adult Treatment Panel
CHD	coronary heart disease
LDL	low-density lipoprotein
HDL	high-density lipoprotein
ERP	event-related potentials
HR	heart rate
ATP	adenosine 5'-triphosphate
AILD	angioimmunoblastic lymphadenopathy
IGFBP	insulin-like growth factor binding protein
IGF	insulin-like growth factor
PVG	parietovisceral ganglion
ICVT	Intracranial venous thrombosis
AED	antiepileptic drugs
CENTRAL	Controlled Trials
PP	paired-pulse
APS-R	Almost Perfect Scale-Revised
FFM	Five-Factor Model of Personality
SHBG	sex hormone-binding globulin
E2	estradiol
FSH	follicle-stimulating hormone
LH	luteinizing hormone
Prl	prolactin
DELFIA	dissociation-enhanced lanthanide fluoro immunoassay
CI	confidence interval
OVLT	organum vasculosum of the lamina terminalis
normal	nonpsychiatrically ill
BAC	Bacterial artificial chromosome
CGH	comparative genomic hybridization
CGHa	CGH approach
SKY	spectral karyotyping
HRP	horseradish peroxidase
HRT	hormone replacement therapy
RR	Relative risks
RR	relative risk
CD	Crohn's disease
vitD	vitamin D
DEX	dexamethasone
IL	induce interleukin
HC	healthy controls
IFN	inflammatory Interferon
TNF	Tumor Necrosis Factor
TAP	transporter associated with antigen processing
HCl-MeOH	HCl-methanol
PC	peer counseling
EBF	exclusive breastfeeding
GERD	gastroesophageal reflux disease
OTC	over-the-counter
PPI	proton pump inhibitors
H2RA	histamine H2-receptor antagonists
EO	erosive oesophagitis
NPH	normal pressure hydrocephalus
RCSF	resistance to outflow of cerebrospinal fluid
CRP	Continuous relative phase
RMS	root-mean-square
MRI	magnetic resonance imaging
T3	triiodothyronine
VNO	vomeronasal organ
HRQL	health-related quality of life
SF-36	Study 36-item short form health survey
NGF	nerve growth factor
NLS	nuclear location signal
DPDL	diffuse poorly differentiated lymphocytic lymphoma
DML	diffuse mixed histiocytic lymphocytic
DHL	diffuse histiocytic lymphoma
2.5 years	2.7-4.1 years
QET	quality extinction test
m-PDS	methylprednisolone
FasL	Fas ligand
ARP	Arginine-rich protein
NO	Nitric oxide
eNOS	endothelial NO synthase
iNOS	inducible NO synthase
Ang II	angiotensin II
PES	polyelectrolyte solution
MDA	malondialdehyde
SOD	superoxide dismutase
CABG	coronary artery bypass grafting
ECG	electrocardiogram
OB	oligoclonal IgG bands
CSF	cerebrospinal fluid
CNS	cell response accompanying central nervous system
MS	multiple sclerosis
CIS	clinically isolated syndrome
TiAl6V4	titanium alloy
PLTP	phospholipid transfer protein
HDL	High-density lipoproteins
PLTP	Phospholipid transfer protein
CETP	cholesteryl ester transfer protein
LPS	lipopolysaccharide
MDA	malondialdehyde
BHT	butylated hydroxytoluene
PDX1	pancreatic duodenal homeobox factor-1
Ngn3	Neurogenin3
bHLH	basic helix-loop-helix
PAX6	paired box factor 6
ISL1	Islet-1
iNOS	inducible nitric oxide synthase
SMS	Sheng mai san
NO	nitric oxide
BUN	blood urea nitrogen
cystic hygroma	cervical cystic lymphangioma
NTG	nitroglycerin
HR	heart rate
SVR	systemic vascular resistance
Finapres	Finger arterial pressure was monitored continuously
SV	stroke volume
CO	cardiac output
DAP	diaminopurine
D-GA	D-glucaric acid
CBZ	carbamazepine
PMD	primidone
PB	phenobarbitone
ASA 400	angiograms done using Kodak TRI-X Pan film
EEG	electroencephalogram
JME	juvenile myoclonic epilepsy
VPA	valproate
anti-HCV	Antibody to HCV
DEC	diethylcarbamazine
C-group	control group
E-group	experimental group
PBS	phosphate buffered saline
NCEs	new chemical entities
CYP450	cytochrome P450
PXR	pregnane X receptor
PDT	percutaneous dilational tracheostomy
ICP	intracranial pressure
TMP	trimethylpsoralen
2b	2:3 club sandwich
2c	2:4 bridged dinuclear complex
ASTM	astromicin
CFS	cefsulodin
CPZ	cefoperazone
CAZ	ceftazidime
PIPC	piperacillin
FOM	fosfomycin
EVT	extravillous trophoblasts
VT	villous trophoblasts
CT	cytotrophoblasts
ST	syncytiotrophoblasts
PD	potential difference
Rt	resistance
DOCA	deoxycorticosterone acetate
IN	immunodeficiency virus type 1 integrase
tHcy	total homocysteine
FA	folate
AMPA	alpha-amino-3-hydroxy-5-methyl-4-isoxazolepropionic acid
F8A	factor VIII-associated gene A
tretinoin	trans-retinoic acid
IL-6	interleukin-6
TNF alpha	tumor necrosis factor alpha
AML	acute monoblastic leukemia
ELISA	Enzyme-Linked Immunosorbent Assay
PEG	polyethylene glycol
STZ-D	streptozocin-induced diabetic
PMN	peripheral blood polymorphonuclear leukocytes
CL	chemiluminescence
GHRF	Growth hormone releasing factor
CSF	cerebrospinal fluid
RBC	Ranitidine bismuth citrate
PMR	progressive muscle relaxation
PMR + Cog	PMR plus cognitive therapy
IL-6	Interleukin-6
LHR	luteinizing hormone receptor
MSA	mitosis-specific activator
KN	KNOLLE
LDL	low density lipoproteins
MuSK	muscle-specific tyrosine kinase
MuSK+	MuSK antibodies
CTA	compare CT angiography
MRA	MR angiography
ARN	Acute retinal necrosis
CSII	Continuous subcutaneous insulin infusion
MDI	multiple daily injections
rh	recombinant human
DNA	deoxyribonuleic acid
alpha-MEM	alpha-minimum essential medium
CLIE	Crossed line immunoelectrophoresis
CIE	crossed immunoelectrophoresis
CRIE	crossed radioimmunoelectrophoresis
E9	embryonic day
ms	means of on and off transients
SD	Shine-Dalgarno
RdRp	RNA-dependent RNA polymerase
OnuMV6	O. novo-ulmi mitovirus 6
SCS	structurally novel succinyl-CoA synthetase
CK	creatine kinase
MFH	Mac-1-regulated forkhead
HIV	Human immunodeficiency virus
bHLH	basic helix-loop-helix
RT-PCR	reverse transcription-polymerase chain reaction
Sj	Schistosoma japonicum
MZ	monozygotic
DZ	dizygotic
IZ	isoleucine zipper
EC	embryonal carcinoma
SLI	Somatostatin-like immunoreactivity
RIA	radioimmunoassay
IVC	inferior vena cava
VT	ventricular tachyarrhythmia
MI	myocardial infarction
ACE	angiotensin-converting enzyme
MADIT	Multicenter Automatic Defibrillator Implantation Trial
AAU	acute anterior uveitis
cod	cyclooctadiene
cod	cyclooctadiene
RROLT	reversible round-off linear transformation
MDR	minimum dynamic range
CR	compression ratio
PRD	percentage root-mean-square difference
MAE	maximum amplitude error
AB	anti-I-Ak antibody
APC	antigen-presenting cell
BINP	Bangladesh Integrated Nutritional Program
BINP	Bangladesh Integrated Nutritional Program
DMARD	disease modifying antirheumatic drugs
CYP	Cytochrome P450
IMI	imipramine
PM	putative S-mephenytoin poor metabolizer
EM	extensive metabolizer
AV	atrioventricular
DAP	diazepam
BP	blood pressure
BMI	body mass index
DBP	diastolic blood pressure
ACE	angiotensin-converting enzyme
LOCF	last observation carried forward
SBP	supine systolic blood pressure
bpm	beats per minute
PGBs	Polyglucosan bodies
UPSC	uterine papillary serous carcinoma
UPSC	Uterine papillary serous carcinoma
radiation	referred to as chemoradiation
mbp	mean blood pressure
MHC-II	major histocompatibility complex II
DC	dendritic cells
SAH	subarachnoid hemorrhage
CI	confidence interval
PTCA	percutaneous transluminal coronary angioplasty
PPRF	paramedian pontine reticular formation
PPRF	paramedian pontine reticular formation
CET	Clinical Evaluation Tool
Cyp	cyanopindolol
AC	Alpha coma
TC	theta coma
ATC	alpha-theta coma
MRI	magnetic resonance imaging
OTC	ornithine transcarbamylase
EMCV	encephalomyocarditis virus
MAs	monoclonal antibodies
CBA	competition binding assay
EIA	enzyme immunoassay
HRPO-	horse radish peroxidase
TTE	transthoracic echocardiography
TEE	transesophageal echocardiography
CA	conventional angiography
DSA	digital subtraction angiography
CT	contrast medium
DES	diethylstilbestrol
L1	L1CAM
HPV	hypoxic pulmonary vasoconstriction
PASM	pulmonary artery smooth muscle
IPA	intrapulmonary arteries
T3	thoracic
TRPV-1	transient receptor potential vanilloid receptor-1
FHPD	family history method for DSM-III anxiety and personality disorders
PI	proteinase inhibitor
CTL	cytotoxic T lymphocytes
SPIO-MRI	still unclear whether Super Paramagnetic Iron Oxide-Magnetic Resonance Imaging
HCC	hepatocellular carcinoma
3dHCT	3-phase dynamic helical CT
RMR	resting metabolic rate
RT	resistance-trained
UT	untrained
RPMS	rating of perceived muscle soreness
HFD	Human Figure Drawings
HTP	House-Tree-Person
AVM	angiographically proven cerebral arteriovenous malformations
CNS	central nervous system
mEGF	Merino wethers were given mouse epidermal growth factor
MAP	Mitogen-activated protein
IL-1beta	interleukin 1beta
CPA	cyclopiazonic acid
THC	Tetrahydrocannabinol
ECM	extracellular matrix
Ang	angiotensin
cdc2	cell division control
SHR	spontaneously hypertensive
WKY	Wistar Kyoto
Glu	glutamate
Glu current	Glu elicits a slow outward potassium current
QA	quisqualate
CaM-KII	calmodulin-dependent protein kinase II
NHP	non-human primate
ATD	alpha-1-antitrypsin deficiency
ATD	alpha-1-antitrypsin deficiency
sGC	soluble guanylate cyclase
NO	nitric oxide
PMEC	pulmonary microvascular endothelial cells
HO-1	human heme oxygenase-1
BAL	bronchoalveolar lavage
VMH	ventromedial hypothalamus
VTA	ventral tegmental area
EB	estradiol benzoate
5alpha-red-IR	5alpha-reductase immunoreactivity
GBR-IR	GBR immunoreactivity
ATP	adenosine triphosphate
PAR 2	Protease-activated receptor 2
SAR	seasonal allergic rhinitis
CNS	central nervous system
P3-6	postnatal opossums 3-6 days old
C7	cervical level
NI-35/250	neurite growth-inhibiting proteins
DON	deoxynivalenol
NIV	nivalenol
ZEA	zearalenone
BtA	botulinum toxin-A
WCE	wireless capsule endoscopy
CD	Crohn's disease
IIPs	Idiopathic interstitial pneumonias
CCR7	CC chemokine receptor 7
SCID	severe combined immunodeficiency
bg	beige
pPE	phosphatidylethanolamine plasmalogen
pPC	phosphatidylcholine plasmalogen
LTH	localized tumor hyperthermia
GM-CSF	granulocyte macrophage colony-stimulating factor
RM-1	response in a syngeneic murine model of prostate cancer
AdGMCSF	adenovirus-expressing murine GM-CSF
SDB	sleep-disordered breathing
PSA	prostate-specific antigen
TEV	tobacco etch potyvirus
hAR	human androgen receptor
TIF-2	transcription intermediary factor-2
UASs	upstream activation sites
NAPL	non-aqueous-phase-liquid
HR	heart rate
SCB	silacyclobutane
HWCVD	hot-wire chemical vapor deposition
VUV	vacuum ultraviolet
SPI	single-photon ionization
TOF	time-of-flight
EBV	Epstein-Barr virus
CHAMPUS	Civilian Health and Medical Program of the Uniformed Services
PKC	protein kinase C
MS	Mirizzi syndrome
MS	MS I
MS	MS II
LC	Laparoscopic cholecystectomy
HNF-4	hepatocyte nuclear factor 4
Epo	erythropoietin
HIF-1	hypoxia-inducible factor 1
PA	permeability-surface area product
TBE	tick-borne encephalitis
TBE	tick-borne encephalitis
FCR	Fuji Computed Radiography
FLU	fluoxetine
MRI	magnetic resonance imaging
CT	computerized tomography
HCM	Hypertrophic cardiomyopathy
CPS	Cancer Prevention Study
RR	rate ratios
DES	diethylstilbestrol
CM	cimetidine
BZN	benzene
PCEs	polychromatic erythrocytes
PCOS	polycystic ovarian syndrome
SS	somatostatin
beta-EP	beta-endorphin
Dyn A	dynorphin A
HMG-CoA	hydroxy-3-methylglutaryl-CoA
PIN	prostatic intraepithelial neoplasia
SNP	single nucleotide polymorphisms
IL-10	interleukin-10
HBV	hepatitis B virus
HCC	hepatocellular carcinoma
PCR-SSCP	polymerase chain reaction-single strand conformation polymorphism
pRBCs	parasitized red blood cells
IFN	interferon
IL	interleukin
RT-2	rat glioblastoma model
NOS	nitric oxide synthase
NO	nitric oxide
NHBD	non-heart-beating donors
NO	nitric oxide
QOL	quality of life
SCI	spinal cord injury
SCLC	small-cell lung cancer
HRR	homologous recombination repair
MASCC	Multinational Association for Supportive Care in Cancer
SPME	solid-phase microextraction
GC/ECD	gas chromatography employing an electron capture detector
RH	relative humidity
TSK	Tight skin
GAG	glycosaminoglycans
EF-G	elongation factor G
E site	exit site
AA	amino acids
MFN	metabolic fecal nitrogen
TD	True digestibility
siRNA	Short interfering RNA
RNAi	RNA interference
Ago2	Argonaute2
TRACP	tested in cells stably expressing human tartrate-resistant acid phosphatase
EC	endothelial cells
ECM	extracellular matrix
EPC	endothelial progenitor cells
HMO	health maintenance organization
PPO	preferred provider organization
OR	odds ratio
CI	confidence interval
PBPC	peripheral blood progenitor cells
S. pyogenes	Streptococcus Pyogenes
M-	M protein-negative
M+	M protein-positive
R.D.T.	regular dialytic treatment
r/hCRF	rat/human CRF
BTV	bluetongue virus
MLV	monovalent modified live virus
MAO	monoamine oxidase
IVA-SIV	Ivanovas-Sieve colony
CR	Charles River
VLDL	very-low-density lipoproteins
TG	their triglyceride
HDL	High-density lipoprotein
CRRT	continuous renal replacement therapy
PER	potassium-dextran preservation solution Perfadex
EC	Euro-Collins
IL-8	Interleukin-8
MPO	measure myeloperoxidase
W/D	wet/dry pulmonary weight ratio
PMN	polymorphonuclear neutrophil count
FA	fluorescent-antibody
ASD	atrial septal defects
PLA2	phospholipase A2
SPM	synaptic plasma membranes
TNBS	trinitrobenzene sulfonic acid
PE	phosphatidylethanolamine
PS	phosphatidylserine
RR	relative risk
Cl	confidence interval
CSA	childhood sexual abuse
PFO	patent foramen ovale
PTHrP	parathyroid hormone-related protein
IUDs	intrauterine devices
HIV	human immunodeficiency virus
HUVEC	human umbilical vein endothelial cells
MB	Mallory body
CK-8	cytokeratin-8
COPD	chronic obstructive pulmonary disease
CrBs	Creola bodies
ARF	acute renal failure
u-BMG	Urinary beta 2 microglobulin
u-NAG	urinary N-acetyl-beta-D-glucosaminidase
HUS	hemolytic uremic syndrome
XAS	X-ray absorption spectroscopy
TNF-alpha	Tumor necrosis factor-alpha
wTNF-alpha	wild-type TNF-alpha
sp-PEG-mTNF-alpha-K90R	Site-specifically mono-PEGylated mTNF-alpha-K90R
HAM-D	Hamilton Depression Scale
GDS	Geriatric Depression Scale
MMSE	Mini-Mental State Examination
MS	Multiple sclerosis
LHON	Leber's hereditary optic neuropathy
UVA	ultraviolet A
LED	light-emitting diode
2'dCF	2'deoxycoformycin
TA	Telomerase activity
hTEP-1	human telomerase-associated pretein
hTERT	human telomerase reverse transcriptase
pH	pHo
ENL	Erythema nodosum leprosum
IC	immune complex
TPEFM	Two-photon excitation fluorescence microscopy
ICP-MS	inductively coupled plasma mass spectrometry
LDL	labeling human low density lipoproteins
apoB-100	apolipoprotein B-100
MAbs	monoclonal antibodies
IGF	insulin-like growth factor
IGF	insulin-like growth factor
IGFBP-3	IGF-binding protein-3
SBT/CPZ	sulbactam/cefoperazone
HIV	Human immunodeficiency virus
HIV	human immunodeficiency virus
DAP	day one after parturition
DAI	days two to ten after infection
TM-IclR	Thermotoga maritima TM0065 gene codes for a protein
NA	noradrenaline
SNS	sympathetic nervous system
CES-D	Center for Epidemiologic Studies-Depression Scale
FOS	fructooligosaccharides
MP	Mastoparan
MDA	malondialdehyde
PUFA	polyunsaturated fatty acids
ABC	and specificity of 3 avidin-biotin complex
ASFV	African swine fever virus
ABC-AP	ABC-alkaline phosphatase
ABC-PO	ABC-peroxidase
FA	fluorescent antibody
ERCP	endoscopic retrograde cholangiopancreatography
GCT	Giant cell tumor
ABC	aneurysmal bone cyst
SBC	simple bone cyst
SCE	sister chromatid exchangec
PUVA	plus UVA irradiation
L+	litter size
L-W+	low litter size and high BW
L+W-	low BW
IL-4	Interleukin-4
IL-2	interleukin-2
TNF	tumour necrosis factor
AK	activated killer
MVD	microvascular decompression
TN	trigeminal neuralgia
NGF	nerve growth factor
CI	cold ischemia
SCM	slow correcting gaze movements
VOR	vestibulo-ocular reflex
EDRF	erythroid differentiation-related factor
HI	hypoxia ischemia
ERK	extracellular signal-related kinases
HIF	Hypoxia-inducible factor
HPS	Hermansky-Pudlak Syndrome
aa	amino acids
MRSA	methicillin-resistant Staphylococcus aureus
NICU	neonatal intensive care unit
PFGE	pulsed-field gel electrophoresis
spa	S. aureus protein A
HCW	healthcare workers
N2O	Nitrous Oxide
TEM	transmission electron microscope
TIE	transport of intensity equation
N2O	nitrous oxide
TMJ	temporomandibular joint
input	including the mechanoreceptors
PGs	Polygalacturonases
At2g41850	abscission-related PG
CKD	chronic kidney disease
CLL	chronic lymphocytic leukemia
CLL	chronic lymphocytic leukemia
RE	retinol equivalents
OR	odds ratio
FEV1	forced expiratory volume in 1 s
FVC	forced vital capacity
GIS	Geographic information systems
AIDS	acquired immunodeficiency syndrome
GABA	gamma-aminobutyric acid
ICl	induced chloride current
ED 16	embryonic day 16
QOL	quality of life
ACP	antibiotic control programs
E2	Elevated oestradiol
MI	myocardial infarction
BDI	Beck Depression Inventory
PSSS	Perceived Social Support Scale
SD	Shine-Dalgarno
DR	downstream region
MRI	magnetic resonance imaging
TGF-beta	Transforming growth factor beta
CEPH	Care for Elderly People at Home project
STD	sexually transmitted disease
CYP	Cytochrome P450
DMSO	dimethylsulphoxide
RPHV	right posterior hepatic veins
IPAA	ileal pouch-anal anastomosis
SIRS	systemic inflammatory response syndrome
SIRS	systemic inflammatory response syndrome
LR	likelihood ratios
PPV	positive predictive value
NPV	negative predictive value
WHO	World Health Organization
BMD	bone mineral density
ATD	antithyroid drugs
HCV	hepatitis C virus
EPN	emphysematous pyelonephritis
PCD	percutaneous drainage
IVU	intravenous urography
RP	retrograde pyelography
CT	computed tomography
QOL	quality of life
DQOLS	Dermatology Quality Of Life scale
HMM	hidden Markov models
NG	neuropile glial
CL-EE	coated regenerated cellulose hollow fiber dialyzer
MDA	Malondialdehyde
AGE	advanced glycation end products
GFAp	glial fibrillary acidic protein
MBP	myelin basic protein
CSF	cerebrospinal fluid
ACS	acute coronary syndromes
AGC	acting front-end automatic gain control
CPPD	calcium pyrophosphate dihydrate
PGE2	prostaglandin E2
TNF	tumor necrosis factor
NF1	Neurofibromatosis type 1
NGF	Nerve growth factor
bFGF	basic fibroblast growth factor
EGF	epidermal growth factor
TGF-alpha	transforming growth factor alpha
NSAID	Nonsteroidal anti-inflammatory drugs
MMP	matrix metalloproteinase
COXs	cyclo-oxygenases
TKA	total knee arthroplasty
DSEK	Descemet's stripping endothelial keratoplasty
FEV1	forced expiratory volume in 1s
SD	Standardized FEV1
tract	trachea and main bronchi
NP	nasal polyposis
IL	interleukin
MAOI	mono-amine oxidase inhibitors
sleep	stage 3 and 4 throughout the episode
NREM	non-rapid eye movement
DT	Delirium tremens
SAS	Surface Air System
TJ	tight junctions
PAH	Polycyclic aromatic hydrocarbons
P450	P450
cypress	Cupressus arizonica Greene
MPCE	micronucleated polychromatic erythrocytes
MNCE	micronucleated normochromatic erythrocytes
IL-1beta	interleukin-1beta
MTT	mean transit time
RF	residual fraction
RI	retrograde index
EG	esophagogastrostomy
EIC	esophagoiliocolostomy
HIV	human immunodeficiency virus
AAP	Alzheimer amyloid precursor protein
pro-IL-1 beta	pro-interleukin 1 beta
HIV	human immunodeficiency virus
TMEP	Telangiectasia macularis eruptiva perstans
MV CBCT	megavoltage cone beam computed tomography
LINAC	linear accelerator
AN	anorexia nervosa
OB	obese
ECM	extracellular matrix
TGF-beta1	transforming growth factor-beta1
HVJ	hemagglutinating virus of Japan
CTT	computerized transmission tomography
SSPG	steady-state plasma glucose
NO	nitric oxide
TNF alpha	tumor necrosis factor alpha
IL-6	interleukin-6
IL-10	interleukin-10
SNOMED	Systematized Nomenclature of Medicine
US	Ultrasonographic
DSC	differential scanning calorimetry
PA	phosphoric acid
NRC	non-rinse conditioner
SEM	selected scanning electron microscopy
eEF1A	eukaryotic translation elongation factor 1A
JHSB	Journal of Health and Social Behavior
SB	straightbred
SG	Santa Gertrudis
W/DA	weight/day of age
ADG	average daily gain
NOD	number of days
AV	atrioventricular
ERP	effective refractory period
RDP	ruminally degradable protein
H. pylori	Helicobacter pylori
E-test	Epsilometer test
log P	log n-octanol/water partition coefficients
VIP	Vasoactive intestinal polypeptide
NE	norepinephrine
PD	potential difference
CF	cystic fibrosis
BVD	bovine viral diarrhoea
NCP	Non-cytopathogenic
BVD	bovine viral diarrhoea
CP	cytopathogenic
END	exaltation of Newcastle disease virus
END+	END phenomenon positive
END-	END phenomenon negative
C14	catalyzes the transfer of myristate
GER	gastroesophageal reflux
OSA	obstructive sleep apnea
BIS	bispectral index
TCI	target-controlled infusion
IBW	ideal body weight
FS	fast-spiking
LTS	low threshold-spiking
ePSPs	electrical postsynaptic potentials
IPSP	inhibitory postsynaptic potentials
TCR	T-cell receptor
beta F1	beta chain
Pgp	P-glycoprotein
MRP1	multidrug-resistance-associated protein 1
LRP	lung resistance protein
DS	Down syndrome
TBMN	this basement membrane nephropathy
ECG	electrocardiographic
ES	Embryonic stem
TBI	traumatic brain injury
M2-10B4	marrow stromal fibroblast
BP	blood pressure
USPIOs	Ultrasmall superparamagnetic iron oxides
FISH	fluorescence in-situ hybridization
GOT	glutamate-oxaloacetate transaminase
GPT	glutamate-pyruvate transaminase
ERK	extracellular signal-related kinase
MAPK	mitogen activated protein kinase
IPTG	isopropyl thiogalactoside
SP	substance P
NKA	neurokinin A
NKB	neurokinin B
UBN	upbeat nystagmus
DBN	downbeat nystagmus
HD	hemodialysis
BDI-II	Beck Depression Inventory-II
TAS-20	Toronto Alexithymia Scale
EC	enterochromaffin
HNa	high NaCl diet
NPY	neuropeptide Y
NE	norepinephrine
NO	nitric oxide
DAN	diaminonaphthalene
NAT	naphthotriazole
NBS	N-bromosuccinimide
G-CSF	granulocyte colony-stimulating factor
HD C/T	high-dose cytarabine-containing chemotherapy
GM1	GM1 ganglioside
HPV	high risk human papilloma virus
PCR	polymerase chain reaction
CI	confidence interval
cdks	cyclin-dependent kinases
SRF	serum response factor
SRF	Serum Response Factor
TCF	Ternary Complex Factor
SRE	Serum Response Element
PMA	phorbol myristate acetate
PB	phenobarbital
PHT	phenytoin
CBZ	carbamazepine
VPA	valproate
excitation	elicitation
VOC	volatile organic compounds
VOC	volatile organic compounds
NIH	National Institutes of Health
IEF	Industrial Epidemiology Forum
IEA	International Epidemiological Association
CIOMS	Council of International Organizations of Medical Sciences
SDF-1alpha	stromal cell-derived factor-1alpha
MDV	Marek's disease virus
BV	Bacterial vaginosis
HIV	human immunodeficiency virus
IL	interleukin
TNF	tumor necrosis factor
AOR	adjusted odds ratio
CI	confidence interval
25-OH-CC	25-hydroxycholecalciferol
MTC	Medullary thyroid carcinoma
ICU	intensive care unit
HDU	high dependency unit
FR	food restriction
TAM	Tamoxifen
Egr-1	encoding Early Growth Response-1
siRNA	small interfering RNA
MAP	mitogen-activated protein
PRRSV	porcine reproductive and respiratory syndrome virus
PI-3	parainfluenza type-3
BRSV	bovine respiratory syncytial virus
BVDV	bovine viral diarrhoea virus
IOP	intraocular pressure
group	Group III
LAN	low-altitude natives
MAN	moderate-altitude natives
SCN	suprachiasmatic nuclei
EBV	Epstein-Barr virus
CCE	Clear cell ependymoma
FISH	Fluorescence in situ hybridization
PDL	pulsed dye laser
PWS	port-wine stains
EC 1.11.1.9	erythrocyte glutathione peroxidase
GAD	generalized anxiety disorder
HAMA	Hamilton Anxiety Scale
RA	radial arteries
WWTP	wastewater treatment plants
SPE	solid-phase extraction
LC-MS	liquid chromatography-mass spectrometry
LASs	linear alkylbenzene sulfonates
CDEAs	coconut diethanol amides
NPEOs	nonylphenol ethoxylates
PLE	pressurized liquid extraction
NP	Nonylphenol
PCB	polychlorinated biphenyls
CTL	cytotoxic T lymphocyte
Th	T helper
MHC	major histocompatibility complex
LCMV NP	lymphocytic choriomeningitis virus nucleoprotein
Tgfbr2	type II TGFbeta receptor
PTA	persistent truncus arteriosus
CT	computed tomography
apo	apolipoprotein
rhM-CSF	recombinant human macrophage colony-stimulating factor
WBC	white blood cell
HDL	high density lipoprotein
C-to-U	Cytidine to uridine
rps12	ribosomal protein subunit 12
rps3	ribosomal protein subunit 3
BPS	British Pharmacological Society
BJP	British Journal of Pharmacology
BVDV	bovine virus diarrhoea virus
SNP	single nucleotide polymorphisms
SNP	single nucleotide polymorphisms
LIT	liver infusion tryptose
DA	direct agglutination
DNP	dinitrophenyl
JTB	Jumping translocation breakpoint
JTB	Jumping translocation breakpoint
ADC	AIDS dementia complex
HIV	human immunodeficiency virus
IUD	intrauterine contraceptive device
BWPB	Big White Polish Breed
CRP	cardiac rehabilitation program
CT	computed tomographic
MRI	magnetic resonance imaging
HGPRT	hypoxanthine-guanine phosphoribosyl transferase
HGPRT	hypoxanthine-guanine phosphoribosyl transferase activity
GC-MS	gas chromatography-mass spectrometry
SIM	selected ion mode
IgG	immunoglobulin G
PSA	polysialic acid
beta-HCG	beta-human chorionic gonadotropin
SQV	saquinavir
ZDV	zidovudine
EG	Ester gum
RA	retinoic acid
ALP	alkaline phosphatase
CV	Coefficient of variation
Cl	confidence interval
SVneo	simian virus 40 transcription signals
f-i	firing rate-injected current
PS-OCT	Polarization-sensitive Optical Coherence Tomography
CMV	cytomegalovirus
OAG	of human neutrophils induced by 1-oleoyl-2-acetylglycerol
PMA	phorbol 12-myristate 13-acetate
C-kinase	Ca2+-activated phospholipid-dependent protein kinase
AA	aminoglycosidic antibiotic
GM	gentamicin
OHC	outer hair cells
FDA	Food and Drug Administration
NMR	Nuclear magnetic resonance
BMD	bone mineral density
WDTC	well-differentiated thyroid cancer
OCCA	Ovarian clear cell adenocarcinoma
T-ALL	T cell acute lymphoblastic leukemia
B-ALL	B cell acute lymphoblastic leukemia
mAbs	monoclonal antibodies
PAS	periodic acid Schiff
AP	acid phosphatase
MDR	multidrug resistant
P-gp	P-glycoprotein
ET	Endothelin
IL-6	interleukin-6
BM	bone marrow
DAG	diacylglycerol
OME	otitis media with effusion
D6.1	demonstrated by a monoclonal antibody
FNABs	fine needle aspiration biopsies
MAb	monoclonal antibody
MTAA	melanoma-tumor-associated antigen
A-MuLV	Abelson murine leukemia
NA	noradrenaline
SNGFR	single nephron glomerular filtration rate
AVP	arginine vasopressin
PB	phenobarbital
SST	supersonic transport
SSRI	Selective serotonin reuptake inhibitors
CE	cholesteryl ester
LDL-IC	LDL-containing immune complexes
BE	beta-endorphin
IR-BE	immunoreactive beta-endorphin
AP	anterior pituitary
NIL	neurointermediate lobe of the pituitary
MTN	mesencephalic trigeminal nucleus
DC	diamond-coated
SS	stainless-steel
SEM	scanning electron microscope
Rb1	retinoblastoma
MGUS	monoclonal gammopathies of undetermined significance
MM	multiple myelomas
PCL	plasma cell leukemias
GC	ganglion cells
PLS-2	partial least squares
DMI	dry matter intake
MY	milk yield
MPY	milk protein yield
HBV	hepatitis B virus
anti-HBc	antibody to hepatitis B core antigen
HBsAg	hepatitis B surface antigen
C.A.H.	Chronic Aggressive Hepatitis
C.P.H.	cases Chronic Persistent Hepatitis
C.L.H.	Chronic Lobular Hepatitis
rPHA	reverse passive haemagglutination
REC	radioelectrocomplexing
MVC	maximal voluntary contraction
EMG	Electromyographic signals
MPF	mean power frequency
HD	hemodialysis
MR	magnetic resonance
PKC	protein kinase C
SMC	smooth muscle cells
PMA	PKC stimulator phorbol 12-myristate 13-acetate
PLS	Papillon-Lefevre syndrome
DA	dopamine
CSLM	confocal scanning laser microscopy
TLC	Thin-layer chromatography
RP-HPLC	reversed-phase high-performance liquid chromatography
ATRA	All-trans-retinoic acid
WT	wild type
SID	sudden infant death
TNF-alphaIP1	Tumor necrosis factor-alpha-induced protein 1
TG	triglyceride
RSV	respiratory syncytial virus
SP	Substance P
TPA	tetradecanoylphorbol-13-acetate
GABA	gamma-aminobutyric acid
MDCK	Madin-Darby canine kidney
SLE	systemic lupus erythematosus
EC-SOD	Extracellular-superoxide dismutase
FGAC	fiber glass supported activated carbon
MCL	maximum contaminant level
GAC	granular activated carbon
CPB	cardiopulmonary bypass
ACT	activated clotting time
HMT	heparin management test
PGE1	prostaglandin E1
BK	basal keratinocytes
VLA	very late antigens family
LH-RH	luteinizing hormone-releasing hormone
sys	Symplastic spermatids
CT	computerized tomography
HT	Hashimoto thyroiditis
CIU	chronic idiopathic urticaria
MTF-1	Mouse metal response element-binding transcription factor-1
MT genes	metallothionein genes
EGF	epidermal growth factor
MDA	malondialdehyde
AVE	Abstinence Violation Effect
rIFN-gamma	recombinant interferon gamma
IL-4	interleukin-4
rTNF-alpha	recombinant tumor necrosis factor alpha
HIV	human immunodeficiency virus
NK	natural killer
PB	peripheral blood
BALF	bronchoalveolar lavage fluid
EUS	endoscopic ultrasonography
HSD	hairy shaker disease
MDV	mucosal disease virus/bovine virus diarrhoea
fMRI	functional MRI
RSAI	resting-state activity index
ADHD	Attention deficit hyperactivity disorder
PC IOL	posterior chamber intraocular lens
BPTI	basic pancreatic trypsin inhibitor
HLE	human leukocyte elastase
MRSA	methicillin-resistant Staphylococcus aureus
sCR1	soluble complement receptor type 1
S9	studies were conducted with and without metabolic activation
TRN	transcriptional regulatory network
MXC	methoxychlor
GnRH	gonadotropin-releasing hormone
FA	Fanconi anemia
