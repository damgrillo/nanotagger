ER	endoplasmic reticulum
cortical ER	cerevisiae consists of a reticulum underlying the plasma membrane
nuclear ER	nuclear envelope
WIP	Wiskott-Aldrich Syndrome Protein-Interacting Protein
Myo5p	Myo3/5p
WASP	Wiskott-Aldrich syndrome protein
WIP	WASP-interacting protein
Mti1p	Myosin tail region-interacting protein
dPGM	dependent phosphoglycerate mutase
APC	anaphase-promoting complex
RNase P	Ribonuclease P
SDS-PAGE	sodium dodecyl sulfate-polyacrylamide gel electrophoresis
MALDI	matrix-assisted laser desorption/ionization
HPLC	high-performance liquid chromatography
CPF	cleavage and polyadenylation factor
CPF	components of the yeast
ER alpha	estrogen receptor alpha
LBD	ligand-binding domain
HMS-PCI	high-throughput mass spectrometric protein complex identification
TAP	tandem-affinity purification
RI	recently proposed general inhibitor
PMS	postmeiotic segregation
PKC	protein kinase C
TM	trabecular meshwork
RLC	regulatory light chain
RhoGDI	Rho GDP dissociation inhibitor
PKA	protein kinase A
CPT	camptothecin
helix	helix 3 in yeast cofilin
Hsp	heat-shock protein
Hsp40	Heat-shock protein 40
NO	Nitric oxide
SGA	synthetic genetic array
WASP	Wiskott-Aldrich Syndrome protein
RP	regulatory particle
CSN	COP9 signalosome
MMS	methyl methanesulfonate
INCENP	inner centromere protein
INCENP	inner centromere protein
GTF	general transcription factor
PIC	preinitiation complex
TBP	TATA box binding protein
UAS	upstream activating sequence
TALE	three-amino acid loop extension
BEL1	BELL1
STM	SHOOT MERISTEMLESS
ARK	actin-regulating Ser/Thr kinase
Sir2/3/4	silent information regulator
CPY	carboxypeptidase Y
CNRS	Centre National de la Recherche Scientifique
HIV-1	human immunodeficiency virus type 1
IN	integrase
YPK	Yeast pyruvate kinase
PEP	phosphoenolpyruvate
H46C	His46Cys
H120C	His120Cys
CTD	C-terminal heptapeptide repeat domain
HAD	haloacid dehalogenase
AF-1	activation function 1 domain
domain	domain D
RGS	regulator of G-protein signaling
E2s	enzymes
Ub	ubiquitin
ER	endoplasmic reticulum
HAT	histone acetyltransferase
SAGA	SPT-ADA-GCN5 acetylase
SAP130	spliceosome-associated protein 130
DSB	double-strand break
RNP	ribonucleoprotein
N-BDs	N-binding domains
aa	amino acid
pol II	polymerase II
GED	GTPase effector domain
HFIP	hexafluoroisopropanol
TFE	trifluoroethanol
B55	Balpha
PP2A	protein phosphatase 2A
NF2	neurofibromatosis type 2
Pol III	polymerase III
TFIIIB	transcription initiation factor IIIB
HBx	hepatitis B virus X protein
HSF	heat shock transcription factor
GBB	Gbeta binding
iron	interactions between Mars
PRF	programmed ribosomal frameshifting
hSec3	Human Sec3
GFP	Green fluorescent protein
MDCK	Madin-Darby canine kidney
NTF2	Nuclear transport factor 2
AP	adaptor
hREV1	human REV1
hREV7	Human REV7
SNX1	Sorting nexin 1
PX	Phox homology
TFIIIB	transcription factor IIIB
TBP	TATA-binding protein
GHL	gyrase b/Hsp90/MutL
MMR	mismatch repair
PCNA	proliferating cell nuclear antigen
hFEN-1	human flap endonuclease-1
PCNA	proliferating cell nuclear antigen
hFEN-1	human FEN-1
hPCNA	human PCNA
TAF	TBP-associated factors
MCD	morphogenesis checkpoint dependent
PtdSer	phosphatidylserine
PtdEtn	phosphatidylethanolamine
MCD	morphogenesis checkpoint dependent
GPI	glycosylphosphatidylinositol
TIM	triosephosphate isomerase
ABC	ATP binding cassette
MRP1	multidrug-associated protein
NBD1	nucleotide binding domain
TMD1	transmembrane domain
rDNA	ribosomal DNA
Pol I	polymerase I
HOT1	hot spot
eIF3	eukaryotic initiation factor-3
PCI	proteasome-COP9 signalosome-eIF3
NER	Nucleotide excision repair
TCR	transcribing genes
GS	glucan synthase
GAP	GTPase-activating proteins
WT	wild type
MMR	mismatch repair
HDL	high density lipoprotein
apoE	apolipoprotein E
NF2	neurofibromatosis type 2 gene
sch-1	schwannomin isoform-1
eIF2	eukaryotic initiation factor-2
dsRBDs	dsRNA-binding domains
GCR	Gross chromosome rearrangements
DSB	double-strand breaks
SCR	sister chromatid recombination
sn	small nuclear
psi	pseudouridine
PDI	protein disulphide-isomerase
trx	thioredoxin
GAPDH	glyceraldehyde-3-phosphate dehydrogenase
AP	adaptor protein
NPC	nuclear pore complexes
ubl	ubiquitin-like
MEN	mitotic exit network
DBD	DNA-binding domain
AD	activation domain
TMD	transmembrane domain
CTD	carboxy-terminal domain
COX	cytochrome c oxidase
TBP	TATA-binding protein
SnRKs	Snf1-related protein kinases
SCF	Skp1-cullin-F-box
VPS	vacuolar protein sorting
hVPS11	human VPS11
hVPS18	human VPS18
hVPS11	hVps11
hVPS18	hVps18
CD	Circular dichroism
CDK	cyclin dependent kinases
5-FOA	5-fluoroorotic acid
GFP	green fluorescent protein
LC8	light chain
DGK-zeta	diacylglycerol kinase-zeta
CF I	cleavage factor I
Rpa1	replication protein A
RFC	replication factor C
Tom	the mitochondrial outer membrane
p85-PI3K	phosphoinositide-3-kinase
TBP	TATA-binding protein
PTP	phosphate transport protein
TM	transmembrane
MTP	mitochondrial transport protein
tRNA	Transfer RNA
HisRS	histidyl-tRNA synthetase
CTLs	CD8+ cytotoxic T lymphocytes
DC	dendritic cells
MHC	major histocompatibility complex
NER	nucleotide excision repair
UBA	ubiquitin-associated
ORC	origin recognition complex
HsORC	human ORC
GAL4-dd	GAL4 dimerization domain
NMR	Nuclear magnetic resonance
NIEHS	National Institute of Environmental Health Sciences
CLP	coactosin-like protein
5LO	5-lipoxygenase
F-actin	filamentous actin
HSF	heat shock transcription factor
HSF	heat shock transcription factor
GST	glutathione-S-transferase
MMS	methyl methanesulfonate sulfate
SRK	S receptor kinase
ORC	origin recognition complex
HP-1	heterochromatin protein 1
Orc5	origin recognition complex
SOC	store-operated channels
FBP	F-box proteins
H+	have named 'regulator of the
S4	site mutations G371A
Trax	translin-associated factor X
Trax	Translin-associated factor X
Mts1	metastasis-associated protein S100A4
SNAP	Soluble NSF Attachment Protein
Gaf-1	gamma-SNAP associate factor-1
PDZ	PSD95/Dlg/ZO-1
MAGUK	membrane-associated guanylate kinase
HAT	histone acetyltransferase
TOM	the yeast mitochondrial outer membrane
400 K	400-kDa
GIP	general import pore
RPA	replication protein A
NER	nucleotide excision repair
SP-A	Surfactant proteins A
Pol II	polymerase II
SNARE	soluble N-ethylmaleimide-sensitive factor attachment receptor
C-RBD	C-terminal RNA binding domain
BMV	Brome mosaic virus
Cdc6p	Cdc6 protein
ORC	origin recognition complex
Mcm	minichromosome maintenance
Rap1p-DBD	Rap1p and its DNA-binding domain
intermolecular	interactions between cytochrome c and other electrostatically oriented physiological partners
FHA	Forkhead-associated
FHA1	FHA domain
AP1	APETALA1
SRS	SOS recruitment system
EPEC	Enteropathogenic Escherichia coli
IBD	intimin-binding domain
CCVs	clathrin-coated vesicles
TGN	trans-Golgi network
RRM	RNA recognition motif
Gal4p	Gal4p-DNA interaction in a
F2	fusion protein pre-cytochrome P450scc--adrenodoxin reductase-adrenodoxin
Adx	adrenodoxin
AdxRed	adrenodoxin reductase
ACP	acyl carrier protein
ORC	origin recognition complex
reverse RRS	reverse Ras recruitment system
the prey	the protein partner
chCAF-1	chicken chromatin assembly factor-1
ALP	alkaline phosphatase
CHCR	C-terminal clathrin heavy-chain repeat
W356Y	with either tyrosine
NBD	nucleotide-binding domains
WT	wild-type
NFkappaB	nuclear factor kappaB
SRF	serum response factor
AD	autonomous transactivation domain
CBP	CREB binding protein
Cdc5	cdc5DeltaN
FA	Fanconi anaemia
A-G	at least seven complementation groups
ORC	origin recognition complex
Cvt	cytoplasm to vacuole targeting
Apg	autophagy
Cc	complexes involving yeast iso-1-ferricytochrome c
Cnr	copy number regulation
Fur4p	fluid phase and uracil permease
CMM	Centre for Molecular Medicine
Top3	topoisomerase III
RFC	Replication factor C
Cc	Cytochrome c
Apaf-1	apoptosis protease activation factor-1
FHA	Forkhead-associated
MAPK	mitogen-activated protein kinase
SPB	spindle pole body
MEF2	myocyte enhancer factor-2
HDAC	histone deacetylases
CaMK	Calcium/calmodulin-dependent protein kinase
NMD	Nonsense-mediated mRNA decay
SMT	sterol methyl transferase
FHA	Forkhead-associated
TPR	tetratricopeptide repeat
eIF4A	eukaryotic translation initiation factor 4A
UBA	ubiquitin associated domains
HIV-1	human immunodeficiency virus
EGFP	enhanced green fluorescent protein
NMD	nonsense-mediated mRNA decay
eIF4G	eukaryotic initiation factor 4G
TTSS	type III secretion system
SPI-1	Salmonella pathogenicity island 1
bHLH	basic helix-loop-helix
bHLHzip	basic region helix-loop-helix zipper protein
HDACs	histone deacetylases
FHA	forkhead-associated
GFP	green fluorescent protein
cen3	carry chromosomes containing mutant centromere DNA
cdc	cell division cycle
HTLV-I	human T-cell leukemia virus type I
HTLV-I	Human T-cell leukemia virus type I
p13K30	p13 from K30p
POPC	phosphocholine
POPS	phosphoserine
hAR	human androgen receptor
TIF-2	transcription intermediary factor-2
GFP	green fluorescent protein
RNAP II	RNA polymerase II
eIF2	eIF2betagamma
eIF3	Eukaryotic initiation factor 3
TBP	TATA box-binding protein
TAF	TBP-associated factors
yTAF145	yeast TAF145
TAND	TAF N-terminal domain
TAND	TAND
WGA	wheat germ agglutinin
SRP	signal recognition particle
NAC	nascent polypeptide-associated complex
Hsp70	heat shock protein
TCP1	tailless complex polypeptide 1
klps	kinesin-like proteins
snRNAs	small nuclear RNAs
nSec1	neuronal Sec1
FGFR3	fibroblast-derived growth factor receptor 3
pol II	polymerase II
PCNA	proliferating cell nuclear antigen
Ccm	cytochrome c maturation
CTD	carboxy-terminal heptapeptide repeat domain
Cdk	cyclin-dependent kinase
RGS	regulator of G protein signaling
IAP	inhibitor of apoptosis proteins
TF	transcription factor
AD	Activation domains
GR	glucocorticoid receptor
TBP	TATA-binding protein
HAT	histone acetyltransferase
Cdk7	Cyclin-dependent kinase 7
CTD	C-terminal domain
HIT	histidine triad
HFD	histone fold domains
END	essential N-terminal domain
A648P	Ala-648 to Pro
pre-RC	prereplicative complex
ORC	origin recognition complex
MCM	minichromosome maintenance proteins
CHIP	chromatin immunoprecipitation assay
HSF	heat shock factor
HSE	heat shock elements
cAMP	cyclic AMP
PKA	protein kinase A
MAPK	mitogen-activated protein kinase
UAS1	Upstream activation site 1
CRE	cAMP response element
MTP	Mitochondrial transport proteins
SPB	spindle pole body
SRP	Signal recognition particle
MMR	Mismatch repair
DSB	DNA double-strand breaks
NHEJ	nonhomologous end-joining
OT	Oligosaccharyltransferase
ER	endoplasmic reticulum
PNPOx	pyridoxine 5'-phosphate oxidase
PLP	pyridoxal 5'-phosphate
PNP	pyridoxine 5'-phosphate
PMP	pyridoxamine 5'-phosphate
FMN	flavin mononucleotide
rtER	rainbow trout estrogen receptor
hER	human estrogen receptor alpha
ERE	estrogen response element
Lsm	Like Sm
snRNPs	small nuclear ribonucleoproteins
SL	spliced leader
snRNAs	small nuclear RNAs
Dol-P-Man	Dolichol-phosphate mannose
IGPD	Imidazole glycerol phosphate dehydratase
EPR	electron paramagnetic resonance
ENDOR	electron nuclear double resonance
ESEEM	electron spin echo envelope modulation
TPS	TPS1-encoded trehalose-6-phosphate synthase
Tre6P	trehalose 6-phosphate
ISREC	Institute for Experimental Cancer Research
SCF	Skp1p-cullin-F-box
hrt1-C81Y	HRT1/RBX1 gene in budding yeast
URE3	URE2 function synergistically in cis to induce
GFP	green fluorescent protein
pol epsilon	polymerase epsilon
pol epsilon	polymerase epsilon
MAO A	monoamine oxidase A
TIM	triose phosphate isomerase
CFTR	cystic fibrosis transmembrane conductance regulator
AMPK	AMP-activated protein kinase
ER	endoplasmic reticulum
NR	nitrate reductase
AF-1	activation function-1
AF-2	autonomous activation function-2
MR	mineralocorticoid receptor
HATs	histone acetyltransferases
RNAPs	RNA polymerases
subunit 4	subunit b
subunit 6	subunit a
DBD	DNA-binding domain
HAT	histone acetyltransferase
rGcn5p	recombinant yeast Gcn5p
bp	base pairs
hRPA	human replication protein A
TBP	TATA box binding protein
TAF	TBP-associated factors
TAFind	TAF requirement
hER alpha	human estrogen receptor-alpha
E2	estradiol
RPTP	receptor-like protein-tyrosine phosphatase
GCN5	general control nonrepressed protein 5
ADA	alteration/deficiency in activation
SRC-1	steroid receptor coactivator-1
hTRbeta1	human T3 receptor beta1
HAT	histone acetyltransferase
BrD	bromodomain
NR	nuclear receptor
STAT	signal transducer and activator of transcription
SH2	Src homology 2 domain
eIF2B	Eukaryotic translation initiation factor 2B
eIF2	exchange factor for protein synthesis initiation factor 2
GCD6	gene encoding eIF2Bepsilon
HSF	heat shock transcription factor
CTE	constitutive transport element
UCP	Uncoupling proteins
AD	adipocyte activation domain
RNA pol II	RNA polymerase II
HECT	homologous to E6-AP carboxyl terminus
CTD	carboxyl-terminal domain
Rsp5 WW2	Rsp5 WW domain
NER	nucleotide excision repair
XRCC1	X-ray cross-complementing group 1
beta	beta-Pol
NTD	N-terminal domain
BRCT-I	breast cancer susceptibility protein-1
RRS	Ras recruitment system
CBP	CREB-binding protein
Arm	Armadillo
Pmts	protein O-mannosyltransferases
Dcp1	decapping enzyme
CDKs	Cyclin-dependent kinases
GDI	GDP dissociation inhibitor
AtGDI1	Arabidopsis GDI
HRP	horseradish peroxidase
EBA	Expanded bed adsorption
RTD	residence time distributions
HBV	Hepatitis B virus
HBX	hepatitis B virus X protein
PLC-beta	phospholipase C-beta
isotypes	isotypes 1-4
GPCR	G protein-coupled receptors
PDZ	putative PSD-95/Dlg/ZO-1
MCB	MluI cell-cycle box
SCB	Swi4/Swi6 cell-cycle box
GEF	guanine nucleotide exchange factor
AIP	ABI3-interacting proteins
EBA	Expanded bed adsorption
Clb	cyclin B
HD-Zip	homeodomain-leucine zipper
HCV	hepatitis C virus
RPA	Replication protein A
NPC	nuclear pore complex
FRET	fluorescence resonance energy transfer
ECFP	enhanced cyan and yellow fluorescent proteins
KH	K-homology
TF	transcription factors
OT	oligosaccharyl transferase
NO	nitric oxide
NO	Nitric oxide
DSB	double strand break
TSWV	tomato spotted wilt tospovirus
TSWV	tomato spotted wilt tospovirus
AP	apurinic/apyrimidinic site
HhH-GPD	helix-hairpin-alpha-helix-Gly/Pro-Asp
8-OxoG	8-dihydro-8-oxoguanine
Pr	propanediol
Cy	cyclopentanol
PIE	polyadenylation-inhibitory element
aa	amino acids
SPT	scaled particle theory
ds	double-stranded
Spnr	spermatid perinuclear RNA-binding
dsRBMs	dsRNA-binding motifs
dIMP	dAMP generates 2'-deoxy-inosine 5'-monophosphate
HX	Hypoxanthine
3-meAde	3-methyl-adenine
HSV-1	herpes simplex virus 1
IE	immediate early
GABP	GA-binding protein
Cx43	connexin 43
AAV	adeno-associated virus
RRS	Rep recognition sequence
Sir2	Sir2p
WSMV	wheat streak mosaic virus
CP	coat protein
HC-Pro	helper component-proteinase
CI	cylindrical inclusion protein
PAP	phosphatase which catalyses the hydrolysis of 3'-phosphoadenosine-5'-phosphate
CTD	carboxyl-terminal domain
pol II	polymerase II
CID	CTD-interaction domain
A-H	anemia is a chromosomal breakage disorder with eight complementation groups
PDC	pyruvate decarboxylase
Hxk2	hexokinase PII
SH3	Src homology domain 3
HRDC	helicase and RNaseD C-terminal
NMR	nuclear magnetic resonance
IBDV	infectious bursal disease virus
BP230/BPAG1	bullous pemphigoid autoantigen
BP180/BPAG2	bullous pemphigoid antigen of 180 kDa
pombe	PombePD
chc1-521	clathrin heavy chain gene
TGN	trans-Golgi network
VPS	vacuolar protein sorting genes
HIV	human immunodeficiency virus
PR	protease
D-N	dominant-negative
mdr	multidrug resistance
HSF	heat shock transcription factor
setpoints	sequence of operations for which optimal operating conditions
CF IA	cleavage factor IA
CF IB	cleavage factor IB
CF II	cleavage factor II
PF I	polyadenylation factor I
GA	glucoamylase G2
ANT-1	adenine nucleotide translocase-1
Ras	Ras
Ce	Caenorhabditis
uaf-2	U2AF35
cyp-13	cyclophilin
elF4A	eukaryotic translation initiation factor 4A
SIR	Saccharomyces cerevisiae Silent Information Regulator
HSF	heat shock factor
TBP	TATA-binding protein
AcCoA	acetyl coenzyme A
GNAT	Gcn5-related N-acetyltransferase
CTD	carboxy-terminal domain
Ceg1	capping enzyme guanylyltransferase
CAP	cyclase-associated protein
TPO1	transport protein
NIF	nifedipine dehydrogenase
MROD	methoxyresorufin O-deethylase
Pol I	polymerase I
BRR	basic residue-rich
TBP	TATA-binding protein
RNP	ribonucleoprotein
MBP	maltose-binding protein
BERI	Biomolecular Engineering Research Institute
CP	capsid protein
BMV	brome mosaic virus
Cpt	camptothecin
Tyr723	tyrosine
TBP	TATA-binding protein
Tra	transfer
pACTII	pilus formation in a yeast GAL4 activation domain vector
TPR	tetratrico peptide repeat
ER	endoplasmic reticulum
aap	amino acid permease
MAPKs	mitogen-activated protein kinases
CH2	Cdc25 homology
EBV	Epstein-Barr virus
Ah	aryl hydrocarbon
Arnt	Ah nuclear translocator
ECM	extracellular matrix
NER	nucleotide excision repair
XP	xeroderma pigmentosum
PRR	postreplication repair
GAP	GTPase activating protein
UASs	upstream activating sequences
DRSs	downstream repressing sequences
Tel2p	TEL2-encoded protein
RNP	ribonucleoprotein
RITA	RNP interaction trap assay
RNP	ribonucleoproteins
RITA	RNP interaction trap assay
rLa	recombinant La
Ro60	rRo60
rhY	recombinant hY RNAs
RoBPI	Ro RNP-binding protein
LDTI	leech-derived tryptase inhibitor
LDTI	leech-derived tryptase inhibitor
ADH	alcohol dehydrogenase
Plc1p	phosphatidylinositol-specific phospholipase C
YUH1	Yeast ubiquitin hydrolase 1
CCP	cytochrome c peroxidase
MCD	magnetic circular dichroism
HRP	horseradish peroxidase
Mb	myoglobin
Arg 48	arginine of CCP
FGF	Fibroblast growth factors
bFGF	Basic FGF
MNN9 family	mutations in any of the Golgi glycosylation complex genes
TAO1	thousand and one-amino acid protein kinase 1
MEKs	mitogen-activated protein/extracellular signal-regulated kinase kinases
top2	topoisomerase II
SBF	SCB binding factor
MBF	MCB binding factor
NOT2	NOT1-
yCBC	yeast nuclear cap-binding complex
SL	show synthetic lethality
snRNP	small nuclear RNP
eIF-2	eukaryotic initiation factor-2
IMT	initiator methionine tRNA
eEF-1alpha	eukaryotic elongation factor 1alpha
RPR	Reaper
HID	Head Involution Defective
GRIP-1	glucocorticoid receptor-interacting protein-1
PPAR	peroxisome proliferator-activated receptor
Sla1	Sla1C
GCR	gross chromosomal rearrangements
TRX	Trithorax
trxG	trithorax group
Ubx	Ultrabithorax
G protein	guanine nucleotide-binding protein
Sme1p	sporulation by interacting with the regulatory domain of Ime2p
ATM	a homolog of the human gene
8-bromo-cGMP	8-bromo-cyclic guanosine monophosphate
8-bromo-cAMP	8-bromo-cyclic adenosine monophosphate
PMA	phorbol-12-myristate-13-acetate
PGK	phosphoglycerate kinase from yeast
CPSase	carbamoyl phosphate synthetase
ATCase	aspartate transcarbamoylase
PALA	phosphonacetyl-L-aspartate
QSAR	quantitative structure-activity relationships
CYP51	cytochrome P450
QSAR	quantitative structure-activity relationship
EIP	electrostatic isopotential
5'SS	5' splice site
UCSF	University of California San Francisco
gro	groucho
REDOR	rotational echo double resonance
Elc1	elongin C
Ela1	elongin A
mhsp70	mitochondrial 70-kDa heat-shock protein
GEF	guanine nucleotide exchange factors
BFA	brefeldin A
YUH1	yeast ubiquitin hydrolase
UCH-L3	ubiquitin C-terminal hydrolase L3
SCE1-PPase	Saccharomyces cerevisiae pyrophosphatase
ts	temperature-sensitive
eIF4F	eukaryotic initiation factor 4F
4E-BPs	4E-binding proteins
HIV-1	HIV type 1
LTR	long terminal repeat
CycT1	cyclin T1
P-TEFb	positive transcription elongation factor b
DHFR	dihydrofolate reductase
PrD	prion-determining domain
ALAD	aminolaevulinic acid dehydratase
PS-1	presenilin-1
TPR	two tetratricopeptide repeats
Nef	negative factor
GAR	glycine- and arginine-rich
HSF	Heat shock transcription factors
UMIST	University of Manchester Institute of Science and Technology
5'UTR	5' untranslated region
SA	salicylic acid
PR	pathogenesis-related
DRPLA	Dentatorubral-pallidoluysian atrophy
IRSp53	insulin receptor tyrosine kinase substrate protein of 53 kDa
LST	lethal with sec-thirteen
Pma1p	plasma membrane proton-ATPase
Hap1	heme activator protein 1
HRM	Hap1 contains seven heme-responsive motifs
HMC	high-molecular-weight complex
tRNA	tRNAiMet
IRS	Insulin receptor substrates
PH	proteins contain pleckstrin homology
PTB	phosphotyrosine binding
Ub	ubiquitin
E2s	enzymes
E214k+loop	E214k with an inserted loop
E1	enzyme
HC-Pro	helper component-proteinase
LMV	lettuce mosaic virus
PVY	potato virus Y
GFP	green fluorescent protein
APC	anaphase-promoting complex
topo	The gene encoding human DNA topoisomerase
sc	supercoiled
CPT	camptothecin
RS	regulators is characterized by arginine/serine
TCA	tricarboxylic acid cycle
TCA	tricarboxylic acid
NAD-IDH	NAD+-dependent isocitrate dehydrogenase
CTL1	capping enzyme RNAtriphosphatase-like 1
KUB	Ku70-binding proteins
DNA-PK	DNA-dependent protein kinase
DNA-PKcs	DNA-PK contains a catalytic subunit
apoJ	apolipoprotein J
XIP8	X-ray-inducible transcript 8
TRPM-2	testosterone-repressed prostate message-2
Pol III	polymerase III
alpha	a C-terminal domain essential for binding the polymerase
codon bias	codon associations
HRR	homologous recombinational repair
IR	ionizing radiation
mRad54	mouse Rad54
hRad54	human Rad54
ds	double-stranded
Pkc1	protein kinase C
NR	Nuclear receptors
WASP	Wiskott-Aldrich syndrome protein
WAS	Wiskott-Aldrich syndrome
WASP	WAS protein
WIP	WASP interacting protein
G-G	guanine-guanine
mtDNA	mitochondrial DNA
ORF	open reading frame
PGK	phosphoglycerate kinase
DSC	differential scanning calorimetry
CD	circular dichroism
HBV	hepatitis B virus
HBsAg	hepatitis B surface antigens
GST	glutathione S-transferase
GFP	Green fluorescent protein
SPB	spindle pole body
PolII	polymerase II
ORC	origin recognition complex
Hb	haemoglobin
BMV	Brome mosaic virus
PP1	protein kinase and the type 1 protein phosphatase
ER	endoplasmic reticulum
hMSH5	human MSH5
PMNs	polymorphonuclear leukocytes
FPR	formyl peptide receptor
CHO	Chinese hamster ovary
NSCF	non-serum-dependent chemotactic factor
the protein	the so-called prion-forming domain
Fn	Fibronectin
FBP	fibrin-binding peptide/protein
mAb	monoclonal antibody
BPAG2	bullous pemphigoid antigen
K18	keratin 18
snRNAs	Saccharomyces cerevisiae spliceosomal U small nuclear RNAs
Psi	Pseudouridine
UsnRNAs	U small nuclear RNAs
pol2-4	polymerase epsilon
HSF	heat shock factor
HSE1	heat shock element 1
ScHSF	Saccharomyces cerevisiae HSF
PP2C	protein phosphatase type 2C
lb	leghemoglobin
TPE	telomeric position effect
NPC	nuclear pore complex
ER	estrogen receptor-alpha
Ub	ubiquitin
Cdk5	Cyclin-dependent kinase 5
TPR	tetratricopeptide repeat
Hsp90	heat shock protein 90
TPR	tetratricopeptide repeat
GAP	GTPase-activating proteins
ER	endoplasmic reticulum
SH3	src homology 3
PH	pleckstrin homology
GAP	GTPase-activating protein
ABC	ATP-binding cassette
Mdl1p	multidrug resistance-like transporter
GABA	gamma-aminobutyric acid
NPC	nuclear pore complex
GFP	green fluorescent protein
prion	protein from the insoluble
Hsp82	Hsp90
Pol II	polymerase II
VDR	vitamin D receptor
ER	endoplasmic reticulum
ITS1	internal transcribed spacer 1
ETS	external transcripted spacer
snoRNPs	small nucleolar ribonucleo protein particles
ORC	origin recognition complex
PAK	p21-GTPase-activated protein kinase
FP	fluorescence polarization
CD2BP2	CD2 binding protein 2
ER	endoplasmic reticulum
NPC	nuclear pore complex
D5	Domain 5
TBP	TATA Binding Protein
GPCR	G protein-coupled receptor
UMIST	University of Manchester Institute of Science and Technology
TCR	T cell receptor
RNAPs	RNA polymerases
CTD	carboxy-terminal domain
NER	Nucleotide excision repair
Cdk2	cyclin-dependent kinase 2
snRNPs	small nuclear ribonucleoprotein particles
yPrp4	yeast Prp4
RasGAP	Ras GTPase-activating protein
SH2	Src homology 2
TAND	TAF N-terminal domain
dTAFII230	Drosophila TAFII230
yTAFII145	yeast TAFII145
yTAND	yTAFII145 TAND
CsA	cyclosporin A
TPR	tetratricopeptide repeat
Cns1	cyclophilin seven suppressor
dsRNA	double-stranded RNA
DRBM	dsRNA binding motifs
TMD	transmembrane domains
pre-rRNA	pre-ribosomal RNA
TBP	TATA box-binding protein
DHEA	dehydroepiandrosterone
DHEA	Dehydroepiandrosterone
ER	estrogen receptor
E2	estradiol
androstenedione	androstene-17-dione
PI 3-kinase	phosphoinositide 3-kinase
MAPK	MAP kinase
ATMPK4	a MAPK
Hsp90	heat-shock protein 90
Hsp90	Hsp82
vg	vestigial
sd	scalloped
ptc	patch
RSC	remodeling of the structure of chromatin
DRBD	dsRNA binding domain
PI3K	Phosphatidyl-inositol-3-kinase
SHS1	Seventh Homolog of Septin
CDKs	cyclin-dependent protein kinases
CAK	CDK-activating kinase
FH	formin homology
SH3	Src homology 3
HOF1	homolog of cdc 15
sHsps	six known mammalian small heat-shock proteins
SR	serine-arginine
nuc-1	nucleosome
ter1-kpn	telomerase template mutation
IN	integrase
BMV	brome mosaic virus
APC	anaphase-promoting complex
cdks	cyclin-dependent kinases
MEF2	myocyte enhancer factor 2
MEF2	Myocyte enhancer factor 2
MAP kinase	mitogen-activated protein kinase
RD	runt domain
Gro	Groucho
TCR	T cell receptor
Pan	Pangolin
Arm	Armadillo
Reb1p	Reb1 protein
END5	end5 delta
WIP	WASp-interacting protein
rDNA	ribosomal DNA
RBS	RNA binding site
ArgRS	arginyl-tRNA synthetase
aaRS	a class I aminoacyl-tRNA synthetase
IFN	interferon
pol delta	polymerase delta
RFC	replication factor C
Sp	Schizosaccharomyces pombe
Sc	Saccharomyces cerevisiae
HTLV-1	human T-cell leukemia virus type 1
PKA	protein kinase A
Nexo	naturally exofacial N terminus
SIs	Sterol Delta8-Delta7 isomerases
EBP	emopamil binding protein
RPA	Replication protein A
ssDNA	single-stranded DNA-binding
SAGA	SPT-ADA-GCN5 acetyltransferase
TBP	TATA-binding protein
hSPT3	human SPT3
hGCN5-L	human GCN5
STAGA	SPT3-TAFII31-GCN5-L acetyltransferase
MCK	muscle creatine kinase
MEF-2	myocyte-specific enhancer factor-2
TIF2	transcriptional intermediary factor2
LBD	ligand-binding domain
AR	androgen receptor
CHO	Chinese hamster ovary
AD	AF-2 activation domain
TIF2	transcriptional intermediary factor 2
MMTV	mouse mammary tumor virus
TSRI	The Scripps Research Institute
yTFIIS	yeast TFIIS
ITAM	immunoreceptor tyrosine-based activation motifs
tsf	temperature-sensitive-for-function
NSF	N-ethyl maleimide-sensitive factor
Phd	phosducin
PhLP1	phosducin-like protein 1
Gbetagamma	G-protein beta/gamma subunits
BD	binding domain
GST	glutathione S-transferase
ySUG1	yeast SUG1
TF	transcription factors
SPB	spindle pole body
ER	endoplasmic reticulum
COPII	coat protein complex II
GFP	green fluorescent protein
SR	signal recognition particle and its receptor
ARF1	arf1-3
GAP	GTPase-activating protein
Arf	Arf.GTP
CTR	C-terminal hydrophobic repeat
HSF	heat shock transcription factor
Ahr	aryl hydrocarbon receptor
Arnt	aryl hydrocarbon receptor nuclear translocator
IE	immediate-early
HCMV	human cytomegalovirus
POD	PML oncogenic domains
eIF3	eukaryotic translation initiation factor 3
Hsp70s	heat shock proteins
ER	endoplasmic reticulum
V-ATPase	vacuolar ATPase
eIF3	eukaryotic translation initiation factor 3
PAKs	p21-activated kinases
pak2+	pak gene
HIV-1	human immunodeficiency virus type 1
HIVPsi	HIV-1 RNA encapsidation signal
RNAP	RNA polymerase
alphaNTD	alpha subunit amino-terminal domain
rLDTI	recombinant leech-derived tryptase inhibitor
M3	mutant
Bfr2p	BFR2 gene product
NS	nonstructural
PIG-P	Phosphoinositolglycan-peptide
PI	phosphatidylinositol
NEM	N-ethylmaleimide
PLC	PI-specific phospholipase C
RPA	replication protein A
rDNA	ribosomal DNA
UAF	upstream activation factor
TBP	TATA-binding protein
CF	core factor
SE	salt-extracted mitochondrial peripheral membrane proteins
ER	endoplasmic reticulum
TRAPP	transport protein particle
TM	Tropomyosins
CD	circular dichroism
SPC25	signal peptidase complex
Jaks	Janus protein tyrosine kinases
Stats	signal transducers and activators of transcription
