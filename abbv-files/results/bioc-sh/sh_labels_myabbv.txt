ER	endoplasmic reticulum
WIP	Wiskott-Aldrich Syndrome Protein-Interacting Protein
Myo5p	Myo3/5p
WASP	Wiskott-Aldrich syndrome protein
WIP	WASP-interacting protein
Mti1p	Myosin tail region-interacting protein
dPGM	cofactor-dependent phosphoglycerate mutase
APC	anaphase-promoting complex
PP1(C)	protein phosphatase
SDS-PAGE	sodium dodecyl sulfate-polyacrylamide gel electrophoresis
MALDI	matrix-assisted laser desorption/ionization
HPLC	high-performance liquid chromatography
CPF	cleavage and polyadenylation factor
CPF	components of the yeast
LBD	ligand-binding domain
HMS-PCI	high-throughput mass spectrometric protein complex identification
TAP	tandem-affinity purification
PP1(C)	protein phosphatase 1
Glc7p	sTable 1:1 complex with yeast PP1(C)
Ts(-)	Temperature-sensitive
SUISEKI	System for Information Extraction on Interactions
RI	recently proposed general inhibitor
PMS	postmeiotic segregation
CA/TG	sequence or a 39
PKC	protein kinase C
TM	trabecular meshwork
RLC	regulatory light chain
RhoGDI	Rho GDP dissociation inhibitor
PIP(2)	phosphatidylinositol 4,5-bisphosphate
PKA	protein kinase A
Top1p	topoisomerase I
CPT	camptothecin
Hsp	heat-shock protein
Hsp40	Heat-shock protein 40
NO	Nitric oxide
Needleman-Wunsch	analogous to the way local sequence alignment (the Smith-Waterman algorithm) is derived from global alignment
SGA	synthetic genetic array
SGS1	synthesis and repair
WASP	Wiskott-Aldrich Syndrome protein
RP	regulatory particle
CSN	COP9 signalosome
MMS	methyl methanesulfonate
QCR	cytochrome bc1 complex
INCENP	inner centromere protein
INCENP	inner centromere protein
GTF	general transcription factor
PIC	preinitiation complex
TBP	TATA box binding protein
UAS	upstream activating sequence
4Ac	a mutation that enhances the interaction
TALE	three-amino acid loop extension
BEL1	BELL1
STM	SHOOT MERISTEMLESS
ARK	actin-regulating Ser/Thr kinase
CPY	carboxypeptidase Y
CNRS	Centre National de la Recherche Scientifique
HIV-1	human immunodeficiency virus type 1
IN	integrase
SAP18	repressor of the hematopoietic pathway
YPK	Yeast pyruvate kinase
PEP	phosphoenolpyruvate
FBP	fructose 1,6-bisphosphate
CTD	C-terminal heptapeptide repeat domain
Asp-X-Asp-X-Thr	presence of a conserved motif
HAD	haloacid dehalogenase
AF-1	activation function 1 domain
RGS	regulator of G-protein signaling
E2s	enzymes
Ub	ubiquitin
Ubc1	Ubc1(Delta450)
Ubc1(Delta450)	Ub wraps around the E2 protein terminating in the thiolester between C88
PNGase	peptide:N-glycanase
ER	endoplasmic reticulum
HAT	histone acetyltransferase
SAGA	SPT-ADA-GCN5 acetylase
TFTC	TATA-binding-protein-free TAF(II)-containing complex
STAGA	SPT3-TAF(II)31-GCN5L acetylase
DSB	double-strand break
RNP	ribonucleoprotein
N-BDs	N-binding domains
GED	GTPase effector domain
Dad1p	for Duo1 and Dam1 interacting
HFIP	phosphoglycerate kinase from yeast induced by hexafluoroisopropanol
TFE	trifluoroethanol
PP2A	protein phosphatase 2A
NF2	neurofibromatosis type 2
TFIIIB	transcription initiation factor IIIB
dT-dA	to mutations in a
dT-dA	conclude that the
HBx	hepatitis B virus X protein
bis-ANS	1,1'-bis(4-anilino-5-napththalenesulfonic acid
HSF	heat shock transcription factor
GBB	Gbeta binding
PRF	programmed ribosomal frameshifting
hSec3	Human Sec3
GFP	Green fluorescent protein
MDCK	Madin-Darby canine kidney
NTF2	Nuclear transport factor 2
AP	adaptor
hREV1	human REV1
REV3	hREV3
hREV7	Human REV7
SNX1	Sorting nexin 1
PX	Phox homology
TFIIIB	transcription factor IIIB
TBP	TATA-binding protein
GHL	gyrase b/Hsp90/MutL
MMR	mismatch repair
PCNA	proliferating cell nuclear antigen
hFEN-1	human flap endonuclease-1
PCNA	proliferating cell nuclear antigen
hFEN-1	human FEN-1
hPCNA	human PCNA
TAFs	TBP-associated factors
yTAF17	yTAF60 and yTAF61
MCD	morphogenesis checkpoint dependent
PtdSer	presence of a null allele for the  phosphatidylserine
PtdEtn	phosphatidylethanolamine
MCD	morphogenesis checkpoint dependent
GPI	glycosylphosphatidylinositol
TIM	triosephosphate isomerase
Ycf1p	yeast cadmium factor
ABC	ATP binding cassette
MRP1	multidrug-associated protein
NBD1	nucleotide binding domain
TMD1	transmembrane domain
rDNA	ribosomal DNA
HOT1	hot spot
eIF3	eukaryotic initiation factor-3
Pci8p	cerevisiae protein
PCI	proteasome-COP9 signalosome-eIF3
NER	Nucleotide excision repair
TCR	transcribing genes
SOS1	antiporters at the vacuolar (NHX1) and plasma membrane
Co-Bind	for COperative BINDing
GS	synthase
GAPs	GTPase-activating proteins
WT	wild type
MMR	mismatch repair
HDL	high density lipoprotein
apoE	apolipoprotein E
NF2	neurofibromatosis type 2 gene
eIF2	eukaryotic initiation factor-2
dsRBDs	dsRNA-binding domains
GCRs	Gross chromosome rearrangements
DSBs	double-strand breaks
SCR	sister chromatid recombination
PDI	protein disulphide-isomerase
GAPDH	glyceraldehyde-3-phosphate dehydrogenase
AP	adaptor protein
NPC	nuclear pore complexes
mRNP	competent ribonucleoprotein complex
MEN	mitotic exit network
DBD	DNA-binding domain
AD	activation domain
Ptr2p	transporter
TMD	transmembrane domain
LMU	Munich
CTD	carboxy-terminal domain
COX	c oxidase
2D-PAGE	two-dimensional polyacrylamide gel electrophoresis
TBP	TATA-binding protein
TATATATA	The interactions of Saccharomyces cerevisiae TBP with the E4
TATAAAAG	and adenovirus major late
SnRKs	Snf1-related protein kinases
SCF	Skp1-cullin-F-box
AtCUL1	ASK1 is co-immunoprecipitated with a cullin SCF subunit
VPS	vacuolar protein sorting
hVPS11	human VPS11
hVPS18	human VPS18
hVps11	hVPS11
hVps18	hVPS18
Gq5	G alpha(q) sequence
Gs5	G alpha(s) sequence
CD	Circular dichroism
CDK	cyclin dependent kinases
5-FOA	5-fluoroorotic acid
GFP	green fluorescent protein
LC8	light chain
DGK-zeta	diacylglycerol kinase-zeta
RPR1	RNase P RNA
Rpa1	replication protein A
Rpa1N	single-stranded DNA binding domains and an N-terminal domain
RFC	replication factor C
RFA1	Rpa1
Tom	the mitochondrial outer membrane
p85-PI3K	p85-regulatory subunit of the phosphoinositide-3-kinase
TBP	TATA-binding protein
eIF5-CTD	its C-terminal domain
PHGGGWGQ	PrP also contains similar oligopeptide repeats and we show here that a human PrP repeat
PTP	phosphate transport protein
TM	transmembrane
MTP	mitochondrial transport protein
PTPs	except that with the matrix loop Ser158Thr
tRNA	Transfer RNA
HisRS	histidine tRNAs and residues in the histidyl-tRNA synthetase
CTLs	cytotoxic T lymphocytes
DCs	dendritic cells
MHC	major histocompatibility complex
NER	nucleotide excision repair
UBA	ubiquitin-associated
ORC	origin recognition complex
HsORC	human ORC
GAL4-dd	GAL4 dimerization domain
GAL11P	like GAL11
NMR	Nuclear magnetic resonance
NIEHS	National Institute of Environmental Health Sciences
VDR	1alpha,25-dihydroxyvitamin D(3) receptor
CLP	coactosin-like protein
5LO	5-lipoxygenase
F-actin	of the ability of CLP to bind 5LO and filamentous actin
HSF	heat shock transcription factor
HSF	heat shock transcription factor
GST	glutathione-S-transferase
MMS	methyl methanesulfonate sulfate
ARC1	and three interacting proteins
SRK	S receptor kinase
ORC	origin recognition complex
HP-1	heterochromatin protein 1
Orc5	origin recognition complex
SOCs	store-operated channels
CRANC	release-activated nonselective cation
CIF	influx factor
FBPs	F-box proteins
Rav2	Rav1) and Ydr202
RAVE	ATPase of the vacuolar and endosomal membranes'
ATPase	V-ATPase
Trax	translin-associated factor X
Trax	Translin-associated factor X
Mts1	metastasis-associated protein S100A4
beta-SNAP	Soluble NSF Attachment Protein
Gaf-1	gamma-SNAP associate factor-1
PDZ	PSD95/Dlg/ZO-1
PDZ	PSD95/Dlg/ZO-1
MAGUK	membrane-associated guanylate kinase
HAT	histone acetyltransferase
Grg/TLE	Tcf factors also interact with members of the Groucho
TOM	the yeast mitochondrial outer membrane
GIP	general import pore
RPA	replication protein A
NER	nucleotide excision repair
SP-A	Surfactant proteins A
SII	II elongation factor TFIIS
SNARE	soluble N-ethylmaleimide-sensitive factor attachment receptor
Pap1	poly(A) polymerase
C-RBD	C-terminal RNA binding domain
BMV	Brome mosaic virus
Cdc6p	Cdc6 protein
ORC	origin recognition complex
Mcm	minichromosome maintenance
Rap1p	repressor activator protein 1
Rap1p-DBD	Rap1p and its DNA-binding domain
Asp	whereas residue 90
FHA	Forkhead-associated
FHA1	FHA domain
CGRI	growth regulatory gene
AP1	APETALA1
SRS	SOS recruitment system
EPEC	Enteropathogenic Escherichia coli
IBD	intimin-binding domain
CCVs	clathrin-coated vesicles
TGN	trans-Golgi network
RRM	RNA recognition motif
Gal4p	Gal4p-DNA interaction in a
Adx	adrenodoxin
AdxRed	adrenodoxin reductase
ACP	Enoyl-acyl carrier protein
Hematology/Oncology	Department of Biochemistry and Medicine
ORC	origin recognition complex
GST	glutathione S:-transferase
chCAF-1	chicken chromatin assembly factor-1
ALP	alkaline phosphatase
CHCR	clathrin heavy-chain repeat
NBD	nucleotide-binding domains
WT	wild-type
NFkappaB	nuclear factor kappaB
SRF	serum response factor
AD	autonomous transactivation domain
CBP	CREB binding protein
Cdc5	cdc5DeltaN
FA	Fanconi anaemia
A-G	complementation groups
ORC	origin recognition complex
Cvt	cytoplasm to vacuole targeting
Apg	autophagy
Cc	iso-1-ferricytochrome c
CcP	ferricytochrome c peroxidase
Cnr	copy number regulation
Fur4p	fluid phase and uracil permease
CMM	Centre for Molecular Medicine
Top3	topoisomerase III
RFC	Replication factor C
Cc	Cytochrome c
Apaf-1	apoptosis protease activation factor-1
FHA	Forkhead-associated
MAPK	mitogen-activated protein kinase
rpo41Delta2	temperature-sensitive phenotype of a deletion mutation
AtCBLs	A new family of calcium sensors called calcineurin B-like proteins
CIPKs	interact with a family of protein kinases
SPB	spindle pole body
MEF2	myocyte enhancer factor-2
HDAC	histone deacetylases
CaMK	calmodulin-dependent protein kinase
NMD	Nonsense-mediated mRNA decay
hUpf1p	h) Upf1 protein (p)
KQFDSFHILLINleSAQSLLVPSIIFILAYSLK	of the peptides M3-35 (KKKNIIQVLLVASIETSLVFQIKVIFTGDNFKKKG) and M6-31
KKKFDSFHILLIMSAQSLLVPSIIFILAYSLKKKS	whereas the homologous wild-type sequence
SMT	sterol methyl transferase
FHA	Forkhead-associated
TPR	tetratricopeptide repeat
Pho2p	transcription factor that acts together with the homeodomain-related Bas2p
BIRD	Bas1p interaction and regulatory domain
eIF4A	eukaryotic translation initiation factor 4A
UBA	ubiquitin associated domains
HIV-1	human immunodeficiency virus
EGFP	enhanced green fluorescent protein
NMD	nonsense-mediated mRNA decay
eIF4G	eukaryotic initiation factor 4G
4GH	eIF4G homology
BAF	also called hSWI/SNF-A
TTSS	the type III secretion system
SPI-1	Salmonella pathogenicity island 1
SipA	SspA
bHLH	basic helix-loop-helix
bHLHzip	basic region helix-loop-helix zipper protein
HDACs	histone deacetylases
FHA	forkhead-associated
HKIII	half of mammalian Type III hexokinase
GFP	HKIII-green fluorescent protein
HTLV-I	human T-cell leukemia virus type I
HTLV-I	Human T-cell leukemia virus type I
(2)H-NMR	nuclear magnetic resonance
POPC	2-oleoyl-sn-glycero-3-phosphocholine
POPS	2-oleoyl-sn-glycero-3-phosphoserine
hAR	human androgen receptor
TIF-2	transcription intermediary factor-2
GFP	Cla4p-green fluorescent protein
Ts(-)	heat-sensitive
eIF2	eIF2betagamma
SUI2	structural gene
eIF3	Eukaryotic initiation factor 3
TBP	TATA box-binding protein
TAFs	TBP-associated factors
yTAF145	yeast TAF145
TAND	TAF N-terminal domain
TAND	taf145 Delta TAND
v-SNAREs	Snc1 and Snc2
SNC1	SNC1(ala43)
WGA	wheat germ agglutinin
SRP	signal recognition particle
NAC	nascent polypeptide-associated complex
Hsp70	heat shock protein
TCP1	tailless complex polypeptide 1
TRiC/CCT	ring complex/chaperonin containing TCP1
snRNAs	small nuclear RNAs
nSec1	neuronal Sec1
FGFR3	fibroblast-derived growth factor receptor 3
PCNA	proliferating cell nuclear antigen
Ccm	cytochrome c maturation
CTD	carboxy-terminal heptapeptide repeat domain
Tfg1p	the Fcp1p-binding motif KEFGK in the RAP74
Cks1	cyclin-dependent kinase subunit 1
Cdk	cyclin-dependent kinase
RGS	regulator of G protein signaling
IAPs	inhibitor of apoptosis proteins
MPsi	minimal packaging sequences
Lp(a)	Lipoprotein(a)
TF	transcription factor
ADs	Activation domains
Cbln1	Precerebellin
GR	glucocorticoid receptor
TBP	TATA-binding protein
HAT	histone acetyltransferase
Cdk7	Cyclin-dependent kinase 7
CTD	C-terminal domain
HIT	histidine triad
HFD	histone fold domains
END	essential N-terminal domain
pre-RC	prereplicative complex
ORC	origin recognition complex
MCMs	minichromosome maintenance proteins
CHIP	chromatin immunoprecipitation assay
HSF	heat shock factor
HSEs	heat shock elements
hsp82-DeltaHSE1	as our model a dinucleosomal heat shock promoter
cAMP	cyclic AMP
PKA	protein kinase A
MAPK	mitogen-activated protein kinase
UAS1	Upstream activation site 1
CRE	cAMP response element
Gic2p(W23A)	Gic2p harboring a single-amino-acid substitution in this domain
pre-tRNAs	Eukaryotic transfer RNA precursors
MTP	Mitochondrial transport proteins
Ski6p	subunit Rrp41p
SPB	spindle pole body
SRP	Signal recognition particle
MMR	Mismatch repair
DSBs	double-strand breaks
NHEJ	nonhomologous end-joining
OT	Oligosaccharyltransferase
ER	endoplasmic reticulum
Pkc1p	protein kinase C
PNPOx	pyridoxine 5'-phosphate oxidase
PLP	pyridoxal 5'-phosphate
PNP	pyridoxine 5'-phosphate
PMP	pyridoxamine 5'-phosphate
FMN	flavin mononucleotide
rtER	rainbow trout estrogen receptor
hER	human estrogen receptor alpha
ERE	estrogen response element
Lsm	Like Sm
snRNPs	small nuclear ribonucleoproteins
SL	spliced leader
snRNAs	small nuclear RNAs
Dol-P-Man	Dolichol-phosphate mannose
Nanosep	fluorogenic analogue) were separated by ultrafiltration using commercially available centrifugal protein microconcentrators
IGPD	Imidazole glycerol phosphate dehydratase
EPR	electron paramagnetic resonance
ENDOR	electron nuclear double resonance
ESEEM	electron spin echo envelope modulation
TPS	trehalose-6-phosphate synthase
Tre6P	trehalose 6-phosphate
tps2Delta	Tre6P phosphatase
ISREC	Institute for Experimental Cancer Research
SCF	Skp1p-cullin-F-box
hrt1-C81Y	have isolated a novel allele of the HRT1/RBX1 gene in budding yeast
URE3	rate of
URE3	URE2 function synergistically in cis to induce
GFP	green fluorescent protein
TIM	triose phosphate isomerase
CFTR	cystic fibrosis transmembrane conductance regulator
AMPK	AMP-activated protein kinase
ER	endoplasmic reticulum
NR	nitrate reductase
Req	increase in steady-state binding level
AF-1	activation function-1
AF-2	activation function-2
MR	mineralocorticoid receptor
HATs	histone acetyltransferases
Rap1p	repressor activator protein 1
ABC	"ATP-binding cassette
RNAPs	RNA polymerases
Dig1p	also called Rst1p
Dig2p	also called Rst2p
DBD	DNA-binding domain
HAT	histone acetyltransferase
rGcn5p	recombinant yeast Gcn5p
hRPA	human replication protein A
TBP	TATA box binding protein
TAFs	TBP-associated factors
TAFdep	that either are dependent on multiple TAFs
TAFind	TAF requirement
4-OHT	trans-4-hydroxytamoxifen
RPTP	receptor-like protein-tyrosine phosphatase
GCN5	general control nonrepressed protein 5
ADA	alteration/deficiency in activation
GRIP1	glucocorticoid receptor interacting protein 1
SRC-1	steroid receptor coactivator-1
hTRbeta1	human T3 receptor beta1
HAT	histone acetyltransferase
BrD	bromodomain
NR	nuclear receptor
STAT	signal transducer and activator of transcription
SH2	Src homology 2 domain
PIAS1	amino acids 392-541
Stat1	amino acids 1-191
eIF2Bepsilon	Identification of domains and residues within the epsilon subunit of eukaryotic translation initiation factor 2B
eIF2B	Eukaryotic translation initiation factor 2B
eIF2	exchange factor for protein synthesis initiation factor 2
eIF2Bepsilon	eIF2B catalytic function can be provided by the largest (epsilon) subunit alone
GCD6	gene encoding eIF2Bepsilon
eIF2Bepsilon	residues 518 to 712
NPCs	ofnuclear pore complexes
Gly168Arg	Glu334Lys) and 168
HSF	heat shock transcription factor
CTE	constitutive transport element
UCPs	Uncoupling proteins
MATP	mitochondrial anion carrier proteins
AD	activation domain
HECT	homologous to E6-AP carboxyl terminus
CTD	carboxyl-terminal domain
YSPTSPS	The CTD comprises a heptamer
NER	nucleotide excision repair
DNA-photolyase	photoreactivation
ARS1	an origin of replication
ORC	bound in the ARS region
XRCC1	X-ray cross-complementing group 1
beta-Pol	protein that forms complexes with DNA polymerase beta
NTD	N-terminal domain
BRCT-I	breast cancer susceptibility protein-1
yTBP	Saccharomyces cerevisiae TATA-binding protein
RRS	Ras recruitment system
CBP	CREB-binding protein
Arm	Armadillo
Pmts	protein O-mannosyltransferases
Lsm	family of Sm-like
Lsm1-Lsm7	show that mutations in seven yeast Lsm proteins
Dcp1	decapping enzyme
Pat1/Mrt1	decapping activator
CDKs	Cyclin-dependent kinases
GDI	GDP dissociation inhibitor
AtGDI1	Arabidopsis GDI
HRP	horseradish peroxidase
EBA	Expanded bed adsorption
RTDs	residence time distributions
HBV	Hepatitis B virus
HBX	hepatitis B virus X protein
PLC-beta	phospholipase C-beta
GPCRs	G protein-coupled receptors
PDZ	PSD-95/Dlg/ZO-1
NHERF2	exchanger regulatory factor 2
MCB	MluI cell-cycle box
SCB	Swi4/Swi6 cell-cycle box
GEF	guanine nucleotide exchange factor
AIPs	ABI3-interacting proteins
EBA	Expanded bed adsorption
Clb	cyclin B
HD-Zip	homeodomain-leucine zipper
Oshox3	another HD-Zip family II protein
HCV	hepatitis C virus
RPA	Replication protein A
YIL109C	protein of 926 amino acids
v-SNAREs	Sec22p and Bet1p
NPC	nuclear pore complex
FRET	fluorescence resonance energy transfer
ECFP	EYFP
KH	K-homology
TFs	transcription factors
OT	oligosaccharyl transferase
NO	nitric oxide
NO	Nitric oxide
DSB	double strand break
TSWV	tomato spotted wilt tospovirus
TSWV	tomato spotted wilt tospovirus
AP	apyrimidinic site
HhH-GPD	alpha-helix-hairpin-alpha-helix-Gly/Pro-Asp
8-OxoG	7,8-dihydro-8-oxoguanine
Pr	3-propanediol
Cy	cyclopentanol
PIE	polyadenylation-inhibitory element
SPT	scaled particle theory
Spnr	spermatid perinuclear RNA-binding
dsRBMs	dsRNA-binding motifs
eIF2alpha	eukaryotic translation initiation factor 2
dIMP	2'-deoxy-inosine 5'-monophosphate
HX	Hypoxanthine
3-meAde	3-methyl-adenine
AlkA	ANPG) and E.coli
HCF	The novel coactivator C1
HSV-1	herpes simplex virus 1
IE	immediate early
HCF	the cellular factor C1
GABP	GA-binding protein
AAV	adeno-associated virus
RRS	Rep recognition sequence
Sir2	Sir2p
WSMV	wheat streak mosaic virus
CP	coat protein
HC-Pro	helper component-proteinase
CI	cylindrical inclusion protein
Hal2p	HAL2 gene
PAP	3'-phosphoadenosine-5'-phosphate
CTD	carboxyl-terminal domain
CID	CTD-interaction domain
PDC	pyruvate decarboxylase
Hxk2	hexokinase PII
SH3	Src homology domain 3
Las17p	COOH-terminal acidic domain of Bee1p
HRDC	helicase and RNaseD C-terminal
NMR	nuclear magnetic resonance
ID-Lelystad	Institute for Animal Science and Health
IBDV	infectious bursal disease virus
Finish"	the transition from metaphase to anaphase
BP230/BPAG1	plaque is the 230-kDa bullous pemphigoid autoantigen
BP180/BPAG2	bullous pemphigoid antigen of 180 kDa
GBD	DNA-binding domain of Gal4
PombePD	Schizosaccharomyces pombe
CalPD	and with the fungal pathogen Candida albicans
TGN	trans-Golgi network
VPS	vacuolar protein sorting genes
HIV	human immunodeficiency virus
PR	protease
D-N	dominant-negative
HSF	heat shock transcription factor
IA	CF IA
IB	CF IB
Pap1p	poly(A) polymerase
Pab1p	poly(A)-binding protein I
GA	glucoamylase G2
ANT-1	adenine nucleotide translocase-1
Smy1p	interactions in yeast have indicated that a kinesin-related protein
Ras	H-Ras
Dm	Drosophila
Ce	Caenorhabditis
Hs	Human
elF4A	eukaryotic translation initiation factor 4A
SIR	Silent Information Regulator
HSF	heat shock factor
TBP	TATA-binding protein
AcCoA	acetyl coenzyme A
GNAT	Gcn5-related N-acetyltransferase
Q-SNAREs	SNARE binding partners via conserved glutamine
R-SNAREs	or arginine
CTD	carboxy-terminal domain
Ceg1	capping enzyme guanylyltransferase
CYR1	crucial for activation of Saccharomyces cerevisiae adenylyl cyclase
CAP	cyclase-associated protein
